<?php

/**
 * Autoloader for including all the classes.
 */
spl_autoload_register(function ($class_name) {
    include $class_name . '.php';
});


/**
 * A class to manage all aspects of the notifications and sending of emails for the work experience app. 
 * Extends the parent class, Main.
 * Includes the Learner and Hashids class. 
 * Sets the email address headers. 
 */
class Email extends Main
{
	public function __construct()
	{	
		try
		{
			// Call the parent first and instantiate the database SQL class instance.
			parent::__construct();
		
			// HashGenerator.php & Hashids.php.
			$this->hashids = new Hashids('',25); // Pad the URL to 25.

			// Instantiate a new instance of learner.
			$this->learner = new Learner;

			// Email headers for all emails sent in the class. 
			$this->headers = 
			'From: mailer@boltoncc.ac.uk' . "\r\n" .
		    'Reply-To: noreply@boltoncc.ac.uk' . "\r\n" .
		    //'Cc: mike.thornley@boltoncc.ac.uk' . "\r\n".
		    'MIME-Version: 1.0' . "\r\n".
		    'Content-type: text/html; charset=iso-8859-1' . "\r\n".
		    'X-Mailer: PHP/' . phpversion();
		}

		// Catch some PDO errors.
		catch (PDOException $e)
		{
			throw new Exception('PDO Database connection failed to connect to SID failed in line: ' .__LINE__ . ' of file: '. __FILE__ . $e->getMessage());
		}
	}

	/**
	 * This is created when the coach review is initially created.
	 * From the placement id.
	 * Get the learner id.
	 * Get the learners full name. 
	 * Get the LDM details - their name.
	 * Handles all the outcomes of a career coach review and the notifications to be send after.
	 * Prepare the email to tell the LDM the outcome of the coach review of the placement. 
	 * @param  [string] $placement_attended_id [This is the string placement_attended_id passed]
	 * @return [boolean] [True or false depending on how it's gone.]
	 */
	public function emailReviewNotification($placement_attended_id,$status)
	{
		// Get the status of the end of the review and capitalise the first letter.
		$status = ucfirst($status);
		
		// Get the learner ID from the placement attended id.
		$learnerID = $this->learner->getLearnerId($placement_attended_id)[0]['learner'];

		// Get the learners full name. 
		$lFullName = $this->learner->learnerDetailsByID($learnerID);

		// Get the learners first name
		$lFirstName = explode(' ',$lFullName)[0];

		// Get the coach id 
		$coachID = $this->conn->select('SELECT `coach_id` FROM `we_placement_attended` WHERE `id`=? ',[$placement_attended_id])[0]['coach_id'];

		// Get the coach details from the coach id
		$coach = $this->learner->learnerWorkCoachByCoachID($coachID);
		$coachName = $coach[0]['name'];
		$coachFirst = explode(' ', $coachName)[0];
		$coachEmail = $coach[0]['email'];
		$coachPhone = $coach[0]['phone'];
		$coachMobile = $coach[0]['mobile'];

		// Get the learners LDMs full details.
		$LDMDetails = $this->conn->select
		(
			"SELECT DISTINCT `tutor_name` FROM `ext_ebs_data_tutorgroup` `tgp` INNER JOIN `ext_ebs_data_learnergroup` `lgp` ON `tgp`.`unique_group_name` = `lgp`.`unique_group_name` WHERE `lgp`.`course_title` LIKE ? AND `lgp`.`learner` = ?",
			[
				'%Learning and Development%',
				$learnerID
			]
		);

		// Get the CL full details.
		$clDetails = $this->conn->select("SELECT DISTINCT `elp`.`cl` FROM `ext_ebs_data_tutorgroup` `tgp` INNER JOIN `ext_ebs_data_learnergroup` `lgp` ON `tgp`.`unique_group_name` = `lgp`.`unique_group_name` 
		LEFT OUTER JOIN `elp_curriculums` `elp` ON binary `elp`.`section`= binary `lgp`.`section`

		WHERE `lgp`.`course_title` 
		LIKE '%Learning and Development%' AND `lgp`.`learner` = ?",
		[
			$learnerID
		]
		);

		// Get the CL email. 
		$clEMail = $clDetails[0]['cl'];

		// This is for the hyperlink hash of the (placement attended id) sent in the email
		$urlToken = $this->hashids->encode($placement_attended_id);

		// Get the LDMs full name.
		$LDMname = $LDMDetails[0]['tutor_name'];

		// Get the LDMs first name
		$LDMfirst = explode(' ', $LDMDetails[0]['tutor_name'])[0];

		// Get the LDMs full email address from  `ext_ebs_data_tutorgroup` and `ext_userid`
		$lDMEmailAddress = $this->conn->select("SELECT DISTINCT `email` FROM `ext_ebs_data_tutorgroup` `etg` 
		LEFT OUTER JOIN `ext_userid` `euid` ON binary `etg`.`tutor` = binary `euid`.`idnumber`  
		WHERE `etg`.`tutor_name` LIKE '%{$LDMname}%'");

		// This is the LDMs email address.
		$lDMEmail = $lDMEmailAddress[0]['email'];

		$to = ''; // empty placeholder for the to field of the email to be populated. 

		// empty string to hold if a further wex is required. 
		$wexRequired = '';

		// Supplemental stuff to be done/added to emails. LDM only message. This let's the LDM know, action is required. 
		$LDMaction = ($status == 'Completed with learner issues' || $status == 'Failed with learner issues') ? "{$LDMfirst} you are required to action this." : '';

	
		// CL and LDM to be notified
		if($status == 'Passed')
		{
			$to = 'mike.thornley@boltoncc.ac.uk'; // "{$lDMEmail}";
		}

		if($status == 'Completed with learner issues' || $status == 'Completed with employer issues')
		{
			$to = 'mike.thornley@boltoncc.ac.uk'; // "{$lDMEmail}";
			$wexRequired = 'A new work experience placement is not required.';
		}

		if($status == 'Completed with both employer and learner issues')
		{
			$to = 'mike.thornley@boltoncc.ac.uk'; // "{$lDMEmail},{$clEMail}";
			
			/*CL and LDM to be notified; wex not needed*/
			$wexRequired = 'A new work experience placement is not required.';
		}


		if($status == 'Failed with learner issues')
		{
			/*LDM to be notified.*/
			$to = 'mike.thornley@boltoncc.ac.uk'; // "{$lDMEmail}";

			$wexRequired = 'A new work experience placement is required and the learner has been set to \'not work ready\'.';
			
			/*WR set to red*/
			$this->learner->setLearnerNotWorkReady($learnerID);
		}

		if($status == 'Failed with employer issues')
		{
			/*LDM to be notified.*/
			$to = 'mike.thornley@boltoncc.ac.uk'; // "{$lDMEmail}";

			$wexRequired = 'A new work experience placement is required and the learner has been set to \'not work ready\'.';
		}

		
		if($status == 'Failed with both employer and learner issues')
		{

			$to = 'mike.thornley@boltoncc.ac.uk'; // "{$lDMEmail},{$clEMail}";
			
			/*CL and LDM to be notified; wex IS needed*/
			
			$wexRequired = 'A new work experience placement is required and the learner has been set to \'not work ready\'.';
			
			/*WR set to red*/
			$this->learner->setLearnerNotWorkReady($learnerID);
		}
	
		// Get the coach review for this learner placement attended id from the learner class. 
		$careerCoachreviewText = trim($this->learner->getCoachReviewFromPaid($placement_attended_id)[0]['comments']);
	
		if($status=='Passed') $status = '<span style="color:green;">Passed</span>';

		/**
		 * Start to compose the body with all the details of the review
		 */

		// The subject line of the email. 
		$subject = "Workspace App notification: Coach review for {$lFullName} has been completed on the Workspace App.";

// Prepare the body of the email
$message = <<<SOQ

<strong>Message from the WorkSpace app:</strong>
<br/>

<p>A work placement review for {$lFullName} [$learnerID] has been <strong>completed</strong> by coach {$coachName}.</p>

<p>{$lFullName} [$learnerID] has: '{$status}.'</p>

<p>{$wexRequired}</>

<p><strong>Overall comments:</strong><br/>
'{$careerCoachreviewText}'</p>

<p>Click <a href='http://www.boltoncc.ac.uk/workexperience/coach/admin.php?token={$urlToken}'>here</a> to view the work placement review for {$lFirstName}.</p>

<p>{$coachFirst} can be contacted by email at: {$coachEmail} or by calling: {$coachPhone} and {$coachMobile}.</p>

<p>Regards</p>

<p>{$coachName} (Career coach).</p>

<p>This email is automatically generated and this address mailbox doesn't receive messages from users. </p>
SOQ
;

		/**
		 * Finally send the email.  
		 */
		
		// Finally send the email out. This is just a test at this point in time.  
		$this->sendEmail($to,$subject,$message);

}


	/**
	 * This is where after the update by the coach to the career coach review, the confirmation email is sent.
	 * The coach, LDM and the CL all get a copy of the update email and the coach contact details are contained within the body of the email. 
	 * @param  [string] $placement_attended_id [This is the placement attended id]
	 * @param  [string] $learner [This is the learner id as a string]
	 * @param  [string] $status [This is the status of the coach review as a string]
	 * @return [boolean] [This is a boolean about the outcome, either true or false]
	 */
	public function sendUpdatedCareerCoachReviewEmail($placement_attended_id,$learner,$status)
	{
		// Get the student full name
		$lfullName = $this->learner->learnerDetailsByID($learner);

		// Get the placement id - the actual row of the unique placement, 
		$placementID = $this->conn->select('SELECT * FROM `we_placement_attended` WHERE `id`=? ',[$placement_attended_id])[0]['placement_id'];

		// Get the coach id 
		$coachID = $this->conn->select('SELECT `coach_id` FROM `we_placement_attended` WHERE `id`=? ',[$placement_attended_id])[0]['coach_id'];

		// Get the coach details from the coach id
		$coach = $this->learner->learnerWorkCoachByCoachID($coachID);
		$coachName = $coach[0]['name'];
		$coachFirst = explode(' ', $coachName)[0];
		$coachEmail = $coach[0]['email'];
		$coachPhone = $coach[0]['phone'];
		$coachMobile = $coach[0]['mobile'];

		// Get the LDM
		$LDMDetails = $this->conn->select
		(
			"SELECT DISTINCT `tutor_name` FROM `ext_ebs_data_tutorgroup` `tgp` INNER JOIN `ext_ebs_data_learnergroup` `lgp` ON `tgp`.`unique_group_name` = `lgp`.`unique_group_name` WHERE `lgp`.`course_title` LIKE ? AND `lgp`.`learner` = ?",
			[
				'%Learning and Development%',
				$learner
			]
		);

		// Get the LDM full name as string
		$LDMFullName = $LDMDetails[0]['tutor_name'];

		// Get the LDMs full email address array from  `ext_ebs_data_tutorgroup` and `ext_userid`
		$LDMEmailAddress = $this->conn->select("SELECT DISTINCT `email` FROM `ext_ebs_data_tutorgroup` `etg` 
		LEFT OUTER JOIN `ext_userid` `euid` ON binary `etg`.`tutor` = binary `euid`.`idnumber`  
		WHERE `etg`.`tutor_name` LIKE '%$LDMFullName%'");

		// LDM email address as string
		$LDMEmail = $LDMEmailAddress[0]['email'];

		// Ge the CL details
		$clDetails = $this->conn->select("SELECT DISTINCT `elp`.`cl` FROM `ext_ebs_data_tutorgroup` `tgp` INNER JOIN `ext_ebs_data_learnergroup` `lgp` ON `tgp`.`unique_group_name` = `lgp`.`unique_group_name` 
		LEFT OUTER JOIN `elp_curriculums` `elp` ON binary `elp`.`section`= binary `lgp`.`section`

			WHERE `lgp`.`course_title` 
				LIKE '%Learning and Development%' AND `lgp`.`learner` = ?",
		[
			$learner
		]
		);

		// This is for the hyperlink hash of the (placement attended id) sent in the email
		$urlToken = $this->hashids->encode($placement_attended_id);

		// Get the coach review for this learner placement attended id from the learner class. 
		$careerCoachreviewText = $this->learner->getCoachReviewFromPaid($placement_attended_id)[0]['comments'];

		// Get the LDMs first name
		$LDMfirst = explode(' ', $LDMDetails[0]['tutor_name'])[0];

		// This is the LDMs email address.
		$lDMEmail = $LDMDetails[0]['email'];

		// Get the CL email. 
		$clEMail = $clDetails[0]['cl'];

		//$to = "{$coachEmail},{$LDMEmail},{$clEMail}";
		$to = 'mike.thornley@boltoncc.ac.uk'; // For testing purposes.

		// The subject line of the email. 
		$subject = "Workspace App notification: Coach review has been updated for {$lfullName} on the Workspace App.";

// Prepare the body of the email
$message = <<<SOQ

<strong>Message from the WorkSpace app:</strong>
<br/>

<p>A work placement review for {$lfullName} [$learner] has been <strong>updated</strong> by coach {$coachName}.</p>

<p>{$lfullName} [$learner] has: '{$status}.'</p>

<p><strong>Overall comments:</strong><br/>
'{$careerCoachreviewText}'</p>

<p>Click <a href='http://www.boltoncc.ac.uk/workexperience/coach/admin.php?token={$urlToken}'>here</a> to view the work placement review for {$LDMFullName}.</p>

<p>{$coachFirst} can be contacted by email at: {$coachEmail} or by calling: {$coachPhone} and {$coachMobile}.</p>

<p>Regards</p>

<p>{$coachName} (Career coach).</p>

<p>This email is automatically generated and this address mailbox doesn't receive messages from users. </p>
SOQ
;

		/**
		 * Finally send the email.  
		 */

		// Finally send the email out. This is just a test at this point in time.  
		$this->sendEmail($to,$subject,$message);
}

	/**
	 * Send the email finally (phew!).
	 * @param  [string] $to [This is the recipient string]
	 * @param  [string] $subject [This is the subject line of the email]
	 * @param  [string] $message [This is the email message body]
	 * @return [boolean] [This is set to the boolean value if things are successful or not.]
	 */
	public function sendEmail($to,$subject,$message)
	{
		mail($to,$subject,$message,$this->headers);
	}

	public function __destruct()
	{
		// Close the database connection. 
		$this->conn = null;
	}
}


