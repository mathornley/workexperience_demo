<?php

/**
 * Get the PSR4 autoloader and load all the classes required.
 */
spl_autoload_register(function ($class_name) {
    include 'classes/'.$class_name . '.php';
});

// Stop the cache
require_once 'stop_cache.php';

// Display errors
ini_set('display_errors', 'On');

error_reporting(0);

/*
 * This page establishes who the person is, whether that be a learner or coach, anyone else is redirected to Google for testing.
 * This works off sibboleth and checks for the learner id or the username, if staff.
 * Coach is checked and if there are details, we know this is a coach.
 * Learner details are checked and if there some we know this is a coach
 */ 

// New instance of learner.
$learner = new Learner;
// Create a new imstance of the work experience class.
$we = new Workexperience;

// Get all the learner CVs imnstances.
$CVS = $learner->getLearnerCVs();

// Get the learner details to prepop the form at the TOP - this is disabled to the user.
$ldetails = $learner->learnerDetails();

// Get the learner work experience from the work experience app.
$placement = $we->getAllLearnerPlacements();
//echo '<pre>';print_r($placement);die;

/**
 * The click is following one of the EDIT buttons on a CV instance being clicked over on edit.php.
 * Wait for click and the CV_id: $_POST['CV_edit']
 * Include the _form.php file via JS below in .update_cv.
 * Once the button is clicked on the CV instance to update, pull the sections of the CV and it's parts and populate the form.
 */
if(isset($_POST['CV_edit']) && $_POST['CV_edit']) 
{
  // Get the CV instance information
  $CVInfo = $learner->getCVInstance(NULL,$_POST['CV_edit']);
  // Get the personal statement. 
  $personal_statement = $learner->getStatement(NULL,$_POST['CV_edit']);
  // Get the skills for the instance.
  $skills = $learner->getSkills(NULL,$_POST['CV_edit']);
  // Get the CV references
  $references = $learner->getCVReferences(NULL,$_POST['CV_edit']);
  //echo '<pre>';print_r($personal_statement);die;
  // Get the SID quals
  $lQuals = $learner->getLearnerQuals();
  // The quals recorded
  $Quals = $learner->getUserQuals(NULL,$_POST['CV_edit']);
  // Get the user chosen work experience placements - use the CV_id.
  $userChosenPlacements = $we->getAllLearnerChosenPlacements(NULL,$_POST['CV_edit']);
  //echo '<pre>';print_r($placement);
  //echo '<pre>';print_r($userChosenPlacements);die;
}

// Form has finished being edited and now the changes need saving.
if(isset($_POST['update_CV']) && $_POST['update_CV']) 
{
  echo '<pre>';print_r($_POST);die;
}

?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>Welcome to the online CV Builder app</title>

  
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/sticky-footer-navbar.css" rel="stylesheet">

    <script
    src="https://code.jquery.com/jquery-3.3.1.js"
    integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
    crossorigin="anonymous"></script>

    <script defer src="js/generic.js"></script>
  </head>

  <body>

     <header>
      <!-- Fixed navbar -->
      <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="/CV/index.php">Online CV Builder application</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link">
                <?=($learner->learnerDetailsByID($learner->stuID)!=false) ? $learner->learnerDetailsByID($learner->stuID) : 'STAFF';?> <span class="sr-only">(current)</span>
              </a>
            </li>
             <li class="nav-item">
              <a class="nav-link" href="index.php">Introduction</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="create.php">Create a CV</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="edit.php">Edit your CVs</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="save.php">Save your CV</a>
            </li>
          </ul>
          
        </div>
      </nav>
    </header>

    <!-- Open the edit box on the page and pull the record to edit the CV -->
    <?php if(isset($_POST['CV_edit']) && $_POST['CV_edit']):
      ?>
      <script>
        $(document).ready(
          function(){
          //$("body").hide("slow");  
          $(".update_cv").fadeToggle( "slow", "linear" );
          }
        );
      </script>
    <?php endif;?>
    <!-- Open the edit box on the page and pull the record to edit the CV -->


    <!-- Begin page content -->
    <main role="main" class="container">

      <h3 class="mt-5">Edit your CV below with the Online CV Builder application</h3>
      <hr/>

      <?php if($CVS!=false):?>
        <p class="lead">
          You currently have
          <?php echo (count($CVS)>0 && count($CVS)<2) ? '<strong>('.count($CVS).') CV</strong>' : '<strong>('.count($CVS).') CVs</strong>' ; ?>
        </p>
        <p class="lead">Below you can edit any of yours CV instances.</p>

      <?php else:?>
        <p class="lead">You don't currently have any CVs. Create one <a href='create.php'>here</a></p>
      <?php endif;?>
     
      <!-- The edit form goes here -->
      <div class='update_cv'>

         <div class="row"><!--Start of row -->
          <div class="col-md-12"><!--Start of col -->
             <!-- An include for the form to perform an edit on the CV -->
            <?php require_once 'includes/_form.php';?>
          </div>
        </div>
      </div>
      <!-- The edit form ends here -->

        <?php if($CVS!=false):?>
            <div class="row"><!--Start of row -->
              <div class="col-md-12"><!--Start of col -->

                <div class="table-responsive">
                  <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th scope="col">CV name</th>
                          <th scope="col">Date created</th>
                          <th scope="col">Edit CV</th>
                        </tr>
                      </thead>
                        <tbody>
                          <?php for($i=0;isset($CVS[$i]);$i++):?>
                          <tr>
                            <form method='POST'>
                              <td><?=(!isset($CVS[$i]['CV_name'])) ? 'Unamed by student' : $CVS[$i]['CV_name']?></td>
                              <td><?=date('l jS \of F Y H:i:s',strtotime($CVS[$i]['date_created']))?></td>
                              <td><button id='edit_CV_instance' class="btn btn-danger" name='CV_edit' value='<?=$CVS[$i]['id']?>'>Edit</button></td>
                            </form>
                          </tr>
                        <?php endfor;?>
                      </tbody>
                    </table>
                </div>  
              </div>
          </div> <!--End of row -->
        <?php endif;?>
    </main>

    <!-- Footer include -->
    <?php require_once 'includes/_footer.php';?>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    
    
  </body>
</html>
