

<script>
var studID = "<?=$_GET['studID']?>";
//alert(studID);
</script>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>My Workspace app</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/theme.css" rel="stylesheet">

    <!--<link href="css/ada.css" rel="stylesheet">-->

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

    <script src="js/workexperience.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

      <!-- Bootstrap 3 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Font Awesome 5 -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="crossorigin="anonymous"></script>

    <!-- Include the jQuery for buttons and navs -->
    <script src="js/includes/button_div_jquery.js"></script>

    <style>
      
      img.img-fluid {
        width: 100%;
      }
    
    </style>
    
  </head>
  <body>

  
     <!-- Begin page content -->
    <main role="main" class="container">

      <div class="row"><!--Start of row -->
        <div class="col-md-12"><!--Start of col -->

          <h3 class="mt-5" style='text-align:center;'>I see you are using Internet Explorer. Please download and install <a title='Download Google Chrome here' href='https://www.google.co.uk/chrome/?brand=CHBD&gclid=CjwKCAiA2fjjBRAjEiwAuewS_SzUcn04YjS1fVNlUAO-GXxn8gknAir2kk5udYxUOmBurHXoqOZi8RoCGEkQAvD_BwE&gclsrc=aw.ds'>Google Chrome</a> to use this application, as the date picker on the logbook <em>requires</em> this when completing your logbook.</h3>
           <hr/>

          <p><img src='images/notie.png' class="img-fluid" title='Please download Google Chrome'/></p>
          <p class="mt-5" style='text-align:center;'> Google Chrome can be downloaded <a title='Download Google Chrome here' href='https://www.google.co.uk/chrome/?brand=CHBD&gclid=CjwKCAiA2fjjBRAjEiwAuewS_SzUcn04YjS1fVNlUAO-GXxn8gknAir2kk5udYxUOmBurHXoqOZi8RoCGEkQAvD_BwE&gclsrc=aw.ds'>here</a>. When you have done that, you can then return go on to <a title='Click this to start creating your work placement logbook entry' href='logbook.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>'>create your logbook entry</a></p>
          
        </div>
      </div>

    </main>

    <hr/>
    <!-- Footer include -->
    <?php require_once 'includes/footer.php';?>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="crossorigin="anonymous"></script>

  </body>
</html>
