# Work Experience Application (aka WEX, WorkSpace, Work experience app)

The work experience application also known as 'WorkSpace' or 'WEX', runs on the **LAMP Stack** and the core code is written in **Object Orientated PHP** &amp; **MySQL,** using the **PDO driver**. 

The application firstly uses authentication with the college's Shibboleth auth. Once the learner signs in to the BCC network, this sets the global server variable SERVER (``$_SERVER['sAMAccountName']``) created on logon and this is then used throughout the application and identifies the current user enabling and disabling/enabling certain rights and privileges.  There is a separate Employer section, with an external, ***separate and secure login***, and this enables an outside organisation a learner has visited or is visiting on placement to login and review those learners. The employer section will be covered more later but is a separate protected area of the application, not governed by Shibboleth.

The application has 3 main views depending on who is logged in and "who" they are considered to be i.e a learner, coach, employer etc. Access to staff members is only set by the user being entered in to the `we_coach` table. Access to users using the application can be granted in the 'Manage Staff' section by anyone already added. Once added you've root access of sorts and this then allows you permissions to add further users. 

The application sits on the Bolton College domain at: 
[WorkSpace](http://www.boltoncc.ac.uk/workexperience/) and the filesystem is on the ITL Main 1 server at: `10.3.112.228` `/var/www/web/workexperience/`

# Technologies used

Several open source development technologies are used within the application:

- HTML5
- CSS &amp; Bootstrap (front-end framework).
- PHP
- MSSQL/MySQL (PDO driver in this instance)
- jQuery API/Javascript &amp; AJAX.
- Salesforce &amp; Google Maps API
- Linux (CRON jobs on a per minute basis on ITL Main 1 box).

Additionally also, the Ada assistant is sat on each page of the student section and this code is managed as a ``webpack.js`` bundle. Jonathan Hart maintains and updates this Javascript and HTML code when needed, so this could potentially be changed at a later date.  

# Areas of use

 - Learner area (learner and staff access, with learner 'sudo' mode)
 - Coach area (general CRM)
 - Employer area (separate access from outside the network)

# Modes of use

There are 3 main modes, or views within the application:

- Learner: This is the only section the learner sees and the only area they can interact with. This is their personal journey through the application and contains all their work placement info.
- Coach (within the CRM section and also allows staff to mimic the learner of choice via a "pseudo mode", as well as allowing full CRM access).
- Employer (separate HTTPS login, credentials assigned by BDU in the Manage Employer section. WEX issues these). This area allows the employer to login, view and review any learner who has attended a placement with their organisation. It is not governed by Shibboleth authentication, so the employer can simply login via the secure page and be immediately presented with the learners they have with their organisation. The employer section is accessible here:[Employer section](https://www.boltoncc.ac.uk/workexperience/employer/). If the employer forgets their password, this can be reassigned and the credentials are sent separately by email. 

## Learner

This is the view the learner sees. Initially, on signing in to their account they are greeted with their previous and current work placements as a series of buttons displaying the date of the placement on the first page (index.php). They're also notified if they have to sign their agreement. Once they have selected the required placement instance, they can then browse through that placement accessing and completing the following areas.

- Introduction to WEX
- Where is my placement (Includes a Google Map with directions to the placement from home)
- Work placement agreement (Needs to be ticked to say the learner has read and understood the terms &amp; conditions. This is a legal requirement and unless ticked and agreed to, the learner isn't permitted to go any further in the application and it will appear locked).
- Aims and objectives (Mandatory, the learner must select 4).
- Logbook (To be completed at the end of each day and logs the number of days and hours a learner has been on placement. This also includes sickness and partial days of sickness).
- Your placement review. (learner prospective)
- Employer review.
- Coach review.
- Your midpoint review. This process is started by the coach and until the coach does their part, ***neither the learner nor the employer*** can complete their section. Additionally, the coach can complete both the learner and employer review on behalf of the others and complete all reviews. 
- There is also a report a bug section that will ***log an issue*** to the ILT dept in a table if a user reports one - this does not notify though as was an initial feature never used. This is a link on the Introduction page that takes the user to a form where they will report the bug to ILT. The table for this is `elp`.`we_debug`

## Coach

The coach section enables staff access to 2 areas, one is the CRM with full control over all aspects and also the ability to ***sudo mode*** any learner they search and choose at the very beginning. Once they've searched the required learner, they can have a simulated view of the learner experience where they can browse the details of the entire student placement - this section is also editable by the coach. The CRM access allows the learner, staff, coach and employees to be fully managed through a variety of sections, via the easy to use GUI and this is where the application can be fully administered.

## Employer

This is the employer login area, viewable outside the college network and is not regulated and controlled by Shibboleth authentication. The login is custom written and handled by us and is protected by **HTTPS** and some custom code to ensure the logon is always secure - the connection to secure is forced should the user attempts to use http. The employer has an `id` with us and this makes them uniquely identifiable against their placement learners, allowing them to view all their learners and write reviews etc. 

The username and password for the employer are created by the college BDU Dept [here](http://www.boltoncc.ac.uk/workexperience/CRM/employer.php)  and once set this is then emailed to the organisation. This process is handled by the CRM in the 'Manage Employer' section. If later on the employer needs to do a password reset having forgotten their credentials, a link to 'reset the password' is on the sign in page and emails a password reset link to the user. Alternatively through the CRM, BDU can again re-assign and email some fresh credentials using this page. This process is all automated and the password and username are always emailed separately. 

After a registered employer logs in, they are able to tick select review options and finally write a review on the learners they have had at their organisation on placement. ***Social Action learners on placement however will not show up in this Employer area, as this was a stated requirement made later in the applications development.***  If in doubt, see Sharon Marriot.

## CRM

The CRM area is only accessible by staff who are in the `we_coach` table. Anyone else will be presented with a message suggesting they contact the ILT Dept to be added. Learners are blocked from access. 

The CRM has several areas for the admin management of the application and its users, learners, placements, employers and coaches and they can all be created, edited and managed here easily. 

#### Review learners

This is the section where the coach can review all their individual learners on placement. It also shows the status of a placement and if it is complete, as well as importantly, which learners have agreed to the workplace agreement in the first instance.

Any learner can be searched here, regardless of who the coach managing them is and who is logged in and their review section can be viewed/updated or created from there.

#### Manage learners

This is the section where the learner can be added to an organisational placement with a coach assigned to that placement. These placements are fed by the Salesforce CRM, via the API.  

In the case of an ALS learner, any considerations are displayed when staff try and add the user to the placement so placements aren't assigned to learners inappropriately. The considerations are as exceptions, or just additional advisory information that needs to be considered if a learner has special requirements in order to attend a placement. An Exception will block that learner being assigned a placement, where as considerations simply advise on any specific needs that should be considered by staff before the learner goes out on placement. 

#### Manage employers

This is the section where the employers username and password are created and can also be reset. The credentials are set by the BDU dept and then those credentials are emailed to the employer. Both the username and password are sent separately in order to avoid a security issue.  There is also separate functionality so the user is able to reset their own password through a password reset link sent via email after the user clicks a link on the home page. Staff can also reassign passwords and usernames in the 'Manage Employer section'.

#### Manage staff

This is the section that allows staff to have initial access to the application and is achieved by adding the staff member to the role of coach. In addition to the staff being added, they can also be classed as a `Curriculum Leader` or `Teacher` who will be responsible in the next wave of development for finally signing off the learner.

When staff are added to the coach table and granted access there are 2 options:

 - Add as coach (Show up in the coach assignment list when the learner is added to a placement).
 - Add as admin access ***only***. (These staff members will not be able to have learners assigned to them and this access is only to view the application data).

#### Manage ALS (Additional Learning Support) 

In this section of the CRM,  learners and their needs and EHCPs can be added to the application. When a learner is added to the app, if they are considered exempt or have special requirements, these are logged and enable the staff assigning the placement to be notified once they attempt to add the learner. This is a means to assist in assigning appropriate placements or none at all depending on the learner needs. If the learner is exempt, the placement will not be allowed to be created and the form for adding a placement will lock. Any queries should be directed to ALS. The EHCP information is also stored, so staff can upload this enabling the learner to be better supported in the choice of placements and avoiding inappropriate ones. 

## Databases

The tables for the application are housed inside the `elp` database on IP: `10.3.112.172` All the WorkSpace tables are prefixed with the `we_` prefix, so are easily identifiable.

Additionally now also, when a new placement is created, the academic year is used in Wex and saved along with the placement. This is provided by Promonitor (this is thought to be the most accurate means of gathering this). A simple query is run against the database `select dbo.CurrentAcademicYear (GetDate()) AS acc_year`  For this functionality, a simple database connection to Promonitor is established at: `10.3.112.209`  using the username `Ada` and the password obtained from the MIU Dept. 

## API connections

The application uses 2 main APIs at this point:
 - Google maps (paid through the Google account we hold on a PAYG basis with ILT credit card) and this shows the learners route to the work placement from their home address. The Google account is connected to the API via (m.thornleyboltoncollege@gmail.com) with the console to monitor usage etc. (Login credentials are in Key Pass). 
 - Salesforce CRM API. A cron job is scheduled to run every 5 minutes which pulls businesses from the Salesforce CRM and creates an entry or updates any existing ones. Once the businesses are in the application table, WEX can then simply use them to assign learners to placements. These business details are written to `elp`.`we_placement` These placements also have an places field which is used to govern how many placements the employer will take. BDU can increase or decrease this and the code will then manage this and allow or disallow placements based on available places. The places are shown along with the business name when an attempt to create a placement is made. If there are sufficent places available, with no exemptions, the placement will add successfully.   

>> NB If the CRM scripts fail pulling the businesses from Salesforce into the `we_placement` table this is most likely an account issue and before, this has been as simple as the account password needing to be changed on the Salesforce backend. If this is the case, don't forgot, once you've changed the password with Salesforce, you'll then also need to changed the password in your script too ***

## Cron Jobs 

A cron job is set to run every 5 minutes and calls the Salesforce API pulling all the businesses in to the table `elp`.`we_placement`. The PHP script for this is at:
`/var/www/web/workexperience/api/salesforce/salesforcetest.php` As well as the business details, the query also gets the number of total places at that business as part of the query - total places are set by BDU at the college, through Salesforce. Our logic then simply works out what placement numbers are left from what is assigned and then that is also added to the table. As the count is monitored, no learners can be mistakenly added to a placement where there is no room. BDU and some Careers Coach staff decide how many places remain and know the limits these businesses will take in regard to numbers of learners they'll have. If the available places needs to be increased, BDU are the people controlling that within Salesforce and staff should speak to them if they're unable to add a placement.   

## Reporting by BDU (Rhys Crombie) 

Various reports are generated from the WorkSpace data by MIU and some of this data can be used to pre-empt when a learner is not in college but is in fact on work placement. This enables registers to be pre-populated so the college is aware of who is not in college due to attending their work placement.

Initially when the app was first created, when a placement was added to the application regardless of the placement type, it had a start and end date and things were as simple as that. Now however, when a learner is on an industrial placement, the coach or whoever adds them will need to add each individual day individually via a date picker and these dates are now stored in a separate table `we_ind_placement_dates`.  The social and 30hr/5 day places start and end dates are stored as they were for now and recorded in `we_placement_attended`. Importantly, as far as the overall use of the app is concerned, there are no changes  experienced by the user, the data is just read from this other table when the learner is on an Industrial Placement. 

## More recent changes

Most recent changes requested and sometimes undertaken have been to the CRM section and aren't too complex but the learner.php page is becoming increasingly busy. The 'add a learner' area and the coach section have seen lots of amendments with the last few updates. 

Now, where a coach is added, a flag can be added to include whether they are a teacher in the options being added to the select list and various programmatic changes have been completed on the classes. Also, when a learner is added to a placement, further placement options have been added to the type. All these options are now displayed at the bottom of the page in a simple select list. These changes are fairly simple.  

Over time, more options for the placement type have been added to the CRM also in the 'add a learner' section [CRM/add learmer](http://www.boltoncc.ac.uk/workexperience/CRM/learner.php) for when a placement is created for the learner by the coach. 

All the placements below are 30hr/5 days and have a start and end date, <em>apart from</em> the Industrial Placement, which requires 50 days of dates to be entered by the coach through the date picker which will appear inside the HTML form when this option is chosen. 

The industrial placement dates are then stored in the `we_ind_placement_dates`  table against the placement attended id and all other placement dates are stored with the rest of the information in `we_placement_attended` 

Also remember now when a placement is created, Promonitor is used to get the current academic year and this along with the learner and placement details will be added. 
  
  ## Placement type options when adding a learner to a placement
  
- Youth Social Action/volunteering
- Realistic Work Environment
- 5 Day/30hr Placement
- Embedded
- P2P
- Supported Internship
- Industrial Placement (50 day placement) *50 days req, so date picker appears* 
 





