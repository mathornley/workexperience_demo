<?php
/**
 * Get the PSR4 autoloader and load all the classes required.
 * This page handles the validation to the updated review with PHP in the class (editReview()) and then finally Javascript
 */
spl_autoload_register(function ($class_name){
    include '../classes/'.$class_name . '.php';
});

// Stop the cache
require_once '../stop_cache.php';

// Error reporting.
ini_set('display_errors','On');

// Instantiate a new instance of coach.
$coach = new CareerCoachReview;

// ***** First find out if this is a coach - coaches, see all work experience. Check the coaches table for the users Shib and if it's there, they're a coach. 
if($isCareerCoach = $coach->isCareerCoach())
{
  // Get the coach details. 
  $isCareerCoach = $coach->isCareerCoach();
}
else
{
  // Not a coach so redirect later.
  echo 'Not a career coach stop';die;
}// ***** End find out if career coach 


/*
* $_GET['token'] is the placement_attended_id and we use token as a red herring. It is passed in from index.php via $_GET.
* This is coming from the index.php page and the student has been selected, so we now present them with a form to add their coach review.
* We'll use this in the review form when it's posted so we know what review placement_attended_id is being updated.
 */
$placement_attended_id =  $coach->hashids->decode($_GET['token'])[0]; // This is the placement_attended_id


// Set up an instance of the Employee review
$employee = new EmployeeReview;

// Get the table headings for the table to be used below in the HTML. 
$employerTableFields = $employee->employerTableFields();


// Edit the review
if(isset($_POST) && isset($_POST['update_employer_review']))
{
  // Update the review
  $editedReview = $coach->editCoachReview($_POST);
  // Hash the placement_attended_id
  $placement_attended_id = $coach->hashids->encode($placement_attended_id);
  // Redirect to the initial screen where updated review will show. 
  header("location: /workexperience/coach/admin.php?token={$placement_attended_id}");
}

// Pull all placements, regardless of the coach.
$allplacements = $coach->getPlacements();
//print_r($allplacements);die;

// Pull all the coach specific placements using the coachID from call above.
$coachPlacements = $coach->getPlacementsByCoachID($isCareerCoach[0]['id']);
//print_r($coachPlacements);die;

$coachName = $coach->getCoachName(2); // Test **

// This gets the student, placement and the coach details for the Jumbo area.
$employee = new EmployerReview;
$jumboDetails = $employee->getStudentDetailsForJumbo($placement_attended_id);

// Get the review if completed
$employerReview = $employee->getLearnerReview($placement_attended_id);


// First, check if review has already been submitted. This will be true or false.
$reviewCompleted = $coach->CheckCoachReview($placement_attended_id);

// This gets the logbook entries for the student so the coach can see them before completing the review.
$logbookEntries = $employee->getLogbookForEmployerReview($placement_attended_id);

// Check if review has already been submitted. This will be true or false. This also holds the input if any that has been submitted by this form so can use it to prepopulate. 
$reviewCompleted = $coach->CheckCoachReview($placement_attended_id);
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Work experience app</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="../css/bootstrap-theme.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="../css/theme.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

    <!--<script src="js/validatereview.js"></script>-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

    <!-- Fixed navbar -->
    <!--<nav class="navbar navbar-inverse navbar-fixed-top">-->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
           <a class="navbar-brand" href="/workexperience/">Work experience app</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <!--<li class="active"><a href="#">Home</a></li>-->
            <!--<li><a href="#about">About</a></li>-->
            <!--<li><a href="#contact">Contact</a></li>-->
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav><!-- end fixed navbar -->


  <div class="container-fluid theme-showcase" role="main">
    <div class="row"><!--Start of row -->
        <div class="col-md-12"><!-- content main full page-->
          <h2>Edit Career coach review section.</h2>
          <p>The completed review will be shown to the learner once completed. Below you can make changes to the reviewand then finally save.</p>  
          </div>
        </div>
      </div>

  <!-- Jumbatron section showing the details of who they are reviewing-->
  <div class="container-fluid theme-showcase" role="main">
    <div class="jumbotron">
      <h3>Student name:  <?=$jumboDetails['student_name']?></h3> 
        <p>Placement: <?=$jumboDetails['placement']?></p>
         <p>Address: <?=$jumboDetails['placement_address']?>, <?=$jumboDetails['post_code']?> </p>
          <p>Agreement signed: <?=$jumboDetails['agreement_signed_date']?></p>
          <hr/>
          <p>Coach name: <?=$jumboDetails['coach_name']?></p>
          <!--<p>PAID <?//=$jumboDetails['placement_attended_id']?></p>-->
        
    </div>
    <p>Below is the logbook entries for the learner who has been on placement with your organisation. You can view the learners hours and then finally complete the review at the very bottom of the page.</p>
    <p>The learner will be able to see your remarks and review of their performance once you've completed the review section.</p> 
  </div>
  <!-- End Jumbatron section showing the details of who they are reviewing-->


<!-- Start the learner logbook section-->
<div class="container-fluid theme-showcase" role="main">
  <div class="row"><!--Start of row -->
    <div class="col-md-12"><!-- content main full page--> 
   
    <h3>Student hours:</h3>

    <?php 
    // Only show if the student has logbook enytries.
    if($logbookEntries!=false):?>

    <fieldset>
      <div class="table-responsive">          
        <table class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>Date</th>
              <th>Start</th>
              <th>Finish</th>
              <th>Sick during when</th>
              <th>Entire sick day</th>
              <th>Absence details</th>
            </tr>
          </thead>
          <tbody>
              <?php 

              $x=0; // Loopy.
              $hours=0; // Keep track of the hours

              for($i=0;isset($logbookEntries[$i]);$i++):?>
                  <tr>
                      <td><?=date('l jS \of F Y',strtotime($logbookEntries[$i]['date']))?></td>
                      <td><?php if(isset($logbookEntries[$i]['start'])) echo date('H:i',strtotime($logbookEntries[$i]['start']));?></td>
                       <td><?php if(isset($logbookEntries[$i]['finish'])) echo date('H:i',strtotime($logbookEntries[$i]['finish']));?></td>
                      <td><?=($logbookEntries[$i]['sick_during']=='0') ? '' : $logbookEntries[$i]['sick_during'];?></td>
                      <td><?php if($logbookEntries[$i]['sickness']=='1') echo '<span class="glyphicon glyphicon-ok"></span>';?></td>
                      <td><?=$logbookEntries[$i]['details_of_absence']?></td>
                   </tr>
                  <?php ++$x;

                  // Work out the total hours, keeping a tally as you go.
                  $start = strtotime($logbookEntries[$i]['start']);
                  $finish = strtotime($logbookEntries[$i]['finish']);
                  $hours += round(abs($finish - $start) / 3600,2);
                 
                  endfor; // End the for loop

                  ?>

                <tr>
                  <th>Total hours</th>
                  <td><?=$hours?> hrs</td>
                </tr>

              </tbody>
            </table>
          </div> <!-- table-responsive ends-->
        </fieldset>
     
      <?php 
      // No logbook entries for the student.
      else:?>
        <p> No logbook entries for this student so far.</p>
      <?php endif;?>

    </div>
  </div>
</div>
<!-- End the learner logbook section-->



<div class="container-fluid theme-showcase" role="main">



    <div class="row"><!--Start of row -->
        <div class="col-md-12"><!-- content main full page-->      
         
         <!-- If the review has been completed, disable the form -->

          <h3>Employer review (to be completed by the coach):</h3>

          <fieldse.>
            <form method='POST'>
                 <div class="table-responsive">          
                    <table class="table table-bordered table-striped">
                      <thead>
                        <tr>
                          <th></th>
                          <th>Excellent</th>
                          <th>Good</th>
                          <th>Satisfactory</th>
                          <th>Requires improvement</th>
                        </tr>
                      </thead>
                      <tbody>
                          <?php $x=0;?>
                          <?php foreach($employerTableFields AS $heading):?>
                            <tr>
                                <td>
                                  <input type="hidden" name="review[<?=$x?>]['heading']" value="<?=$heading?>"/>
                                  <input type="hidden" name="placement_attended_id" value="<?=$_GET['placement_attended_id']?>"/>
                                <?=$heading?>
                                </td>
                                <td <?php if($employerReview[$x]['excellent']=='1') echo 'class="bg-success"';?>>
                                 <input type="radio" name="review[<?=$x?>]['score']" value="excellent" <?php if($employerReview[$x]['excellent']=='1') echo 'checked';?>/>
                                </td>
                                <td <?php if($employerReview[$x]['good']=='1') echo 'class="bg-success"';?>>
                                 <input type="radio" name="review[<?=$x?>]['score']" value="good" <?php if($employerReview[$x]['good']=='1') echo 'checked';?>/>
                                </td>
                                <td <?php if($employerReview[$x]['satisfactory']=='1') echo 'class="bg-success"';?>>
                                  <input type="radio" name="review[<?=$x?>]['score']" value='satisfactory' <?php if($employerReview[$x]['satisfactory']=='1') echo 'checked';?> />
                                </td>
                                <td <?php if($employerReview[$x]['requires_improvement']=='1') echo 'class="bg-success"';?>>
                                  <input type="radio" name="review[<?=$x?>]['score']" value="requires_improvement" autofocus required="required" <?php if($employerReview[$x]['requires_improvement']=='1') echo 'checked';?>/>
                                </td> 
                            </tr>
                            <?php ++$x;?>
                            <?php endforeach;?>
                            <tr>
                              <td colspan=5>
                              <p>Employers review comments:</p>
                                <textarea required class='textarea_review' placeholder='Please give a little more information on why you have made this choice' name="review['comments']"><?=$employerReview[0]['comments']?></textarea>
                            </tr>
                        </tbody>
                      </table>

                      <p><input type="checkbox" name="confirm_hours" value="1"
                        <?php if($employerReview[0]['confirm_hours']=='1') echo 'checked';?>/> I can confirm these hours are correct as per the students logbook entries.
                      </p>
                    
                    </div> <!-- table-responsive ends-->

                    <!-- The review is not compelted, so show the save button -->
                    <?php if(!isset($_POST['save_employer_review'])):?>
                      <p><button type='submit' class="btn btn-primary" name='save_employer_review' <?php if(isset($_POST['save_employer_review'])) echo 'disabled';?>>Save</button></p>
                   <?php endif;?>
                  </form>
                 
                  </fieldset>

                 <!-- The review is completed, so show the edit button -->
                <fieldset>
                <form method='POST'>
                  <?php if(isset($_POST['edit_employer_review']) || isset($_POST['save_employer_review']) ):?>
                    <p>Review completed</p>
                    <p>
                      <button type='submit' class="btn btn-danger" name='edit_employer_review' value="<?=$_GET['placement_attended_id']?>">Edit</button>
                    </p>
                  <?php endif;?>
                </form>
                 <!--<p><a href="/workexperience/employer/list.php?placement_id=<?=$placement_id_link?>"><< Back to all your learners list</a></p>-->
              </fieldset>
      
        </div> <!-- col ends-->  
      </div><!-- row ends-->







































      <div class="row"><!--Start of row -->
        <div class="col-md-12"><!-- content main full page-->      
         
         <!-- If the review has been completed, disable the form -->

          <h3>Career coach review:</h3>
            <p>Please be very concise and clear when completing the review as a copy will be emailed along with the other review details to the LDM and CLs involved with this learner and placement. </p>
          <fieldset>
            <form method='POST'>
                 <div class="table-responsive">          
                    <table class="table table-bordered table-striped">
                      <tbody>
                        <tr>
                          <td>
                            <p>Career coach review comments:</p>
                            <input type='hidden' name='row_id' value='<?=$reviewCompleted[0]['id']?>'>
                            <input type='hidden' name='coach_id' value='<?=$isCareerCoach[0]['id']?>'>
                            <textarea required class='textarea_review' placeholder='Please give a little more information on how well you think the student did in their placement.' name="review['comments']"><?php if(isset($reviewCompleted[0]['comments'])) echo $reviewCompleted[0]['comments'];?></textarea>
                          </td>
                        </tr>
                        </tbody>
                      </table>
                    </div> <!-- table-responsive ends-->

                     <!-- Area to for the coach to record the outcome of the work experience placement -->

                    <div class="form-group">
                      <label for="sel1">Completion status:</label>
                      <select required name='status' class="form-control" id="sel1">
                        <option value=''>Please select an outcome</option>
                        <option <?php if(isset($reviewCompleted[0]['status']) && $reviewCompleted[0]['status']=='Passed') echo 'selected'; ?>>Passed</option>
                        <option <?php if(isset($reviewCompleted[0]['status']) && $reviewCompleted[0]['status']=='Completed with learner issues') echo 'selected'; ?>>Completed with learner issues</option>
                        <option <?php if(isset($reviewCompleted[0]['status']) && $reviewCompleted[0]['status']=='Completed with employer issues') echo 'selected'; ?>>Completed with employer issues</option>
                        <option <?php if(isset($reviewCompleted[0]['status']) && $reviewCompleted[0]['status']=='Completed with both employer and learner issues') echo 'selected'; ?>>Completed with both employer and learner issues</option>
                        <option <?php if(isset($reviewCompleted[0]['status']) && $reviewCompleted[0]['status']=='Failed with learner issues') echo 'selected'; ?>>Failed with learner issues</option>
                        <option <?php if(isset($reviewCompleted[0]['status']) && $reviewCompleted[0]['status']=='Failed with employer issues') echo 'selected'; ?>>Failed with employer issues</option>
                        <option <?php if(isset($reviewCompleted[0]['status']) && $reviewCompleted[0]['status']=='Failed with both employer and learner issues') echo 'selected'; ?>>Failed with both employer and learner issues</option>
                      </select>
                    </div>

                    <!-- The review is not compelted, so show the save button -->
                      <p>
                         <p><input <?php if($reviewCompleted[0]['hours_confirmed']==1) echo 'checked';?> type="checkbox" name="hours_confirmed" value="1"/>
                        I can confirm these hours are correct as per the students logbook entries.
                      </p>
                      <p><button type='submit' class="btn btn-primary" name='update_employer_review' value="<?=$placement_attended_id?>">Save</button>
                      </p>
                  </form>
              </fieldset>
      
        </div> <!-- col ends-->  
      </div><!-- row ends-->

    </div> <!--end main container -->

    <!-- footer here -->
    <footer class="footer">
    <hr/>
      <div class="container">
        <p style="text-align:center;">Bolton College 2017</p>
      </div>
    </footer>
    <!-- end footer end -->

    <!-- Bootstrap core JavaScript ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
