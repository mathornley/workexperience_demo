<?php

/**
 * This view is for the coaches learners that are on placement when clicked on by another coach from the index.php page
 * This enables multiple coaches to complete reviews for one another. 
 * This page has also been set not to cache in the META Html code. 
 */

/**
 * Get the PSR4 autoloader and load all the classes required.
 */
spl_autoload_register(function ($class_name) {
    include '../classes/'.$class_name . '.php';
});

ini_set('display_errors', 'On');

// Stop the cache
require_once '../stop_cache.php';

// Instantiate a new instance of coach, placement and learner.
$coach = new CareerCoachReview;
$placement = new Placement;
$learner = new Learner;

// First find out if this is a coach - coaches, see all work experience. Check the coaches table for the users Shib and if it's there, they're a coach. 
if($isCareerCoach = $coach->isCareerCoach())
{
  // Get the coach details. 
  $isCareerCoach = $coach->isCareerCoach();
}
else
{
  // Not a coach so redirect later.
  echo 'Not a career coach so no access';die;
}

// Get the coach name. 
$coachName = $coach->getCoachName($coach->hashids->decode($_GET['token'])[0]); // Test
$coachFirstName = explode(' ', $coachName)[0]; // Get the coach first name.

// Pull all the coach specific placements using the coachID from call above - decoded by $_GET.
$coachPlacement = $coach->getPlacementsByCoachID($coach->hashids->decode($_GET['token'])[0]); // This is the coachID

$coachPlacements = $coach->alphaOrderArray($coachPlacement);
//echo '<pre>';print_r($coachPlacements);die;
?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <!-- Don't let this page cache -->
    <meta http-equiv="Pragma" content="no-cache">

    <title>WorkSpace</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap theme -->
    <link href="../css/bootstrap-theme.min.css" rel="stylesheet">


     <!-- Some jQuery to handle the buttons and divs for the forms-->
     <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="crossorigin="anonymous"></script>

    <!-- Include the jQuery for buttons and navs -->
    <script src="../includes/button_div_jquery.js"></script>

    <!-- This is the form filter and pagination setup stuffs -->
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  </head>

  <body>

    <!-- Fixed navbar -->
    <!--<nav class="navbar navbar-inverse navbar-fixed-top">-->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
           <a class="navbar-brand" href="/workexperience/coach/">My Workspace app</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Coach review section</a></li>
            <!--<li><a href="#about">About</a></li>-->
            <!--<li><a href="#contact">Contact</a></li>-->
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav><!-- end fixed navbar -->

  
      <div class="container-fluid theme-showcase" role="main">

         <script>
            // The filtered table stuff here  - order by total_score DESC
            $(document).ready( function(){
              $('#results').DataTable({
                "order": [[ 3, "desc" ]]
              });
            })
          </script>

        <div class="row"><!--Start of row -->
          <div class="col-md-12">
            <h3>Coach review</h3>
          </div><!-- End col -->
        </div> <!-- End row -->

       
        <!-- Start all coach specific learners - all learners for this coach -->
        <div class="row"><!--Start of row -->
          
          <div class="col-md-12">
            <h2>Career coach review</h2>
            <hr/>
            <p>Career coach: <strong><?=$coachName?></strong></p>
            <hr/>
            <p>This section allows you to see both your assigned learners and the other learners, the other coaches look after.</p>
            <p>From this section, you can choose and then finally review your student by clicking against their name. If you need to review one of the other students not assigned to you, that's fine, just click the learner and complete the review as you would normally. As a Career coach, you will have access to all learner reviews. </p>
            
            <h3>These are the <?=count($coachPlacements)?> learners who can be reviewed for <?=$coachName?></h3>
            <hr/>
            
             <p>Immediately below are your specific learners on placement. These results are from <strong>31st of July <?php if(date('Y')>'2018') echo (date('Y')-1); else echo date('Y'); ?> to the present</strong>. </p>

            <?php if($coachPlacements!=false):?>

                <fieldset>
                   <div class="table-responsive">          
                      <table id="results" class="table table-bordered table-striped">
                        <thead>
                          <tr>
                            <th>Student</th>
                            <th>Placement</th>
                            <th>Course</th>
                            <th>Start Date</th> 
                            <th>Days</th>
                            <th>Hours</th>
                            <th>Midpoint Completed</th>
                             <th>Withdrawn</th>
                            <th>Signed off placement</th>
                            <!--<th>Signed off placement</th>-->
                          </tr>
                        </thead>
                        <tbody>
                          <?php foreach($coachPlacements AS $learnerPlacement):?>
                            <tr>
                              <td>
                                <?php $placement_attended_id = $coach->hashids->encode($learnerPlacement['paid']);?>
                                <a href="/workexperience/coach/admin.php?token=<?=$placement_attended_id?>"><?=$coach->learner->learnerDetailsByID($learnerPlacement['learner_id'])?></a><br/>
                                [<?=$learnerPlacement['learner_id']?>]
                              </td>
                              <td><?=$coach->placementDetails->getPlacementDetails($learnerPlacement['placement_id'])?></td>
                              <td>
                               <?=$learner->getCourseAndLevel($learnerPlacement['learner_id'])?>
                              </td>
                               <td>
                                <!-- Find out the start date of the placement. Show if not industrial, otherwise -->
                                <?=(isset($placement->getPlacementAttenedInfo($learnerPlacement['paid'])[0]['start_date'])) ? ucfirst(date('d/m/Y', strtotime($placement->getPlacementAttenedInfo($learnerPlacement['paid'])[0]['start_date']))) : $learner->getDatesIndStart($learnerPlacement['paid']);?>
                              </td>
                              <td>
                               <?=$learner->logbookDaysCompleted($learnerPlacement['paid'])?>
                             </td>
                              <td>
                               <?=$learner->logbookHoursCompleted($learnerPlacement['paid'])?>
                              </td>
                              <td>
                               <?=($learner->midPointCompleted($learnerPlacement['paid'])==1) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>';?>
                              </td>
                               <td>
                                 <?=($learner->isLearnerWithdrawnFromCollege($learnerPlacement['learner_id'])!=false) ? $learner->isLearnerWithdrawnFromCollege($learnerPlacement['learner_id']) : '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>';?>
                              </td>
                               <td>
                                <?=($coach->isLearnerSignedOffPlacement($learnerPlacement['paid'])==true) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>';?></td>
                              <!--<td><?//=($learnerPlacement['coach_signed_off']!='0') ? '<span class="glyphicon glyphicon-ok"></span>' : '<span class="glyphicon glyphicon-remove"></span>';?></td>-->
                            </tr>
                          <?php endforeach;?>
                            </tbody>
                          </table>
                      </div> <!-- table-responsive ends-->
            </fieldset>
              <?php else:?>
                <p> There are no placements needing reviewing for <?=$coachFirstName?> at the current time.</p>
          <?php endif;?>
          <hr/>
          <p><a href='/workexperience/coach/'>Click to return to your reviews list</a></p>
        </div> <!-- End col -->
      </div> <!-- End row -->
      <!-- End all coach specific learners -->

        
    </div> <!--End outer container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
