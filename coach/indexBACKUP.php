<?php

/**
 * Get the PSR4 autoloader and load all the classes required.
 */
spl_autoload_register(function($class_name) {
    include '../classes/'.$class_name.'.php';
});

ini_set('display_errors', 'On');

// Stop the cache
require_once '../stop_cache.php';


// Instantiate a new instance of coach.
$coach = new CareerCoachReview;

// First find out if this is a coach - coaches, see all work experience. Check the coaches table for the users Shib and if it's there, they're a coach. 
if($isCareerCoach = $coach->isCareerCoach())
{
  // Get the coach details. 
  $isCareerCoach = $coach->isCareerCoach();
}
else
{
  // Not a coach so redirect later.
  echo 'Not a career coach so no access';die;
}

// Pull all placements, regardless of the coach.
$allplacements = $coach->getPlacements();
//print_r($allplacements);die;

// Pull all the coach specific placements using the coachID from call above.
$coachPlacement = $coach->getPlacementsByCoachID($isCareerCoach[0]['id']);
//echo '<pre>';print_r($coachPlacement);die;

// Get all the coaches in the we_coach table.
$allCoaches = $coach->getAllCoach();

// Instantiate an instance of the autocomplete class we'll use for search.
$ac = new AutoComplete;
$learner = new learner;
$coach = new CareerCoachReview;

// Learner has been selected from the autocomplete text search box. Now go off and get it!
if(isset($_POST['learner_search']))
{
  // The learner name and the learner id string converted to array.
  $learnerNameAndID = explode(' ',$_POST['learner_search']);
  // The id is the last element, so grab that.
  $learner = end($learnerNameAndID);
  // Send that off to the function. 
  $learnerPlacements = $coach->getTheLearnerPlacements($learner);
  //echo '<pre>';print_r($learnerPlacements);die;
}

$coachPlacements = $coach->alphaOrderArray($coachPlacement);
//if($coachPlacements) echo '<pre>';print_r($coachPlacements);die;
?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Coach section of the My Workspace application.</title>

    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/themes/base/minified/jquery-ui.min.css" type="text/css" />

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- JQuery -->
    <script
    src="https://code.jquery.com/jquery-1.12.4.js"
    integrity="sha256-Qw82+bXyGq6MydymqBxNPYTaUXXq7c8v3CwiYwLLNXU="
    crossorigin="anonymous"></script>

     <!-- Autocomplete stuff -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
   


    <!--Autcomplete listener here here -->
    <script>
      $(function(){
        $("#autocomplete").autocomplete({
          source: "search.php", //autocomplete here  - source is the PHP script: search.php
          minLength: 1 // Match for how many letters before the magic starts.
        }); 

        $( "#autocomplete").focus(function() {
          $("span.message").css( "display", "inline" ).fadeOut(5000);
        });
      });
    </script>

    <!-- Brief styles -->
    <link rel="stylesheet" type="text/css" href="../css/styles.css">
    <!-- Brief styles end --> 

  </head>

  <body>

    <!-- Fixed navbar -->
    <!--<nav class="navbar navbar-inverse navbar-fixed-top">-->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
           <a class="navbar-brand" href="/workexperience/">My Workspace app</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Coach review section</a></li>
            <!--<li><a href="#about">About</a></li>-->
            <!--<li><a href="#contact">Contact</a></li>-->
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav><!-- end fixed navbar -->

  
      <div class="container-fluid theme-showcase" role="main">

        <div class="row"><!--Start of row -->
          <div class="col-md-12">
            <h3>Coach review</h3>
          </div><!-- End col -->
        </div> <!-- End row -->

       
        <!-- Start all coach specific learners - all learners for this coach -->
        <div class="row"><!--Start of row -->
          
          <div class="col-md-12">
            <h2>Career coach review</h2>
            <hr/>
            <p>Career coach: <strong><?=$isCareerCoach[0]['name']?></strong></p>
            <hr/>
            <p>This section allows you to see both your assigned learners and the other learners, the other coaches look after.</p>
            <p>From this section, you can choose and then finally review your student by clicking against their name. If you need to review one of the other students not assigned to you, that's fine, just click the learner and complete the review as you would normally. As a Career coach, you will have access to all learner reviews. </p>
            
            <h3>Your <?=count($coachPlacements)?> learner result(s)</h3>
            
            <p>Immediately below are your specific learners on placement. These results are from <strong>31st of July <?php if(date('Y')>'2018') echo (date('Y')-1); else echo date('Y'); ?> to the present</strong>. </p>

            <?php if($coachPlacements):
              // Only show the reviews, not the empty table if the coach as none. 
            ?>
            <fieldset>
                   <div class="table-responsive">          
                    <table class="table table-bordered table-striped">
                      <thead>
                        <tr>
                          <th>Student</th>
                          <th>Student ID</th>
                          <th>Coach</th>
                          <th>Placement</th>
                          <th>Agreement signed</th>
                          <th>Signed off placement</th>
                          <!--<th>Signed off placement</th>-->
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach($coachPlacements AS $learnerPlacement):?>
                          <tr>
                            <td>
                              <?php $placement_attended_id = $coach->hashids->encode($learnerPlacement['paid']);?>
                              <a href="/workexperience/coach/admin.php?token=<?=$placement_attended_id?>"><?=$coach->learner->learnerDetailsByID($learnerPlacement['learner_id'])?></a>
                            </td>
                            </td>
                            <td><?=$learnerPlacement['learner_id']?></td>
                            <td><?=$coach->getCoachName($learnerPlacement['coach_id'])?></td>
                            <td><?=$coach->placementDetails->getPlacementDetails($learnerPlacement['placement_id'])?></td>
                            <td><?php echo ($learnerPlacement['agreement_signed_date']==NULL) ? '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>' : date('l, jS \of F Y H:i \h\r\s',strtotime($learnerPlacement['agreement_signed_date']));?></td>
                             <td><?=($coach->isLearnerSignedOffPlacement($learnerPlacement['paid'])==true) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>';?></td>
                            <!--<td><?//=($learnerPlacement['coach_signed_off']!='0') ? '<span class="glyphicon glyphicon-ok"></span>' : '<span class="glyphicon glyphicon-remove"></span>';?></td>-->
                          </tr>
                        <?php endforeach;?>
                          </tbody>
                        </table>
                      </div> <!-- table-responsive ends-->
            </fieldset>
          <hr/>
        </div> <!-- End col -->
      </div> <!-- End row -->
      <?php else:?>
        <p>You currently have no learners to review. </p>
      <?php endif;?>
      <!-- End all coach specific learners -->


        <!-- Start all other learners, regardless of who is their coach.-->
        <div class="row"><!--Start of row -->
           <div class="col-md-12">
            <h3>All other coaches and their learners</h3>
              <p>Below are a list of all My Workspace coaches with learners on placement. Select a coach to see the list of learners assigned to that coach.</p>

              <fieldset>
                   <div class="table-responsive">          
                    <table class="table table-bordered table-striped">
                      <thead>
                        <tr>
                          <th>Coach</th>
                          <th>Phone</th>
                          <th>Mobile</th>
                          <th>Email</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <?php for($i=0;isset($allCoaches[$i]);$i++):?>
                            <!--Don't show the same coach learners as above again, just exclude theirs--> 
                            <?php if($allCoaches[$i]['id']!==$isCareerCoach[0]['id'] && $allCoaches[$i]['admin']=='0'):?> 
                              <?php $coachID = $coach->hashids->encode($allCoaches[$i]['id']);?>
                              <tr>
                                <td>
                                  <a href="/workexperience/coach/list.php?token=<?=$coachID?>"><?=$allCoaches[$i]['name']?></a>
                                </td>
                                <td>
                                  <?=$allCoaches[$i]['phone']?>
                                </td>
                                 <td>
                                  <?=$allCoaches[$i]['mobile']?>
                                </td>
                                <td>
                                 <?=$allCoaches[$i]['email']?>
                                </td>
                            </tr>
                            <?php endif;?> 
                          <?php endfor;?>
                        </tr>
                        </tbody>
                      </table>
                    </div> <!-- table-responsive ends-->
           </fieldset>
          <hr/>
          </div> <!-- End col -->
        </div>
        <!-- End all other learners -->


         <style>
          input[type="search"]::-webkit-search-cancel-button{
            -webkit-appearance: searchfield-cancel-button;
          }
        </style>

        <!-- Search all other learners, regardless of coach -->
        <div class="row"><!--Start of row -->
            <div class="col-xs-12" class="col-md-12">
            <h3>Search for any learner</h3>
              <p>Below you can search for any learners registered.</p>
              
        
          <form method='POST'>
            <div class="inner-addon left-addon">
              <i class="glyphicon glyphicon-user"></i>
              <input type="search" class="form-control" name='learner_search' value='' placeholder='Enter the student details. You can search for firstname, surname or student ID' id='autocomplete'/>
            </div>
            <span class='message'>You can search for the firstname, surname, or the learner ID.</span>  
          </form>
        </div>
      </div> <!-- End row -->


    
      <!-- Autocomplete results here -->
      <div class="row"><!--Start of row -->
        <div class="col-xs-12" class="col-md-12">
          <div class='autocomplete_results'>
          <?php if(isset($learnerPlacements) && $learnerPlacements!=false):?>
            <h3>(<?=count($learnerPlacements)?>) learner result(s): <?//=$learner->learnerDetailsByID($learnerPlacements[0]['learner_id'])?> </h3>
                
                <fieldset>
                   <div class="table-responsive">          
                    <table class="table table-bordered table-striped">
                      <thead>
                        <tr>
                          <th>Student</th>
                          <th>Student ID</th>
                          <th>Coach</th>
                          <th>Placement</th>
                          <th>Agreement signed</th>
                          <!--<th>Signed off placement</th>-->
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach($learnerPlacements AS $learnerPlacement):?>
                          <tr>
                            <td>
                              <?php $placement_attended_id = $coach->hashids->encode($learnerPlacement['id']);?>
                              <a href="/workexperience/coach/admin.php?token=<?=$placement_attended_id?>"><?=$coach->learner->learnerDetailsByID($learnerPlacement['learner_id'])?></a>
                            </td>
                            </td>
                            <td><?=$learnerPlacement['learner_id']?></td>
                            <td><?=$coach->getCoachName($learnerPlacement['coach_id'])?></td>
                            <td><?=$coach->placementDetails->getPlacementDetails($learnerPlacement['placement_id'])?></td>
                            <td><?=date('l, jS \of F Y',strtotime($learnerPlacement['agreement_signed_date']))?></td>
                            <!--<td><?//=($learnerPlacement['coach_signed_off']!='0') ? '<span class="glyphicon glyphicon-ok"></span>' : '<span class="glyphicon glyphicon-remove"></span>';?></td>-->
                          </tr>
                        <?php endforeach;?>
                          </tbody>
                        </table>
                      </div> <!-- table-responsive ends-->
            </fieldset>
          <hr/>
           <?php endif;?>
          <?php if(isset($_POST['learner_search']) && (!isset($autoCompleteResult) || $autoCompleteResult==false)):?>
            <p>No results found for the learner <?php if(isset($_POST['learner_search'])) echo $_POST['learner_search'];?> </p>
          <?php endif;?>
         
        </div>
      </div>
    </div>
     <!-- Autocomplete results end here -->
             
    </div> <!-- End container-->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
