<?php

/**
 * Autoloader for including all the classes.
 */
spl_autoload_register(function ($class_name) {
    include $class_name . '.php';
});

// Stop the cache
require_once 'stop_cache.php';

// Display errors
ini_set('display_errors', 'On');

/**
 *  Class to handle the aims and objectives we'll be displaying and processing from the learner.
 */
class Aims extends Main
{
	public function __construct()
	{	
		try
		{
			// Call the parent first and instantiate the database SQL class instance.
			parent::__construct();
			// Instantiate a new instance of learner.
			$this->learner = new Learner;
			// Get an instance of coach
			$this->coach = new CareerCoachReview;
		}

		// Catch some PDO errors.
		catch (PDOException $e)
		{
			throw new Exception('PDO Database connection failed to connect to SID failed in line: ' .__LINE__ . ' of file: '. __FILE__ . $e->getMessage());
		}
	}

	/**
	 * Displays all the aims and objectives via a MySQL describe call to the aims table and formats them for display.
	 * This function gets the column names and formats them to a capitlised letter and removes underscores.
	 * @return [type] Returns an array with the column names all formatted.
	 */
	public function displayAimsFormatted()
	{
		// Describe command on the table.
		$aims = $this->conn->select('DESCRIBE `we_aims`');

		for($i=0;isset($aims[$i]['Field']);$i++)
		{
			// Iterate the aims and drop any that are ID related and not needed.
			if($aims[$i]['Field']!='id' && $aims[$i]['Field']!='placement_id' && $aims[$i]['Field']!='placement_attended_id')
			{
				// Capitalise and format the aims.
				$array[] = $this->capitaliseAndStringReplace($aims[$i]['Field'], '_');
			}	
			
		}
		// Return the formatted aims.
		return $array;
	}

	/**
	 * Display all the aims and objectives via a MySQL describe call to the aims table and get the raw column names.
	 * There is no formatting on this arrays fields.
	 * They can be used in an html form fields for the name= vaue
	 * @return [type] [description]
	 */
	public function displayAims()
	{
		// Describe command on the table.
		$aims = $this->conn->select('DESCRIBE `we_aims`');

		for($i=0;isset($aims[$i]['Field']);$i++)
		{
			// Iterate the aims and drop any that are ID related and not needed.
			if($aims[$i]['Field']!='id' && $aims[$i]['Field']!='placement_attended_id')
			{
				// Capitalise and format the aims.
				$array[] = $aims[$i]['Field'];
			}	
			
		}

		// Return the raw unformatted aims from the table.
		return $array;
	}


	/**
	 * Select the learner chosen aims from the aims table based on the placement id. 
	 * These will be unique to the placement_attended data table.
	 * These are unformatted results. 
	 * Filter out the aims the learner didn't choose and just see their selection. 
	 * Uses the placement id from the learner table.
	 * @return Either FALSE for no result or the row of the learner aims. 
	 */
	public function learnerAims($studID,$placement_attended_id=null)
	{
		// Get the learner details for that learner id.	
		$learner = $this->learner->learnerDetails($studID,$placement_attended_id);

		// Get the placement id from the learner/placement query in learner
		$placementID = $learner[0]['id'];

		// Query the aims table to see if they have completed their aims already and return row. If not, return false.
		// The aims are connected with the placement_attended_id
		$row =  $this->conn->select('SELECT * FROM  `we_aims` WHERE `placement_attended_id`=?',[$placementID]);

		//print_r($row);die;


		// Has aims.
		if($row!=FALSE)
		{
			// Placeholder array for the chosen values.
			$array = array();

			// Iterate the fields.
			foreach($row AS $rows=>$item)
			{
				foreach($item AS $key=>$value)
				{
					// Iterate the aims and drop any that are ID related fields of the table and not aims.
					if($key!='id' && $key!='placement_attended_id')
					{
						// Get the chosen fields only.
						if($value=='1') $array[$key] =$value;
					}
				}
			}
			// Return the learner aims.
			return $array;
		}

		// No aims, return FALSE.
		else
		{
			return FALSE;
		}
	}

	/**
	 * Select the learner chosen aims from the aims table based on the placement id. 
	 * These will be unique to the placement_attended data table.
	 * These are nicely formatted results. 
	 * Uses the placement id from the learner table.
	 * @return Either FALSE for no result or the row of the learner aims. 
	 */
	public function learnerAimsFormatted($studID,$placement_attended_id=null)
	{

		// Get the placement id from the learner/placement query in learner
		$placementID = $placement_attended_id;

		// Query the aims table to see if they have completed their aims already and return row. If not, return false.
		// The aims are connected with the placement_attended_id
		$row =  $this->conn->select('SELECT * FROM  `we_aims` WHERE `placement_attended_id`=?',[$placementID]);

		// Has aims.
		if($row!=FALSE)
		{
			// Placeholder array for the chosen values.
			$array = array();

			// Iterate the fields.
			foreach($row AS $rows=>$item)
			{
				foreach($item AS $key=>$value)
				{
					// Iterate the aims and drop any that are ID related fields of the table and not aims.
					if($key!='id' && $key!='placement_attended_id')
					{
						// Get the chosen fields only.
						if($value=='1') $array[] = $this->capitaliseAndStringReplace($key,'_',' ');
					}
				}
			}
			// Return the learner aims.
			return $array;
		}

		// No aims, return FALSE.
		else
		{
			return FALSE;
		}
		
	}

	/**
	 * These are the aims that will be added to the table using the placement attended ID.
	 * @param [type] $POST [description]
	 */
	public function addLearnerAims($POST,$placement_attended_id)
	{

		//print_r($_POST);die;

		// Get the learner details for that learner id.	
		$learner = $this->learner->learnerDetails();

		// Start building the query - set the placement_attended_id firtsly.
		$query = 'INSERT INTO `we_aims` (placement_attended_id,';

		// Iterate the column names and add, so we can start the beginning of the query,
		foreach($POST AS $column=>$value)
		{
			// Get each column name
			$query.= $column.',';	
		}
		// Get rid of the extra comma at the end of the row.
		$query = rtrim($query,', ');

		// Now get the number of values (domn't forget these are just '?')
		$query.=') VALUES(';

		// Placeholder for the values parameters
		$args = array();

		// Make the first parameter the placement_attended_id
		$args[] = $placement_attended_id;

		// Add this so we are adding the placement_attended_id placeholder first (as per the columns set above)
		$query.= "?,";	

		foreach($POST AS $column=>$value)
		{	
			$query.= "?,";	// Get all the placeholders, the foreach tells how many.
			$args[] = $value; // Populate the args
		}

		// Get rid of the extra comma at the end of the row.
		$query = rtrim($query,',');

		// Finally close the query. 
		$query.=");";

		// Send the query and params off to be executed by the SQL class.
		$row =  $this->conn->insert($query,$args);	
	}

	/**
	 * These are the updated aims that will be added to the table using the placement attended ID.
	 * The old aims, if not still chosen, will need to be turned back to NULL to not show now.
	 * @param  [array] $POST The updated POSTED aims the user has chosen. 
	 * @return [array] A new array with the new values and NULLs for anything not chosen.
	 */
	public function editLearnerAims($POST)
	{
		//echo '<pre>';print_r($POST);die;

		// Placeholderarray
		$array = array();
		
		// Foreach over the full array of aims and check if our posted aims are in there.
		foreach($this->displayAims() AS $key)
		{
			if(isset($POST[$key]) && $POST[$key]=='1')
			{
				// If they're there, just add 1
				$array[$key] = 1;
			}
			else
			{
				// Not in there so just now make this null as we don't want anything not chosen.
				$array[$key] = NULL;
			}
		}

		// Send off the finished array so the new aims can be added to the aims table. 
		return $this->updateLearnerAims($array,$POST['studID'],$POST['placement_attended_id']);

	}

	public function updateLearnerAims($array, $studID, $placement_attended_id)
	{
		// Placeholder for query args.
		$args = array(); 

		// Start building the query - set the placement_attended_id firtsly.
		$query = 'UPDATE `we_aims` SET ';

		// Iterate the column names and add, so we can start the beginning of the query,
		foreach($array AS $column=>$value)
		{
			// Get each column name
			$query.= "`{$column}`=?".',';	
		}

		// Get rid of the extra comma at the end of the row.
		$query = rtrim($query,', ');

		// Add this so we are adding the placement_attended_id placeholder first (as per the columns set above)
		$query.= " WHERE `placement_attended_id`={$placement_attended_id}";	

		//$args[] = $placement_agreement_id;

		foreach($array AS $column=>$value)
		{	
			$args[] = $value; // Populate the args
		}

		// Finally close the query. 
		$query.=";";

		//echo $query;die;

		// Send the query and params off to be executed by the SQL class.
		return $result =  $this->conn->update($query,$args);	

	}


	public function __destruct()
	{
		$this->conn = null;
	}



}


