<?php

/**
 * Autoloader for including all the classes.
 */
spl_autoload_register(function ($class_name) {
    include $class_name . '.php';
});

/**
 * A class to be associated with the learner details. 
 */
class LDM extends Main
{
	public function __construct($placement_attended_id=null,$stuID=null)
	{	
		try
		{
			// Call the parent first and instantiate the database SQL class instance.
			parent::__construct();

			if(isset($stuID))
			{
				$this->stuID = $stuID;
			}
		}

		// Catch some PDO errors.
		catch (PDOException $e)
		{
			throw new Exception('PDO Database connection failed to connect to SID failed in line: ' .__LINE__ . ' of file: '. __FILE__ . $e->getMessage());
		}
	}

	/**
	 * Get the learners LDM from the $stuID
	 * If $stuID is passed in, then use that or just use shibboleth.
	 * @param  [string] $stuID [This is the learner id, if it is passed in]
	 * @return [array] [This will either be the learner id or FALSE if there are no details]
	 */
	public function getLearnerLDM($stuID=null)
	{
		if(isset($stuID))
		{
			$this->stuID = $stuID;
		}

		return $this->conn->select(

		'SELECT `ldm`.`name` from `elp`.`ext_ebs_data_learnergroup` `lgp` inner join `elp`.`elp_ldm` `ldm` on `ldm`.`curriculum` = `lgp`.`section` WHERE `learner` = ?',[$this->stuID]
		);

		// $ldm = $db->querywhatever("select ldm.name from ext_ebs_data_learnergroup lgp inner join elp_ldm ldm on ldm.curriculum = lgp.section where learner = ?LEARNERID ")
	}

	public function __destruct()
	{
		$this->conn = null;
	}


}
