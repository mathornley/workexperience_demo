<?php

/**
 * Autoloader for including all the classes.
 */
spl_autoload_register(function ($class_name) {
    include $class_name . '.php';
});

// turn cache off for this project
ini_set('opcache.enable', '0'); 

/**
 * The parent class of the project. All other classes will be child classes of Main.
 * The $stuID is set from the Shabboleth server global variable, set when the user logs in. This is stored in the $_SERVER var. 
 * All helper and utility functions for all classes in here and are shared with the subclasses.
 */
class Main
{
	public $conn;
	public $stuID;

	public function __construct()
	{	
		// Connection to the database and use try/catch to catch any issues. 
		try
		{
			// Get the databae connection.
			$this->conn = new SQL();

			// Get the learner id (number for the student, or a string for staff test account in learner if not) from Shibboleth as it's now configured on the workexperience application for.
			
			// This is getting the staff member from the shibboleth.
			$this->stuID = (isset($_SERVER['sAMAccountName'])) ? $_SERVER['sAMAccountName'] : 'johnd'; 

			// HashGenerator.php & Hashids.php.
			$this->hashids = new Hashids('',25); // Pad the URL to 25.
		}

		// Catch some PDO errors.
		catch (PDOException $e)
		{
			throw new Exception('PDO Database connection instantiation failed in line: ' . __LINE__ . ' of the file: '. __FILE__ .' with the message: '. $e->getMessage());
		}
	}

	/**
	 * Take one postcode at a time and returns an array of a lat & lng for the postcode.
	 * Takin the postcode, uses cURL to pass that to Google Geocoding service and returns lat & long for that postcode.
	 * @param  [string] $postcode The postcode to be geocoded that is passed in.
	 * @return [array] Returns an array of the postcode lat & lng to be used in the page.
	 */
	public function reverseGEOcodePostcode($postcode)
	{
		// Get cURL resource
		$curl = curl_init();

		// Set some options - we are passing in a useragent too here.
		curl_setopt_array($curl, array(
		    CURLOPT_RETURNTRANSFER => 1,
		    CURLOPT_URL => "https://maps.googleapis.com/maps/api/geocode/json?address={$postcode},+UK&key=<APIKEY>",
		    CURLOPT_USERAGENT => 'Codular Sample cURL Request'
		));

		// Send the request & save response to $response
		$response = curl_exec($curl);

		// Close request to clear up some resources
		curl_close($curl);

		// Make a PHP array from the JSON.
		$result = json_decode($response, TRUE);

		// Change the decoded results to a string.
		$lat = $result['results'][0]['geometry']['location']['lat'];
		$lng = $result['results'][0]['geometry']['location']['lng'];

		//$curl==NULL;

		// Add both now converted lat & long to an array and return.
		return array($lat,$lng);
	}


	/**
	 * A function that strips out any required character of string and capitaises that string.
	 * $string passed in is the string and $character is the string to be stripped out.
	 * Default setting to replace the character is a space set in $replace. To change just pass 3 params.  
	 * @param  [string] $string This the string we're adjusting.
	 * @param  [string] $character Is the replacement.
	 * @return [string] $replace. Where in the string we'll be replacing with $character.
	 */
	public function capitaliseAndStringReplace($string, $character, $replace=' ')
	{
		return ucfirst(str_replace($character,$replace, $string));
	}

	/**
	 * Capitalise the first letter of any string.
	 * @param  [string] $string The string passed in to be capitalised.
	 * @return [string] The capitalised string is returned. 
	 */
	public function capitaliseString($string)
	{
		return ucfirst($string);
	}


	/**
	 * Simple helper function to sanitise strings.
	 * Takes a string and then returns the value cleansed.
	 * @return [string] [The cleansed string]
	 */
	public function cleanseVars($string)
	{
		return filter_var($string, FILTER_SANITIZE_STRING);
	}

	/**
	 * A simple function to get all the files from the directory.
	 * If an ext is specified, files are only inc that have that ext. Otherwise, it gets all files. 
	 * @param  [string] $dir This is the directory to scan for files. 
	 * @return [array] The array of files (filtered or otherwise by flag $fileExt).
	 */
	public function getFilesFromDirectoryForDebug($dir, $fileExt=NULL)
	{
		// File extension isn't set so just get all the files. 
		if(!isset($fileExt)) return $files = scandir($dir);

	
		// Filter files by ext according to $fileExt.
		else
		{
			// Placeholder for files.
			$array = array();

			// Scan the dir and get ALL files.
			$files = scandir($dir); 

			// Filter the files by $fileExt.
			foreach($files AS $file)
			{
				// Exclude these files.
				if($file=='index.php') continue;
				if($file=='stop_cache.php') continue;

				if(preg_match('/.php/',$file)) $array[] = $file;

			}
		}

		// return the filtered array.
		return $array;
	}

	/**
	 * This is a function to report a bug to the `we_debug` table. 
	 * @param  [array] This is the array data posted 
	 * @return [type]       [description]
	 */
	public function logBug($POST)
	{
		unset($POST['report_bug']); // Unset the form flag.

		$args = array();
		$date_created = date('Y-m-d H:i:s');
		
		// Get the files if more than one and make the $files string. Also, the query will be different.
  		if(isset($POST['files']))
  		{
  			$files = implode('&',$POST['files']); // Make the files away one string.

  			// Query for a few files
  			$query="INSERT INTO `we_debug` (`person_code`, `file`, `files`, `issue`, `date_created`) VALUES(?,?,?,?,?)";
  			$args = array($POST['person_code'],$POST['filename'],$files,$POST['nature_of_issue'],$date_created);

  		}

  		// Not multiple files as issue.
  		else
  		{
  			// Query for a just one file
  			$query="INSERT INTO `we_debug` (`person_code`, `file`, `issue`, `date_created`) VALUES(?,?,?,?)";
  			$args = array($POST['person_code'],$POST['filename'],$POST['nature_of_issue'],$date_created);
  		}	

  	
  		return $this->conn->insert($query,$args);

	}

	/**
	 * Simple function to check that the learner id from SHIB is the same as the one in the $_GET request.
	 * This is to stop learners using the URL of the app to view other peoples instancees.
	 * If the IDS are different and this is NOT a coach, this is not a legitimate learner.
	 * If legit, the learner is allowed to view the page, if not, logic in the oage requesting the function dictates what they see.  
	 * @param  string  $learner_id This is the string learner id sent from the page and the $_GET
	 * @return boolean This returns true or false, depending on if the request is legitimate.
	 */
	public function doesTheLearnerIdMatch($learner_id)
	{

		if($this->stuID==$learner_id)
		{
			return true;
		}

		else
		{
			return false;
		}
	}


	/**
	 * Close the connection to PDO. 
	 */
	public function __destruct()
	{
		$this->conn = NULL;
	}
}

