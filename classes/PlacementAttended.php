<?php

/**
 * Autoloader for including all the classes.
 */
spl_autoload_register(function ($class_name) {
    include $class_name . '.php';
});

class PlacementAttended extends Main
{
	public function __construct($studID=null)
	{	
		try
		{
			// Call the parent first and instantiate the database SQL class instance.
			parent::__construct();
			
			// Instantiate a new instance of learner so we can access those details.
			$this->learner = new Learner;

			if(isset($studID)) $this->stuID = $studID;
		}

		// Catch some PDO errors.
		catch (PDOException $e)
		{
			throw new Exception('PDO Database connection failed to connect to SID failed in line: ' .__LINE__ . ' of file: '. __FILE__ . $e->getMessage());
		}
	}

	/**
	 * Get the placement attendance row details for the learner and that placement attended id. 
	 * This contains the information on if the agreement has been signed and at what time. 
	 * @return [array] Returns true or false on whether the student agreement has been signed.
	 */
	public function agreementSigned($stuID,$placement_attended_id)
	{	
		// Pull the row from the placement attended table for the student id
		$array =  $this->conn->select('SELECT * FROM  `we_placement_attended` WHERE `learner_id`=? AND `id`=?',[$stuID,$placement_attended_id]);

		// TRUE if signed, otherwise false. 
		return ($array[0]['agreement_signed']=='1') ? TRUE : FALSE;
	}

	/**
	 * A function to accept the form $_POST vars from the agreement.php page and finally sign the agreement.
	 * @param  [array] $POST The posted vars from the form inside the agreement.php student agreement page. 
	 */
	public function signAgreement($POST)
	{
		$studID = $POST['studID'];
		$placement_attended_id = $POST['placement_attended_id'];
		$date = date('Y-m-d H:i:s');

		// Update the table to show that the learner has signed the agreement.
		return $this->conn->update('UPDATE `we_placement_attended` SET `agreement_signed`=?,`agreement_signed_date`=? WHERE `learner_id`=? AND `id`=?',
			[1,$date,$studID,$placement_attended_id]);
	}

	/**
	 * Pull all placements from the placements_attended_id table. 
	 * @return [array] [All placements in the we_placements_attended table as array]
	 */
	public function getPlacements()
	{
		// Pull all the placements.
		return $this->conn->select('SELECT * FROM `we_placement_attended` ORDER BY `date_created` DESC');
	}

	/**
	 * Get all the placements via the coachID passed in to the `we_placement_attended` id.
	 * These placements are only from the end of July [THIS YEAR] on wards.
	 * @param  [string] $coachID [The coachID passed in]
	 * @return [array] [Returns an array of all the placements with the $coachID]
	 */
	public function getPlacementsByCoachID($coachID)
	{

		$endOfJuly = date('Y').'-07-31';

		// Pull all the placements.
		$placements = $this->conn->select('SELECT * FROM `we_placement_attended` WHERE `coach_id`=? AND `date_created` BETWEEN ? AND NOW() ORDER BY `agreement_signed_date` DESC',
			[
				$coachID,
				$endOfJuly
			]
		);
		return $placements;
	}

	public function getPlacementPostCode($placementID)
	{
		// Pull all the placements.
		return $this->conn->select('SELECT `post_code` FROM `we_placement` WHERE `id`=?',[$placementID]);
	}


	/**
	 * Check that the learner has placements in the placement_attended table. If not, return false.
	 * @return [boolean] True or false depending on if they have placements or not. 
	 */
	public function checkLearnerPlacements()
	{
		// Get the learner ID from the learner instance.
		$learnerId = $this->learner->stuID;

		// Pull all the placements.
		$placements  = $this->conn->select('SELECT * FROM `we_placement_attended` WHERE `learner_id`=? ORDER BY `agreement_signed_date` DESC',[$learnerId]);

		// Yes there are placements.
		if($placements!==FALSE)
		{
			return true;
		}

		// No there are no placements.
		else
		{
			return false;
		}
	}
}


