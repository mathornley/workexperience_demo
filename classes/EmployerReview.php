<?php

/**
 * Autoloader for including all the classes.
 */
spl_autoload_register(function ($class_name) {
    include $class_name . '.php';
});

class EmployerReview extends Main
{
	/**
	 * Class is instantiated with the placement_id initially.
	 * Also used when there is no placement_id so set that to NULL if isn't included.
	 * @param [string] $placement_id
	 */
	public function __construct($placement_id=null)
	{	
		try
		{
			// Call the parent first and instantiate the database SQL class instance.
			parent::__construct();
		}

		// Catch some PDO errors.
		catch (PDOException $e)
		{
			throw new Exception('PDO Database connection failed to connect to SID failed in line: ' .__LINE__ . ' of file: '. __FILE__ . $e->getMessage());
		}

		// This is the placement_id (The place, not the instance.)
		$this->placement_id = $placement_id; 
	}

	/**
	 * The instances of the placements are called here using the placement_id is used (The place, not the instance).
	 * This is the section were the learners are presented to the employer.
	 * The employer clicks them and is presented with a form for him to review that person. 
	 * @return [type] [description]
	 */
	public function getReviewList()
	{
		// Get all the placement instances associated with this placement id passed via get from the login page. 
		return $instances = $this->conn->select('SELECT * FROM `we_placement_attended` WHERE `placement_id`=? ORDER BY `date_created` DESC',[$this->placement_id]);
	}

	// ****************************************** This is the list.php loop content ***********************************

	// NOT placement attended id, juust the placement id - where, address etc.
	public function getPlacement($placement_id)
	{
		// Get all the placement instances associated with this placement id passed via get from the login page. 
		$result = $this->conn->select('SELECT `name` AS `placement_name` FROM `we_placement` WHERE `id`=?',[$placement_id]);
		return $result[0]['placement_name'];
	}

	public function getLearner($person_code)
	{
		// Get all the placement instances associated with this placement id passed via get from the login page. 
		$result = $this->conn->select('SELECT CONCAT(`forename`," ",`surname`) as `name` FROM `ext_a_learner` WHERE `person_code`=?',[$person_code]);
		return $result[0]['name'];
	}

	public function getCoach($coach_id)
	{
		//echo 'coach_id'.$coach_id;die;
		// Get all the placement instances associated with this placement id passed via get from the login page. 
		$result = $this->conn->select('SELECT `name` AS `coach_name` FROM `we_coach` WHERE `id`=?',[$coach_id]);
		return $result[0]['coach_name'];
	}

	public function agreementSigned($id)
	{
		// Get all the placement instances associated with this placement id passed via get from the login page. 
		return $result = $this->conn->select('SELECT if(agreement_signed=1,true, false) FROM `we_placement_attended` WHERE id=?',[$placement_attended_id]);
	}

	/**
	 * Get the student details, the placement details and the date and such from the placement_attended_id.
	 * This will be used in the Jumbo display on 'admin.php' when the employer reviews the learner.
	 * @param  [string] $placement_attended_id passed in from $_GET
	 * @return [array] Create an array of the learner and placement details. 
	 */
	public function getStudentDetailsForJumbo($placement_attended_id)
	{
	
		// Get the placement instance associated with this placement attended id passed in. 
		$placement_attended_info = $this->conn->select('SELECT * FROM `we_placement_attended` WHERE `id`=?',[$placement_attended_id]);

		// Get all the student details from the person id above from the learner table. 
		$name = $this->conn->select('SELECT CONCAT(`forename`," ",`surname`) AS `name` FROM `ext_a_learner` WHERE `person_code`=?',[$placement_attended_info[0]['learner_id']]);

		// Get all the student details from the person id  above. 
		$placement = $this->conn->select('SELECT * FROM `we_placement` WHERE `id`=?',[$placement_attended_info[0]['placement_id']]);

		// Get the coach details from the coach id of the placement_attended
		$coach =  $this->conn->select('SELECT `name` AS `coach` FROM `we_coach` WHERE `id`=?',[$placement_attended_info[0]['coach_id']]);

		// Create a new array for the jumbo area of the page, so we can fill in the details on the 'admin.php' page.
		$array['student_name'] = $name[0]['name'];
		$array['placement'] = $placement[0]['name'];
		$array['placement_address'] = $placement[0]['address'];
		$array['post_code'] = $placement[0]['post_code'];
		$array['agreement_signed_date'] = $placement_attended_info[0]['agreement_signed_date'];
		$array['coach_name'] = $coach[0]['coach'];
		$array['placement_attended_id'] = $placement_attended_id;
		$array['placement_id'] = $placement_attended_info[0]['placement_id'];
		
		return $array;
	}

	// Get the learner review for that instance of the placement_attended_id passed in.
	public function getLearnerReview($placement_attended_id)
	{
		$result = $this->conn->select('SELECT * FROM `we_employer_review` WHERE placement_attended_id=?',[$placement_attended_id]);

		if($result!=false) return $result;
		else return false;
	}

	/**
	 * Get the logbook entries for the learners instance from the logbook using the placement_attended_id.
	 * @param  [string] $placement_attended_id [description]
	 * @return [array][Array of logbook entries from the learners instance of the logbook]
	 */
	function getLogbookForEmployerReview($placement_attended_id)
	{
		// Use the placement_attended_id and search the we_logbook table. (placement_attended_id==placement_id in logbook)
		return $logBook = $this->conn->select('SELECT * FROM `we_logbook` WHERE placement_id=? ORDER BY `date` DESC',[$placement_attended_id]);
	}



	function getTotalDays($placement_attended_id)
	{
		// Use the placement_attended_id and search the we_logbook table. (placement_attended_id==placement_id in logbook)
		return $logBook = $this->conn->select('SELECT COUNT(*) AS `total_days` FROM `we_logbook` WHERE placement_id=?',[$placement_attended_id]);
	}

	/**
	 * This function enables the employer to record an absence, per day, for the student.
	 * $placement is the name of the employer.
	 * Script only allows one absence per day to be recorded.
	 * @param  [string] $placement_attended_id This is the placement_attended_id for the instance.
	 * @return array or boolean array of result set if true, otherwise false.
	 */
	public function recordAbsence($placement_attended_id)
	{
		// Unpack some vars.
		$date_created = date('Y-m-d H:i:s');//date('Y-m-d', strtotime('now'));
		
		// Build the query
		$query = 'INSERT INTO `we_absence` (`placement_attended_id`,`date_created`,`user`) VALUES(?,?,?)';

		// Add the entry to the `we_student_withdrawals` table for the withdrawal.
		$result = $this->conn->insert($query,[

			$placement_attended_id,
			$date_created,
			$this->stuID
		]);

		// Update the table to show the learner has been withdrawn.
		if($result!=false)
		{
			return array('message'=>': The learner absence was recorded.');
		}
		// The update failed.
		else
		{
			return false;
		}
	}

	public function getThePlacementID($placement_attended_id)
	{
		// Select the absences for today, if there are any.
		$query = 'SELECT `placement_id` FROM `we_placement_attended` WHERE `id`=?';
		$result = $this->conn->select($query,[$placement_attended_id]);
		return $result[0]['placement_id'];
	}

	public function hasSickbeenRecorded($placement_attended_id)
	{
		// Select the absences for today, if there are any.
		$query = 'SELECT `date_created` FROM `we_absence` WHERE `placement_attended_id`=? AND `date_created` LIKE ?';

		// Add the entry to the `we_student_withdrawals` table for the withdrawal.
		$result = $this->conn->select($query,[

			$placement_attended_id,
			date('Y-m-d',strtotime('today')).'%'
		]);

		// Return the absence which is already there for today.
		if($result!=false)
		{
			return $result;
		}
		// There is no entry for today.
		else
		{
			return false;
		}
	}
}


