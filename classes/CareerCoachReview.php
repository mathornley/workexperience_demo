<?php

/**
 * Autoloader for including all the classes.
 */
spl_autoload_register(function ($class_name) {
    include $class_name . '.php';
});


/**
 * A class to manage all aspects of the CareerCoachReview review. 
 * Extends the parent class, Main.
 * This class will also handle the career coach comments on the aims, logbook and the workplace review page. 
 */
class CareerCoachReview extends Main
{
	public function __construct()
	{	
		try
		{
			// Call the parent first and instantiate the database SQL class instance.
			parent::__construct();

			// Instantiate a new instance of learner.
			$this->learner = new Learner;

			// Get a new instance of the placement_attended_id class.
			$this->placement = new PlacementAttended;

			// Get a new instance of the placement class.
			$this->placementDetails = new Placement;

			// We'll use the Shibboleth id and stick this in coachID as not to confused. Just for this class! 
			$this->coachID = $this->stuID;

			// Instantiate an instance of the logbook
			$this->logbook = new Logbook;

			// Instantiate an instance of the email class
			$this->emailNotifications = new Email;
		}

		// Catch some PDO errors.
		catch (PDOException $e)
		{
			throw new Exception('PDO Database connection failed to connect to SID failed in line: ' .__LINE__ . ' of file: '. __FILE__ . $e->getMessage());
		}
	}

	/**
	 * We're using the career coach name from shibboleth - this is usually the learner_id.
	 * Check if this is a career coach accessing the career coach page.
	 * Use the $_SERVER['sAMAccountName'] variable from AD and Shibboleth and check the coach table for a match.
	 * If query matches, this is a coach and all ok, otherwise, direct to the homepage.
	 * All career coaches can see one anothers learners, in case one coach is needed to replace another.
	 * @return boolean [true or false whether this is a career coach]
	 */
	public function isCareerCoach()
	{
		// the student id is now the coach id from Shibboleth.
		$username = strtolower($this->coachID);
		
		// Create the query to query the coqach table to get the coach details. 
		$query = "SELECT * from `we_coach` WHERE username=?";

		// Get the coach info if there is any, if not, this is not a coach.
		$coach =  $this->conn->select($query,[$username]);
		
		return $coach;
	}

	/**
	 * Get all the coaches from the we_coach table.
	 * Make sure the coaches are not the admins. (Admin==0) will accomplish this.  
	 * @return [array] [Returns the entire row for the coach]
	 */
	public function getAllCoach()
	{
		// Create the query to query the coach table to get the coach details. 
		$query = "SELECT * from `we_coach` WHERE `admin`=? ORDER BY `name` ASC";

		// Get the coach info if there is any, if not, this is not a coach.
		return $this->conn->select($query,[0]);
	}

	/**
	 * Get the coach name by the coach id
	 * @param  [string] $coachID [This is the id of the coach]
	 * @return [string] [Returns the coach name as a string]
	 */
	public function getCoachName($coachID)
	{
		// Create the query to query the coqach table to get the coach details. 
		$query = "SELECT `name` from `we_coach` WHERE id=?";

		// Get the coach info if there is any, if not, this is not a coach.
		$coach =  $this->conn->select($query,[$coachID]); // $coach is false if fails. 

		// Get the coach name from the results and make this a string from array.
		$coachName = $coach[0]['name'];

		// return the coach name as a string.
		return $coachName;
	}


	/**
	 * Get the coach for the learner, using the placement_attended_id to get the coach id from `we_placement_attended`
	 * Then the coach id to get the coach name from the coach table `we_coach`. 
	 * @param  [string] $placement_attended_id Placement_attended_id.
	 * @return [type] [description]
	 */
	public function getCoachNameForLearner($placement_attended_id)
	{
		// First use the placement attended id and table, and get the coach id 
		$query = 'SELECT `coach_id` FROM `we_placement_attended` WHERE `id`=?';
		$coachID =  $this->conn->select($query,[$placement_attended_id]); 

		// With the coach id, query the coach table and pull back the coach name.
		$query = 'SELECT `name` FROM `we_coach` WHERE `id`=?';
		$coachDetails =  $this->conn->select($query,[$coachID[0]['coach_id']]);

		// There is a coach name returned from the query so return it as a string.
		if($coachDetails!==false)
		{
			return $coachDetails[0]['name']; // Return the coach details as a string
		}

		// Query has no results, so return false.
		else
		{
			return false;
		}
	}

	
	public function getCoachDetails($placement_attended_id)
	{
		// First use the placement attended id and table, and get the coach id 
		$query = 'SELECT `coach_id` FROM `we_placement_attended` WHERE `id`=?';
		$coach =  $this->conn->select($query,[$placement_attended_id]); 

		// With the coach id, query the coach table and pull back the coach name.
		$query = 'SELECT * FROM `we_coach` WHERE `id`=?';
		$coachInfo =  $this->conn->select($query,[$coach[0]['coach_id']]);

		// There is a coach name returned from the query so return it as a string.
		if($coachInfo!==false)
		{
			return $coachInfo; // Return the coach details as a string
		}

		// Query has no results, so return false.
		else
		{
			return 'There are no details';
		}
	}

	/**
	 * Get all the placements.
	 * @return [array] [Get all the placements as an array]
	 */
	public function getPlacements()
	{
		return $this->placement->getPlacements();
	}

	/**
	 * Get the learner placements for the coach CRM section coach/index.php page from the learner class learner id
	 * @param  [string] $learner [This is the learner number as a string]
	 * @return [array] [This is the array of the learner placements or FALSE on failure]
	 */
	public function getTheLearnerPlacements($learner)
	{
		// Head off to the learner class and fetch the learner placements. Functions returns: array or false
		return $this->learner->getTheLearnerPlacements($learner);
	}

	/**
	 * Get the placements for the coach using their coach id.
	 * @param  [string] $coachID [coachID passed in]
	 * @return [array][Returns an array with all the coach specific placements]
	 */
	public function getPlacementsByCoachID($coachID)
	{
		// Uses PlacementAttended->getPlacementsByCoachID($coachID);
		return $this->placement->getPlacementsByCoachID($coachID);
	}

	/**
	 * This confirms if the the 'I can confirm these hours are correct as per the students logbook entries.' box has been completed on the bottom of the coach review for the learner. 
	 * @param [string] $placement_attended_id [This is the placement attended id]
	 */
	public function CheckCoachReview($placement_attended_id)
	{
		// Query all the details about the career coach instance for the particular placement. 
		$query = 'SELECT * FROM `we_career_coach_review` WHERE `placement_attended_id`=?';
		$result =  $this->conn->select($query,[$placement_attended_id]);

		// Return the result set or false;
		return $result;
	}

	/**
	 * Function to add a review submitted by a coach and finally sign off the student from the work placement.
	 * Regardless of which coach completes the review, their id is regarded against the addition.
	 * Uses the placement_attended_id to differentiate what review to add.
	 * @param [array] $POST [Posted variables from the career coach review]
	 * @return [boolean] [true or false on outcome]
	 */
	public function AddCoachReview($POST)
	{
		// Unpack the variables from post
		$placement_attended_id = $POST['save_employer_review']; // this is the placement_attended_id
		$coach_id = $POST['coach_id'];
		$comments = $POST['review']["'comments'"];
		$hours_confirmed = (isset($POST['hours_confirmed'])) ? $POST['hours_confirmed'] : 0;
		$status = $POST['status']; // This is the status of review, pass, fail... blah..blah
		unset($POST); // Unset POST

		// Add the row to show the coach review - the coach of whoever does the review will be added. 
		$query = 'INSERT INTO `we_career_coach_review` (`placement_attended_id`,`coach_id`,`comments`,`hours_confirmed`,`status`) VALUES(?,?,?,?,?)';
		$insert =  $this->conn->insert($query,
			[
				$placement_attended_id,
				$coach_id,
				$comments,
				$hours_confirmed,
				$status
			]
		);

		// If all goes well, send some emails out. 
		if($insert!=false)
		{
			switch(strtolower($status))
			{
				case 'passed':
					$this->emailNotifications->emailReviewNotification($placement_attended_id,'passed');
					break;
				case 'completed with learner issues':
					$this->emailNotifications->emailReviewNotification($placement_attended_id,'completed with learner issues');
					break;
				case 'completed with employer issues':
					$this->emailNotifications->emailReviewNotification($placement_attended_id,'completed with employer issues');
					break;
				case 'completed with both employer and learner issues':
					$this->emailNotifications->emailReviewNotification($placement_attended_id,'completed with both employer and learner issues');
					break;
				case 'failed with learner issues':
					$this->emailNotifications->emailReviewNotification($placement_attended_id,'failed with learner issues');
					break;
				case 'failed with employer issues':
					$this->emailNotifications->emailReviewNotification($placement_attended_id,'failed with employer issues');
				case 'failed with both employer and learner issues':
					$this->emailNotifications->emailReviewNotification($placement_attended_id,'failed with both employer and learner issues');
				break;
			}

			// Finally after all the notifications, return true.
			return true;
		}

		// Insert didn't work.
		else return $insert; // Bool FALSE;

	}

	public function isLearnerSignedOff($placement_attended_id)
	{
		// Add the row to show the coach review - the coach of whoever does the review will be added. 
		$query = 'SELECT `hours_confirmed` FROM `we_career_coach_review` WHERE `placement_attended_id`=?';
		$result =  $this->conn->select($query,[$placement_attended_id]);

		//echo '<pre>';print_r($result);die;

		// If all goes well, return true.
		if($result[0]['hours_confirmed']=='1') return true;
		else return $result; // Bool FALSE;
	}

	/**
	 * A function to work out if the learner has passed their placement as per the 4 pass options on the career coach review page - the coach completes this section. 
	 * @param  [string] $placement_attended_id [This is the placement attended id of the placement instance]
	 * @return boolean [True or false depending on if they have passed or not]
	 */
	public function isLearnerSignedOffPlacement($placement_attended_id)
	{

		// Get the status of the placement if there is one.  
		$query = 'SELECT `status` FROM `we_career_coach_review` WHERE `placement_attended_id`=?';
		$result =  $this->conn->select($query,[$placement_attended_id]);

		// One of the 4 options to pass as below. 
		if($result[0]['status']=='Passed' || $result[0]['status']=='Completed with learner issues' || 
			$result[0]['status']=='Completed with employer issues' || $result[0]['status']=='Completed with both employer and learner issues'){return true;}
		
		// They've not completed the placement or they have failed. 
		else return false; // Bool FALSE;
	}

	/**
	 * Function to update a review already submitted by a coach.
	 * Regardless of which coach completes the review, their id is regarded against the update.
	 * @param  [array] $POST [Posted variables from the career coach review to update]
	 * @return [boolean] [true or false on outcome]
	 */
	public function editCoachReview($POST)
	{
		// Unpack the variables from post
		$placement_attended_id = $POST['update_employer_review']; // this is the placement_attended_id
		$row_id = $POST['row_id'];
		//$coach_id = $POST['coach_id'];
		$comments = $POST['review']["'comments'"];
		$date_updated = date('Y-m-d H:i:s'); 
		$hours_confirmed = (isset($POST['hours_confirmed'])) ? $POST['hours_confirmed'] : 0;
		$status = $POST["status"];
		unset($POST); // Unset POST

		// Get the learner id from the placement attended instance, using the placement attended id. 
		$learnerID = $this->conn->select('SELECT `learner_id` `learner` FROM `we_placement_attended` WHERE `id`=?',[$placement_attended_id])[0]['learner'];

		/**
		 * This is the work ready for the learner being reset to 'not work ready' in the `elp_workready2` table.
		 * @var [type]
		 */
		
		// Turn it off, set them to 'not work ready'.
		if($status == 'Failed with learner issues' || $status == 'Failed with both employer and learner issues')
		{
			/* WR set to red*/
			$this->learner->setLearnerNotWorkReady($learnerID);
		}

		// Turn it on - set them 'to work ready'.
		if(
			$status == 'Passed' || $status == 'Completed with learner issues' ||
			$status == 'Completed with employer issues' || $status == 'Completed with both employer and learner issues' ||
			$status == 'Failed with employer issues'
		)
		{
			/* WR set to green*/
			$this->learner->setLearnerToBeWorkReady($learnerID);
		}

		// Add the row to show the coach review - the coach of whoever does the review will be added. 
		$query = 'UPDATE `we_career_coach_review` SET placement_attended_id=?,comments=?,date_updated=?, `hours_confirmed`=?, `status`=? WHERE id=?';
		
		// This will returned the row affected by the update or false if it fails. 
		$update = $this->conn->update($query,[$placement_attended_id,$comments,$date_updated,$hours_confirmed,$status,$row_id]);

		// Send the update email out to the LDMs, the CLs and the coach that edited the learner coach review
		$this->emailNotifications->sendUpdatedCareerCoachReviewEmail($placement_attended_id,$learnerID,$status);

		return $update; // This will returned the row affected by the update or false if it fails. 

	}


	public function getTheCoachIdForLearner($placement_attended_id)
	{
		// Add the row to show the coach review - the coach id of whoever does the review, will be added. 
		$query = 'SELECT * FROM `we_career_coach_review` WHERE `placement_attended_id`=?';
		// Return the result.
		$result =  $this->conn->select($query,[$placement_attended_id]);

		// Return the result set or false
		return $result;
	}

	/**
	 * Get the amount of days the learner has done from the logbook.
	 * Use the placement_attended_id and get the placement id. 
	 * Use the placement id andf search the logbook table for the sum of the hours.
	 * @return [boolean][True or false depending on if there has been 5 days completed or not]
	 */
	public function countLearnerDays($placement_attended_id)
	{
		// Add the row to show the coach review - the coach id of whoever does the review, will be added. 
		$query = 'SELECT COUNT(*) as `days` FROM `we_logbook` WHERE `placement_id`=?';
		$result =  $this->conn->select($query,[$placement_attended_id]); // learner[0]['id'] is the 

		// Return the result set of true if 5 days completed. Else, false.
		if($result[0]['days'] >=5) return true;
		else return false;
	}

	/**
	 * This is used on the page establishing if this is a learner or a coach. 
	 * Get the coach details with the username of the coach bing held in Shibboleth and then passed to $this->coachID
	 * If this is a coach, the coach details are returned as an array.
	 * @return [array] [The row of the coach details, returned as an array]
	 */
	public function coachDetails()
	{
		// Create the query to query the coach table to get the coach details. 
		$query = "SELECT * from `we_coach` WHERE `username`=?";

		// Get the coach info if there is any, if not, this is not a coach.
		return $this->conn->select($query,[$this->coachID]);
	}

	public function getCoachComments($placement_attended_id)
	{
		// Get the coach review comments for the aims, logbook and the work placement page comments by the coach at the bottom
		$query = "SELECT * from `we_coach_comments` WHERE `placement_attended_id`=? ORDER BY `date_created` DESC";

		// Return the results array.
		return $this->conn->select($query,[$placement_attended_id]);
		
	}

	/**
	 * Pull all the comments for the coach for the aims.php page
	 * @param  String $placement_attended_id [The placement attended id passed in]
	 * @return Array The results array returned
	 */
	public function getCoachCommentsAims($placement_attended_id)
	{
		// Get the coach review comments for the aims, logbook and the work placement page comments by the coach at the bottom
		$query = "SELECT * from `we_coach_comments` WHERE `placement_attended_id`=? AND `comments_aims` IS NOT NULL ORDER BY `date_created` DESC";

		// Return the results array.
		return $this->conn->select($query,[$placement_attended_id]);
		
	}

	/**
	 * Pull all the comments for the coach for the logbook.php page
	 * @param  String $placement_attended_id [The placement attended id passed in]
	 * @return Array The results array returned
	 */
	public function getCoachCommentsLogbook($placement_attended_id)
	{
		// Get the coach review comments for the aims, logbook and the work placement page comments by the coach at the bottom
		$query = "SELECT * from `we_coach_comments` WHERE `placement_attended_id`=? AND `logbook_comments` IS NOT NULL ORDER BY `date_created` DESC";

		// Return the results array.
		return $this->conn->select($query,[$placement_attended_id]);
		
	}

	/**
	 * Pull all the coach comments for the work placement review page: placement_review.php page
	 * @param  String $placement_attended_id [The placement attended id passed in]
	 * @return Array The results array returned
	 */
	public function getCoachCommentsPlacementReview($placement_attended_id)
	{
		// Get the coach review comments for the aims, logbook and the work placement page comments by the coach at the bottom
		$query = "SELECT * from `we_coach_comments` WHERE `placement_attended_id`=? AND `workplacement_comments` IS NOT NULL ORDER BY `date_created` DESC";

		// Return the results array.
		return $this->conn->select($query,[$placement_attended_id]);
		
	}

	/**
	 * Get the coach ID from the coach username passed in.
	 * @param  string Coach username
	 * @return The ID of the coach
	 */
	public function getCoachDetailsFromUsername($username)
	{
		// Get the coach review comments for the aims, logbook and the work placement page comments by the coach at the bottom
		$query = "SELECT `id` from `we_coach` WHERE `username`=?";

		// Results array.
		$result = $this->conn->select($query,[$username]);

		// Get and return the id of the coach from the username.
		return $id = $result[0]['id'];
	}

	/**
	 * The midpoint revirw check to see if the learner, the coach and the emoployer have signed. 
	 * All 3 need to have signed their respective reviews to get a True returned, otherwise false. 
	 * @param  [string] $placement_attended_id [This is the placement attended id of the placement]
	 * @return [boolean] [This is true or false]
	 */
	public function midpointReviewCompleted($placement_attended_id)
	{
		// Query, get the row of the review, if there is one.
		$query = "SELECT * from `we_midpoint_assessment` WHERE `placement_attended_id`=?";

		// Query it...
		$review = $this->conn->select($query,[$placement_attended_id]);

		// If there's a row and all 3 have signed, the learner, the coach and the employer, return true.
		if($review[0]['learner_signed']=='1' && $review[0]['employer_signed']=='1' && $review[0]['coach_signed']=='1' )
		{
			return true; // All have signed. 
		}

		else
		{
			return false; // One or several have not signed, return false. 
		}
	}

	/**
	 * Test if this is a staff member according to Shibboleth.
	 * @return boolean true for staff member; false for anything else (and not permnitted access in the template).
	 */
	public function isStaffMember()
	{
		// Staff member so true.
		if(is_numeric($this->learner->stuID)===false)
		{
			return true;
		}

		// No access so false. 
		else
		{
			return false;
		}
	}

	/**
	 * Array of data is passed in from the template.
	 * A function to alphabetcially, by 'name', sort an array of learner placements for the coach review section.
	 * Function also reformats the array better so this is easier to use when iterating over in the template.
	 * @param  [array] $array [the array of coach placements]
	 * @return [array] [the sorted array of coach placements. ]
	 */
	public function alphaOrderArray($array)
	{
		// the placeholder for the new sorted array here. 
		$sortedArray = array();

		// Iterate the array and populate the new one with elements making more sense. 
		for($i=0;isset($array[$i]);$i++)
		{
			// Grab the placement attended id. 
			$placement_attended_id = $array[$i]['id'];
			// Get the learners name from the learner id. 
			$sortedArray[$i]['name'] = $this->learner->learnerDetailsByID($array[$i]['learner_id']);
			$sortedArray[$i]['paid'] = $array[$i]['id'];
			$sortedArray[$i]['learner_id'] = $array[$i]['learner_id'];
			$sortedArray[$i]['coach_id'] = $array[$i]['coach_id'];
			$sortedArray[$i]['placement_id'] = $array[$i]['placement_id'];
			$sortedArray[$i]['agreement_signed_date'] = $array[$i]['agreement_signed_date'];
			$sortedArray[$i]['coach_signed_off'] = ($this->isLearnerSignedOff($placement_attended_id)!=false) ? '1' : '0';

			// Unset the placement attended id just to make sure!
			unset($placement_attended_id);
		}   

		// Alphabetically, sort the array by name, and return the sorted array. 
		asort($sortedArray);
		// Return the sorted array to the template. 
		return $sortedArray;
	               
	}

	public function __destruct()
	{
		// Close the database connection. 
		$this->conn = null;
	}


}


