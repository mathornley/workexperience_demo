<?php

/**
 * Autoloader for including all the classes.
 */
spl_autoload_register(function ($class_name) {
    include $class_name . '.php';
});

/**
 * A class to be associated with the learner details. 
 */
class Logbook extends Main
{
	public function __construct()
	{	
		try
		{
			// Call the parent first and instantiate the database SQL class instance.
			parent::__construct();
			// Instantiate a new instance of learner.
			$this->learner = new Learner;
			// Get an instance of coach
			//$this->coach = new CareerCoachReview;
		}

		// Catch some PDO errors.
		catch (PDOException $e)
		{
			throw new Exception('PDO Database connection failed to connect to SID failed in line: ' .__LINE__ . ' of file: '. __FILE__ . $e->getMessage());
		}
	}

	/**
	 * Get all the learner logbook entries with no full sick days. sick=0
	 * Each day is one logbook entry made by the learner. 
	 * Looks for all active records.
	 * Searches on the placemment_attended_id
	 * Sickness is a 1 in the sickness column. 1 is a full days sick. Zero is non sick that day.
	 * @return [array] Returns an array of all he entries in the logbook the learner has made.
	 */
	public function logBook($placement_attended_id)
	{
		// Get all the logbook entries for this learner that are not hidden (deleted is hidden=1)
		return $this->conn->select('SELECT * FROM  `we_logbook` WHERE `sickness`=? AND `active`=? AND `placement_id`=? ORDER BY `date` DESC',['0','1',$placement_attended_id]);

	}

	/**
	 * Get a logbook entry for the day - not a full day of sickness.
	 * @param  [INT] $id [The number of the row passed in]
	 * @return [array] [This is an array of the days log book queried.]
	 * We'll use this to show in the logbook template for both view and update so the form is populated. 
	 */
	public function logBookEntry($id)
	{
		// Get all the logbook entries for this id for the learner for the logbook template
		return $this->conn->select('SELECT * FROM  `we_logbook` WHERE `sickness`=? AND `active`=? AND `id`=?',['0','1',$id]);

	}

	/**
	 * Looks for all occasions of a full days sick, sick=1
	 * Looks for all active records.
	 * Searches on the placemment_attended_id
	 * @return [type] [description]
	 */
	public function logBookSickness($placement_attended_id)
	{
		
		// Get all the logbook entries for this learner that are not hidden (deleted is hidden=1)
		return $this->conn->select('SELECT * FROM  `we_logbook` WHERE `sickness`=? AND `left_early_sick`=? AND `active`=? AND `placement_id`=? ORDER BY `date` DESC',['1','0','1',$placement_attended_id]);

	}

	/**
	 * Used in the edit view of the logbook entries for sick. 
	 * Get the sick day row for the id that is passed in
	 * @param  [INT] $id [The id that is passed in to the form]
	 * @return [array] [Returns the row of the id]
	 */
	public function logBookSickDay($id)
	{
		// Get the sick day row for the id.
		return $this->conn->select('SELECT * FROM  `we_logbook` WHERE `sickness`=? AND `active`=? AND `id`=?',['1','1',$id]);

	}

	/**
	 * Update the full day sickness record from the edit view
	 * @param  [array] $POST [The array row sourced using the ID of the full day of sickness]
	 * @return [boolean] [returns true or false depending on success]
	 */
	public function updateLogBookSickDay($POST)
	{

		if(isset($POST['id'])) unset($POST['id']); // Unset the flag used in the form if set
		if(isset($POST['edit_full_day_sick'])) unset($POST['edit_full_day_sick']); // Unset the flag used in the form button if set

		// Check for a duplicate date in the table. 
		$duplicate = $this->checkDate($POST,'update');

		// A duplicate return false to the template.
		if($duplicate===false)
		{
			return true;
		}

		// This isn't a duplicate and is fine. 
		else
		{	
			$id = $this->cleanseVars($POST['row_id']);
			$date = $this->cleanseVars($POST['date']);
			$details_of_absence = $this->cleanseVars($POST['details_of_absence']);
			$date_updated = date('Y-m-d H:i:s');
			
			$result = $this->conn->update('UPDATE `we_logbook` SET `date`=?, `details_of_absence`=?, `date_updated`=? WHERE `id`=?',[$date,$details_of_absence,$date_updated,$id]);

			return false;
		}
	}

	/**
	 * Update a day of the logbook from the edit template.
	 * This will update a day of the log book, full days and if the learner went home earlier but did some hours. 
	 * @param  [array] $POST [The array variables passed in]
	 */
	public function updateLogBookEntry($POST)
	{
		// Check for a duplicate date in the table. 
		$duplicate = $this->checkDate($POST,'update');

		// A duplicate return false to the template.
		if($duplicate===false)
		{
			return true;
		}

		// This is not a duplicate so allow it.
		else
		{
			// If left_sick_early && sick_during aren't set, set them to zero as they've been unselected.
			if(!isset($POST['left_early_sick'])) $POST['left_early_sick']='0';
			if(!isset($POST['sick_during'])) $POST['sick_during']='0';

			// Make sure the row shows an date_updated entry.
			$POST['date_updated']=date('Y-m-d H:i:s');
			$id=$POST['placement_attended_id'];

			unset($POST['update_log_book_entry']); // Unset the flag used in the form button
			unset($POST['studID']); // Unset the 'studID' used in the form 
			$row_id = $POST['row_id']; // Capture the row id to use in the query.
			unset($POST['row_id']); // Unset the id used in the form button as we'll iterate the POST array to update.

			// If none of the other aims are selected on the update - set to NULL to change from last insert.
			if(!isset($POST['aim_1'])) $POST['aim_1']=NULL;
			if(!isset($POST['aim_2'])) $POST['aim_2']=NULL;
			if(!isset($POST['aim_3'])) $POST['aim_3']=NULL;
			if(!isset($POST['aim_4'])) $POST['aim_4']=NULL;

			// Posted through is 'placement_attended_id' and we need to change that to 'placement_id' for the foreach loop, otherwise, it'll break.
			if(isset($POST['placement_attended_id']))
			{
				// Get the placement_attended_id and stick that in $POST['placement_id']
				$POST['placement_id'] = $POST['placement_attended_id'];
				// Unset $POST['placement_attended_id']
				unset($POST['placement_attended_id']);
			}

			unset($POST['id']);

			// Set the array for the values. 
			$values = array();

			// Start building the query here.
			$query = "UPDATE `we_logbook` SET ";

			// Iterate the column names and add the values, so we can start the beginning of the query,
			foreach($POST AS $column=>$value)
			{
				// Get each column name
				$query.= "`{$column}`=?,";	
				$values[] = $value;
			}

			// Get rid of the extra comma at the end of the row.
			$query = rtrim($query,', ');

			// Set the id of the row
			$query.= " WHERE `id`=?";
			// Add the id to the end of the array.
			$values[] = $row_id;

			$this->conn->update($query,$values);

			// Format the start and finish times so we can use some MYSQL FUNCTIONS
			$start = strtotime($POST['start']);
			$finish = strtotime($POST['finish']);

			// Set the hours and minutes
			$hours = round(abs($finish - $start) / 3600,2);

			// Work out the hours the learner has done for that day and add them to the table also and format. Let MySQL work this out - this is just to get the hours calculated.
			$query = "SELECT ROUND((TIME_TO_SEC(?) - TIME_TO_SEC(?))/60/60) AS `hours` FROM `we_logbook` WHERE id=?";

			// Get the learner hours that entry - one just added.
			$learnerHours =  $this->conn->select($query,[$finish, $start, $row_id]);

			// Now, update the row to show the learner hours. 
			$query = "UPDATE `we_logbook` SET `hours`=? WHERE id=?";
			$update =  $this->conn->update($query,[$hours, $row_id]);

			return false;
		}
	}



	/**
	 * The user has submitted the full day sick form. 
	 * @param  [array] $POST The parameters that have been posted from the form. 
	 * @return [type]       [description]
	 */
	public function sickness($POST)
	{
		// Check for a duplicate date in the table. 
		$duplicate = $this->checkDate($POST,'insert');

		// A duplicate returns false.
		if($duplicate===false)
		{
			return true;
		}

		else
		{
			//print_r($POST);die;

			// First job get rid of a flag from the form we don't want in the POST array
			unset($POST['full_day_sick']);

			// Get the placement attended ID for this learner instance.
			$placement_attended_id = $POST['placement_attended_id'];

			// Start building the query - set the placement_attended_id firtsly.
			$query = 'INSERT INTO `we_logbook` (placement_id,sickness,';

			// As we're usimg the foreach we don't want stuff getting sucked in to the foreach loop from POST.
			unset($POST['id']); // Unset some stuff we don't need now 
			unset($POST['studID']);  // Unset some stuff we don't need now 
			unset($POST['placement_attended_id']); // Unset some stuff we don't need now 

			// Iterate the column names and add, so we can start the beginning of the query,
			foreach($POST AS $column=>$value)
			{
				// Get each column name
				$query.= $column.',';	
			}

			// Get rid of the extra comma at the end of the row.
			$query = rtrim($query,', ');

			// Now get the number of values (domn't forget these are just '?')
			$query.=') VALUES(';

			// Placeholder for the values parameters
			$args = array();

			// Make the first parameter the placement_attended_id
			$args[] = $placement_attended_id;
			$args[] = 1; // The flag for a full days sickness

			// Add this so we are adding the placement_attended_id and the sickness flag of 1 (as per the columns set above)
			$query.= "?,?,";	

			foreach($POST AS $column=>$value)
			{	
				$query.= "?,";	// Get all the placeholders, the foreach tells how many.
				$args[] = $value; // Populate the args
			}

			// Get rid of the extra comma at the end of the row.
			$query = rtrim($query,',');

			// Finally close the query. 
			$query.=");";

			// Send the query and params off to be executed by the SQL class.
			$this->conn->insert($query,$args);

			return false;
		}
	}

	/**
	 * A new logbook entry has been made - this can include a partial sick day (Morning/afternoon).
	 * If sickness occurs, the `left_early_sick` flag is set to 1
	 * @param [type] $POST [The posted details fron the daily log book entry]
	 */
	public function addLogbook($POST)
	{
		//echo '<pre>';
		//print_r($POST);die;
		
		// Drop the form flag passed in from the template in POST.
		unset($POST['add_logbook']); 
		unset($POST['studID']); 

		// Check for a duplicate date in the table. 
		$duplicate = $this->checkDate($POST,'insert');

		// A duplicate return false.
		if($duplicate===false)
		{
			return true;
		}

		else
		{

			unset($POST['id']); 

			// If they've done some hours but went home sick during the morning or afternoon, set sick=1
			if(isset($POST['left_early_sick']) && $POST['left_early_sick']=='1' && $_POST['sick_during'])
			{
				$POST['sickness']='0'; // Set this as post so is picked up by the loop through later
			}

			// Get the placement attended ID for this learner instance.
			$placement_attended_id = $POST['placement_attended_id'];
			unset($POST['placement_attended_id']);

			// Start building the query - set the placement_attended_id firstly.
			$query = "INSERT INTO `we_logbook` (placement_id,";

			// Iterate the column names and add, so we can start the beginning of the query,
			foreach($POST AS $column=>$value)
			{
				// Get each column name
				$query.= $column.',';	
			}


			// Get rid of the extra comma at the end of the row.
			$query = rtrim($query,', ');

			// Now get the number of values (don't forget these are just '?')
			$query.=') VALUES(';

			// Placeholder for the values parameters
			$args = array();

			$args[] = $placement_attended_id;

			// Add this so we are adding the placement_attended_id and the sickness flag of 1 (as per the columns set above)
			if(isset($POST['left_early_sick']) && $POST['left_early_sick']=='1' && $_POST['sick_during'])
			{
				$query.= "?,";
			}	
			else
			{
				$query.= "?,";
			}	

			foreach($POST AS $column=>$value)
			{	
				$query.= "?,";	// Get all the placeholders, the foreach tells how many.
				$args[] = $value; // Populate the args
			}

			// Get rid of the trailing comma at the end of the row.
			$query = rtrim($query,',');

			// Finally close the query. 
			$query.=");";

			// Send the query and params off to be executed by the SQL class and add the logbook entry to the table.
			// Get the last insert ID for the record we just added so we can update the hours
			$lastInsertId = $this->conn->insert($query,$args);

			// Format the start and finish times so we can use some MYSQL FUNCTIONS
			$start = strtotime($POST['start']);
			$finish = strtotime($POST['finish']);

			// Set the hours and minutes
			$hours = round(abs($finish - $start) / 3600,2);

			// Work out the hours the learner has done for that day and add them to the table also and format.
			$query = "SELECT ROUND((TIME_TO_SEC(?) - TIME_TO_SEC(?))/60/60) AS `hours` FROM `we_logbook` WHERE id=?";

			// Get the learner hours that entry
			$learnerHours =  $this->conn->select($query,[$finish, $start, $lastInsertId]);

			// Update the row to show the learner hours. 
			$query = "UPDATE `we_logbook` SET `hours`=? WHERE id=?";
			$update =  $this->conn->update($query,[$hours, $lastInsertId]);

			return false;
		}
	}

	/**
	 * Check for a duplicate prior to an insertion or update to a row from the logbook.
	 * Check for a duplicate record when the record is attempted to be updated. 
	 * The row is pulled to see if the record exists - an update would have one entry for a date, everything else doesn't.
	 * @param  [array] $POST [The parameters that are passed in for the record]
	 * @param  [string] $mode [a flag to establish what the operation is: there's an insert or update]
	 * @return [boolean] [Returned is either true or false]
	 */
	public function checkDate($POST,$mode=NULL)
	{

		/**
		 * If this is an insert, the id will be the placement_attended_id to check across the entire placement.
		 * If this is an update, the id will be the id of the row to be updated.
		 * @var [type]
		 */
		
		// Update, so the id is the row id to update.
		if(isset($POST['placement_attended_id']) && $POST['placement_attended_id'] && $mode=='update')
		{
			$id = $POST['placement_attended_id'];
			$row_id = $POST['row_id'];
		}
		// This is an insert, so the id relates to the placement attended id
		else
		{
			$id = $POST['placement_attended_id'];
		}

		$date = isset($POST['date']) ? $POST['date'] : '';

		$row = $this->conn->select('SELECT * FROM `we_logbook` WHERE `date`=? AND `placement_id`=? AND `active`=?  ORDER BY `date` DESC',[$date,$id,1]);

		$rowCount = $this->conn->select('SELECT COUNT(*) AS `duplicates` FROM  `we_logbook` WHERE `date`=? AND `placement_id`=? AND `active`=?  ORDER BY `date` DESC',[$date,$id,1]);

		$duplicates = $rowCount[0]['duplicates'];


		// Check before update to see if that date is taken or the one you are updating. 
		if($mode=='update')
		{
			// Same row, so update it with.
			if($duplicates>0 && $mode=='update' && $row_id==$row[0]['id'])
			{
				//echo 'OK';die;
				return true;
			}
			// This is a new row as rows don't exist.
			if($duplicates==0 && $mode=='update')
			{
				//echo 'NO';die;
				return true;
			}
			if($duplicates>0 && $mode=='update' && $row_id!=$row[0]['id'])
			{
				//echo 'NO';die;
				return false;
			}
		}

		// Check before inserting a row.
		if($mode=='insert')
		{
			// Row already with that date.
			if($duplicates>0 && $mode=='insert')
			{
				return false;
			}
			// This is a new row as rows don't exist.
			if($duplicates==0 && $mode=='insert')
			{
				return true;
			}
		}

	}

	/**
	 * Get the total hours and days that the learner has completed so far for that instance.
	 * The days will include all full days of sick unless the query is adjusted. 
	 * @return [array] [An arry of the hours and days the learner has completed].
	 */
	public function logbookTime($studID=null,$placement_attended_id)
	{
		// Get the total hours and days so far for the placement instance.
		$query = "
		SELECT SUM(hours) as `hours`, 
			COUNT(*) as `days` 
			FROM `we_logbook` WHERE `placement_id`=?
		";

		// Get the learner hours and days that entry and return it.
		return $learnerHours =  $this->conn->select($query,[$placement_attended_id]);
	}

	/**
	 * Here is where the coach will add a comment to the logbook section. 
	 * @param array $POST The posted vars from the add coach comment section. 
	 */
	public function addCoachComments($POST)
	{
		$placement_attended_id = $POST['placement_attended_id'];
		$comments_logbook = $POST['comments_logbook'];
		$date_created = date('Y-m-d H:i:s');

		// using the coachID passed in above, get that coach ID.
		$coachID = $this->coach->getCoachDetailsFromUsername($this->stuID);
		
		// Add the comments from the coach to the placement_attended_id instance from this coach.
		$query = 'INSERT INTO `we_coach_comments` (placement_attended_id,coach_id,logbook_comments,date_created) VALUES(?,?,?,?)';

		return $this->conn->insert($query,[$placement_attended_id,$coachID,$comments_logbook,$date_created]);
	}

	public function editCoachComments($POST)
	{
		
		$placement_attended_id = $_POST['placement_attended_id'];
		$updated_comments = $_POST['coach_comments'];
		$row_id = $_POST['row_id'];

		// Add the comments from the coach to the placement_attended_id instance from this coach.
		$query = 'UPDATE `we_coach_comments` SET `comments_aims`=? WHERE `id`=? AND `placement_attended_id`=?';

		return $this->conn->update($query,[$updated_comments,$row_id,$placement_attended_id]);
	}

	public function __destruct()
	{
		$this->conn = null;
	}


	

}
