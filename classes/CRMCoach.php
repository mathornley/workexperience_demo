<?php

/**
 * Autoloader for including all the classes.
 */
spl_autoload_register(function ($class_name) {
    include $class_name . '.php';
});


/**
 * A class to manage all aspects of the CareerCoachReview review. 
 * Extends the parent class, Main.
 * This class will also handle the career coach comments on the aims, logbook and the workplace review page. 
 */
class CRMCoach extends Main
{
	public function __construct()
	{	
		try
		{
			// Call the parent first and instantiate the database SQL class instance.
			parent::__construct();
		}

		// Catch some PDO errors.
		catch (PDOException $e)
		{
			throw new Exception('PDO Database connection failed to connect to SID failed in line: ' .__LINE__ . ' of file: '. __FILE__ . $e->getMessage());
		}
	}

	
	/**
	 * Get all the coaches from the we_coach table.
	 * @return [array] [Returns the entire row for the coaches on e by one]
	 */
	public function getAllCoach()
	{
		// Create the query to query the coqach table to get the coach details. Latest first.
		$query = "SELECT * from `we_coach` ORDER BY `date_created` DESC";

		// Get the coach info if there is any, if not, this is not a coach.
		return $this->conn->select($query);
	}

	public function getCoachByID($id)
	{
		// Create the query to query the coqach table to get the coach details. Latest first.
		$query = "SELECT * from `we_coach` WHERE `id`=?";

		// Get the coach info if there is any, if not, this is not a coach.
		return $this->conn->select($query,[$id]);
	}

	/**
	 * Add a coach to the CRM part of the Work exp app on page: /workexperience/CRM/coach.php 
	 * @param  [array] $POST [This is the posted data from the form.]
	 * @return [array] [Only returns an array if there is an error]
	 */
	public function create_wp_coach($POST)
	{
		// Drop this flag from the HTML form, we don't need this now.
		unset($POST['create_wp_coach']);
		
		// Sort the vars, remove the spaces etc.
		$this->name = ucfirst($POST['first_name']).' '.ucfirst($POST['last_name']);
		$this->phone = str_replace(' ', '', $POST['phone']);
		$this->mobile = str_replace(' ', '', $POST['mobile']);

		$this->email = $POST['email'];
		$this->username = $POST['username'];
		$this->admin = (isset($POST['admin'])) ? 1 : 0;
	
		/**
		 * Does the form have any issues with the input it's received.
		 * @var [strings] Mobile, phone and email address.
		 * If no issues, an empty array is returned - otherwise, array has errors and is returned to the form to alert the user.
		 */
		$errors = $this->validateFormInput($this->phone,$this->mobile, $this->email);

		/**
		 * An empty array has been returned so no errors. 
		 * Next check will be if this is already in the table as a coach. If already there, an error is returned.
		 */
		if(empty($errors))
		{
			// Check if the user already exists in the table of coaches so we don't get a duplicate. 
			$duplicate = $this->checkForDuplicateCoach($this->name,$this->phone,$this->mobile,$this->email ,$this->username = $POST['username']);

			// This is a duplicate so tell the user
			if($duplicate===true)
			{
				// Populate the error array on the form.
				return array('duplicate'=>'This is a duplictate and there is a <strong>user with the same details</strong>.');
			}

			// Not a duplicate, so add them to the coach table `we_coach`.
			else
			{
				if($this->addCoachToTable()!==false)
				{
					return array('success'=>'Coach successfully added.');
				}
			}
				
		}

		/**
		 * Error array has content so return this to the Add a coach form to let the user know.
		 * The user will see the output as errors in the form.
		 */
		else
		{
			return $errors;
		}
	}

	/**
	 * Finally add the coach to the coach table.
	 * All validation and checking on the entered form details has been completed.
	 */
	public function addCoachToTable()
	{
		// Date and time now.
		$date = date('Y-m-d H:i:s');

		// Start building the query to insert the coach.
		$query = 'INSERT INTO `we_coach` (`name`,`phone`,`email`,`date_created`,`mobile`,`username`, `admin`) VALUES(?,?,?,?,?,?,?)';

		// Send the query and params off to be executed by the SQL class.
		return $this->conn->insert($query,[$this->name,$this->phone,$this->email,$date,$this->mobile,$this->username,$this->admin]);
	}

	
	/** Add a coach to the CRM part of the Work experience app on page: /workexperience/CRM/coach.php  */
	public function update_wp_coach($POST)
	{
		unset($POST['update_wp_coach']); // Unset the HTML form tag.
		$POST['phone'] = str_replace(' ', '', $POST['phone']);
		$POST['mobile'] = str_replace(' ', '', $POST['mobile']);
		$POST['admin'] = (isset($POST['admin']) && $POST['admin']=='1') ? 1 : 0;

		$row_id = $POST['row_id']; // This is the row id of the row to update.

		$errors = $this->validateFormInput($POST['phone'],$POST['mobile'],$POST['email']);

		if(empty($errors))
		{
			$check=NULL;

			$check = $this->checkUsernameForDuplicate($POST);

			if($check!=true)
			{
				return array('duplicate'=>'That is a duplicate record. You are attepting to use a username already used.');
			}

			else
			{
				$query = "UPDATE `we_coach` SET `name`=?,`phone`=?,`email`=?,`mobile`=?,`username`=?, `admin`=? WHERE `id`=?";
				
				$update =  $this->conn->update($query,[
						$POST['name'],
						$POST['phone'],
						$POST['email'],
						$POST['mobile'],
						$POST['username'],
						$POST['admin'],
						$POST['row_id']
						
				]);	

				return array('success'=>'Coach updated successfully.');
			}

			
		}

		else
		{
			return $errors;
		}

	}

	/**
	 * A function to validate some form input from the add a coach form.
	 * @param  [string] $phone This is the phone number added. (PHP probably tyope converts this to a number).
	 * @param  [string] $mobile [The mobile phone number entered] (PHP probably tyope converts this to a number).
	 * @param  [string] $email  [The email address entered].
	 * @return [array] This will be either empty array with no errors, or have errors set on it. 
	 * The error array is then finally returned to the calling statement and can they can be output as HTML in the form. 
	 */
	public function validateFormInput($phone,$mobile, $email)
	{

		// Placeholder for error handling the form input.
		$error = array();

		// Check the form input for numerics and length.
		if(is_int($phone)==false || str_len($phone)<11  || 
			is_int($mobile)==false || str_len($mobile)<11 || 
			filter_var($email, FILTER_VALIDATE_EMAIL)==false)
		{
		
			// Check the email is correct at this stage and if not return false.
			if(filter_var($email, FILTER_VALIDATE_EMAIL)==false)
			{
				$error['email_message'] = 'That isn\'t a valid email'; 
			}
		
			if(is_numeric($phone)==false)
			{
				$error['phone_message_numeric'] = 'Phone number needs to be numeric.';
			}

			if(strlen($phone)<11)
			{
				$error['phone_message_length'] = 'Not enough numbers to be a phone number.';
			}

			if(is_numeric($mobile)==false)
			{
				$error['mobile_message_length'] = 'Mobile phone number needs to be numeric.';
			}

			if(strlen($mobile)<11)
			{
				$error['mobile_message_numeric'] = 'Not enough numbers to be a mobile phone number.';
			}

			return $error;
		}

	}


	/**
	 * A function to check of a duplicate coach in the database. 
	 * This runs a check on the username ONLY, later adjusted so we don't need to check all the $name,$phone,$mobile & $email details.
	 * @param  [string] $name   The name of the prospective coach
	 * @param  [string] $phone  The phone number of the prospective coach
	 * @param  [string] $mobile   The mobile phone number of the prospective coach
	 * @param  [string] $email    The email address of the prospective coach
	 * @param  [string] $username The username of the prospective coach
	 * @return 
	 */
	public function checkForDuplicateCoach($name,$phone,$mobile,$email,$username)
	{
		// Get all the placement instances associated with this placement id passed via get from the login page. 
		$result = $this->conn->select('SELECT * FROM `we_coach` WHERE `username` LIKE ?',[$username]);

		// If there is an id to the row, that means one already exists and this is a duplicate.
		if($result[0]['id'])
		{
			return true; // This is a duplicate
		}

		// No duplicate so the insert can proceed.
		else
		{
			return false; // // This is NOT a duplicate
		}
	}

	public function checkUsernameForDuplicate($POST)
	{

		// Get all the placement instances associated with this placement id passed via get from the login page. 
		$result = $this->conn->select('SELECT * FROM `we_coach` WHERE `username`=?',[$POST['username']]);

		// No result found on the query so this is ok. 
		if($result==false)
		{
			return true;
		}

		// The result of the id posted and the one queried is the same, same record, this is ok. 
		if($POST['row_id']==$result[0]['id'])
		{
			return true;
		}

		// The IDS are not the same, yet the username is found and is therefore already held for a coach, so this is not ok. 
		if($POST['row_id']!=$result[0]['id'])
		{
			//echo 'NO DO NOT ADD';die;
			return false;
		}
	}

}


