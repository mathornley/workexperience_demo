<?php

/**
 * Autoloader for including all the classes.
 */
spl_autoload_register(function ($class_name) {
    include $class_name . '.php';
});

class WorkPlacementReview extends Main
{
	public function __construct()
	{		
		try
		{
			// Call the parent first and instantiate the database SQL class instance.
			parent::__construct();
			// Instantiate a new instance of learner.
			$this->learner = new Learner;
		}

		// Catch some PDO errors.
		catch (PDOException $e)
		{
			throw new Exception('PDO Database connection failed to connect to SID failed in line: ' .__LINE__ . ' of file: '. __FILE__ . $e->getMessage());
		}
	}


	/**
	 * Pull the learner review
	 * @return [boolean] [If there are no results, false is returned]
	 */
	public function learnerReview($placement_attended_id,$studID=null)
	{
		// Get all the logbook entries for this learner that are not hidden (deleted is hidden=1)
		$result = $this->conn->select('SELECT * FROM  `we_work_placement_review` WHERE `placement_attended_id`=?',[$placement_attended_id]);

		// If there's no result return false.
		if(!$result) return false;
		else return $result;
	}



	/**
	 * Adding the students work placement review to the table. 
	 * Each aim is recorded as a separate row in the table. Thery're updated in the table one at a time. 
	 * @param [array] $POST [The array variable form field elements from the form]
	 */
	public function addReview($POST)
	{
		//echo '<pre>';
		//print_r($POST);die;
		unset($POST['save_review']); // Unset the flag from the HTML form. 
		
		// Create the new array we'll use to store.
		$array = array();

		// Get the placement attended ID that corresponds with this review, this the key for the placement attended table.
		$array['placement_attended_id'] = $POST['placement_attended_id'];

		// Transfer this PHP array to something useable. 
		$reviewArray = $POST;

		// Iterate the arry and get the vaulues that have been posted, as some radio buttons weren't clicked. 
		for($i=0;isset($reviewArray['review'][$i]);$i++)
		{
			if(isset($reviewArray['review'][$i]["'option'"]) && $reviewArray['review'][$i]["'option'"] == 'went_well') $array[$i]['went_well'] = 1;

			if(isset($reviewArray['review'][$i]["'option'"]) && $reviewArray['review'][$i]["'option'"] == 'could_have_done_better') $array[$i]['could_have_done_better'] = 1;

			if(isset($reviewArray['review'][$i]["'aim'"])) $array[$i]['aim'] = $reviewArray['review'][$i]["'aim'"];
			if(isset($reviewArray['review'][$i]["'reason'"])) $array[$i]['reasons_for_choice'] = $reviewArray['review'][$i]["'reason'"];

		}
		
		// Get each aim row (Each aim from the template form)
		for($i=0;isset($array[$i]);$i++)
		{
			$query='';

			// Start building the query.
			$query = 'INSERT INTO `we_work_placement_review`(placement_attended_id,';

			// Add the placement attended id to the start of the arguments as we'll pass that in first.
			$args[] = $array['placement_attended_id'];

			// Get the columns for the query and count the placeholders.
			foreach($array[$i] AS $column=>$value)
			{
				// Load the query with the headers.
				$query .= $column. ',';
				// Fill form details in to the array, 
				$args[] = $value;
			}
			
			// Trim the end of the string and get rid of the trailing comma. 
			$query = rtrim($query,', ');
			$query.=')';

			// Start building the values section. 
			$query.='VALUES(';

			// Get the columns placeholders and input.
			foreach($array[$i] AS $column=>$value)
			{
				$query .= "?, ";
			}

			// Placement attended id placeholder.
			$query.='?,';

			$query = rtrim($query,', ');
			$query.=')';

			// Insert to the table.
			$row =  $this->conn->insert($query,$args);

			// Update the date_created on the record.
			$date = date('Y-m-d H:i:s');//date('Y-m-d', strtotime('now'));
			$query = 'UPDATE `we_work_placement_review` SET date_created=? WHERE placement_attended_id=?';
			$row =  $this->conn->update($query,[$date,$array['placement_attended_id']]);

			// just unset these here to be sure. 
			unset($query);
			unset($args);
			unset($row);
		}

	}

	/**
	 * Edit the already submitted work placement review and then save.
	 * Function first NULLS the values from the form and then enters the posted data to update. 
	 * Details are from the edit form on the edit section on placement_review_edit.php
	 * @param  [array] $POST [The variables to update the record]
	 * @return [type] [description]
	 */
	public function editReview($POST)
	{
		echo '<pre>';
		print_r($POST);
		unset($POST['save_review']); // Unset the flag from the HTML form passed in.

		// Get the placement attended ID for the query.
		$placement_attended_id = $POST['placement_attended_id'];
		// Unset the placement id and row id from the array as this will be used in a foreach.
		unset($POST['placement_attended_id']);

		// Transfer this PHP array to something useable. 
		$reviewArray = $POST;

		// Get each aim row (Each aim from the template form)
		for($i=0;isset($reviewArray['review'][$i]);$i++)
		{

			$reviewArray['review'][$i][$reviewArray['review'][$i]["'option'"]] = 1;
			unset($reviewArray['review'][$i]["'option'"]);
			//print_r($reviewArray['review'][$i]);die;

			$rowID = $reviewArray['review'][$i]["'id'"];
			unset($reviewArray['review'][$i]["'id'"]);


			$query = 'UPDATE `we_work_placement_review` SET aim=?,went_well=?, could_have_done_better=?,reasons_for_choice=? WHERE id=?';
			$this->conn->update($query,[NULL, NULL, NULL, NULL,$rowID]);

		
			// Start building the query.
			$query = 'UPDATE `we_work_placement_review` SET ';

			// Get the columns for the query and count the placeholders we'll need.
			foreach($reviewArray['review'][$i] AS $column=>$value)
			{
				// For some reason, column names in array had quotes, so we'll remove those before the MySQL statement. 
				$column = str_replace("'","",$column);
				
				if($column == 'option')
				{
					unset($column);
					continue;
				}

				// Load the query with the headers and the placeholders.
				$query .= "$column=?,";
				// Fill form details in to the array.
				$args[] = $value;

			}

			$args[] = $rowID;
		
			// Trim the end of the string and get rid of the trailing comma. 
			$query = rtrim($query,', ');
			$query.=' WHERE `id`=?';

			echo $query;
			print_r($args);

			// Insert the updated review the table.
			$this->conn->update($query,$args);

			// Add the date_updated to rows of the review just updated.
			$query = 'UPDATE `we_work_placement_review` SET date_updated=? WHERE id=?';
			$date = date('Y-m-d H:i:s');//date('Y-m-d', strtotime('now'));
			$this->conn->update($query,[$date,$rowID]);

			// just unset these to be sure. 
			unset($query);
			unset($args);
			unset($row);
			unset($rowID);

		} // End get each aim row (Each aim from the template form)	

		return true; // All good, return true.	
	}


	/**
	 * Insert a new row for the learners action plan comments. (These are in the placement review section /workexperience/placement_review.php)
	 * @param [array] $POST array of the comments from the form. 
	 */
	public function addActionPlanComments($POST)
	{
		//echo '<pre>';print_r($POST);die;

		// Unset a flag.
		unset($POST['add_action_plan_comments']);

		// Set some vars.
		$studID = $POST['studID'];
		$date = date('Y-m-d H:i:s');//date('Y-m-d', strtotime('now'));
		$comments = trim($POST['action_plan_comments']);
		$placement_attended_id = $POST['placement_attended_id'];

		// Get the learners name from the student id.
		$learnerName = $this->learner->learnerDetailsByID($studID); 

		// Prepare the insert query.
		$query = 'INSERT INTO `we_action_plans` (`learner_id`,`date_created`,`comments`,`name`,`placement_attended_id`) VALUES(?,?,?,?,?)';
		
		// Perform the update with MySQL
		$result = $this->conn->update($query,[$studID,$date,$comments,$learnerName,$placement_attended_id]);

		// Insert worked, return some details in an array
		if($result!=false)
		{
			return array
			(
				'name'=>$learnerName,
				'learner_id'=>$studID
			);
		}

		// The creation of the action comments has failed. 
		else
		{
			return false;
		}
	}

	public function updateActionPlanComments($POST)
	{
		// Unset a flag.
		unset($POST['update_action_plan_comments']);

		// Set some vars.
		$studID = $POST['studID'];
		$date = date('Y-m-d H:i:s');//date('Y-m-d', strtotime('now'));
		$comments = trim($POST['action_plan_comments']);
		$placement_attended_id = $POST['placement_attended_id'];
		$row_id = $POST['row_id'];

		// Get the learners name from the student id.
		$learnerName = $this->learner->learnerDetailsByID($studID); 

		// Prepare the insert query.
		$query = 'UPDATE `we_action_plans` SET `learner_id`=?,`date_created`=?,`comments`=?,`name`=?,`placement_attended_id`=? WHERE `id`=?';
		
		// Perform the update with MySQL
		$result = $this->conn->update($query,[$studID,$date,$comments,$learnerName,$placement_attended_id,$row_id]);

		// Insert worked, return some details in an array
		if($result!=false)
		{
			return array
			(
				'name'=>$learnerName,
				'learner_id'=>$studID
			);
		}

		// The creation of the action comments has failed. 
		else
		{
			return false;
		}
	}

	public function getLearnersActionPlanComments($studID)
	{
	
		// Prepare the insert query.
		$query = 'SELECT * FROM `we_action_plans` WHERE `learner_id`=?';
		
		// Perform the update with MySQL
		$result = $this->conn->select($query,[$studID]);

		// Insert worked, return some details in an array
		if($result!=false)
		{
			return $result;
		}

		else
		{
			return false;
		}
	}
}




