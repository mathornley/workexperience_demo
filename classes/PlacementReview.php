<?php

/**
 * Autoloader for including all the classes.
 */
spl_autoload_register(function ($class_name) {
    include $class_name . '.php';
});

/**
 * ~A class to manage all aspects of the Placement review the student will write. 
 */
class PlacementReview extends Main
{
	public function __construct()
	{	
		try
		{
			// Call the parent first and instantiate the database SQL class instance.
			parent::__construct();
			// Instantiate a new instance of learner.
			$this->learner = new Learner;
			// Instantiate a new instance of aims.
			$this->aims = new Aims;
		}

		// Catch some PDO errors.
		catch (PDOException $e)
		{
			throw new Exception('PDO Database connection failed to connect to SID failed in line: ' .__LINE__ . ' of file: '. __FILE__ . $e->getMessage());
		}
	}

	/**
	 * Getters for the learner chosen aims, all formatted as an array of aims all prettified.
	 * @return [array] Returns a result set of the aims as an array,
	 */
	public function learnerAimsFormatted()
	{	
		return $this->aims->learnerAimsFormatted();
	}

	/**
	 * Getters for the learner chosen aims, raw and unformatted as an array of aims all prettified.
	 * @return [array] Returns a result set of the aims as an array,
	 */
	public function learnerAims()
	{
		return $aims = $this->aims->learnerAims();
	}
}


