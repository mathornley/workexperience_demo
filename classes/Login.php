<?php

/**
 * Autoloader for including all the classes.
 */
spl_autoload_register(function ($class_name) {
    include $class_name . '.php';
});

// Turn error reporting on. 
ini_set('display_errors', 'On');

// turn cache off for this project
ini_set('opcache.enable', '0'); 

/**
 * This is a class to handle a password login by a placement. 
 */
class Login extends Main
{
	/**
	 * Set all our vars here to use during the login/password reset process
	 * @param [array] $POST [Username and password is posted in]
	 */
	public function __construct($POST=NULL)
	{	
		try
		{
			// Call the parent first and instantiate the database SQL class instance.
			parent::__construct();

			// Session where we'll hold the learner and instance id. If session not set, set it. 
			if(!isset($_SESSION)) 
			{
				session_start();
			}
		}

		// Catch some PDO errors.
		catch (PDOException $e)
		{
			throw new Exception('PDO Database connection failed to connect to SID failed in line: ' .__LINE__ . ' of file: '. __FILE__ . $e->getMessage());
		}

		// unset some vars we don't need form the Main class. 
		unset($this->stuID);unset($this->debug);

		// Set a global date variable of now. 
		$this->date = date('Y-m-d H:i:s');

		// These are our password salts, production would see them more complex!
		$this->salt1 = '$ALT1';
		$this->salt2 = '$alt2';

		// Username and password - cleanse them
		$this->username = (isset($POST['username'])) ? $this->cleanseVars($POST['username']) : NULL;
		$this->password = (isset($POST['password'])) ? $this->cleanseVars($POST['password']) : NULL;

		// HASH the password with SHA1 and salts. When the password is set, salts were applied.
		$this->hashedPassword = SHA1($this->salt1.$this->password.$this->salt2);
	}

	/**
	 * Check the users username & password and return either false or the employer details as array.
	 * @return [boolean] [the employers details as array or false depending on success of the login]
	 */
	public function checkLogin()
	{

		// Query the database for the user and if there is one, return the INT value of the rows. Ideally, 1
		$login = $this->conn->select('SELECT * FROM `we_placement` WHERE `username` =? AND `password`=?',[$this->username, $this->hashedPassword]);

		//echo '<pre>';print_r($login);die;

		// If the username and password is there, they're fine, so now pull back their details.
		if($login!=false)
		{

			$_SESSION['logged_in'] = 1;

			// Record the time the employer logs in, so we can use the timeout and check on each page.
			$this->conn->insert('UPDATE `we_placement` SET `login_date` =? WHERE `id`=?',[$this->date,$login[0]['id']]);

			// Get the employers details.
			$employer = $this->getEmployersDetails();

			return $employer;
		}

		// Login failed, return false to the template.
		else
		{
			return $_SESSION['logged_in'] = 0;
			//return false;
		}

	}

	public function checkSession()
	{
		if($_SESSION['logged_in']=='1') return true;
		if($_SESSION['logged_in']=='0') return false;
	}

	/**
	 * A 30 minute timeout is set to the whole of the employer section.
	 * @param  [string] $placement_id This is the placement_id that is passed in from the page.
	 * There is no return variable, the page simply redirects to the login area.
	 */
	public function checkTimeOut($placement_id)
	{
		// Query the database for the user and if there is one, return the INT value of the rows. Ideally, 1
		$login = $this->conn->select('SELECT `login_date` FROM `we_placement` WHERE `id` =?',[$placement_id]);

		// Query the login time for this user.
		$loginTime = $login[0]['login_date'];
		// Get the date and time now
		$dateNow = date('Y-m-d H:i:s');

		// Set some date stuff to work from.
		$login = new DateTime($loginTime);
		$now = new DateTime($dateNow);
		$interval = $now->diff($login);
		$elapsedTime = $interval->format('%i');

		// If they've been logged in longer than 30 minutes, redirect them to sign in again. 
		if($elapsedTime>='30')
		{
			// Redirect to the sign in page. 
			header('location: /workexperience/employer/');
		}
	}

	/**
	 * Get the employers details from the we_placement table.
	 * Won't be false, as the previous call checks that the employer is in the table.
	 * @return [array] [Array of the employers details.]
	 */
	public function getEmployersDetails()
	{	
		// Query the database for the user details
		$result = $this->conn->select('SELECT * FROM  `we_placement` WHERE `username` =? AND `password`=?',[$this->username, $this->hashedPassword]);

		// Return the employers details. 
		return $result;
	}

	/**
	 * Create and send the password email reset link to the user.
	 * Employer email is used to firstly check that they are a registered user. If not, immediate false returned.
	 * If user exists, get the row id of the placement and hash this for when sent as email. Also, use token instead of key to not be obvious.
	 * Send the email to the registered account.
	 * @param  [array] $POST [arrtay with the user entered email address]
	 * @return [boolean false or array of employer details] [Employer details]
	 */
	public function sendPasswordEmail($POST)
	{
		// Get the input of the email. 
		$employer_email = $POST['reminder_email'];

		// Query employer table for the email and see if there is an employer with that email. 
		$employer = $this->conn->select('SELECT * FROM `we_placement` WHERE `email`=?',[$employer_email]);

		// Email isn't found, so return false and tell the user.
		if($employer==false)
		{
			return false;
		}
		
		$date = date('d-m-Y H:i:s'); // The date now, which is when the request was made.
		
		// Encode the placement id (NOT placement attended id). Use token instead of ID so isn't obvious what it is beiong passed,
		$placement_id = $this->hashids->encode($employer[0]['id']); // We'll call this token on the URL
		
		// Send the user an email with the link to click for the password reset page and send the placement id (placement, not placement attended instance). Their hashed id is to be used in the link with the key token instead of id.

$message=<<<message

<h3>Password reset email</h3>

<p>Hi {$employer[0]['manager']},</p>

<p>
We have received a password reset request from you which was requested at {$date} to reset your account for {$employer[0]['name']}.
</p>

<p>
If you did not make this request, then simply ignore this email. If however you did request to reset your password, please click <a href='https://www.boltoncc.ac.uk/workexperience/employer/password_reminder.php?token={$placement_id}'>here</a> and you'll be able to change your password in a couple of simple steps.
</p>

<p>Regards,</p>

<p>
Bolton College.
</p>
message;


		// Compose end the message to the person.
		$to = $employer[0]['email'];
		$from = 'noreply@boltoncc.ac.uk';
		$subject = "Password reset request for {$employer[0]['manager']} at {$employer[0]['name']}";

		// Email headers for all emails sent in the class. 
		$headers = 
		'From: mailer@boltoncc.ac.uk' . "\r\n" .
	    'Reply-To: noreply@boltoncc.ac.uk' . "\r\n" .
	    'Cc: mike.thornley@boltoncc.ac.uk' . "\r\n".
	    'MIME-Version: 1.0' . "\r\n".
	    'Content-type: text/html; charset=iso-8859-1' . "\r\n".
	    'X-Mailer: PHP/' . phpversion();
	
		// Send the email. 
	    mail($to,$subject,$message,$headers);

		return $employer; // return the employers details array.
	}


	/**
	 * Change the users password.
	 * Check that the user has provided a password they want, which is the same twice, then check they both match.
	 * If both passwords don't match for the change, return false.
	 * Hash the password and using the placement_id (just id in the query), and change the password.
	 * return true to the front end template.  
	 * @param  [array] $POST [array containing the new user chosen password]
	 * @return [boolean or false] [Either, true or false is returned on the outcome of the update]
	 */
	public function changePassword($POST)
	{
		// Get the passwords entered.
		$password1 = $POST['password1'];
		$password2 = $POST['password2'];
		
		// Get the placement id from the $_POST array - we added this in the template before this function call. 
		$placement_id = $POST['placement_id'][0];

		// Test that the user entered the password the same, twice, otherwise reject.
		if($password1!==$password2) return false;

		// Salt the users new password and hash.
		$user_new_password = sha1($this->salt1.$password1.$this->salt2);

		// Update the users password using the placement id (NOT the placement_attended_id).
		$this->conn->update('UPDATE `we_placement` SET `password`=? WHERE `id`=?',[$user_new_password,$placement_id]);

		return true; // All is a success, so return true.
	}

	/**
	 * Get the `placement_id` from the `placement_attended_id`
	 * @param  [string] $placement_attended_id. This is the placement attended id passed in from the page.
	 * @return [string] This is the placement id.
	 */
	public function getPAIDidFromPlacementID($placement_attended_id)
	{
		// Use $placement_attended_id to get the placement_id from the `we_placement_attended` table.
		$paid = $this->conn->select('SELECT `placement_id` FROM `we_placement_attended` WHERE `id`=?',[$placement_attended_id]);

		// return the placement_attended_id.
		return $paid[0]['placement_id'];
	}

	public function __destruct()
	{
		//$_SESSION = array();
		// Finally, destroy the session.
		//session_destroy();
	}
}


