<?php

/**
 * Autoloader for including all the classes.
 */
spl_autoload_register(function ($class_name) {
    include $class_name . '.php';
});

/**
 * A class to be associated with the learner details. 
 */
class ALS extends Main
{
	public function __construct($placement_attended_id=null,$stuID=null)
	{	
		try
		{
			// Call the parent first and instantiate the database SQL class instance.
			parent::__construct();

			// Instantiate an instance of learner.
			$this->learner = new Learner;
		}

		// Catch some PDO errors.
		catch (PDOException $e)
		{
			throw new Exception('PDO Database connection failed to connect to SID failed in line: ' .__LINE__ . ' of file: '. __FILE__ . $e->getMessage());
		}
	}

	/**
	 * Add a learner to the `we_als` (Additional learner support) table.
	 * Add & upload a student doc to that learner which is like a care plan and risk assessment.
	 * @param [type] $POST  This is the form information on the student that goes in to the DB table
	 * @param [type] $FILES The student doc is the document that the staff upload for the student
	 */
	public function addALSLearner($POST,$FILES)
	{

		//echo $FILES["file_upload"]["size"];die;

		// Initially unset some form flags
		unset($POST['submit']);
		unset($POST['add_als_learner']);

		// Get some vars unpacked
		$learner_id = explode(' ', $POST['learner'])[2];
		$learner = explode(' ', $POST['learner'])[0].' '.explode(' ', $POST['learner'])[1];
		$fileExt = explode('/',$FILES['file_upload']['type'])[1];
		$date = date('Y_m_d_H_i_s');

		// Initially check that this learner isn't already added and if they have been already, STOP!
		$query = 'SELECT * FROM `we_als` WHERE `learner_id`=?';	

		// FALSE if the learner isn't in the table.
		if($this->conn->select($query,[$learner_id])!=false)
		{
			// There is a row in the table for this learner, so we'll let the form know they are exemption
			return array('error'=>"Sorry, the learner has already been recorded with ALS. You do not have to enter them again.");
		}

		// Firstly, restrict the file size
		if($FILES["file_upload"]["size"] > 200000000 ) // This is in bytes
		{
			return array('error'=>'Sorry, that filesize is too large to upload');
		}

		// The disallowed list of the file extensions which the form will reject.
		$disallowed = ['mp4','mp3','tif','avi','mov','aif','mpa','ogg','wav','wma','wpl','dmg','php','js','exe'];

		// The file extension isn't allowed.
		if(in_array($fileExt,$disallowed))
		{
			return array('error'=>"Sorry, the '{$fileExt}' file type extension is not permitted.");
		}

		// Check for a file upload error
		if(isset($FILES['file_upload']['error']) && $FILES['file_upload']['error'] === UPLOAD_ERR_OK)
		{
		
			// Sort some file creation vars
			$path = '../student_docs/';
			$file = "{$path}{$learner_id}_{$date}.{$fileExt}";
			$fullFilePath = "/workexperience/student_docs/{$learner_id}_{$date}.{$fileExt}";

			//echo $file;die;

			// All is ok so upload and move the file. 
			if (move_uploaded_file($FILES['file_upload']['tmp_name'],$file))
			{
        		//echo "The file ". basename($FILES['file_upload']['tmp_name']). " has been uploaded.";

        		// Put the ALS learner details in the `we_als` table.
        		$query="INSERT INTO `we_als` (`learner_id`,`exempt`,`support_details`,`exempt_details`,`reduced_further_details`,`further_info`,`completed_by`,`created`,`support`,`reduced`,`student_doc`) VALUES(?,?,?,?,?,?,?,?,?,?,?)";

        		// Our row parameters for the ALS learner
        		$args = array
        		(
        			$learner_id,
        			$POST['exempt'],
        			$POST['support_details'],
        			$POST['exempt_details'],
        			$POST['reduced_further_details'],
        			$POST['further_info'],
        			$this->learner->stuID,
        			$date,
        			$POST['support'],
        			$POST['reduced'],
        			$file
        		);

        	
				if($this->conn->insert($query,$args)!=false)
				{
					return array("success"=>"The record for the ALS learner, {$learner} has been created.");
				}
    		}

    		// File didn't get moved so there is an issue.
    		else
    		{
    			return array("error"=>"There was an error in the file moving process, please contact the ILT dept");
    		}
		}

		// File upload error
		else
		{
			return array('error'=>'File upload error, try a diffferent file.');
		}

	}

	/**
	 * This is the section where the ALS team updates a learner entry. 
	 * $FILE if there are any are the student_docs uploaded (car plan and risk assess etc)
	 * @param  [array] $POST  These are the POSTED vars from the form. 
	 * @param  [array] $FILES These are the files that are updated as the student docs. 
	 * @return [bollean} Either true or false is returned depending on what the action is.
	 */
	public function updateALSLearner($POST,$FILES=NULL)
	{
		echo '<pre>'; 

		// Get the learner name from the forms read-only property we'll use and then immediately unset.
		$learner = $POST['learner'];
		unset($POST['learner']);

		// Initially unset some form flags
		unset($POST['submit']);
		
		// Get some vars unpacked & unset some stuff -- I just like to do this.
		$id = $POST['update_als_learner'];
		unset($POST['update_als_learner']);
		$date = date('Y_m_d_H_i_s');

		// The disallowed list of the file extensions we'll check against the upload.
		$disallowed = ['mp4','mp3','tif','avi','mov','aif','mpa','ogg','wav','wma','wpl','dmg','php','js','javascript','exe'];

		// Firstly, restrict the file sizes to nothing too big. This is in bytes.
		if(isset($FILES["file_upload"]["size"]) && $FILES["file_upload"]["size"] > 200000000) 
		{
			//return array('error'=>'Sorry, that filesize is too large to upload');
			return array('error'=>"Sorry, the file type is too large.");
		}

		// Get the file extension if there is one. If not, false.
		if(isset($FILES['file_upload']['type']) && $FILES['file_upload']['type'])
		{
			$fileExt = explode('/',$FILES['file_upload']['type'])[1];

			// The file extension isn't allowed.
			if(isset($fileExt) && in_array($fileExt,$disallowed))
			{
				return array('error'=>"Sorry, the '{$fileExt}' file type extension is not permitted.");
			}
		}

		// No file extension.
		else
		{
			$fileExt = NULL;
		}

		// Put the ALS learner details in the `we_als` table.
		$query="UPDATE `we_als` SET ";

		// Iterate the columns and the header and build the query up and add the input to $args.
		foreach($POST AS $column=>$value)
		{
			$query.="`{$column}`=?,";
			$args[] = $value;
		}

		// Trim off the trailing comma from the query.
		$query = rtrim($query,', ');

		$query .= " WHERE `id`=?";
		$args[] = $id;

		// The POSTED data has been added to the table. Now, check for a file upload to that row also.
		if($this->conn->update($query,$args)!=FALSE)
		{
			// There have been files posted during the update
			if($FILES["file_upload"]["size"]>0 && isset($fileExt))
			{				
				// Check for a file upload error
				if(isset($FILES['file_upload']['error']) && $FILES['file_upload']['error'] === UPLOAD_ERR_OK)
				{			
					// Sort some file creation vars
					$path = '../student_docs/';
					$file = "{$path}{$learner_id}_{$date}.{$fileExt}";
					$fullFilePath = "/workexperience/student_docs/{$learner_id}_{$date}.{$fileExt}";

					// All is ok so upload and move the file. 
					if (move_uploaded_file($FILES['file_upload']['tmp_name'],$file))
					{
		        		//echo "The file ". basename($FILES['file_upload']['tmp_name']). " has been uploaded.";

		        		// Put the ALS learner details in the `we_als` table.
		        		$query="UPDATE `we_als` SET `student_doc`=? WHERE `id`=? AND (`student_doc` <> ? OR `student_doc`= ?)";

						if($this->conn->update($query,[$file,$id,$file,$file])!=FALSE)
						{
							return array("success"=>"The record for the ALS learner, {$learner} has been updated.");
						}
		    		}

		    		// File didn't get moved so there is an issue.
		    		else
		    		{

		    			//echo "The file has NOT moved";die;
		    			return array("error"=>"There was an error in the file moving process, please contact the ILT dept");
		    		}
				}

				// File upload error
				else
				{
					return array('error'=>'File upload error, try a diffferent file.');
				}
			}

			// No files to upload, so return the message that all is ok. 
			else
			{
				return array("success"=>"The record for the ALS learner, {$learner} has been updated.");
			}


		}

		// Not an update to the POST data, just update the $FILES array
		if($this->conn->update($query,$args)==FALSE & isset($FILES))
		{
			
			$path = '../student_docs/';
			$file = "{$path}{$learner_id}_{$date}.{$fileExt}";
			$fullFilePath = "/workexperience/student_docs/{$learner_id}_{$date}.{$fileExt}";

			// All is ok so upload and move the file. 
			if (move_uploaded_file($FILES['file_upload']['tmp_name'],$file))
			{
	    		//echo "The file ". basename($FILES['file_upload']['tmp_name']). " has been uploaded.";

	    		// Put the ALS learner details in the `we_als` table.
	    		$query="UPDATE `we_als` SET `student_doc`=? WHERE `id`=? AND (`student_doc` <> ? OR `student_doc`= ?)";

				if($this->conn->update($query,[$file,$id,$file,$file])!=FALSE)
				{
					return array("success"=>"The record for the ALS learner, {$learner} has been updated.");
				}
			}

			// File didn't get moved so there is an issue.
			else
			{

				//echo "The file has NOT moved";die;
				return array("error"=>"There was an error in the file moving process, please contact the ILT dept");
			}
		}
	}

	/**
	 * Get all the ALS learner results from the `we_ALS tables.`
	 * @return boolean FALSE or $array result set
	 */
	public function getAllALSLearners()
	{
		$query = 'SELECT * FROM `we_als` ORDER BY `created` DESC';
		
		if($this->conn->select($query)!=false)
		{
		 	return $this->conn->select($query);
		}

		return false; // No result set, so return false.

	}

	/**
	 * Get the learner from the ALS table with the row id of the table.
	 * @param  string $id This is the string row id of the row we're passing in from the forms edit button.
	 * @return Return the result set to the form to populate the edit form on workexperience/CRM/als.php 
	 * @return Boolean or false. This is the result set if there is one and false if there isn't.
	 */
	public function getALSLearnerByID($id)
	{
		$query = 'SELECT * FROM `we_als` WHERE `id`=?';
		
		if($this->conn->select($query,[$id])!=false)
		{
			// Return the result set to the form
		 	return $this->conn->select($query,[$id]);
		}

		return false; // No result set, so return false.

	}

	public function getUserALSDetails($learnerID)
	{
		echo 'TEST';die;
	}

}
