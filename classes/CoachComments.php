<?php

/**
 * Autoloader for including all the classes.
 */
spl_autoload_register(function ($class_name) {
    include $class_name . '.php';
});

/**
 * A class to be associated with the learner details. 
 */
class CoachComments extends Main
{
	public function __construct()
	{	
		try
		{
			// Call the parent first and instantiate the database SQL class instance.
			parent::__construct();

			$this->coach = new CareerCoachReview;
			
		}

		// Catch some PDO errors.
		catch (PDOException $e)
		{
			throw new Exception('PDO Database connection failed to connect to SID failed in line: ' .__LINE__ . ' of file: '. __FILE__ . $e->getMessage());
		}
	}

	/**
	 * Here is where the coach will add a comment to the aims, logbook and workplace review section section. 
	 * @param [array] $POST [The posted variables from the form fields for aims, logbook and the work experience]
	 * @param [string] $type [This is the flag to say if this is the aims, logbook or the work experience form]
	 */
	public function addCoachComments($POST, $type)
	{

		$placement_attended_id = $POST['placement_attended_id'];
		$date_created = date('Y-m-d H:i:s');

		// using the coachID passed in above, get that coach ID.
		$coachID = $this->coach->getCoachDetailsFromUsername($this->stuID);

		// Add comment to the aims page.
		if($type=='aims')
		{
			$comments = $_POST['comments_aims'];

			// Add the comments from the coach to the placement_attended_id instance from this coach.
			$query = 'INSERT INTO `we_coach_comments` (placement_attended_id,coach_id, comments_aims,date_created) VALUES(?,?,?,?)';

			return $this->conn->insert($query,[$placement_attended_id,$coachID,$comments,$date_created]);

		}

		// Add comment to the logbook page.
		if($type=='logbook')
		{
			$comments = $POST['comments_logbook'];

			// Add the comments from the coach to the placement_attended_id instance from this coach.
			$query = 'INSERT INTO `we_coach_comments` (placement_attended_id,coach_id,logbook_comments,date_created) VALUES(?,?,?,?)';

			return $this->conn->insert($query,[$placement_attended_id,$coachID,$comments,$date_created]);

		}

		// Add comment to the workplacement review page.
		if($type=='workplacement')
		{
			//print_r($POST);die;
			$comments = $POST['comments_placement'];

			// Add the comments from the coach to the placement_attended_id instance from this coach.
			$query = 'INSERT INTO `we_coach_comments` (placement_attended_id,coach_id,workplacement_comments,date_created) VALUES(?,?,?,?)';

			return $this->conn->insert($query,[$placement_attended_id,$coachID,$comments,$date_created]);

		}
	}

	/**
	 * Here is where the coach will edit a comment to the aims, logbook and workplace review section section. 
	 * @param [array] $POST [The posted variables from the form fields for aims, logbook and the work experience]
	 * @param [string] $type [This is the flag to say if this is the aims, logbook or the work experience form]
	 */
	public function editCoachComments($POST,$type)
	{
		$placement_attended_id = $POST['placement_attended_id'];

		// Edit the aims section of the page.
		if($type=='aims')
		{
			$comments = $POST['coach_comments'];
			$row_id = $POST['row_id'];

			// Add the comments from the coach to the placement_attended_id instance from this coach.
			$query = 'UPDATE `we_coach_comments` SET `comments_aims`=? WHERE `id`=? AND `placement_attended_id`=?';

			return $this->conn->update($query,[$comments,$row_id,$placement_attended_id]);
		}

		// Edit the logbook section of the page.
		if($type=='logbook')
		{
			$comments = $POST['coach_comments'];
			$row_id = $POST['row_id'];

			// Add the comments from the coach to the placement_attended_id instance from this coach.
			$query = 'UPDATE `we_coach_comments` SET `logbook_comments`=? WHERE `id`=? AND `placement_attended_id`=?';

			return $this->conn->update($query,[$comments,$row_id,$placement_attended_id]);
		}

		// Edit a comment to the workplacement review page.
		if($type=='workplacement')
		{
			$comments = $POST['comments_placement'];
			$row_id = $POST['row_id'];

			// Add the comments from the coach to the placement_attended_id instance from this coach.
			$query = 'UPDATE `we_coach_comments` SET `workplacement_comments`=? WHERE `id`=? AND `placement_attended_id`=?';

			return $this->conn->update($query,[$comments,$row_id,$placement_attended_id]);
		}
	}

}
