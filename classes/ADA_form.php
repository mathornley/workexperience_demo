 <div id="ADA-CONTAINER">
        <div id="ADA-HEADER">
            <h4><i class="fas fa-comment-dots"></i>Ask Ada</h4>
            <div id="ADA-OPENCLOSE">click to open</div>
            <i id="ADA-DIRECTIONARR" class="fas fa-angle-up"></i>
        </div>
        <div id="ADA-CONTENT-WRAPPER">
            <div id="ADA-CONTENT">
                <div id="ADA-Q-CONTAINER">
                    <div id="ADA-Q-TXT">Who is Ada?</div>
                    <div class="spinner">
                        <div class="bounce1"></div>
                        <div class="bounce2"></div>
                        <div class="bounce3"></div>
                    </div>
                </div>
                <div class="ADA-BUBBLE">
                    I'm Ada, your personal assistant. You can ask me a question about anything to do with your studies and I'll try and give you a useful answer. I can also give you some hints and tips about this app. Below are some example questions I understand.
                </div>
                <div class="ADA-EXAMPLES">
                    <ul class="list-unstyled">
                    <li><a href="index.html"><i class="fas fa-wrench"></i>How do I work this?</a></li>
                    <li><a href="index.html"><i class="far fa-question-circle"></i>Am I right? Am I wrong?</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <form name="adaForm" id="ADA-FORM" action="" method="post" autocomplete="off">   
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1"><i class="fas fa-keyboard"></i></span>
                    <input type="text" class="form-control form-control-sm" name="query" id="ADAINPUT" maxlength="144" placeholder="Type a question">
                </div>
                <button id="ASKADA" type="submit" class="btn">Submit</button>
            </div>
        </form>

    </div>