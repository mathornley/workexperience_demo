<?php

/**
 * Autoloader for including all the classes.
 */
spl_autoload_register(function ($class_name) {
    include $class_name . '.php';
});

/**
 * A class to be associated with the workexperience application details. 
 */
class Workexperience extends Main
{
	public function __construct($placement_attended_id=null,$stuID=null)
	{	
		try
		{
			// Call the parent first and instantiate the database SQL class instance.
			parent::__construct();

			if(isset($stuID))
			{
				$this->stuID = $stuID;
			}
		}

		// Catch some PDO errors.
		catch (PDOException $e)
		{
			throw new Exception('PDO Database connection failed to connect to SID failed in line: ' .__LINE__ . ' of file: '. __FILE__ . $e->getMessage());
		}
	}

	/**
	 * Get all the learners placements. If learner id is passed in, use that, if not use shibboleth
	 * Uses the placement_attended_id to let us know what has or hasn't been chosen. 1 it has, 2 not so ignore.
	 * Once we know what placements have been chosen by the form we'll query again on that.
	 * @param  [string] $stuID [This is the student id, if passed, otherwise shibboleth is used]
	 * @return [array or false] [If the call is successful, the result set is an array and if not FALSE]
	 */
	public function getAllLearnerPlacements($stuID=null)
	{
		// If a student id is passed in, then use that, if not, use shibboleth as the loggged in user.
		if(isset($stuID))
		{
			$this->stuID = $stuID;
		}

		// Return the query or FALSE.
		return $this->conn->select(

		'SELECT *,
			`wp`.`id` AS `placement_id`,
			`we`.`id` AS `paid`
			FROM `elpmiketest`.`we_placement_attended` `we` LEFT OUTER JOIN `elpmiketest`.`we_placement` `wp` ON `wp`.`id`=`we`.`placement_id` WHERE `we`.`learner_id`=?',[$this->stuID]
		);

	}

	/**
	 * This is a function to get the learner chosen placements to be included on the CV
	 * @param  [string] $stuID [This is the student id that could be passed in later by URL]
	 * @param  [string] $CV_id [This is the string CV_id from the edit.php __form.php form
	 * @return [array or false] [Array of the placement data or FALSE]
	 */
	public function getAllLearnerChosenPlacements($stuID=null,$CV_id)
	{
		// If a student id is passed in, then use that, if not, use shibboleth as the loggged in user.
		if(isset($stuID))
		{
			$this->stuID = $stuID;
		}

		// Return the query or FALSE.
		return $this->conn->select(

		'SELECT *
			FROM `CV`.`workexperience` `we` WHERE `CV_id`=?',
			
			[
				$CV_id
			]
		);
	}


	public function checkPlacementsArray($id,$array)
	{
		// If that placement id (From all placements) is in the array ( as a user chosen options), then return true, otherwise false.
		
		/**
		 * search the array with the user choices in and match that against the overall result set. If you get a match return true so the form can be checked on the radio buttons
		 * @var integer
		 */
		for($i=0;isset($array[$i]);$i++)
		{
			if($array[$i]['placement_attended_id']==$id)
			{
				return true;
			}
		}

		// The placement isn't found so return false.
		return false;
	
	}


	public function __destruct()
	{
		$this->conn = null;
	}


}
