<?php

/**
 * Autoloader for including all the classes.
 */
spl_autoload_register(function ($class_name) {
    include $class_name . '.php';
});

// turn cache off for this project
ini_set('opcache.enable', '0'); 

class Placement extends Main
{
	public function __construct()
	{	
		try
		{
			// Call the parent first and instantiate the database SQL class instance.
			parent::__construct();

			// Instantiate a new instance of learner so we can access those details.
			$this->learner = new Learner;

			// Instantiate a new instance of learner so we can access those details.
			$this->login = new Login;

			// Instantiate an instance of the email class
			$this->emailNotifications = new Email;
		}

		// Catch some PDO errors.
		catch (PDOException $e)
		{
			throw new Exception('PDO Database connection failed to connect to SID failed in line: ' .__LINE__ . ' of file: '. __FILE__ . $e->getMessage());
		}
	}

	/**
	 * Used on the location.php page.
	 * Get the learner and placement postcode for this instance of work experience.
	 * Make a cURL request to the Google service: http://maps.googleapis.com/maps/api/geocode/json?address={$postcode},+UK&sensor=false" to get the postcodes 'Geocoded' and get lat & long back. 
	 * Save these into an array for us to pass in to the page which inserts into and handles the JS in the map.
	 * @return [type] [description]
	 */
	public function populateGoogleMap($stuID, $placement_attended_id=null,$placement_id=null)
	{
		// Get the learner details from learners table.
		$learner = $this->learner->learnerDetails($stuID);

		// Get the learner postcode and remove spaces
		$learner_postcode = str_replace(' ', '', $learner[0]['post_code_1'].$learner[0]['post_code_2']);
		
		$placement_postcode = $this->getPlacementPostcodeFromID($placement_id);
		
		// Get the placement postcode on a call to the placements table and remove spaces.
		$placement_postcode = str_replace(' ', '', $placement_postcode[0]['post_code']);

		// Call the Geolocation function in the parent class that Geocodes a postcode to lat & long.
		
		$sourceCoords = $this->reverseGEOcodePostcode($learner_postcode);
		$destCoords = $this->reverseGEOcodePostcode($placement_postcode);

		$coords = array('source'=>$sourceCoords,'destination'=>$destCoords);

		return $coords; // Return coordinates.
	}

	/**
	 * Used on the location.php page.
	 * Student id and the placement ID for this instance.
	 * Get the placement details from a placement ID call to the placement table.
	 * @return [type] [description]
	 */
	public function getPlacementDetailsFromID($studID=null,$placement_attended_id=null,$placementID=null)
	{

		// Get the learner details from learners table.
		$learner = $this->learner->learnerDetails($studID,$placement_attended_id);

		// Get the placement id from the learner/placement query in learner
		$placementID = $learner[0]['placement_id'];

		return $this->conn->select('SELECT * FROM  `we_placement` WHERE `id` =?',[$placementID]);
	}

	/**
	 * Get the placement name from the placementID passed in.
	 * @param  [string] $placementID [The placementID]
	 * @return [string] [A string of the placement name from the passed in placementID]
	 */
	public function getPlacementDetails($placementID) // This is the place not the instance.
	{
		$result = $this->conn->select('SELECT `name` FROM  `we_placement` WHERE `id` =?',[$placementID]);

		// Get the name and return it.
		return $result[0]['name'];
	}


	function getPlacementAttenedInfo($placement_attended_id)
	{
		$result = $this->conn->select('SELECT * FROM  `we_placement_attended` WHERE `id` =?',[$placement_attended_id]);

		if($result!=false) return $result;
		else false;

	}

	/**
	 * Get the postcode of the placement from the placement ID that is passed in.
	 * @param  [string] $placementID This is the ID of the row of the placement. 
	 * @return [array] This is the row of the placement, identified from the ID.
	 */
	public function getPlacementPostcodeFromID($placementID)
	{
	
		return $this->conn->select('SELECT `post_code` FROM  `we_placement` WHERE `id` =?',[$placementID]);
	}


	/**
	 * This is when a new placement instance is created and added to the `we_placement_attended` table.
	 * Checks are made to ensure the same learner isn't assigned the same placement more than once.
	 * The POST var contains the student id, placement id and coach id details for the placement. 
	 * The date the placement is created is also recorded. 
	 * The amount of places available will be determioned by `sf_places` - `places_count`. That give you the remaining places available.
	 * @param [array] $POST The POST vars are the variables posted in from the form to add the placement. 
	 */
	public function addPlacementInstance($POST)
	{

		//echo '<pre>'; var_dump($POST);die;

		unset($POST['submit']); // Unset the HTML form flag.
		unset($POST['add_placement']); // Unset the HTML form flag.

		// Get the string form the array, split that on al the names and grab the last element which is the learner id.
		$array = explode(' ',$POST['learner']);

		// Get the details and set the date.
		$learnerID = array_pop($array);
		$placementID = explode(' ',$POST['placement'])[0];
		$coachID = explode('|',$POST['coach'])[3];
		$date = date('Y-m-d H:i:s');

		$start_date = $POST['start_date'];
		$start_time = $POST['start_time'];
		$end_date = (!$POST['end_date']) ? NULL : $POST['end_date'];
		$end_time = $POST['end_time'];
		$placement_code = (!$POST['placement_type_hidden']) ? NULL : $POST['placement_type_hidden'];

		$placement_type = $_POST['rating']; // Bronze, Silver or Gold. 

		//echo '<pre>';
		//print_r($POST);die;

		// First test that the learner isn't exempt
		$userExempt = $this->conn->select('SELECT * FROM  `we_als` WHERE `learner_id` = ?',[$learnerID]);

		if($userExempt[0]['exempt']=='1')
		{
			return array(
				'is_exempt'=>$userExempt[0]['exempt'],
				'exempt'=>'This learner is exempt from attending a work experience placement.',
				'exempt_details'=>trim($userExempt[0]['exempt_details']),
				'student_name'=>$this->learner->learnerDetailsByID($learnerID),

			);
		}

		// Search the table on the placement_id, coach_id and the learner id firstly. Check it's not been added already.
		$result = $this->conn->select('SELECT *,MONTH(`date_created`) AS `month`,YEAR(`date_created`) AS `year` FROM  `we_placement_attended` WHERE `placement_id` =? AND `coach_id` =? AND `learner_id` = ?',[$placementID,$coachID,$learnerID]);

		// If the placement instance hasn't already been added, add it as new row.
		if($result===false)
		{
			// Get the remaining amount of places left for this placement. 
			$placesLeft = $this->conn->select('SELECT (`sf_places` - `places_count`) AS `places_left`, `name` FROM `we_placement` WHERE `id`=?', [$placementID]);

			// If the remaining places is zero or less, stop!
			if($placesLeft[0]['places_left']<=0)
			{
				return array('error'=>'There are no placements left for this placements.','noplaces'=>$placesLeft[0]['name']);
			}

			// Get the number of already assigned places for this placement.
			$placesCount = $this->conn->select('SELECT `places_count` FROM `we_placement` WHERE `id`=?', [$placementID]);

			// Add the placement to the table as a new row.
			$query = 'INSERT INTO `we_placement_attended` (`placement_id`,`learner_id`,`coach_id`,`date_created`, `start_date`,`start_time`,`end_date`,`end_time`,`placement_code`,`placement_type`) VALUES(?,?,?,?,?,?,?,?,?,?)';

		
			// If the row adds fine to the `we_placement_attended` table and if this is ok, adjust the places
			if($this->conn->insert($query,
				[
					$placementID,
					$learnerID,
					$coachID,
					$date,
					$start_date,
					$start_time,
					$end_date,
					$end_time,
					$placement_code, // This is the EBS to Sid qual code we're pulling & using
					$placement_type // Bronze, silver or gold rating
				]
				)
			)
			{	

			/**
			 * Now ** increment ** the number of `places_count` for this placement in the `we_placement` table. 
			 * 1 place now added,  ** minus sf_places ** , gives you the placement number remaining for that placement.
			 */
			
			// Add the placement to the table.
			$currentPlacesCount = $placesCount[0]['places_count']; // Current number in `places_count`

			// now another place is added, simply increment  ++places_count
			$countUpdate = $this->conn->update('UPDATE `we_placement` SET `places_count`=? WHERE `id`=?',[($currentPlacesCount+1),$placementID]);
				
			return true;
			}
		}

		// There's a placement already in there so return false.
		if($result!==false)
		{
			return false;
		}
	}

	/**
	 * Get all the learner placements, from the placement ID
	 * @param  string $POST The placementID, business name and address.
	 * @return array Returns an array of the placements for that placement ID. 
	 */
	public function getLearnerByPlacementID($POST)
	{
		$placementID = explode(' ',$POST['placement_search'])[0]; // Get the placement ID from the submitted string from the form.
  		
  		// Search the tables on the placement_id and get the learner, coach and student details back. 
		if($result = $this->conn->select
		(
		"
			SELECT *,
				 `coach`.`name` as `coach_name`,
				 `coach`.`phone` as `coach_phone`,
				 `coach`.`email` as `coach_email`,
				 `placement`.`name` as `placement_name`,
				 `wpa`.`id` as `paid`

			FROM `we_placement_attended` `wpa`

			LEFT OUTER JOIN `we_placement` `placement`
			ON `placement`.`id` = `wpa`.`placement_id`

			LEFT OUTER JOIN `ext_a_learner` `learner`
			ON `learner`.`person_code` = `wpa`.`learner_id`

			LEFT OUTER JOIN `we_coach` `coach`
			ON `coach`.`id` = `wpa`.`coach_id`

			WHERE `placement_id`=?


		",[$placementID]

			) // End select
		) // End if 
		{
	  		// Return the result array.
			return $result; 
		}

		// No results, so return false to the template.
		else
		{
			return false;
		}

	}

	/**
	 * Get the placement name, placement id and the coach name.
	 * @param  [string] $POST [This is the array of the first, surname and learner id]
	 * @return [array or boolean] [An array of the result is returned if the call is successful, otherwise FALSE is returned]
	 */
	public function getLearnerPlacementByLearnerID($POST)
	{
		$learner = explode(' ',$POST['learner_placement_search']);
		$learnerID = array_pop($learner);
  		
  		// Search the tables on the placement_id and get the learner, coach and student details back. 
		if($result = $this->conn->select
			(
				"
					SELECT *,`coach`.`name` AS `coach_name`,
						`wp`.`name` AS `placement_name`,
						`wp`.`address` AS `placement_address`,
						`coach`.`name` AS `coach_name`,
						`wpa`.`id` AS `paid`,
						CONCAT(`ext`.`forename`,' ',`ext`.`surname`) AS `full_name`
					FROM `we_placement_attended` `wpa` LEFT OUTER JOIN `we_placement` `wp` on `wp`.`id`= `wpa`.`placement_id` LEFT OUTER JOIN `we_coach` `coach` ON `coach`.`id`= `wpa`.`coach_id` 
							JOIN `ext_a_learner` `ext` on `ext`.`person_code`=`wpa`.`learner_id`
								WHERE `wpa`.`learner_id`= ?

				",
				[$learnerID]
			) // End select
		) // End if 
		{
	  		// Return the result array.
			return $result; 
		}

		// No results, so return false to the template.
		else
		{
			return false;
		}

	}

	/**
	 * Get the placement details usimg the `we_placement` id passed inm from /workexperience/CRM/employer.php
	 * @param  [string] $POST The placement number passed in.
	 * @return [array] The row details for the placement as directed by the placement id. 
	 */
	public function getLearnerByPlacementDetails($POST)
	{

		unset($POST['places_count']);
		$placementID = explode(' ',$POST['placement_search'])[0]; // Get the placement ID from the submitted string from the form.
  		
  		// Search the tables on the placement_id and get the learner, coach and student details back. 
		if($result = $this->conn->select("SELECT * FROM `we_placement` `wpa` WHERE `id`=?",[$placementID]) // End select
		) // End if 
		{
	  		// Return the result array.
			return $result; 
		}

		// No results, so return false to the template.
		else
		{
			return false;
		}

	}

	/**
	 * Get the placement details by the placement_attended_id and use this to populate the edit placement form autocomplete on: /workexperience/CRM/learner.php
	 * @param  [array] $POST POSTED placement_attended_id from the form. 
	 * @return 
	 */
	public function getPlacementDetailsFromInstance($placement_attended_id)
	{
		// Search the tables on the placement_id and get the learner, coach and student details back. 
		if($result = $this->conn->select
		(
		"
			SELECT *,
				 `coach`.`name` as `coach_name`,
				 `coach`.`phone` as `coach_phone`,
				 `coach`.`email` as `coach_email`,
				 `placement`.`name` as `placement_name`,
				 `wpa`.`id` as `paid`

			FROM `we_placement_attended` `wpa`

			LEFT OUTER JOIN `we_placement` `placement`
			ON `placement`.`id` = `wpa`.`placement_id`

			LEFT OUTER JOIN `ext_a_learner` `learner`
			ON `learner`.`person_code` = `wpa`.`learner_id`

			LEFT OUTER JOIN `we_coach` `coach`
			ON `coach`.`id` = `wpa`.`coach_id`

			WHERE `wpa`.`id`=?


		",[$placement_attended_id]

			) // End select
		) // End if 
		{
			return $result; 
		}

		// No results, so return false to the template.
		else
		{
			return false;
		}
	}

	/**
	 * Updating the placement details for a student under the edit section of: /workexperience/CRM/learner.php
	 * @param  Array $POST The placement attended id, the student id, the coach id and the placement id are posted. 
	 * @return Boolean True is returned on success; otherwise false;
	 */
	public function updatePlacementInstance($POST)
	{

		// Strip the HTML flag
		unset($POST['save_edit_placement']);
		
		// Extract our data.
		$placement_id = explode(' ',$POST['edit_placement'])[0];
		$learnerID = $POST['learner_id'];
		$coachID = explode(' | ', $POST['edit_coach'])[3];
		$row_id = $POST['paid']; // The row id of the form.
		$date_updated = date('Y-m-d H:i:s');

		$placement_type = $POST['rating']; // This is the bronze, silver or gold. 

		// Finally update the database table.
		$result = $this->conn->update('UPDATE `we_placement_attended` SET `placement_id`=?, `learner_id`=?, `coach_id`=?, `date_updated`=?, `start_date`=?,`start_time`=?,`end_date`=?,`end_time`=?, `placement_type`=? WHERE `id`=?',
			[
				$placement_id,
				$learnerID,
				$coachID,
				$date_updated,
				$POST['start_date'],
				$POST['start_time'],
				$POST['end_date'],
				$POST['end_time'],
				$placement_type, // This is the bronze, silver or gold placement type.
				$row_id
			]);

		// If the update to the placement attended works, return true.
		if($result!==false)
		{	
			return true;
		}
		// If the update to the placement attended fails, return false.
		if($result==false)
		{
			return false;
		}

	}

	/**
	 * Update the placement for the learner.
	 * @param  Array $POST The new learner placement details posted from the form to update.
	 * @return Boolean or result array to be returned.
	 */
	public function updatedPlacementInfo($POST)
	{
		unset($POST['save_edit_placement']); // Unset the HTML form flag.

		$placement_attended_id = $POST['paid'];

		// Search the tables on the placement_id and get the learner, coach and student details back. 
		if($result = $this->conn->select
		(
		"
			SELECT *,
				 `coach`.`name` as `coach_name`,
				 `coach`.`phone` as `coach_phone`,
				 `coach`.`email` as `coach_email`,
				 `placement`.`name` as `placement_name`,
				 `wpa`.`id` as `paid`

			FROM `we_placement_attended` `wpa`

			LEFT OUTER JOIN `we_placement` `placement`
			ON `placement`.`id` = `wpa`.`placement_id`

			LEFT OUTER JOIN `ext_a_learner` `learner`
			ON `learner`.`person_code` = `wpa`.`learner_id`

			LEFT OUTER JOIN `we_coach` `coach`
			ON `coach`.`id` = `wpa`.`coach_id`

			WHERE `wpa`.`id`=?


		",[$placement_attended_id]

			) // End select
		) // End if 
		{
			return $result; 
		}

		// No results, so return false to the template.
		else
		{
			return false;
		}
	}

	/**
	 * This function takes the details for the placement you're adding.
	 * Various checks are undertaken on the details, in a later function call, to ensure all is ok i.e phone number. 
	 * @param  [type] $POST [description]
	 * @return [type]       [description]
	 */
	public function create_wp_placement($POST)
	{
		// Drop this flag from the HTML form, we don't need this now.
		unset($POST['create_wp_placement']);

		//echo '<pre>';print_r($POST);die;

		// Sort the vars, remove the spaces etc.
		$this->name = $POST['name'];
		$this->address = $POST['address'];
		$this->post_code = $POST['post_code'];
		$this->phone = str_replace(' ', '', $POST['phone']);

		$this->email = $POST['email'];
		$this->manager = $POST['manager'];
		$this->sfplaces = $POST['sfplaces']; // The amount of places for the leaners.

		$this->username = $POST['username'];
	
		/**
		 * Does the form have any issues with the input it's received.
		 * @var [strings] Mobile, phone and email address.
		 * If no issues, an empty array is returned - otherwise, array has errors and is returned to the form to alert the user.
		 */
		$errors = $this->validateFormInput($this->phone,$this->email);
		$id=NULL;

		/**
		 * An empty array has been returned so no errors. 
		 * Next check will be if this is already in the table as a coach. If already there, an error is returned.
		 */
		if(empty($errors))
		{
			// Check if the placement already exists in the table of placements so we don't get a duplicate. 
			$duplicate = $this->checkForDuplicatePlacement($id,$this->name,$this->address,$this->post_code,$this->phone,$this->email,$this->manager,$this->username);

			// This is a duplicate so tell the user
			if($duplicate===true)
			{
				//echo 'STOP here';die;
				// Populate the error array on the form.
				return array('duplicate'=>'This is a duplictate and there is a <strong>placement with the same details</strong>.');
			}

			// Not a duplicate, so add them to the coach table `we_coach`.
			else
			{
				if($this->addPlacementToTable()!==false)
				{
					return array('success'=>'Placement successfully added.');
				}
			}
				
		}

		/**
		 * Error array has content so return this to the 'Add a coach' form to let the user know.
		 * The user will see the output as errors in the form.
		 * Errors are handled by the jQuery in the form/template
		 */
		else
		{
			return $errors;
		}
	}

	/**
	 * Add the placement to the `we_placements` table.
	 */
	public function addPlacementToTable()
	{
		// Date and time now.
		$date = date('Y-m-d H:i:s');

		// Start building the query to insert the coach.
		$query = 'INSERT INTO `we_placement` (`name`,`address`,`post_code`,`manager`,`phone`,`date_created`,`username`,`email`,`sf_places`) VALUES(?,?,?,?,?,?,?,?,?)';

		// Send the query and params off to be executed by the SQL class.
		return $this->conn->insert($query,[$this->name,$this->address,$this->post_code,$this->manager,$this->phone,$date,$this->username,$this->email,$this->sfplaces]);
	}

	/**
	 * A function to validate some form input from the add a coach form.
	 * @param  [string] $phone This is the phone number added. (PHP probably tyope converts this to a number).
	 * @param  [string] $mobile [The mobile phone number entered] (PHP probably tyope converts this to a number).
	 * @param  [string] $email  [The email address entered].
	 * @return [array] This will be either empty array with no errors, or have errors set on it. 
	 * The error array is then finally returned to the calling statement and can they can be output as HTML in the form. 
	 */
	public function validateFormInput($phone,$email)
	{

		// Placeholder for error handling the form input. This will either have errors or be empty.
		$error = array();

		// Check the form input for numerics and length.
		if(is_int($phone)==false || str_len($phone)<11  || 
			filter_var($email, FILTER_VALIDATE_EMAIL)==false)
		{
		
			// Check the email is correct at this stage and if not return false.
			if(filter_var($email, FILTER_VALIDATE_EMAIL)==false)
			{
				$error['email_message'] = 'That isn\'t a valid email'; 
			}
		
			if(is_numeric($phone)==false)
			{
				$error['phone_message_numeric'] = 'Phone number needs to be numeric.';
			}

			if(strlen($phone)<11)
			{
				$error['phone_message_length'] = 'Not enough numbers to be a phone number.';
			}

			return $error;
		}

	}

	/**
	 * A function to check of a duplicate coach in the database.
	 * @param  [string] $name   The name of the prospective coach
	 * @param  [string] $phone  The phone number of the prospective coach
	 * @param  [string] $mobile   The mobile phone number of the prospective coach
	 * @param  [string] $email    The email address of the prospective coach
	 * @param  [string] $username The username of the prospective coach
	 * @return 
	 */
	public function checkForDuplicatePlacement($id=NULL,$name,$address,$post_code,$email,$username)
	{
		// Get all the placement instances associated with this placement id passed via get from the login page. 
		$result = $this->conn->select('SELECT * FROM `we_placement` WHERE `name` LIKE ? AND `address` LIKE ? AND `post_code` LIKE ?',[$name,$address,$post_code]);

		// If there is an id to the row, that means one already exists and this is a duplicate.
		if($result[0]['id'])
		{
		
			if(isset($id) && $result[0]['id']==$id)
			{
				return false; // Not a duplicate
			}

			// Not an update but an insert so this is fine.
			return true; // This is a duplicate
		}

		// No duplicate so the insert can proceed.
		else
		{
			//echo 'NOT duplicate';die;
			return false; // // This is NOT a duplicate
		}
	}

	/**
	 * Function to get all the placement.
	 * @return [array] Ann array of all the current placements. 
	 */
	public function getAllPlacements()
	{
		// Create the query to query the coqach table to get the coach details. Latest first.
		$query = "SELECT * from `we_placement` ORDER BY `date_created` DESC";

		// Get the coach info if there is any, if not, this is not a coach.
		return $this->conn->select($query);
	}


	/**
	 * This function takes the `we_placement` id and then calls that row and gets the placement id.
	 * @param  [string] $placment_id This is the placement id of the row that has been selected form the form to be edited.
	 * @return [array] This is the row array of the information from the `we_placement` table.
	 */
	public function getPlacementByID($placement_id)
	{
		// Create the query to query the coqach table to get the coach details. Latest first.
		$query = "SELECT * from `we_placement` WHERE `id`=?";

		// Get the coach info if there is any, if not, this is not a coach.
		return $this->conn->select($query,[$placement_id]);
	}

	/**
	 * Get the remaining number of placements for the placement using the `we_placement` id.
	 * sf_places minus places_count gives you the remaining places.
	 * @param  [string] $placement_id This the placement id of the placement passed in.
	 * @return [string] Returns a string number.
	 */
	public function getPlacementCountByID($placement_id)
	{
		// Create the query to query the coqach table to get the coach details. Latest first.
		$query = "SELECT (`sf_places`-`places_count`) AS `places` from `we_placement` WHERE `id`=?";

		// Get the coach info if there is any, if not, this is not a coach.
		return $this->conn->select($query,[$placement_id]);
	}

 	/**
 	 * Update the placement.
 	 * @param  [array] $POST The updated information from the form. 
 	 * @return [array] Either this will be the array of errors or a message saying the update was a success.
 	 */
	public function update_wp_placement($POST)
	{
		unset($POST['update_wp_placement']); // Drop this flag from the HTML form, we don't need this now.
		
		// Unpackage & sort the vars, remove the spaces etc.
		$id = $POST['row_id'];
		$this->name = $POST['name'];
		$this->address = $POST['address'];
		$this->post_code = $POST['post_code'];
		$this->phone = str_replace(' ', '', $POST['phone']);

		$this->email = $POST['email'];
		$this->manager = $POST['manager'];
		$this->remaining_places = $POST['remaining_places']; // The amount of places for the leaners.

		$this->username = $POST['username'];

		/**
		 * Test to see if this is duplicate 
		 */
		if($this->checkForDuplicatePlacement($id,$this->name,$this->address,$this->post_code,$this->email,$this->username)==false)
		{

			$result = $this->conn->update('UPDATE `we_placement` SET `name`=?, `address`=?, `post_code`=?,`phone`=?,`email`=?,`manager`=?,`remaining_places`=?,`username`=? WHERE `id`=?',[$this->name,$this->address,$this->post_code,$this->phone,$this->email,$this->manager,$this->remaining_places,$this->username,$id]);

			return array('success'=>'The record was updated successfully');	

		}

		/**
		 * This is not a duplicate so the update can go ahead.
		 */
		else
		{
			return array('duplicate'=>'This is a duplictate and there is a <strong>placement with the same details</strong>.');
		}
	}

	/**
	 * This function receives the username & the password chosen by staff in: /workexperience/CRM/employer.php.
	 * It both updates and creates a username and password by checking the table first and then performining either an INSERT or UPDATE. (This is determined in the HTML form as tables are checked)
	 * This function uses both the salts set in the class: Login (instantiated above).
	 * Take the username and password, add it to the placement table with the placemment id.
	 * At the end, make a call to send the user an email and username by email.
	 * @param  [array] $POST Uses the posted username, password from the employer section form.
	 * @return [boolean] This is the success or failure of the update.
	 */
	public function createPasswordAndUsername($POST)
	{
		// These are our password salts, production would see them more complex!
		$salt1 = $this->login->salt1; // These are the same in the login class and pulled from there.
		$salt2 = $this->login->salt2; // These are the same in the login class and pulled from there.

		// Unpack some vars from the form posted and unset a form flag.
		$placement_id = $POST['save_edit_placement'];
		unset($POST['save_edit_placement']); // Unset the form flag.
		$username = $POST['username'];
		$password = $POST['password'];
		$date_updated = date('Y-m-d H:i:s', strtotime('now')); // The date updated stamp to uniquely identify.
		
		// Hash the POSTED password.
		$hashedPassword = SHA1($salt1.$password.$salt2);

		// Query the placement id
		$query = 'SELECT * FROM `we_placement` WHERE `id`=?';

		// Check if this password is already saved for this user by pulling the row
		$result = $this->conn->select($query,[$placement_id]);

		// Check if the present hashed password is the same as this one POSTED and if it is, don't allow it. 
		if($result[0]['password'] == $hashedPassword)
		{
			return array('password'=>'Sorry, that password is already assigned to this account');
		}

		// Build the query to add the new password.
		$query = 'UPDATE `we_placement` SET `username`=?,`password`=?,`date_updated`=? WHERE `id`=?';


		// Update the table with the new password.
		$result = $this->conn->update($query,[$username,$hashedPassword,$date_updated,$placement_id]);


		// password update was successful.
		if($result!==FALSE)
		{
			// If the email to the user telling them their password & email sends ok, return true.
			if($this->sendPasswordAndUsernameMail($placement_id,$username,$password)===true)
			{
				// Return true to the front end can use a message to le the user know update was a success.
				return true;
			}

			// The enail failed in sending to the user.
			else
			{
				//echo 'HERE';die;
				return false;
			}

			
		}

		// Password update failed to the table with the users details.
		else
		{
			return false;
		}	
		
	}

	/**
	 * Send an email to the user letting them know their username and email has been changed.
	 * @param  [string] $placement_id This is the placement id of the placement in `we_placement`
	 * @param  [string] $username This is the username selected for the user.
	 * @param  [string] $password This is the password selected for the user.
	 * @return Boolean This is either true to a successful update to the username and password and that the email has been sent successfully.
	 */
	public function sendPasswordAndUsernameMail($placement_id,$username,$password)
	{
		// Get the placement details from the placement_id passed in. 
		$employer = $this->conn->select('SELECT * FROM `we_placement` WHERE `id`=?',[$placement_id]);
		
		// Get some vars to compose the message
		$name = $employer[0]['manager'];
		$company = $employer[0]['name'];
		$email = $employer[0]['email'];
		

$message=<<<message

Hi {$name},

We have launched a new Work Experience application at Bolton College in order to fully manage our work experience students placements. The placements can now be accessed, updated and completed online by the learner through the application. 

As '{$company}' are a business within our system and could potentially have students on placement, your credentials have been created and are below. Once signed in, you can manage your learner reviews and will have access to a summary for each student you complete a review for. 

Your login details are as follows:

URL: https://www.boltoncc.ac.uk/workexperience/employer/
Username: {$username}
Password: {$password}

Regards,

Bolton College.

message;
	
		// Compose end the message to the person.
		$to = $email;
		$from = 'noreply@boltoncc.ac.uk';
		$subject = "New Work Experience application";
		$headers = 'From: noreply@boltoncc.ac.uk' . "\r\n" .
	    'Reply-To: noreply@boltoncc.ac.uk' . "\r\n" .
	    'X-Mailer: PHP/' . phpversion();

		if(mail($to, $subject, $message, $headers))
		{
			return true;
		}
		else
		{
			return false;
		}

	}

	/**
	 * A function to add a user as withdrawn in `we_student_withdrawals`.
	 * The function also resets the learner to 'not work ready' in the `elp_workready2` table.
	 * The function finally sends an email to the coach, LDM and the CL of the learner. 
	 * This updates the all tables respectively.
	 * @param  [array] $POST This is the details of the withdrawal.
	 * @return boolean This is TRUE or FALSE depending on if the call was successful.
	 */
	public function withdrawLearner($POST)
	{
		
		// Unset the form flag sent in from the form. 
		unset($POST['submit_withdraw_learner']); // Unset the form flag.

		// Unpack some vars from the form.
		$date_created = date('Y-m-d H:i:s');//date('Y-m-d', strtotime('now'));
		$placement_attended_id = $POST['paid'];
		$placement_id = $POST['placement_id'];
		$details_of_withdrawal = $POST['details_of_withdrawal'];
		$learner_id = $POST['learner_id'];

		// Build the query
		$query = 'INSERT INTO `we_student_withdrawals` (`placement_attended_id`,`placement_id`,`learner_id`,`date_created`,`details_of_withdrawal`,`name`) VALUES(?,?,?,?,?,?)';

		// Add the learner entry to the `we_student_withdrawals` table for the withdrawal.
		$withdrawLearner = $this->conn->insert($query,
		[

			$placement_attended_id,
			$placement_id,
			$learner_id,
			$date_created,
			$details_of_withdrawal,
			$this->learner->learnerDetailsByID($learner_id) // Get the learner name from the ID.
		]);

		// Set up a string to handle the message the user if, no longer work ready.
		$notWorkReady = '';

		// if the user is withdrawn with: 'Failed with learner issues' set them to not work ready and create a var for the email. 
		if($POST['details_of_withdrawal']=='Failed with learner issues')
		{
	
			// Update the workready2 table to show the learner is now not work ready.
			$query = "UPDATE `elp_workready2` SET `Q1_tutor` = ?, `Q2_tutor` = ?,`Q3_tutor` = ?,`Q4_tutor` = ?,`Q5_tutor` = ?,`Q6_tutor` = ?,`Q1_LDM` = ?,`Q2_LDM` = ?,`Q3_LDM` = ?,`Q4_LDM` = ?,`Q5_LDM` = ?,`Q6_LDM` = ? WHERE `learner`=?";

			$makeNotWorkReady = $this->conn->update($query,
			[
				0,0,0,0,0,0,0,0,0,0,0,0,$learner_id
			]);

			// Set work ready off variable for the email.
			$notWorkReady = 'This learner has now been <em>confirmed</em> as <strong>no longer \'work ready\'</strong>. If this has been done in error, you will need to change the withdraw option to one of the other options to get the user back to work ready status.';
		}

		/**
		 * Send an email to the coach, CLs and LDMS to say the learner has been withdrawn and give the reason.
		 */

		// Get the learners full name from the learner id. 
		$lFullName = $this->learner->learnerDetailsByID($learner_id);

		// Get the coach id from the placement attended id. 
		$coachID = $this->conn->select('SELECT `coach_id` FROM `we_placement_attended` WHERE `id`=? ',[$placement_attended_id])[0]['coach_id'];

		// Get the coach details from the coach id.
		$coach = $this->learner->learnerWorkCoachByCoachID($coachID);
		$coachName = $coach[0]['name'];
		$coachFirst = explode(' ', $coachName)[0];
		$coachEmail = $coach[0]['email'];
		$coachPhone = $coach[0]['phone'];
		$coachMobile = $coach[0]['mobile'];

		// Get the learners LDMs full details for the learner.
		$LDMDetails = $this->conn->select
		(
			"SELECT DISTINCT `tutor_name` FROM `ext_ebs_data_tutorgroup` `tgp` INNER JOIN `ext_ebs_data_learnergroup` `lgp` ON `tgp`.`unique_group_name` = `lgp`.`unique_group_name` WHERE `lgp`.`course_title` LIKE ? AND `lgp`.`learner` = ?",
			[
				'%Learning and Development%',
				$learner_id
			]
		);

		// Get the CL full details for the learner.
		$clDetails = $this->conn->select("SELECT DISTINCT `elp`.`cl` FROM `ext_ebs_data_tutorgroup` `tgp` INNER JOIN `ext_ebs_data_learnergroup` `lgp` ON `tgp`.`unique_group_name` = `lgp`.`unique_group_name` 
					LEFT OUTER JOIN `elp_curriculums` `elp` ON binary `elp`.`section`= binary `lgp`.`section`

						WHERE `lgp`.`course_title` 
								LIKE '%Learning and Development%' AND `lgp`.`learner` = ?",
		[
			$learner_id
		]
		);

		// Get the CL email. 
		$clEMail = $clDetails[0]['cl'];

		// Get the LDMs full name.
		$LDMname = $LDMDetails[0]['tutor_name'];

		// Get the LDMs first name
		$LDMfirst = explode(' ', $LDMDetails[0]['tutor_name'])[0];

		// Get the LDMs full email address from  `ext_ebs_data_tutorgroup` and `ext_userid`
		$lDMEmailAddress = $this->conn->select("SELECT DISTINCT `email` FROM `ext_ebs_data_tutorgroup` `etg` 
		LEFT OUTER JOIN `ext_userid` `euid` ON binary `etg`.`tutor` = binary `euid`.`idnumber`  
		WHERE `etg`.`tutor_name` LIKE '%{$LDMname}%'");

		// This is the LDMs email address.
		$lDMEmail = $lDMEmailAddress[0]['email'];

		// Compose the email body
		
		//$to  = "{$clEMail},{$lDMEmail},{$coachEmail}";
		$to = 'mike.thornley@boltoncc.ac.uk';

		// Compose the subject line. 
		$subject = "Workspace app notification: {$lFullName} has been withdrawn from their work placement";

		// Prepare the body of the email
$message = <<<SOQ

<strong>Message from the WorkSpace app:</strong>
<br/>

<p>The learner: {$lFullName} [$learner_id] has been <span style='color:red;font-weight:bold;'>withdrawn</span> by coach {$coachName} from their work placement.</p>

<p><strong>Withdrawal reason:</strong><br/>
<span style='color:red;font-weight:bold;'>'{$details_of_withdrawal}'</span></p>

<p>{$coachFirst} can be contacted by email at: {$coachEmail} or by calling: {$coachPhone} and {$coachMobile}.</p>

<p>{$notWorkReady}</p>

<p>Regards</p>

<p>{$coachName} (Career coach).</p>

<p>This email is automatically generated and this address mailbox doesn't receive messages from users. </p>
SOQ
;

		// Check that they have been both withdrawn and set to not work ready and then send the email.
		if($withdrawLearner!=false)
		{
			mail($to, $subject, $message, $this->emailNotifications->headers);
			return array('message'=>': The learner was added as withdrawn');
		}
		// The update(s) failed.
		else
		{
			return false;
		}
	}

	/**
	 * This is to update the withrawal information that has already been submitted for the student.
	 * @param  [array] $POST [These are the form variables for the learner.]
	 * @return [bollean false or array] [Either a FALSE is fails or an array message to say the update was successful]
	 */
	public function updateWithdrawLearner($POST)
	{

		unset($POST['update_withdraw_learner']); // Unset the form flag.

		$date_updated = date('Y-m-d H:i:s');//date('Y-m-d', strtotime('now'));
		$placement_attended_id = $POST['paid'];
		$placement_id = $POST['placement_id'];
		$details_of_withdrawal = $POST['details_of_withdrawal'];
		$learner_id = $POST['learner_id'];

		// Build the query to withdraw the learner.
		$query = 'UPDATE `we_student_withdrawals` SET `details_of_withdrawal`=? WHERE `placement_attended_id`=? AND `placement_id`=?';

		// Execute the query to withdraw. 
		$result = $this->conn->update($query,[$details_of_withdrawal,$placement_attended_id,$placement_id]);

		// Unless the learner is withdrawn due to learner issues, set them to 'work ready': on!
		if(
		$POST['details_of_withdrawal']=='Failed with employer issues' || 
		$POST['details_of_withdrawal']=='Failed with both employer and learner issues'
		)
		{
			// Update the workready2 table to show the learner is now not work ready.
			$query = "UPDATE `elp_workready2` SET `Q1_tutor` = ?, `Q2_tutor` = ?,`Q3_tutor` = ?,`Q4_tutor` = ?,`Q5_tutor` = ?,`Q6_tutor` = ?,`Q1_LDM` = ?,`Q2_LDM` = ?,`Q3_LDM` = ?,`Q4_LDM` = ?,`Q5_LDM` = ?,`Q6_LDM` = ? WHERE `learner`=?";

			$makeNotWorkReady = $this->conn->update($query,
			[
				1,1,1,1,1,1,1,1,1,1,1,1,$learner_id
			]);

		}

		// If the user is withdrawn due to 'Failed with learner issues': withdraw.
		if($POST['details_of_withdrawal']=='Failed with learner issues')
		{
			// Update the workready2 table to show the learner is now not work ready.
			$query = "UPDATE `elp_workready2` SET `Q1_tutor` = ?, `Q2_tutor` = ?,`Q3_tutor` = ?,`Q4_tutor` = ?,`Q5_tutor` = ?,`Q6_tutor` = ?,`Q1_LDM` = ?,`Q2_LDM` = ?,`Q3_LDM` = ?,`Q4_LDM` = ?,`Q5_LDM` = ?,`Q6_LDM` = ? WHERE `learner`=?";

			$makeNotWorkReady = $this->conn->update($query,
			[
				0,0,0,0,0,0,0,0,0,0,0,0,$learner_id
			]);

		}

		// Make sure the withdraw is successful.
		if($result!=false)
		{
			// They're withdrawn so the row to show this is returned.
			return array('message'=>': The learners withdrawal details have been updated');
		}
		// The learner hasn't been withdrawn so return false.
		else
		{
			return false;
		}
	}

	/**
	 * Check if the learner has been withrawn.
	 * The learner `placement_attended_id` is queried against the table: `we_student_withdrawals`
	 * @param  [string] $placement_attended_id This is the `placement_attended_id` uniquely identifying the row. 
	 * @return boolean or array. If withdrawn an array of the row is returned. If not, false returned.
	 */
	public function checkIfWithdrawn($placement_attended_id)
	{
		// Build the query
		$query = 'SELECT * FROM `we_student_withdrawals` WHERE `placement_attended_id`=?';

		// Query the table. 
		$result = $this->conn->select($query,[$placement_attended_id]);

		// Query the table to see if the learner has been withdrawn.
		if($result!=false)
		{
			// They're withdrawn so the row to show this is returned.
			return $result;
		}
		// The learner hasn't been withdrawn so return false.
		else
		{
			return false;
		}
	}
}


