<?php

/**
 * Autoloader for including all the classes.
 */
spl_autoload_register(function ($class_name) {
    include $class_name .' .php';
});

// Stop the cache
//require_once '../stop_cache.php';

// Display errors
ini_set('display_errors', 'On');

/**
 *  Class to handle the autocomplete functionality in the coaches section.
 *  Also handles the look up of the user after the autocomplete.
 *  This class also acts as a controller and looks up the learner from the both the `ext_a_learner` initially for the autocomplete and then finally, `placement_attended` table once they're found with the autocomplete.
 */
class AutoComplete extends Main
{
	public function __construct()
	{	
		try
		{
			// Call the parent first and connect to the DB. $this->conn
			parent::__construct();
		}

		// Catch some PDO errors.
		catch (PDOException $e)
		{
			throw new Exception('PDO Database connection failed to connect to SID failed in line: ' .__LINE__ . ' of file: '. __FILE__ . $e->getMessage());
		}
	}

	/**
	 * Query the `ext_a_learner` table and populate the search box as an autocomplete
	 * @param  [string] $searchTerm The search term entered in to the search box.
	 * @return json This is echo back to the index.php page and the Jquery in there picks it up and used it as the value in the form for the autocomplete. 
	 * Stuck a 500 limit on this as the list is rebuilt each time requiring the full records and this slows things down. 
	 */
	public function autoComplete($searchTerm)
	{
		$return_arr = array();

		// Query the table using the selectAutoComplete function of the SQL class. This just returns the $stm and we are using this and then iterating over that, instead of the result set.
		$stmt = $this->conn->selectAutoComplete('SELECT `forename`,`surname`,`person_code` FROM `ext_a_learner` WHERE `forename` LIKE :term OR `person_code` LIKE :term OR `surname` LIKE :term ORDER BY `surname` DESC LIMIT 500',array('term'=> "%$searchTerm%"));

	   	while($row = $stmt->fetch())
	    {
	        $return_arr[] =  $row['forename'] .' '. $row['surname'].' '. $row['person_code'];
	    }

	    /* Toss back results as json encoded array which jQuery in index.php will pick up. */
	    echo json_encode($return_arr);
	}

	/**
	 * Function to get all the placements for the autocomplete section of the learner page at: workexperience/CRM/learner.php
	 * @param  [string] $searchTerm [The string the user begins to enter]
	 * @return [json] [JSON is returned to the jQuery block in the page]
	 */
	public function autoCompletePlacement($searchTerm)
	{
		$return_arr = array();

		// Query the table using the selectAutoComplete function of the SQL class. This just returns the $stm and we are using this and then iterating over that, instead of the result set.
		$stmt = $this->conn->selectAutoComplete('SELECT `name`,`id`,`address` FROM `we_placement` WHERE `id` LIKE :term OR `name` LIKE :term OR `address` LIKE :term ORDER BY `name` DESC LIMIT 500',array('term'=> "%$searchTerm%"));

	   	while($row = $stmt->fetch())
	    {
	        $return_arr[] =  $row['id'] .' '. $row['name'].' '. $row['address'];
	    }

	    /* Toss back results as json encoded array which jQuery in index.php will pick up. */
	    echo json_encode($return_arr);
	}

	/**
	 * Function to get all the coaches for the autocomplete section of the learner page at: workexperience/CRM/learner.php
	 * This function also excludes the admins, so they are not shown in the 
	 * @param  [string] $searchTerm [The string the user begins to enter]
	 * @return [json] [JSON is returned to the jQuery block in the page]
	 */
	public function autoCompleteCoach($searchTerm)
	{
		$return_arr = array();

		// Query the table using the selectAutoComplete function of the SQL class. This just returns the $stm and we are using this and then iterating over that, instead of the result set.
		$stmt = $this->conn->selectAutoComplete('SELECT `name`,`email`,`username`,`id`,`admin` FROM `we_coach` WHERE `name` LIKE :term OR `email` LIKE :term OR `username` LIKE :term ORDER BY `name` DESC LIMIT 500',array('term'=> "%$searchTerm%"));

		
		while($row = $stmt->fetch())
	    {
	    	if($row['admin']==1) continue; // We only want the coaches, so exclude the admins. 
	        $return_arr[] =  $row['name'] .' | '. $row['email'].' | '. $row['username'].' | '. $row['id'];
	    }

	    /* Toss back results as json encoded array which jQuery in index.php will pick up. */
	    echo json_encode($return_arr);
	}


	/**
	 * This is the person who has been searched from the result of the autocomplete returning a result, then that person being queried against the placement attended table. 
	 * @param  array $POST This is the forename, surname and the person id of the learner
	 * @return boolean false or the results as an array.
	 */
	public function getResultRow($POST)
	{
		$return_arr = array();

		// Here we have the firstname, surname and ID, to separate that to separate array.
		$array = explode(' ',$POST['learner_search']);

		$firstname = $array[0]; // Firstname
		$surname = $array[1]; // Surname
		$learner_id = $array[2]; // ID

		// We now know what specifically what we want, let's query.
		$stmt = $this->conn->selectAutoComplete('SELECT * FROM `we_placement_attended` WHERE `learner_id` = :learner_id',array('learner_id' => $learner_id));

		// One result row per person.
		while($row = $stmt->fetch())
		{
			if(empty($row)) return false; // No result found so return false.
			$return_arr[] =  $row; // Return the result set.
		}

		return $return_arr; // Return the final result row.
	}

	public function autoCompleteAllPlacements($searchTerm)
	{
		$return_arr = array();

		// Query the table using the selectAutoComplete function of the SQL class. This just returns the $stm and we are using this and then iterating over that, instead of the result set.
		$stmt = $this->conn->selectAutoComplete("SELECT `learner_id` FROM `we_placement_attended` WHERE `learner_id` LIKE :term LIMIT 500",array('term'=> "%$searchTerm%"));

	   	while($row = $stmt->fetch())
	    {
	        $return_arr[] =  $row['learner_id'];
	    }

	    print_r($return_arr);

	    /* Toss back results as json encoded array which jQuery in index.php will pick up. */
	    echo json_encode($return_arr);
	}

	public function autoCompleteIndex($searchTerm)
	{
		$return_arr = array();

		// Query the table using the selectAutoComplete function of the SQL class. This just returns the $stm and we are using this and then iterating over that, instead of the result set.
		$stmt = $this->conn->selectAutoComplete('SELECT `forename`,`surname`,`person_code` FROM `ext_a_learner` WHERE `forename` LIKE :term OR `person_code` LIKE :term OR `surname` LIKE :term ORDER BY `surname` DESC LIMIT 500',array('term'=> "%$searchTerm%"));

	   	while($row = $stmt->fetch())
	    {
	        $return_arr[] =  $row['forename'] .' '. $row['surname'].' '. $row['person_code'];
	    }

	    /* Toss back results as json encoded array which jQuery in index.php will pick up. */
	    echo json_encode($return_arr);
	}

}


