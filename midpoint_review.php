<?php

/**
 * Get the PSR4 autoloader and load all the classes required.
 */
spl_autoload_register(function ($class_name) {
    include 'classes/'.$class_name . '.php';
});

// Stop the cache
require_once 'stop_cache.php';

// Set a new instance of the learner.
$learner = new Learner($_GET['placement_attended_id'],$_GET['studID']);
$coach = new CareerCoachReview();
// Instance of placement
$placement = new PlacementAttended($_GET['studID']);

// Check the work placement agreement is signed. 
$agreement_signed = $placement->agreementSigned($_GET['studID'],$_GET['placement_attended_id']);

// Make sure the user has signed the agreement.
if($agreement_signed!=true)
{
  // Direct back to the agreement page to sign. 
  header("location: agreement.php?placement_attended_id={$_GET['placement_attended_id']}&studID={$_GET['studID']}");
}

// See if there are learner details
if($learnerDetails = $learner->learnerDetails()!=false)
{
  $learnerDetails = $learner->learnerDetails();
}

// See if there are coach details
if($coachDetails = $coach->coachDetails()!=false)
{
  // This is a coach. This is their details.
  $coachDetails = $coach->coachDetails();
  // As this is a coach, get all the learner details for the page below.
  $allLearnerPlacements = $coach->getPlacements();
}

// Gets the learner details row and the placement details for that learner.
$learnerDetails = $learner->learnerDetails();

/**
 * Check if this is a learner only viewing their own part of the app - learner ids have to match.
 * The Shib learner id has to match the URL from the browser, IF they're not a coach. 
 * As usual the coach sees it all.
 * If the user is not authorised, $_SERVER['REQUEST_URI'] is used to keep them on their page.
 */
if(!$coach->isCareerCoach() && $coach->doesTheLearnerIdMatch($_GET['studID'])==false)
{
  // The user is not a coach and not the learner requesting the URL so keep them where they are.
  header('location: /workexperience/');
}

// Student agrees with their midpoint review.
if(isset($_POST['midpoint_review_agree']) && isset($_POST['agree_midpoint_button']))
{
  //echo '<pre>';print_r($_POST);die;
  $studentAgreementSigned = $learner->addStudentMidpointReview($_POST);
}

// Quickly just get the placement attended id. 
$placement_attended_id = $_GET['placement_attended_id'];

// Get the mid point review date
$mpReview = $learner->getMidpointReview($placement_attended_id);
//print_r($mpReview);
?>

<script type="text/javascript">
var studID = "<?php echo $_GET['studID'];?>";
//alert(studID);
</script>



<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>My Workspace app</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/theme.css" rel="stylesheet">

    <!--<link rel="stylesheet" type="text/css" href="css/ada.css" />-->


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

   <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="crossorigin="anonymous"></script>

    <!-- Font Awesome 5 -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
    <!-- ADA CSS -->
    

  </head>

  <body>

    <!-- Fixed navbar -->
    <!--<nav class="navbar navbar-inverse navbar-fixed-top">-->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/workexperience/">My Workspace app</a>
        </div>

        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            
              <li><a href="index.php">Choose placement</a></li>
              <li><a href="introduction.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Introduction</a></li>
              <li><a href="location.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Where is my placement</a></li>
              <li><a href="agreement.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Work placement agreement</a></li>
              <li><a href="aims.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Aims and objectives</a></li>
              <li><a href="logbook.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Logbook</a></li>
              <li><a href="placement_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Your work placement review</a></li>
              <li><a href="employer_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Employer review</a></li>
              <li><a href="coach_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Coach review</a>
              </li>
              <li class="active"><a href="midpoint_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Midpoint Review</a>
              </li>
              <li><a href="debug.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Report a bug</a>
              </li>
            <!--<li><a href="#about">About</a></li>-->
            <!--<li><a href="#contact">Contact</a></li>-->
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav><!-- end fixed navbar -->

    <div class="container-fluid theme-showcase" role="main">
      	<div class="row"><!--Start of row -->
    			<div class="col-md-12">
    				<div class="table-responsive">
    					<table class="table table-bordered">
    		    			<thead>
    		      				<tr>
    						        <th>Student name</th>
    						        <td><?=$learnerDetails[0]['name']?></td>
    						        <th>Student ID</th>
    						        <td><?=$learnerDetails[0]['person_code']?></td>
                        <th>Coach name</th>
                        <td><?=$coach->getCoachNameForLearner($_GET['placement_attended_id'])?></td>
    		      				</tr>
    		    			</thead>	
    	  				</table>
    				</div>	
  			</div>
  		</div> <!--End of row -->

  		<div class="row" id='test'><!--Start of row -->
          <!-- Tabbed content left-->
          <div class="col-md-12">
               <ul class="nav nav-tabs">
                <li><a href="index.php">Choose placement</a></li>
                <li class="active"><a href="introduction.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Introduction</a></li>
                <li><a href="location.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Where is my placement</a></li>
                <li><a href="agreement.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Work placement agreement</a></li>
                <li><a href="aims.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Aims and objectives</a></li>
                <li><a href="logbook.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Logbook</a></li>
                <li><a href="placement_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Your work placement review</a></li>
                <li><a href="employer_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Employer review</a></li>
                <li><a href="coach_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Coach review</a></li>
                <li class="active"><a href="midpoint_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Midpoint Review</a>
              </li>
              </ul>
          <br/>
        </div><!-- end tabbed content left-->
      </div><!--End of row -->

    <div class="row"><!--Start of row -->
      
	    
		     <div class="col-md-12"><!-- content main full page-->

          <?php if(isset($studentAgreementSigned) && $studentAgreementSigned!=false):?>
            <script>
              $(document).ready(function(){
              $("#success_message").show().delay(5000).fadeOut();
            });
            </script>
          <div class="alert alert-success" id='success_message'>
            <strong>Success!</strong> You have successfully completed your midpoint review.
          </div>
          <?php endif;?>

           <?php if(isset($studentAgreementSigned) && $studentAgreementSigned==false):?>
            <script>
              $(document).ready(function(){
              $("#error_message").show().delay(5000).fadeOut();
            });
            </script>
          <div class="alert alert-danger" id='error_message'>
            <strong>Success!</strong> Your midpoint review has failed to be added.
          </div>
          <?php endif;?>

		    	<h4>Hi <?=$learnerDetails[0]['forename']?>. This is your Midpoint Assessment section.</h4>
          <p>This agreement allows you to review and sign, to confirm you agree with the review conducted by your career coach based on your placement.</p>

          <?php if($mpReview==false): // There is no data on this placement ?>
            <p>Your Midpoint asseement has not yet been completed.</p>
          <?php endif;?>


            <?php if($mpReview!=false): // There is data ?>
            <form method='POST'>

              <br/>

              <h4>Behaviour and Social Skills Knowledge and Technical Skills:</h4>

              <fieldset>
                <p>Does not meet expectations: <input 
                disabled type="radio" name="behaviour" value="does_not_meet_expectations" <?php if(isset($mpReview[0]['behaviour']) && $mpReview[0]['behaviour']=='does_not_meet_expectations') echo 'checked';?>/></p>
                <p>Meet expectations: <input type="radio" disabled name="behaviour" value="meet_expectations" <?php if(isset($mpReview[0]['behaviour']) && $mpReview[0]['behaviour']=='meet_expectations') echo 'checked';?>/></p>
                <p>Exceeds expectations: <input type="radio" disabled name="behaviour" value="exceeds_expectations" <?php if(isset($mpReview[0]['behaviour']) && $mpReview[0]['behaviour']=='exceeds_expectations') echo 'checked';?>/></p>

                <hr/>

                 <p>I agree with the above review of my Midpoint Assessment: <input type='checkbox' id='agree_midpoint' name='midpoint_review_agree' required value="<?=$_GET['placement_attended_id']?>" <?php if(isset($mpReview[0]['learner_signed']) && $mpReview[0]['learner_signed']=='1') echo 'disabled checked'; ?>/>
                  <br/>
                  <br/><input type='text' disabled name='student_name' value="<?php if(isset($mpReview[0]['learner_signed']) && $mpReview[0]['learner_signed']=='1') echo $learnerDetails[0]['name'];?>"/> (Learner)</p>
                   <p><button type='submit' name='agree_midpoint_button' value="<?=$mpReview[0]['id']?>" <?php if($mpReview[0]['learner_signed']=='1') echo 'disabled';?> class="btn btn-primary">Submit</button></p>


                   <!-- Coach signed first -->
                   <?php if($mpReview[0]['learner_signed']=='1' && $mpReview[0]['signed_with_coach']=='1' && $mpReview[0]['learner_signed_first']=='0' ):?>
                   <p>This review was completed on: <strong><?=date('l jS \of F Y \a\t h:i A',strtotime($mpReview[0]['date_created']))?></strong> by <strong><?=$learner->getCoachName($mpReview[0]['coach_id'])?></strong></p>
                   <?php endif;?>

                  <!-- Student signed first -->
                   <?php if($mpReview[0]['learner_signed']=='1' && $mpReview[0]['signed_with_coach']=='0' && $mpReview[0]['learner_signed_first']=='1' ):?>
                    <p>Review was completed on: <strong><?=date('l jS \of F Y \a\t h:i A',strtotime($mpReview[0]['date_created']))?></strong> by <strong><?=$learnerDetails[0]['name']?></strong></p>
                   <?php endif;?>

                   <!-- Student signed first -->
                   <?php if($mpReview[0]['learner_signed']=='1' && $mpReview[0]['signed_with_coach']=='1' && $mpReview[0]['learner_signed_first']=='1' ):?>
                    <p>Review was completed on: <strong><?=date('l jS \of F Y \a\t h:i A',strtotime($mpReview[0]['date_created']))?></strong> by <strong><?=$learnerDetails[0]['name']?></strong></p>
                   <?php endif;?>

              </fieldset>
            </form> <!-- Form ends -->

              <!-- If the coach has signed and has comments, show the comments as read-only -->
              <?php if($mpReview[0]['comments']):?>
                <label for="exampleFormControlTextarea1">Coach midpoint review comments:</label>
                <textarea readonly class="form-control" id="coach_comments" name='coach_comments' rows="5"><?=$mpReview[0]['comments']?></textarea>
              <?php endif;?>

  		    </div><!-- row ends-->
        <?php endif;?>
  
  </div>
</div>

    <!-- footer here -->
    <footer class="footer">
    <hr/>
    	<div class="container">
        	<p style="text-align:center;">Bolton College 2017</p>
      	</div>
    </footer>
    <!-- end footer end -->
  </body>

  <?php 
   // Require the ada pop up box
   require_once('ADA_form.php');
   ?>

  </div>

   <!-- Bootstrap core JavaScript ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script defer src="../learner/adanew_dist/adabundle_v1.5.1.js"></script>
    <script src="js/ie10-viewport-bug-workaround.js"></script>
    <script src="js/bootstrap.js"></script>
   



</html>
