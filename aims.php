<?php


/**
 * Get the PSR4 autoloader and load all the classes required.
 */
spl_autoload_register(function ($class_name) {
    include 'classes/'.$class_name . '.php';
});

// Stop the cache
require_once 'stop_cache.php';

// Display errors
//ini_set('display_errors', 'Off');

// Get a new coach instance
$coach = new CareerCoachReview();

// Set an instance of aims.
$aims = new Aims;

// New instance of coach comments for recording the coach comments
$coachComments = new CoachComments;

/**
 * Check if this is a learner only viewing their own part of the app - learner ids have to match.
 * The Shib learner id has to match the URL from the browser, IF they're not a coach. 
 * As usual the coach sees it all.
 * If the user is not authorised, $_SERVER['REQUEST_URI'] is used to keep them on their page.
 */
if(!$coach->isCareerCoach() && $coach->doesTheLearnerIdMatch($_GET['studID'])==false)
{
  // The user is not a coach and not the learner requesting the URL so keep them where they are.
  header('location: /workexperience/');
}



// Get stuff if $_GET.
if(isset($_GET['placement_attended_id']) && isset($_GET['studID']))
{
  $learner = new Learner($_GET['placement_attended_id'],$_GET['studID']);
  $learnerDetails = $learner->learnerDetails($_GET['studID']);
  // Instance of placement
  $placement = new PlacementAttended($_GET['studID']);
  // Get the users aims previously chosen if there are any.
  $learnerAimsChosen = $aims->learnerAims($_GET['studID'],$_GET['placement_attended_id']); 
  //Learner selected aims (just 4, all formatted)
  $formattedAndSelectedAims = $aims->learnerAimsFormatted($_GET['studID']);
}

// Get stuff if $_POST. As this will rinse all the get vars.
if(isset($_POST['placement_attended_id']) && isset($_POST['studID']))
{
  $learner = new Learner($_POST['placement_attended_id'],$_POST['studID']);
  $learnerDetails = $learner->learnerDetails($_POST['studID']);
  $placement = new PlacementAttended($_POST['studID']);
  // Get the users aims previously chosen if there are any.
  $learnerAimsChosen = $aims->learnerAims($_POST['studID'],$_POST['placement_attended_id']); 
  //Learner selected aims (just 4, all formatted)
  $formattedAndSelectedAims = $aims->learnerAimsFormatted($_POST['studID']);
}

// Check the work placement agreement is signed. 
$agreement_signed = $placement->agreementSigned($_GET['studID'],$_GET['placement_attended_id']);

// Make sure the user has signed the agreement.
if($agreement_signed!=true)
{
  // Direct back to the agreement page to sign. 
  header("location: agreement.php?placement_attended_id={$_GET['placement_attended_id']}&studID={$_GET['studID']}");
}

// Get list of both formatted and raw aims so we can use it in the HTML.
$aimsAndObjectives = $aims->displayAimsFormatted();
$aimsAndObjectivesRaw = $aims->displayAims();

// Add the arrays: $aimsAndObjectives,$aimsAndObjectivesRaw to create just one array to use in the form.
// for the options to be selected.
$aimsAndObjectives = array_combine($aimsAndObjectives,$aimsAndObjectivesRaw);

// Check if there has been anything posted by the form. 
if(isset($_POST))
{

  // Add coach comments to the page.
  if(isset($_POST['add_coach_comments']) && $_POST['add_coach_comments'] == '1')
  {
    $add = $coachComments->addCoachComments($_POST, 'aims');
  }

  // Edit one of the coach comments.
  if(isset($_POST['edit_coach_comments']) && $_POST['edit_coach_comments'] == '1')
  {
    //echo 'EDIT';die;
    $edit = $coachComments->editCoachComments($_POST, 'aims');
  }


  //echo 'STOP';die;
  // If the form $_POST is set and submits 4 options and the options haven't been submitted yet.
  if(!$learnerAimsChosen && isset($_POST) && count($_POST)==4)
  {
    // Put the aims in to the aims table for the learner. 
    $addedAimsToTable = $aims->addLearnerAims($_POST,$_GET['placement_attended_id']);
    // Save the details to the DB and immediately refresh the page to show we've the change.
    header("location: aims.php?placement_attended_id={$_GET['placement_attended_id']}&studID={$_GET['studID']}");
  }
}

// The user wants to edit their aims so we can then direct them to the edit template: aims_edit.php.
if($_POST && isset($_POST['edit']) && $_POST['edit']=='1' && isset($_POST['placement_attended_id']) && isset($_POST['studID']) )
{
  header("location: aims_edit.php?placement_attended_id={$_POST['placement_attended_id']}&studID={$_POST['studID']}");
}
// Load up the persons review if there is one. If not, learner review is returning false to use in the bottom edit button.
// If there is no work placement review completed, the aims are ok to still edit, otherwise not. 
$workPlacementReview = new WorkPlacementReview;
$learnerReview = $workPlacementReview->learnerReview($_GET['placement_attended_id'],$_GET['studID']);

// Get an instance of the career coach review.
$coach = new CareerCoachReview;

// See if there are **learner details** from Shibboleth
if($learnerDetails = $learner->learnerDetails()!=false)
{
  $learnerDetails = $learner->learnerDetails();
}

// See if there are **coach details** from Shibboleth
if($coachDetails = $coach->coachDetails()!=false)
{
  // This is a coach. This is their details.
  $coachDetails = $coach->coachDetails();
}

//print_r($coachComments);die;

/**
 * If there are errors on the update to the aims (aims_edit.php) - too many or too little, tell the user on aims.php after redirect.
 * If they are told there is an error and then a comment is added the page still has the error URL flags and then has a message entered, ignore it so the mssage doesn't trigger: $error_message .
 */
if(
  (isset($_GET['error']) && $_GET['error']=='greater' && !isset($_POST['comments_aims']) && !isset($_POST['edit_coach_comments']))

  || (isset($_GET['error']) && $_GET['error']=='less' && !isset($_POST['comments_aims']) && !isset($_POST['edit_coach_comments']))
  )
{
  if($_GET['error'] == 'less')
  {
    $error_message = 'You must choose 4 options for aims';
  }
 if($_GET['error'] == 'greater')
  {
    $error_message = 'You can ONLY choose 4 options for aims';
  }
}

// Get all the coach comments for this placement_attended_id
$coachComments = $coach->getCoachCommentsAims($_GET['placement_attended_id']);

// Decide if the midpoint review should be shown - if 50 days, yes, otherwise, no.
$showMidPointReview = $learner->showMidPointReview($_GET['placement_attended_id']);
//var_dump($showMidPointReview);die;

?>

<script type="text/javascript">
var studID = "<?php echo $_GET['studID'];?>";
//alert(studID);
</script>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>My Workspace app</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/theme.css" rel="stylesheet">

    <!--<link rel="stylesheet" type="text/css" href="css/ada.css"/>-->

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>


    <!-- Bootstrap 3 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Font Awesome 5 -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="crossorigin="anonymous"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <!-- Fixed navbar -->
    <!--<nav class="navbar navbar-inverse navbar-fixed-top">-->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
           <a class="navbar-brand" href="/workexperience/">My Workspace app</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="index.php">Choose placement</a></li>
               <li><a href="introduction.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Introduction</a></li>
              <li><a href="location.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Where is my placement</a></li>
              <li><a href="agreement.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Work placement agreement</a></li>
              <li class="active"><a href="aims.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Aims and objectives</a></li>
              <li><a href="logbook.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Logbook</a></li>
              <li><a href="placement_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Your work placement review</a></li>
              <li><a href="employer_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Employer review</a></li>
              <li><a href="coach_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Coach review</a>
            </li>
             <?php if($showMidPointReview):?>
                <li><a href="midpoint_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Your midpoint review</a>
                </li>
             <?php endif;?>
            <!--<li><a href="#about">About</a></li>-->
            <!--<li><a href="#contact">Contact</a></li>-->
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav><!-- end fixed navbar -->

    <div class="container-fluid theme-showcase" role="main">
      <div class="row"><!--Start of row -->
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-bordered">
              <thead>
                  <tr>
                    <th>Student name</th>
                    <td><?=$learnerDetails[0]['name']?></td>
                    <th>Student ID</th>
                    <td><?=$learnerDetails[0]['person_code']?></td>
                    <th>Coach name</th>
                    <td><?=$coach->getCoachNameForLearner($_GET['placement_attended_id'])?></td>
                  </tr>
              </thead>  
            </table>
        </div>  
      </div>
    </div> <!--End of row -->

      <div class="row" id='test'><!--Start of row -->
        <!-- Tabbed content left-->
        <div class="col-md-12">
            <ul class="nav nav-tabs">
              <li><a href="index.php">Choose placement</a></li>
               <li><a href="introduction.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Introduction</a></li>
              <li><a href="location.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Where is my placement</a></li>
              <li><a href="agreement.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Work placement agreement</a></li>
              <li class="active"><a href="aims.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Aims and objectives</a></li>
              <li><a href="logbook.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Logbook</a></li>
              <li><a href="placement_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Your work placement review</a></li>
              <li><a href="employer_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Employer review</a></li>
              <li><a href="coach_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Coach review</a>
            </li>
             <?php if($showMidPointReview):?>
                <li><a href="midpoint_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Your midpoint review</a>
                </li>
             <?php endif;?>
            </ul>
        <br/>
      </div><!-- end tabbed content left-->
    </div><!--End of row -->
  
      <div class="row"><!--Start of row -->
        <div class="col-md-12"><!-- content main full page-->
          <h3>Aims and objectives</h3>

          <p>In this section you are required to choose <strong>only 4 objectives</strong>. These objectives will reflect through the rest of the application. The aims will also be what is used to complete your final work place review where you'll be expected to rate your progress in those areas.</p>

          <p><span class='red-bold'>Please note:</span> once your work placement review is completed, <u>you will not</u> then be able to come back to this section and then change your aims again. However, before the workplacement review is completed, the aims can be changed at any time.</p>
          
          <p>Be sure about your aims and objectives before you complete your work placement review. </p>


          <?php if(isset($error_message)):?>
            <script>
              $(document).ready(function(){
              $("#error_message").show().delay(5000).fadeOut();
            });
            </script>
            <div class="alert alert-danger" id='error_message'>
              <strong>Sorry!</strong> <?=$error_message?>
            </div>
         <?php endif;?>

          <!-- If the aims have been selected, drop the text asking them to select -->
          <?php if(!$learnerAimsChosen) echo  '<p>Please choose 4 aims and objectives from the list below.</p>'?>
          <fieldset <?php if($learnerAimsChosen || (isset($_POST['submitted']) && $_POST['submitted'])) echo 'disabled'?>>
            <form method='POST'>
               <div class="table-responsive">          
                <table class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>Aim &amp; objectives.</th>
                      <th>Tick to choose option</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach($aimsAndObjectives AS $aim=>$aims):?>
                      <tr/>
                        <td <?php if(isset($learnerAimsChosen[$aims])) echo " class='bg-success'";?>><?=$aim?></td>
                        <!-- If the aims have been selected already, or they have been chosen from the form we want to show this.
                        This uses the 2 arrays: $learnerAimsChosen & $aimsAndObjectives, the aims the learner chose, and $aimsAndObjectives which is all the options.
                        Then, to set the up the form to remember stuff with the chosen details, we use the learners chosen options array $learnerAimsChosen[$aims], and check the general array with the key from $aimsAndObjectives (all options). If this is set in the learners options array, they chose it.
                         -->  
                        <td>
                          <input type="checkbox" id="aim" <?php if((isset($_POST[$aims]) && $_POST[$aims]==1) || isset($learnerAimsChosen[$aims])) echo 'checked';?> name="<?=$aims?>" value="1"/>
                        </td>
                     </tr>
                    <?php endforeach;?>
                  </tbody>
                </table>
              </div> <!-- table-responsive ends-->

              <!-- If the aims have been selcted, change the button -->
              <button type="submit" class="btn btn-primary">
                <?php if($learnerAimsChosen!==FALSE) echo "Your aims have been saved";?>
                <?php if($learnerAimsChosen==FALSE) echo "Save your aims";?>
              </button>  
            </form>
        </fieldset>

        <br/>

        <!-- If the aims have been chosen and the person had completed their work placement review-->
        <?php if($learnerAimsChosen!=FALSE && $learnerReview==false){?>
          <form method='POST' action="<?php echo $_SERVER['PHP_SELF'];?>">
            <input type='hidden' name='placement_attended_id' value="<?=$_GET['placement_attended_id']?>"/>
            <input type='hidden' name='studID' value="<?=$_GET['studID']?>"/>
            <button type="submit" name='edit' value='1' id='edit' class="btn btn-danger">Edit</button>
          </form>
          <?php
          }?>

        </div> <!-- col ends-->  
      </div><!-- row ends-->

      <hr/>

        <!-- The coach comments at the very bottom -->
        <div class="row"><!--Start of row -->
          <!-- Tabbed content left-->
          <div class="col-md-12">

            <?php if($coachDetails!=false): // Only show form if coach ?>

              <h3> Add coach comments</h3>

              <fieldset>
                    <form method='POST'>

                        <div class="form-group">
                          <textarea required class="form-control" id="comments_aims" name='comments_aims' rows="7" placeholder="This is an area for the coach to add comments for the learners atten as the placement reaches various stages in an effort to guide the learner when it is felt some additional help is needed. You learner will be able to view the comments."></textarea>
                        </div>

                      <input type='hidden' name='studID' value="<?=$_GET['studID']?>"/>
                      <input type='hidden' name='placement_attended_id' value="<?=$_GET['placement_attended_id']?>"/>

                      <button type="submit" name='add_coach_comments' value='1' class="btn btn-primary">Add comment</button>  
                    </form>
              </fieldset>

            <?php endif;?>

            <hr/>

            <?php if(isset($coachComments[0]['comments_aims'])):?>
              <h3>Coach comments</h3>
              <?php if($coachDetails!=false):?>
                <p> Your learner will see the comments below on their wall as <strong>read-only</strong></p>
              <?php endif;?>

            <?php else:?>
              
            <?php endif;?>

            <?php for($i=0;isset($coachComments[$i]['comments_aims']);$i++):?>
             <fieldset <?php if($coachDetails==false) echo 'disabled'; // Disable form if not coach ?>>
                  <form method='POST'>

                      <div class="form-group">
                        <textarea class="form-control" id="coach_comments" name='coach_comments' rows="7" placeholder="This is an area for the coach to add comments as the placement reaches various stages in an effort to guide the learner when it is felt some additional help is needed."><?=$coachComments[$i]['comments_aims']?></textarea>
                      </div>
                      <input type='hidden' name='studID' value="<?=$_GET['studID']?>"/>
                      <input type='hidden' name='row_id' value="<?=$coachComments[$i]['id']?>"/>
                      <input type='hidden' name='placement_attended_id' value="<?=$_GET['placement_attended_id']?>"/>
                    <!-- If the aims have been selcted, change the button -->

                    <p>Created: <strong><?=date('l jS \of F Y H:i:s A',strtotime($coachComments[$i]['date_created']))?></strong> by 
                    <strong>
                      <?=$learner->learnerWorkCoachByCoachID($coachComments[$i]['coach_id'])[0]['name']?>
                    </strong></p>

                     <?php 
                    // If the date created and updated date differ, it's been updated.
                    if(strtotime($coachComments[$i]['date_created'])!=strtotime($coachComments[$i]['date_updated'])):?>
                      <p>Updated at: <strong><?=date('l jS \of F Y H:i:s A',strtotime($coachComments[$i]['date_updated']))?></strong></p>
                    <?php endif;?>

                    <?php if($coachDetails!=false): // If coach show update button ?>
                    <button type="submit" class="btn btn-danger" name='edit_coach_comments' value='1'>Update comments</button> 
                  <?php endif;?>
                  </form>
              </fieldset>
              <hr/>
            <?php endfor;?>
          </div>
        </div>
        <!-- The coach comments at the very bottom end -->


    </div> <!--end main container -->

    <!-- footer here -->
    <footer class="footer">
    <hr/>
      <div class="container">
          <p style="text-align:center;">Bolton College 2017</p>
        </div>
    </footer>
    <!-- end footer end -->


    <!-- Some validation for the form fields  ================================================== -->
    <?php 
    // ** Validation ** If less < 4 && > 4 items selected by the user, let the user know that is not allowed. 
    // Also IGNORE THE COACH comments forms for both adds and updates.
    if(
      !empty($_POST) && count($_POST)<4 && 
      (!isset($_POST['add_coach_comments']) && !isset($_POST['edit_coach_comments']))
      ): ?>
      <script>alert('You must choose 4 aims and objectives.');</script>
    <?php endif;?>

    <?php 
     // Also IGNORE THE COACH comments forms for both adds and updates.
    if(
      !empty($_POST) && count($_POST)>4 && 
      (!isset($_POST['add_coach_comments']) && !isset($_POST['edit_coach_comments']))
      ): ?>
      <script>alert('Please don\'t select any more than 4 aims.');</script>
    <?php endif;?>


     <?php 
   // Require the ada pop up box
   require_once('ADA_form.php');
   ?>

    <!-- Bootstrap core JavaScript ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script defer src="../learner/adanew_dist/adabundle_v1.5.1.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
