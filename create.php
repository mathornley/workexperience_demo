<?php

/**
 * Get the PSR4 autoloader and load all the classes required.
 */
spl_autoload_register(function ($class_name) {
    include 'classes/'.$class_name . '.php';
});

// Stop the cache
require_once 'stop_cache.php';

// Display errors
ini_set('display_errors', 'On');

/*
 * This page establishes who the person is, whether that be a learner or coach, anyone else is redirected to Google for testing.
 * This works off sibboleth and checks for the learner id or the username, if staff.
 * Coach is checked and if there are details, we know this is a coach.
 * Learner details are checked and if there some we know this is a coach
 */ 

// New instance of learner.
$CV = new CV;
$learner = new Learner;
$we = new Workexperience;
// New instance of LDM created.
$LDM = new LDM;

// Get the learner details
$ldetails = $learner->learnerDetails();
// Get the learner quals
$lQuals = $learner->getLearnerQuals();
//echo '<pre>';print_r($lQuals);die;

// Get the learner work experience from the work experience app.
$placement = $we->getAllLearnerPlacements();
//echo '<pre>';print_r($placement);die;

// Get the LDM details
$LDMDetails = $LDM->getLearnerLDM();
//print_r($LDMDetails);

// Something posted in the form on the page.
if($_POST)
{
  echo '<pre>';print_r($_POST);die;
  $CVCreated = $CV->createCVInstance($_POST);
  //if(isset($CVCreated['success'])) echo $CVCreated['success'];die;
}
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>Welcome to the online CV Builder app</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/sticky-footer-navbar.css" rel="stylesheet">

    <!-- Add the jQuery here -->
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="crossorigin="anonymous"></script>

    <!-- Generic JS file to handle the create page -->
    <script src="js/create.min.js"></script>


  </head>

  <body>

     <header>
      <!-- Fixed navbar -->
      <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="/CV/index.php">Online CV Builder application</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link">
                <?=($learner->learnerDetailsByID($learner->stuID)!=false) ? $learner->learnerDetailsByID($learner->stuID) : 'STAFF';?> <span class="sr-only">(current)</span>
              </a>
            </li>
             <li class="nav-item">
              <a class="nav-link" href="index.php">Introduction</a>
            </li>
            <li class="nav-item">
             <a class="nav-link" href="create.php">Create a CV</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="edit.php">Edit your CVs</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="save.php">Save your CV</a>
            </li>
          </ul>
          
        </div>
      </nav>
    </header>

     <!-- Begin page content -->
    <main role="main" class="container">

      <div class="alert alert-danger" id='error_message'>
        <strong>Error!</strong> <?=$CVCreated['error']?>
      </div>

      <div class="alert alert-success" id='success_message'>
        <strong>Success!</strong> <?=$CVCreated['success']?>
      </div>

      <!-- *** Success in adding a CV -->
      <?php if(isset($CVCreated['success'])):?>
        <script>
          $(document).ready(function(){
          $("#success_message").show('slow').delay(5000).fadeOut();
          });
        </script>
      <?php endif;?>

      <!-- *** Error in adding a CV -->
      <?php if(isset($CVCreated['error'])):?>
        <script>
          $(document).ready(function(){
          $("#error_message").show('slow').delay(5000).fadeOut();
          });
        </script>
      <?php endif;?>


      <div class="row"><!--Start of row -->
        <div class="col-md-12"><!--Start of col -->

           <h3 class="mt-5">Create your CV below with the Online CV Builder application</h3>
            <p class="lead">Below you can create a CV instance.</p>
            <p class="lead">If anything is incorrect with this data, you will need to contact the MIU dept to get the data updated. All qualifications and work experience below will be <em>included in any CV created by default</em>, unless you choose to exclude it. </p>

          <!-- An inlcude for the form to create a CV -->
          <?php require_once 'includes/_form.php';?>
        
        </div>
      </div>

    </main>

    <!-- Footer include -->
    <?php require_once 'includes/_footer.php';?>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="crossorigin="anonymous"></script>
  </body>
</html>
