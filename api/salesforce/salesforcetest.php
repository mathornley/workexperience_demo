<?php

// Error reporting on. 
ini_set('display_errors',1);
ini_set('allow_url_include',1);

// include the SQL database class.
require_once '/var/www/web/workexperience/classes/SQL.php';

// Identifiers for the Salesforce authentication.
define("CLIENT_ID", "<CLIENT ID>");
define("CLIENT_SECRET", "<SECRET>");
define("LOGIN_URI", "<URL>");
define("USERNAME", "<USERNAME>");
define("PASSWORD", "<PASSWORD>"); // At the last password change, the password had a 1 appended to it. 

/**
 * The function call via Curl to authenticate with Salesforce and get the access token.
 * @param  [string] $username [Assigned username]
 * @param  [strig] $password [Assigned password]
 * @return [type]           [description]
 */
function salesforce_login($username, $password)
{
    $params = "grant_type=password&client_id=".CLIENT_ID.
            "&client_secret=".CLIENT_SECRET.
            "&username=".USERNAME."&password=".PASSWORD;
    
    $token_url = LOGIN_URI . "/services/oauth2/token";
    
    $curl = curl_init($token_url);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
    $json_response = curl_exec($curl);
    $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    
    // Status of the call - should be 200
    if($status != 200)
    {
        die("Error: call to URL: $token_url failed with status $status");
    }

    // return the array with the access token and instance URL
    return json_decode($json_response, true);
}

// Call the function to perform the auth call to cURL to Saleforce. 
$response = salesforce_login(USERNAME, PASSWORD);

// Our access token and instance URL.
$access_token = $response["access_token"];
$instance_url = $response["instance_url"];

// Call to the function to show the accounts and pull the data.
show_accounts($instance_url, $access_token);

// Get the details from the accounts table as per: https://resources.docs.salesforce.com/206/latest/en-us/sfdc/pdf/salesforce_field_names_reference.pdf
function show_accounts($instance_url, $access_token)
{
    // Create a new instance of the SQL class just in case. 
    $SQL = new SQL; 

    $query = 

        "
        SELECT 

            Name,
            Id,
            BillingStreet,
            BillingPostalCode,
            Phone,
            Industry,
            We_count__c,
            Email__c,
            BillingCity,
            WE_Manager__c,
            URN_Number__c 
    
            FROM 
                Account

        ";


    // cURL query above to Slesfoce
    $url = "$instance_url/services/data/v20.0/query?q=" . urlencode($query);

    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HEADER,false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HTTPHEADER,
    array("Authorization: OAuth $access_token"));

    // Get the response. 
    $json_response = curl_exec($curl);

    // Status of the call - should be 200
    $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    
    // Call is made successfully.
    if($status != 200)
    {
        die("Error: call to URL $token_url failed with status $status");
    }

    curl_close($curl);

    // All the records decoded to PHP array.
    $records = json_decode($json_response,true);
    //echo '<pre>';print_r($records);
    $date = date('Y-m-d H:i:s');

    // Print out each record on each loop pass. 
    for($i=0;isset($records['records'][$i]);$i++)
    {
        echo (isset($records['records'][$i]['URN_Number__c'])) ? '<p>'.$records['records'][$i]['URN_Number__c'].'</p>' : '<p><strong> ** ERN not there yet **</strong></p>';
        echo "<p>".$records['records'][$i]['Id']."</p>";
        echo "<p>".$records['records'][$i]['Name']."</p>";
        echo "<p>".$records['records'][$i]['BillingStreet']."</p>";
        echo "<p>".$records['records'][$i]['BillingCity']."</p>"; 
        echo "<p>".$records['records'][$i]['BillingPostalCode']."</p>";
        echo "<p>".$records['records'][$i]['Phone']."</p>";

        /* This is the count of the placements that are held in Salesforce */
        echo "<p>".$records['records'][$i]['We_count__c']."</p>"; 
        echo "<p>".$records['records'][$i]['Email__c']."</p>"; 
        echo "<p>".$records['records'][$i]['WE_Manager__c']."</p>"; 
        echo '<hr/>';

        // See if there is a ERN number and if not, we'll enter NULL to the table.
        $ERN = (isset($records['records'][$i]['URN_Number__c'])) ? $records['records'][$i]['URN_Number__c'] : NULL;

        // Finally, INSERT these to the database table `we_placement` table - ONLY insert the updates.
        // Start building the query - set the placement_attended_id firstly.
        $query = 'INSERT INTO `we_placement` (`salesforce_id`,`name`,`address`,`post_code`,`phone`,`date_created`,`email`,`sf_places`,`manager`,`ERN`) VALUES(:salesforce_id,:name,:address,:post_code,:phone,:date_created,:email,:sf_places,:manager,:ERN)
            ON DUPLICATE KEY UPDATE `name`=:name,`address`=:address,`post_code`=:post_code,`phone`=:phone,`date_created`=:date_created,`email`=:email,`sf_places`=:sf_places,`manager`=:manager,`ERN`=:ERN';

        // Insert the updates in to the placements_table with the `sf_places`.
        $result = $SQL->insert
        (
            $query,
            [
                ':salesforce_id'=>$records['records'][$i]['Id'],
                ':name'=>$records['records'][$i]['Name'],
                ':address'=>$records['records'][$i]['BillingStreet'].', '.$records['records'][$i]['BillingCity'],
                ':post_code'=>$records['records'][$i]['BillingPostalCode'],
                ':phone'=>$records['records'][$i]['Phone'],
                ':date_created'=>$date,
                ':sf_places'=>$records['records'][$i]['We_count__c'],
                ':email'=>$records['records'][$i]['Email__c'],
                ':manager'=>$records['records'][$i]['WE_Manager__c'],
                ':ERN'=>$records['records'][$i]['URN_Number__c']
            ]
        );
    }
}
