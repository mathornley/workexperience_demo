<?php

// Turn error checking on. 
ini_set('display_errors', 'On');

require_once 'twilio-php-master/Twilio/autoload.php';
require_once '/var/www/web/workexperience/classes/SQL.php';
require_once '/var/www/web/workexperience/classes/MSSQL.php';
require_once '/var/www/web/workexperience/classes/CareerCoachReview.php'; // Also has an instance of learner inside it. 


// Use the REST API Client to make requests to the Twilio REST API
use Twilio\Rest\Client;

/**
 *  * This is a workaround script, written under sufference!
 * Cron job to remind coaching staff to remind learners to complete their logbook.
 * This scripts uses some of the workexperience code, so DO NOT MOVE IT as some of the classes power this!!!
 * Checks both placement types: 30hrs/5days and the industrial placements for missing logbook entries.
 * Hours are also checked to see if anything is in the logbook and that the learner for whatever reason hasn't been withdrawn.  
 */
class ReminderLearner
{

	/**
	 * Quick constructor, doesn't do too much. Just sets stuff up. 
	 * Connect to both the SQL WEX tables and the MSSQL tables to get the dbs connections. 
	 */
	public function __construct()
	{
		// Instiate a DB connection from SQL SID.
		$this->SQL = new SQL;

		// Instiate a DB connection from Promonitor SID.
		$this->MSSQL = new MSSQL;

		// Instantiate the career coach review.
		$this->coach = new CareerCoachReview;

		// Instantiate a new instance of Learner.
		$this->learner = new Learner;

	    // Twillio dependances for the SMS messages.
	    $this->sid = '<username>'; // 'Username' for twilio'
        $this->token = '<token>'; // Token for 'twillio'        
		
		// Set up the Twillio instance
		$this->client = new Client($this->sid,$this->token); // Establish new client/

		// Check for the learners and start the ball rolling!
		$this->checkForLearners();		
	}


	/**
	 * Get all the 30hrs/5 day learners who have a placeent on the WEX
	 * Placement code of: ZWRKX001 which is 30hrs/5days.
	 * Who haven't completed any hours in the logbook
	 * Don't include any learners that have been withdrawn for whatever reason.
	 */
	public function checkForLearners()
	{

		// Get all the distinct coaches from the table - the ones in the table that do add learners the portal only. 
		$coaches = $this->coach->getAllCoach();

		/**
		 * Get all the 5days/30hr and Industrial placement learners for each coach.
		 * Add tbe coach name to each set of results per coach.
		 * @var integer
		 */
		for($i=0;isset($coaches[$i]['id']);$i++)
		{

			// Get the 5 days/30hrs

			$this->fiveDayLearner[$coaches[$i]['name']] = $this->getFiveDayLearners($coaches[$i]['id']);
			$this->fiveDayLearner[$coaches[$i]['name']]['coach_name'] = $coaches[$i]['name'];
			$this->fiveDayLearner[$coaches[$i]['name']]['coach_email'] = $coaches[$i]['email'];


			// Get the Industrial placements.

			$this->fiftyDayLearner[$coaches[$i]['name']] = $this->getFiftyDayLearners($coaches[$i]['id']);
			$this->fiftyDayLearner[$coaches[$i]['name']]['coach_name'] = $coaches[$i]['name'];
			$this->fiftyDayLearner[$coaches[$i]['name']]['coach_email'] = $coaches[$i]['email'];
		}

		/**

		 * Iterate the results by coach and email the results to each coach. 
		 */
		foreach($this->fiveDayLearner AS $coach)
		{
			if(!isset($coach[0])) continue; // If they've no learners, ignore them. 

			$string ='';

			$string .=  '<h3><strong>'.$coach['coach_name'].'</strong></h3>';
			$string .= '<h3><strong>'.$coach['coach_email'].'</strong></h3>';
			$string .= '<hr/>';

			// This is -1 as i have above set and don't want to include that in the count. 
			$string .= '<p>Below are your learner(s) with a 30hr/5day placement created on the work experience. These learners have not yet created any logbook entries.</p><hr/>';

			for($i=0;isset($coach[$i]);$i++)
			{

				// Is the learner still active?
				if($this->learner->isLearnerStillActive($coach[$i]['learner_id'])!=false) continue;

				// Withdrawn learner?
				if($this->learner->isLearnerWithdrawnFromCollege($coach[$i]['learner_id'])!=false) continue;


				$string .= '<h3>['.$coach[$i]['learner_id'].'] '.$this->learner->learnerDetailsByID($coach[$i]['learner_id']).'</h3>';
				$string .= '<p>Start date: '.date('d/m/Y',strtotime($coach[$i]['start_date'])).'</p>';
				$string .= ($coach[$i]['agreement_signed']==='1') ? '<p style="color:green;">Agreement signed</p>' : '<p style="color:red;">Hasn\'t signed the agreement</p>';
				$string .= '<p>'.$coach[$i]['placement_code'].'</p>';
				$string .= '<p>'.ucfirst($coach[$i]['placement_type']).'</p>';
				$string .= '<p>'.$coach[$i]['total_hours'].'</p>';
				$string .= '<p>'.$this->getAccYear($coach[$i]['learner_id']).'</p>';
				$string .= '<br/>';

				// Prepare the SMS message for the learner.
				$this->SMSstudent($coach[$i]['learner_id'],$coach[$i]['placement_code'],$coach[$i]['id']);

			}

			// Footnote message
			$string .= '<hr/><br/>Regards<br/><br/>ILT notifications<br/><br/>';
		}


		/**
		 * Iterate the results by coach and email the results to each coach. 
		 */
		foreach($this->fiftyDayLearner AS $coach)
		{

			if(!isset($coach[0]['learner_id'])) continue;

			$string ='';

			$string .= '<h3><strong>'.$coach['coach_name'].'</strong></h3>';
			$string .= '<h3><strong>'.$coach['coach_email'].'</strong></h3>';

			$string .= '<hr/>';

			// This is -1 as i have above set and don't want to include that in the count. 

			$string .= '<p>Below are your learner(s) with an Industrial Placement created on the work experience. These learners have not yet created any logbook entries.</p><hr/>';

			for($x=0;isset($coach[$x]);$x++)
			{
				// They have hours so ignore

				if($coach[$x]['total_hours']>0) continue;

				// Withdrawn learner?
				if($this->learner->isLearnerWithdrawnFromCollege($coach[$x]['learner_id'])!=false) continue;

				$string .= '<h3>['.$coach[$x]['learner_id'].'] '.$this->learner->learnerDetailsByID($coach[$x]['learner_id']).'</h3>';
				//$string .= '<p>Start date: '.date('d/m/Y',strtotime($coach[$x]['start_date'])).'</p>';
				$string .= ($coach[$x]['agreement_signed']==='1') ? '<p style="color:green;">Agreement signed</p>' : '<p style="color:red;">Hasn\'t signed the agreement</p>';
				$string .= '<p>'.$coach[$x]['placement_code'].'</p>';
				$string .= '<p>'.ucfirst($coach[$x]['placement_type']).'</p>';
				$string .= '<p>'.$coach[$x]['total_hours'].'</p>';
				$string .= '<p>'.$this->getAccYear($coach[$x]['learner_id']).'</p>';
				$string .= '<br/>';

				// Prepare the SMS message for the learner.
				$this->SMSstudent($coach[$x]['learner_id'],$coach[$x]['placement_code'],$coach[$x]['id']);
			}

			// Footnote message
			$string .= '<hr/><br/>Regards<br/><br/>ILT notifications<br/><br/>';
		}
	}


	/**
	 * Using the coach id, check for all 5days/30 hr learners with that coach.
	 * Return only learners that have no logbook hours. 
	 * @param  [string] $coach_id [This is the coach id as a string]
	 * @return [array] [These are all the learners for that coach returned]
	 */
	public function getFiveDayLearners($coach_id)
	{

		// Get all the 30hrs/5 day learners who have not completed their logbooks. 
		return $this->fivedayslearners = $this->SQL->select(

			"
			SELECT * FROM (SELECT 
				`pa`.`id`, 
				`pa`.`learner_id`, 
				`pa`.`start_date`,
				`pa`.`coach_id`,
				`pa`.`agreement_signed`,
				IF(`pa`.`agreement_signed_date` IS NULL,'unsigned',`pa`.`agreement_signed_date`) AS `agreement_signed_date`,
				`pa`.`placement_code`,
				`pa`.`placement_type`,
				SUM(`lb`.`hours`) AS `total_hours`

				FROM `we_placement_attended` `pa`
					LEFT OUTER JOIN `we_logbook` `lb` ON `lb`.`placement_id` =  `pa`.`id`
						WHERE `pa`.`placement_code` LIKE 'ZWRKX001' AND  `pa`.`coach_id`=?
				GROUP BY  `pa`.`learner_id` 
				ORDER BY coach_id, `start_date` DESC ) AS `tmptable` WHERE `total_hours` IS NULL
			",
				[
					$coach_id
				]
		);
	}

	/**
	 * Using the coach id, check for all Industrial PLacement learners with that coach.
	 * Return only learner who have no logookm hours.
	 * @param  [string] $coach_id [This is the coach id as a string]
	 * @return [array] [These are all the learners for that coach returned]
	 */
	public function getFiftyDayLearners($coach_id)
	{
		return $this->fiftydaylearners = $this->SQL->select(

			"
			SELECT * FROM (SELECT
				`pa`.`id`, 
				`pa`.`learner_id`, 
				`pa`.`start_date`,
				`pa`.`coach_id`,
				`pa`.`agreement_signed`,
				IF(`pa`.`agreement_signed_date` IS NULL,'unsigned',`pa`.`agreement_signed_date`) AS `agreement_signed_date`,
				`pa`.`placement_code`,
				`pa`.`placement_type`,
				SUM(`lb`.`hours`) AS `total_hours`

				FROM `we_placement_attended` `pa`
					LEFT OUTER JOIN `we_logbook` `lb` ON `lb`.`placement_id` =  `pa`.`id`
						WHERE  `pa`.`placement_code` LIKE 'ZWRKX002' AND `pa`.`coach_id`=?
				GROUP BY  `pa`.`learner_id` 
                ORDER BY `coach_id`, `start_date` DESC) AS `tmptable` WHERE `total_hours` IS NULL
			",

			[
				$coach_id
			]
		);

	}

	/**
	 * A function which takes the coach, subject, message and uses the email headers to send the email to the coach
	 * @param  [string] $coach [This is the coach id as a string]
	 * @param  [type] $subject [This is the subject line of the email]
	 * @param  [type] $message [This is the message body of the email]
	 * @param  [type] $headers [These are the email headers as an HTML string]
	 */
	public function emailCoach($coach, $subject, $message, $headers)
	{
		// E-message is then sent to the coach. 
		mail($coach, $subject,$message, $headers);
	}

	/**
	 * A function to sanitise and reformat the learner phone number
	 * If numeric and a valid number, +44 is prepended to the start of the number. Else false. 
	 * @param  [string] $mobile [This is the learner phone number as a string, unsanitised]
	 * @return [string] [This is the learner formatted andsanitised number]
	 */
	public function sanitiseNumber($mobile)
	{ 
		// Remove the spaces.
		$mobile = str_replace(' ','',$mobile); // strip spaces from number if there are any. 

		// Void format, returm. 
		if(!is_numeric($mobile) && substr($mobile,0,3)=='+44')
		{ 
			// If it's not a digit, it isn't a number so void return
             return $mobile;
       	} 
       	
       	// The number needs to be correctly formatted. 
       	elseif(is_numeric($mobile) && substr($mobile,0,2)=='07')
       	{
			$mobile = preg_replace('/^(0*44|(?!\+0*44)0*)/', '+44', $mobile); // Replace '07' with '+44'
			return $mobile;
		} 

		// number is not numeric and doesn't match the patterm.
		else
		{ 
        	return 0;
        }		

	}


	/**	
	 * Send the learner the SMS message.
	 * @param [string] $learner [This is the learner id as a string].
	 * @param [string] $code [This is the placement code which identifies if this is a 30hr/5day or a Industrial Placement].
	 */
	public function SMSstudent($learner,$code, $paid)
	{
		// Get the student details as array
		$learnerDetails = $this->getStudentDetails($learner);
		
		// Extract the mobile number.
		$mobile = $learnerDetails[0]['mobile'];

		// Find out which type of placement this is and compose the message.
		if($code=='ZWRKX001')
		{
			$message ="Hi {$learnerDetails[0]['forename']} [{$learnerDetails[0]['person_code']}], we notice from the WorkSpace app you have a 30hr/5day placement and have not completed any  <a href='http://www.boltoncc.ac.uk/workexperience/introduction.php?placement_attended_id={$paid}&studID={$learner}'>logbook</a> entries for your placement. Can you please fill in the logbook to show you have attended your placement on the required days. Thank you.";
		}

		if($code=='ZWRKX002')
		{
			$message ="Hi {$learnerDetails[0]['forename']} [{$learnerDetails[0]['person_code']}], we notice from the WorkSpace app you have an industrial placement and have not completed any <a href='http://www.boltoncc.ac.uk/workexperience/introduction.php?placement_attended_id={$paid}&studID={$learner}'>logbook</a> entries for your placement. Can you please fill in the logbook to show you have attended your placement on the required days. Thank you. ";
		}

		// Output the message. 
		echo $message.'<br/>';

		// Finally sanitise the number and send the text message.
		$mobile = $this->sanitiseNumber($mobile);

		$sms = $this->client->messages->create(
			$mobile,
			array(
				'from' => 'Bolton Col',
				'body' => $message
			)
		);

		return;
	}

	/**
	 * Get all the student details from the learmer id.
	 * @param  [string] $learner [This is the learner id as a string]
	 * @return [Array] [this is the result set as an array]
	 */
	public function getStudentDetails($learner)
	{
		// Query the table for the learner details and mobile number. 
		return $this->SQL->select("SELECT `person_code`,`forename`,`mobile` FROM `ext_learnerdetails` WHERE `person_code`=?",[$learner]);

		// Everything is fine, return the results. 
		if($result!=false) return $result;

		// If the result set isn't returned throw. 
		else throw new Exception('Student look up failed on line: '. __LINE__ . 'in class: ' . __CLASS__);
		
	}

	/**
	 * This function uses Promonitors ** MSSQL ** tables to get the acc year for the learner id. 
	 * @param  [string] $learner [This is the learner id as a string]
	 * @return [string] [This is the academic year as a string returned]
	 */
	public function getAccYear($learner)
	{
		$accYear = $this->MSSQL->select("SELECT AcademicYearID FROM ProMonitor.dbo.Student WHERE StudentID=?",[$learner]);

		if(!$accYear) return 'Academic year not set in Promonitor';
		else return $accYear[0]['ACADEMICYEARID'];
	}

}


/**
 * Try to instantiate ReminderLearner and get things moving and see if it works!
 */
try
{
	$reminder = new ReminderLearner;
}

/**
 * Something has gone wrong, throw, get message and email Mike. 
 */
catch(Exception $ex)
{
	error_log('Wex Reminder script failed with the following message: '. $ex->getMessage(), 1, "mike.thornley@boltoncc.ac.uk");
	echo 'Wex Reminder script failed with the following message: '. $ex->getMessage();
}

