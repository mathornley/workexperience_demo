<form method='POST'>
  <div class="form-group">
    <label for="exampleInputEmail1">Full name</label>
    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="full name" placeholder="Please enter your full name" required/>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Street</label>
    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="full name" placeholder="Please enter your street including the house number" required/>
  </div>
   <div class="form-group">
    <label for="exampleInputEmail1">Town</label>
    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="full name" placeholder="Please enter your town" required/>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Postcode</label>
    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="post_code" placeholder="Please enter your post code" required/>
  </div>
   <div class="form-group">
    <label for="exampleInputEmail1">Telephone</label>
    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="telephone" placeholder="Please enter your telephone" required/>
  </div>
   <div class="form-group">
    <label for="exampleInputEmail1">Mobile</label>
    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="mobile" placeholder="Please enter your mobile number"/>
  </div>
  <div class="form-group">
      <label for="comment">Personal statement:</label>
      <textarea class="form-control" rows="13" id="personal_statement" name="personal_statement" placeholder='Please enter your personal statement here' required></textarea>
    </div>
  <br/>
  <h3>Add skills &amp; abilities</h3>
  <p> Here you must pick a minimum of 5 skills and abilities.</p>
  <div class="form-group">
    <label for="exampleInputEmail1">Skill 1</label>
    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="skill_1" name='skill_1' placeholder="Choose skill 1" required/>
  </div>
   <div class="form-group">
    <label for="exampleInputEmail1">Skill 2</label>
    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="skill_2" name='skill_2' placeholder="Choose skill 2" required/>
  </div>
   <div class="form-group">
    <label for="exampleInputEmail1">Skill 3</label>
    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="skill_3" name='skill_3' placeholder="Choose skill 3" required/>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Skill 4</label>
    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="skill_4" name='skill_4' placeholder="Choose skill 4" required/>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Skill 5</label>
    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="skill_5" name='skill_5' placeholder="Choose skill 5" required/>
  </div>

  <button id="add">Add a skill</button>

  <script>
    $(function() {
          $('#add').on('click', function( e ) {
              e.preventDefault();
              $('<div/>').addClass( 'form-group' )
              .html( $('<input type="textbox"/>').addClass( 'someclass' ) )
              .append( $('<button/>').addClass( 'remove' ).text( 'Remove' ) )
              .insertBefore( this );
          });
          $(document).on('click', 'button.remove', function( e ) {
              e.preventDefault();
              $(this).closest( 'some-class' ).remove();
          });
      });
  </script>

</form>
<br/>