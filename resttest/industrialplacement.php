<?php

$learner = $_GET['learner'];

// Include the SQL database class from the work experience.
require_once '../classes/SQL.php';

// Create a new instance of the SQL class just in case. 
$SQL = new SQL; 


$delete = 'DELETE FROM `we_ind_placement_dates` WHERE `learner`=?';

// Insert the updates in to the placements_table with the `sf_places`.
$SQL->select
(
	$delete,
	[
	    $_GET['learner']
	]
);


for($i=0;isset($_GET['date'.$i]);$i++)
{
	$query = 'INSERT INTO `we_ind_placement_dates` (`learner`,`date`) VALUES(?,?)';

	// Insert the updates in to the placements_table with the `sf_places`.
	$SQL->select
	(
		$query,
		[
		    $_GET['learner'],
		    $_GET['date'.$i]
		]
	);
}
