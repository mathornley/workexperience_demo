<?php

// Start the session here
session_start();

//SELECT distinct section FROM `ext_ebs_data_learnergroup` WHERE 1 // Redo these as per `ext_ebs_data_learnergroup`
$headers = 'From: mike.thornley@boltoncc.ac.uk' . "\r\n" .
    'Reply-To: mike.thornley@boltoncc.ac.uk' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

$email = 'mike.thornley@boltoncc.ac.uk';

// Show all the accounts. 
function show_accounts($instance_url, $access_token)
{
    // Get what we want as per: https://resources.docs.salesforce.com/206/latest/en-us/sfdc/pdf/salesforce_field_names_reference.pdf

    $query = "SELECT Name,
         Id,
         BillingStreet,
         BillingPostalCode,
         Phone,
         Industry
            from Account"; // Just get the first 10n records.



    $url = "$instance_url/services/data/v20.0/query?q=" . urlencode($query);

    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HEADER,false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HTTPHEADER,
            array("Authorization: OAuth $access_token"));

    $json_response = curl_exec($curl);
    curl_close($curl);

    $response = json_decode($json_response,true);

    // Going to have this call a script on CRON that enters all these in to the work experience `we_placement` table.

    // We'll output these here for now. 
    foreach ((array) $response['records'] as $record)
    {

        echo $record['Id'] . "<br><br/>" . 
        $record['Name'] . "<br/><br/>". 
        $record['BillingStreet'] . "<br/><br/>" . 
        $record['BillingPostalCode']. "<br/><br/>" .
        $record['Phone']. "<br/><br/>" .
        $record['Industry']. "<br/><br/>";
        echo '<hr/>';
    }
}

// Get these out of the Salesforce session. 
$access_token = $_SESSION['access_token'];
$instance_url = $_SESSION['instance_url'];

// Error handling for the session vars being populated: $access_token 
if (!isset($access_token) || $access_token == "")
{
    mail($email,
        'Issue with the access token on work experience CRON job on:' . date('d-m-Y H:i:s'),
        'Issue with the access token on work expeirence CRON job', 
        $headers
    );
}

// Error handling for the session vars being populated: $instance_url
if (!isset($instance_url) || $instance_url == "")
{
    mail($email,
        'Issue with the instance_url on work experience CRON job on: ' . date('d-m-Y H:i:s'), 
        'There is an issue with the instance_url on work expeirence CRON job', 
        $headers
    );
}

// Call to the function to show the accounts.
show_accounts($instance_url, $access_token);
       