<?php

// Require config file.
require_once 'config.php';

// Get the auth token first
$auth_url = LOGIN_URI. "/services/oauth2/authorize?response_type=code&client_id=". CLIENT_ID . "&redirect_uri=" . urlencode(REDIRECT_URI);

// Redirect to Saleforce
header('Location: ' . $auth_url);
