<?php

/**
 * Get the PSR4 autoloader and load all the classes required.
 */
spl_autoload_register(function ($class_name) {
    include 'classes/'.$class_name . '.php';
});

// Stop the cache
require_once 'stop_cache.php';

// Set an instance of aims.
$aims = new Aims;

// Get an instance of the career coach review.
$coach = new CareerCoachReview;


/**
 * Check if this is a learner only viewing their own part of the app - learner ids have to match.
 * The Shib learner id has to match the URL from the browser, IF they're not a coach. 
 * As usual the coach sees it all.
 * If the user is not authorised, $_SERVER['REQUEST_URI'] is used to keep them on their page.
 */
if($coach->isCareerCoach()==false && $coach->doesTheLearnerIdMatch($_GET['studID'])==false)
{
  // The user is not a coach and not the learner requesting the URL so keep them where they are.
  header('location: /workexperience/');
}

if(isset($_GET['studID']) && isset($_GET['placement_attended_id']))
{

  // Get the learner details so we can populate the top section of the page with the students details. 
  $learner = new Learner($_GET['placement_attended_id'],$_GET['studID']);
  $learnerDetails = $learner->learnerDetails($_GET['studID']);
  // Get the users aims previously chosen if there are any.
  $learnerAimsChosen = $aims->learnerAims($_GET['studID'],$_GET['placement_attended_id']); 
}

if(isset($_POST['studID']) && isset($_POST['placement_attended_id']))
{
  // Get the learner details so we can populate the top section of the page with the students details. 
  $learner = new Learner($_POST['placement_attended_id'],$_POST['studID']);
  $learnerDetails = $learner->learnerDetails($_POST['studID']);
  // Get the users aims previously chosen if there are any.
  $learnerAimsChosen = $aims->learnerAims($_POST['studID'],$_POST['placement_attended_id']); 
}

// Check if the user has any aims and if they don't direct them back to aims page.
if(!$learnerAimsChosen)
{
 //header('location: aims.php');
}

// Get list of both formatted and raw aims so we can use it in the HTML.
$aimsAndObjectives = $aims->displayAimsFormatted();
$aimsAndObjectivesRaw = $aims->displayAims();

// Add the arrays: $aimsAndObjectives,$aimsAndObjectivesRaw to create just one array to use in the form.
// for the options to be selected.
$aimsAndObjectives = array_combine($aimsAndObjectives,$aimsAndObjectivesRaw);

// Check if there has been anything posted by the update form and there are less than 4 choices and the $_POST['update'] which makes it 7 - // 4 choices and some form flags == 7. 
if(isset($_POST))
{

  // If the form $_POST is set and submits 4 options and the $_POST['update'] and the options haven't been submitted yet.
  if(isset($_POST['update']) && count($_POST)==7)
  {
    // Update the aims in to the aims table for the learner. 
    $addedAimsToTable = $aims->editLearnerAims($_POST);
    // Finally redirect the user back to the aims page to view the newly adjusted aims.
    //if($addedAimsToTable!=false)
    header("location: aims.php?placement_attended_id={$_POST['placement_attended_id']}&studID={$_POST['studID']}");
  }

  if(isset($_POST['update']) && count($_POST)>7)
  {
    header("location: aims.php?placement_attended_id={$_POST['placement_attended_id']}&studID={$_POST['studID']}&error=greater");
  }

  if(isset($_POST['update']) && count($_POST)<7)
  {
     header("location: aims.php?placement_attended_id={$_POST['placement_attended_id']}&studID={$_POST['studID']}&error=less");
  }
}


?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>My Workspace app</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/theme.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <!-- Fixed navbar -->
    <!--<nav class="navbar navbar-inverse navbar-fixed-top">-->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
           <a class="navbar-brand" href="/workexperience/">My Workspace app</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="index.php">Choose placement</a></li>
               <li><a href="introduction.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Introduction</a></li>
              <li><a href="location.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Where is my placement</a></li>
              <li><a href="agreement.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Work placement agreement</a></li>
              <li class="active"><a href="aims.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Aims and objectives</a></li>
              <li><a href="logbook.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Logbook</a></li>
              <li><a href="placement_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Your work placement review</a></li>
              <li><a href="employer_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Employer review</a></li>
              <li><a href="coach_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Work coach review</a>
            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav><!-- end fixed navbar -->

    <div class="container-fluid theme-showcase" role="main">
      <div class="row"><!--Start of row -->
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-bordered">
              <thead>
                  <tr>
                    <th>Student name</th>
                    <td><?=$learnerDetails[0]['name']?></td>
                    <th>Student ID</th>
                    <td><?=$learnerDetails[0]['person_code']?></td>
                    <th>Days</th>
                    <td>0</td>
                    <th>Hours</th>
                    <td>0</td>
                  </tr>
              </thead>  
            </table>
        </div>  
      </div>
    </div> <!--End of row -->

     <div class="row" id='test'><!--Start of row -->
        <!-- Tabbed content left-->
         <div class="col-md-12">
            <ul class="nav nav-tabs">
              <li><a href="index.php">Choose placement</a></li>
              <li class="active"><a href="introduction.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Introduction</a></li>
              <li><a href="location.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Where is my placement</a></li>
              <li><a href="agreement.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Work placement agreement</a></li>
              <li><a href="aims.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Aims and objectives</a></li>
              <li><a href="logbook.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Logbook</a></li>
              <li><a href="placement_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Your work placement review</a></li>
              <li><a href="employer_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Employer review</a></li>
              <li><a href="coach_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Career coach review</a></li>
            </ul>
        <br/>
      </div><!-- end tabbed content left-->
    </div><!--End of row -->
  
      <div class="row"><!--Start of row -->
        <div class="col-md-12"><!-- content main full page-->
          <h3>Edit your aims and objectives</h3>
          <!-- If the aims have been selected, drop the text asking them to select -->
          <?php if(!$learnerAimsChosen) echo  '<p>Please choose 4 aims and objectives from the list below.</p>'?>
          <fieldset>
            <form action="<?=$_SERVER['PHP_SELF']?>" method='POST'>
               <div class="table-responsive">          
                <table class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>Aim &amp; objectives.</th>
                      <th>Tick to choose option</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach($aimsAndObjectives AS $aim=>$aims):?>
                      <tr/>
                        <td><?=$aim?></td>
                        <!-- If the aims have been selected already, or they have been chosen from the form we want to show this.
                        This uses the 2 arrays: $learnerAimsChosen & $aimsAndObjectives, the aims the learner chose, and $aimsAndObjectives which is all the options.
                        Then, to set the up the form to remember stuff with the chosen details, we use the learners chosen options array $learnerAimsChosen[$aims], and check the general array with the key from $aimsAndObjectives (all options). If this is set in the learners options array, they chose it.
                         -->  
                        <td>
                          <input type="checkbox" id="aim" <?php if((isset($_POST[$aims]) && $_POST[$aims]==1) || isset($learnerAimsChosen[$aims])) echo 'checked';?> name="<?=$aims?>" value="1"/>
                        </td>
                     </tr>
                    <?php endforeach;?>
                  </tbody>
                </table>
              </div> <!-- table-responsive ends-->

              <input type='hidden' name='placement_attended_id' value="<?=$_GET['placement_attended_id']?>"/>
              <input type='hidden' name='studID' value="<?=$_GET['studID']?>"/>

              <!-- If the aims have been selcted, change the button -->
              <button type="submit" name='update' class="btn btn-danger">
                Update
              </button>  
            </form>
             <p><?php if(isset($error_message)) echo $error_message;?></p>
        </fieldset>

        </div> <!-- col ends-->  
      </div><!-- row ends-->

    </div> <!--end main container -->

    <!-- footer here -->
    <footer class="footer">
    <hr/>
      <div class="container">
          <p style="text-align:center;">Bolton College 2017</p>
        </div>
    </footer>
    <!-- end footer end -->

    <!-- Bootstrap core JavaScript ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
