<?php

/**
 * Get the PSR4 autoloader and load all the classes required.
 */
spl_autoload_register(function($class_name) {
    include '../classes/'.$class_name.'.php';
});

ini_set('display_errors', 'On');

// Stop the cache
require_once '../stop_cache.php';

// Instantiate a new instance of coach.
$coach = new CareerCoachReview;

// Check if this is a staff memeber and if not, deny access.
if($coach->isStaffMember()===false)
{
  echo 'Not a staff member, no access!';die;
}

// First find out if this is a coach - coaches, see all work experience. Check the coaches table for the users Shib and if it's there, they're a coach. 
if($isCareerCoach = $coach->isCareerCoach())
{
  // Get the coach details. 
  $isCareerCoach = $coach->isCareerCoach();
}
else
{
  // Not a coach so redirect later.
  echo 'Not a career coach so no access';die;
}

// Pull all placements, regardless of the coach.
$allplacements = $coach->getPlacements();

// Pull all the coach specific placements using the coachID from call above.
$coachPlacements = $coach->getPlacementsByCoachID($isCareerCoach[0]['id']);

// Get all the coaches in the we_coach table.
$allCoaches = $coach->getAllCoach();

// Instantiate an instance of the autocomplete class we'll use for search.
$ac = new AutoComplete;
$learner = new learner;
$placement = new Placement;
$ALS = new ALS;

// placement has been created and all the form fields have been created.
if(isset($_POST['add_placement']) && $_POST['add_placement']=='1')
{
  
  $addPlacement = $placement->addPlacementInstance($_POST);
  
  // The placement was added successfully, so confirmation message via $_GET on refresh
  if($addPlacement===true)
  {
    header('location: /workexperience/CRM/learner.php?error=false');
  }

  if(isset($addPlacement['error']) && $addPlacement['error'] && $addPlacement['noplaces'])
  {
    header("location: /workexperience/CRM/learner.php?error=true&noplaces={$addPlacement['noplaces']}");
  }

  // The placement was added unsuccessfully, so error message via $_GET on refresh
  if($addPlacement===false)
  {
    header('location: /workexperience/CRM/learner.php?error=true');
  }  

}

// The placement has been searched, now show all the learners for that placement. 
if(isset($_POST['learner_placement_search']) && $_POST['learner_placement_search'])
{
  //echo explode(' ',$_POST['learner_placement_search'])[2];die;
  $learnerPlacements = $placement->getLearnerPlacementByLearnerID($_POST);
}

// The placement has been searched, now show all the learners for that placement. 
if(isset($_POST['save_edit_placement']) && $_POST['save_edit_placement'])
{

  //echo '<pre>';print_r($_POST);die;
  $placementUpdate = $placement->updatePlacementInstance($_POST);

  if($placementUpdate==true)
  {
    //$placementUpdatedInfo = $placement->updatedPlacementInfo($_POST);
    header('location: /workexperience/CRM/learner.php?update=true');
  }
}

// Withdraw the learner with details
if(isset($_POST['submit_withdraw_learner']) && $_POST['submit_withdraw_learner'])
{
  //print_r($_POST);die;
  $withdraw = $placement->withdrawLearner($_POST);
}

// Update the withdraw learner details
if(isset($_POST['update_withdraw_learner']) && $_POST['update_withdraw_learner'])
{
  $withdrawUpdate = $placement->updateWithdrawLearner($_POST);
}



/* The hiding and showing the learner withdrawal or not is at line: 700 */
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Coach section of the My Workspace application.</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="../css/bootstrap-theme.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="../css/theme.css" rel="stylesheet">

    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/themes/base/minified/jquery-ui.min.css" type="text/css" />

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- JQuery -->
    <script
    src="https://code.jquery.com/jquery-1.12.4.js"
    integrity="sha256-Qw82+bXyGq6MydymqBxNPYTaUXXq7c8v3CwiYwLLNXU="
    crossorigin="anonymous"></script>

     <!-- Autocomplete stuff -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


    <link rel='stylesheet' href='http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css'>
    <link rel="stylesheet" href="../css/multidate_style.css">
 


    <!--Autcomplete listeners here here for the form inputs -->
    
    <script>
      $(function(){
        $("#autocompletelearner").autocomplete({
          source: "search_learner.php", //autocomplete here  - source is the PHP script: search.php
          minLength: 1 // Match for how many letters before the magic starts.
        });

        $("#autocompletelearnerbottom").autocomplete({
          source: "search_learner.php", //autocomplete here  - source is the PHP script: search.php
          minLength: 1 // Match for how many letters before the magic starts.
        });

        // This searches and returns the learner but looks up the learner placement when chosen.
        $("#autocompletelearnerplacementsearch").autocomplete({
          source: "search_placement.php", //autocomplete here  - source is the PHP script: search_learner.php
          minLength: 1 // Match for how many letters before the magic starts.
        }); 

        $("#autocompletelearnerplacement").autocomplete({
          source: "search_placement.php", //autocomplete here  - source is the PHP script: search_placement.php
          minLength: 1 // Match for how many letters before the magic starts.
        });

         $("#autocompletelearnercoach").autocomplete({
          source: "search_coach.php", //autocomplete here  - source is the PHP script: search_coach.php
          minLength: 1 // Match for how many letters before the magic starts.
        }); 

        $( "#autocompletelearner").focus(function(){
          $("span.message").css( "display", "inline" ).fadeOut(5000);
        });

        // Open and close the student withdraw section and then change the button text.
        $('#withdraw_learner').on('click',function(){
          $(".withdraw_learner_div").slideToggle("slow");

          // Adjust the button text on the student withdraw section
          ($('#withdraw_learner').text()=='Withdraw this learner from placement') ? $('#withdraw_learner').text('Close withdraw this learner from placement') : $('#withdraw_learner').text('Withdraw this learner from placement');
        });


        // This is the fired after the autocomplete and if when selected a user is in as ALS, the support information is shown underneath the users name dynamically.
        $("#autocompletelearner").blur(function(){
          var learner = $("#autocompletelearner").val(); // Get the learner string when the input if filled on name.
          learnerArray = learner.split(" "); // From the string get an array.
          var learnerId = learnerArray.pop(); // Get the learner id from the end of the array - last element.

         // Use the learner id and send this to the /CRM/searchforsupport.php script. This script queries the ALS table for some support info and if there is some support needs returns it, so we show this in the template in the div searchforsupport.
         var xmlhttp = new XMLHttpRequest();

         // Does the learner require extra support? (ALS)
         xmlhttp.onreadystatechange = function()
         {
             if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
              if(xmlhttp.responseText!='no results'){
                $("#support").slideDown('slow');
                $("p#support_needs").text(xmlhttp.responseText);
              }
             }
         }
         xmlhttp.open("GET", "searchforsupport.php?learner=" + learnerId,false);
         xmlhttp.send(); 

         // Is the learner exempt from a placement?
         xmlhttp.onreadystatechange = function()
         {
             if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
              if(xmlhttp.responseText!='no results'){
                $("#exempt").slideDown('slow');
                $("p#exempt_needs").text(xmlhttp.responseText);
                $("#add_placement_button").attr( "disabled", "disabled" ); // Hide submit button.
              }
             }
         }
         xmlhttp.open("GET", "searchforexempt.php?learner=" + learnerId,false);
         xmlhttp.send();   


        // Is the learner work ready? If not, show message and lock the 'Add a placement button'
        xmlhttp.onreadystatechange = function()
         {
             if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
              if(xmlhttp.responseText==false){
                //$("#work_ready").slideDown('slow');
                //$("#add_placement_button").attr( "disabled", "disabled" ); // Hide the submit button.
              }
             }
         }
         xmlhttp.open("GET", "searchforworkready.php?learner=" + learnerId,false);
         xmlhttp.send();   

        // Get the qual type & course code and see what kind of placement the learner is 
        xmlhttp.onreadystatechange = function()
         {
             if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
             {
              // Course code and such back. 
              if(xmlhttp.responseText!=false)
              {
                // Get the course string as code and description
                var courseString = xmlhttp.responseText;
                // Split the string and just get the code.
                var courses = courseString.split('-');
                // Get the course code for the hidden field of the form. 
                var courseCode = courses[0];
                // Populate the shown placement type field of the form.
                $('#placement_type_shown').val(courses[1]);
                // Populate the hidden placement type field of the form.
                $('#placement_type_hidden').val(courses[0]);
              }
              // No course code etc found for this learner, ask coach to contact MIU
              else $('#placement_type_shown').val('No course code found for this learner, * please contact Rhys or Caroline in MIU before attempting to add this learner. ILT do not deal with this. *');
             }
         }
         xmlhttp.open("GET", "searchforworkplacementtype.php?learner=" + learnerId,false);
         xmlhttp.send();   
      });

      }); // End jQuery
    </script>

    <!--This is the withdraw section to the withdrawal of a learner-->
    
    <?php if(isset($withdraw) && $withdraw==false ):?>
      <script>
        $(document).ready(function(){
        $("#error_message").show().delay(5000).fadeOut();
        setTimeout('window.location.href="/workexperience/CRM/learner.php"', 2000); 
      });
      </script>
    <div class="alert alert-danger" id='error_message'>
      <strong>Sorry!</strong> an error occured adding the learner.
    </div>
    <?php endif;?>

    <?php if(isset($withdraw) && $withdraw!=false ):?>
      <script>
        $(document).ready(function(){
        $("#success_message").show().delay(5000).fadeOut();
        setTimeout('window.location.href="/workexperience/CRM/learner.php"', 2000); 
      });
      </script>
    <div class="alert alert-success" id='success_message'>
      <strong>Success!</strong><?=$withdraw['message']?>
    </div>
    <?php endif;?>

    <?php if(isset($withdrawUpdate) && $withdrawUpdate==false ):?>
      <script>
        $(document).ready(function(){
        $("#error_message").show().delay(5000).fadeOut();
        setTimeout('window.location.href="/workexperience/CRM/learner.php"', 2000); 
      });
      </script>
    <div class="alert alert-danger" id='error_message'>
      <strong>Sorry!</strong> an error occured updating the learner.
    </div>
    <?php endif;?>

    <?php if(isset($withdrawUpdate) && $withdrawUpdate!=false ):?>
      <script>
        $(document).ready(function(){
        $("#success_message").show().delay(5000).fadeOut();
        setTimeout('window.location.href="/workexperience/CRM/learner.php"', 2000); 
      });
      </script>
    <div class="alert alert-success" id='success_message'>
      <strong>Success!</strong><?=$withdrawUpdate['message']?>
    </div>
    <?php endif;?>
    <!--This is the add and update section to the withdrawal of a learner-->

    <!-- Styles for the date picker -->

    <!-- This is for the multidate and single date picker -->
    <script>

      /**
        * This is for the add a placement section of the add a placement. 
      */
          
       $(document).ready(function(){

          $("#industrial_placement").click(function() { 
            if($(this).is(':checked')){ // Industrial placement chosen
              $('.single_dates').hide(); // Hide single date
                $('.multidate').show();  // Hide single date
                $("input[name=multipledates]").prop('required', true); // Add the validation back in

            }
          });

          $("#social").click(function() {
            if($(this).is(':checked')){ // social single date chosen
              $('.single_dates').show(); // Show the single dates
                $('.multidate').hide(); // Hide the multidates
                $("input[name=start_date]").prop('required', true); // Add the validation back in
            }
          });

          $("#ZWRKX001").click(function() {
            if($(this).is(':checked')){ // 30 day placement chosen. 
              $('.single_dates').show(); // Show the single dates
               $('.multidate').hide(); // Hide the multidates
               $("input[name=start_date]").prop('required', true); // Add the validation back in
            }
          });

          /**
           * This is for the edit section of the add a placement. 
           */
          
          if($("#industrial_placement").is(':checked')){ // Industrial placement chosen
              $('.single_dates').hide(); // Hide the single dates
              $('.multidate').show(); // Show the single dates
              $("input[name=multipledates]").prop('required', true); // Add the validation back in
              
          }

          if($("#social").is(':checked')){ // Social action placement chosen
              //$('#industrial_placement').hide(); // Hide the industrial placement
               $('.single_dates').show(); // Show the single dates
                $('.multidate').hide(); // Show the single dates
                 $("input[name=start_date]").prop('required', true); // Add the validation back in
              
          }

          if($("#ZWRKX001").is(':checked')){ // 5day/30hr
              //$('#industrial_placement').hide(); // Hide the industrial placement
              $('.single_dates').show(); // Show the single dates
               $('.multidate').hide(); // Show the single dates
                $("input[name=start_date]").prop('required', true); // Add the validation back in
          }



        }); // End of the jQuery
     </script>
     <!-- This is for the multidate and single date picker -->

    <!-- Brief styles -->
    <link rel="stylesheet" type="text/css" href="../css/styles.css">
    <!-- Brief styles end --> 

  </head>

  <body>

    <!-- Fixed navbar -->
    <!--<nav class="navbar navbar-inverse navbar-fixed-top">-->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
           <a class="navbar-brand" href="/workexperience/">CRM - My Workspace app</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="index.php">Introduction</a></li>
            <li class="active"><a href="learner.php">Manage Learners</a></li>
            <li><a href="coach.php">Manage Staff</a></li>
            <!--<li><a href="placement.php">Manage Placement</a></li>-->
            <li><a href="employer.php">Manage Employer</a></li>
            <li><a href="als.php">Manage ALS</a></li>
            <li><a href="../coach/">Review learners</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav><!-- end fixed navbar -->

  
      <div class="container-fluid theme-showcase" role="main">

         <div class="row"><!--Start of row -->
            <!-- Tabbed content left-->
            <div class="col-md-12">
                <ul class="nav nav-tabs">
                   <li><a href="index.php">Introduction</a></li>
                  <li class="active"><a href="learner.php">Manage Learners</a></li>
                  <li><a href="coach.php">Manage Staff</a></li>
                  <!--<li><a href="placement.php">Manage Placement</a></li>-->
                  <li><a href="employer.php">Manage Employer</a></li>
                  <li><a href="als.php">Manage ALS</a></li>
                  <li><a href="../coach/">Review learners</a></li>
                </ul>
            <br/>
          </div><!-- end tabbed content left-->
        </div><!--End of row -->
        <!-- End all other learners -->

         <style>
          input[type="search"]::-webkit-search-cancel-button{
            -webkit-appearance: searchfield-cancel-button;
          }
        </style>

        <?php if(!isset($_POST['edit_placement'])):?>
        <form method='POST'>
        <!-- Search all other learners, regardless of coach -->
        <div class="row"><!--Start of row -->
            <div class="col-xs-12" class="col-md-6" class="col-lg-6">
            <h3>Add a learner to a work placement:</h3>
            
             <p>Complete the following form to add a student to a work placement:</p>


              <?php 
              // Fire to let the user know the update is successful. Only fire after update, not after update and during search 
              if(isset($_GET['update']) && $_GET['update']=='true' && !isset($_POST['placement_search'])):?>
              <script>
                $(document).ready(function(){
                $("#success_message").show().delay(5000).fadeOut();
                setTimeout('window.location.href="/workexperience/CRM/learner.php"', 2000); 
              });
              </script>
              <div class="alert alert-success" id='success_message'>
                <strong>Success!</strong> That record has been updated.
              </div>
              <?php endif;?>

              <?php if(isset($_GET['error']) && $_GET['error']=='true' && !isset($_GET['noplaces'])):?>
              <script>
                $(document).ready(function(){
                $("#error_message").show().delay(5000).fadeOut();
                setTimeout('window.location.href="/workexperience/CRM/learner.php"', 2000); 
              });
              </script>
              <div class="alert alert-danger" id='error_message'>
                <strong>Sorry!</strong> Error, that is a duplicate placement you are trying to enter.
              </div>
              <?php endif;?>

              <?php if(isset($_GET['error']) && $_GET['error']=='true' && isset($_GET['noplaces'])):?>
              <script>
                $(document).ready(function(){
                $("#error_message_places").show().delay(10000).fadeOut();
                setTimeout('window.location.href="/workexperience/CRM/learner.php"', 5000); 
              });
              </script>
              <div class="alert alert-danger" id='error_message_places'>
                <strong>Sorry!</strong> Error! there are not enough places left to assign that learner with '<?=$_GET['noplaces']?>'. You need to contact either Carolyn Ashworth (2028) or Lucy Scott-Rushton (2036) in BDU about this needing to be updated or corrected on to the Saleforce application. 
              </div>
              <?php endif;?>

              <?php if(isset($_GET['error']) && $_GET['error']=='false'):?>
              <script>
                $(document).ready(function(){
                $("#success_message").show().delay(5000).fadeOut();
                setTimeout('window.location.href="/workexperience/CRM/learner.php"', 2000); 
              });
              </script>
              <div class="alert alert-success" id='success_message'>
                <strong>Successfully added.</strong> That placement has now been added to the application.
              </div>
              <?php endif;?>

            <div class='learner_form_section'>
              <label for="exampleInputEmail1">Add the learner:</label>
              <div class="inner-addon left-addon">
                <i class="glyphicon glyphicon-user"></i>
                <input type="search" class="form-control" name='learner' class='add_learner' value='' placeholder='Start to enter the student details. You can search for firstname, surname or student ID.' id='autocompletelearner' pattern=".{12,}" required title="Something is wrong. Search again"/>
              </div>
              <span class='message'>You can search for the firstname, surname, or the learner ID.</span> 
            </div> 

            <?php if(isset($addPlacement['is_exempt']) && $addPlacement['is_exempt']=='1'):?>
            <script>
              $(document).ready(function(){
              $(".card").slideDown('slow');
            });
            </script>

            <div class="card" style="width:100%;">
              <div class="card-body">
                <h5><?=$addPlacement['student_name']?> is <u>exempt</u> from attending a My Workspace placement</h5>
                <p>The details for the learner exemption are outlined below:</p>
                <p class="card-text"><?=$addPlacement['exempt_details']?></p>
                <a class="btn btn-primary btn-sm" href="/workexperience/CRM/learner.php" class="card-link">Click to add another learner</a>
              </div>
            </div>
            <?php endif;?>

            <div id="support" style="width:100%;">
              <div class="card-body">
                <p class="support-text">Support needs identified for this learner are:</p>
                <p id='support_needs'></p>
              </div>
            </div>

             <div id="exempt" style="width:100%;">
              <div class="card-body">
                <p class="support-text">This student is exempt because:</p>
                <p id='exempt_needs'></p>
              </div>
            </div>

            <div id="work_ready" style="width:100%;">
              <div class="card-body">
                <p class="support-text">This student is not work ready yet:</p>
                <p>This learner is not <em>fully work ready</em> for any placements and cannot be added at this time. The learners LDMs and tutors are required to have signed off the learner in the first instance in SID, which enables the learner to be finally 'work ready'. Once the sign off is fully completed, you'll be able to revisit this page and add them as normal.</p>
                <!--<p><a href='learner.php'>Clear the form and search again</a></p>-->

                <button class="btn btn-info" type="reset" onclick="location.href='learner.php'">
                   Click to reset the form
                </button>
              
              </div>
            </div>
          
            <div class='learner_form_section'>
              <label for="exampleInputEmail1">Add the placement:</label>
              <div class="inner-addon left-addon">
                <i class="glyphicon glyphicon-home"></i>
                <input type="search" class="form-control" name='placement' class='add_placement' placeholder='Start to enter the placement details. You can search for name or address of the premises.' name='' value='' id='autocompletelearnerplacement' pattern=".{20,}" required title="Something is wrong. Search again"/>
              </div>
            </div>
            <br/>

             <!--<div class='learner_form_section'>
              <label for="exampleInputEmail1">Type of placement (read-only):</label>
              <div class="inner-addon left-addon">
                <i class="glyphicon glyphicon-home"></i>
                <input type="text" disabled class="form-control" name='placement_type_shown' id='placement_type_shown' placeholder='This will be display the placement type once you have searched for a learner above.'/>
                 <input type="hidden" class="form-control" name='placement_type_hidden' id='placement_type_hidden'/>
              </div>
            </div>-->

            <!-- Placement type -->

            <label for="exampleInputEmail1">Is the placement Work Experience or Social Action?</label>

            <div class="radio">
              <label><input type="radio" name="placement_code" id='social' value='social' autofocus required="required">Social Action</label>
            </div>

            <div class="radio">
              <label><input type="radio" name="placement_code" id='ZWRKX001' value='ZWRKX001'>5 Day/30hr Placement</label>
            </div>
            <div class="radio disabled">
              <label><input type="radio" name="placement_code" id='industrial_placement' value='ZWRKX002'>Industrial Placement</label>
            </div>
            <br/>

             <!-- Placement rating, bronze, silver and gold -->


            <label for="exampleInputEmail1">Is the placement Bronze, Silver or Gold?</label>
            <div class="radio">
              <label><input type="radio" name="placement_type" value='bronze' autofocus required="required">Bronze</label>
            </div>
            <div class="radio">
              <label><input type="radio" name="placement_type" value='silver'>Silver</label>
            </div>
            <div class="radio disabled">
              <label><input type="radio" name="placement_type" value='gold'>Gold</label>
            </div>


            <div class='single_dates'> <!-- Start the single dates -->

               <div class='learner_form_section'>
                <label for="exampleInputEmail1">Add the start date of the placement:</label>
                <div class="inner-addon left-addon">
                  <i class="glyphicon glyphicon-home"></i>
                  <input type="date" class="form-control" name='start_date'/>
                </div>
              </div>

              <div class='learner_form_section'>
                <label for="exampleInputEmail1">Add the end date of the placement:</label>
                <div class="inner-addon left-addon">
                  <i class="glyphicon glyphicon-home"></i>
                  <input type="date" class="form-control" name='end_date'/>
                </div>
              </div>

            </div> <!-- End the single dates -->



            <div class='multidate'> <!-- Start the multi dates -->

              <label for="exampleInputEmail1">Select multiple dates for this industrial placement:</label>
              <p style='color:red;font-weight:bold;'> Please not: You will not be able to change from 'Industrial Placement' to another placement type later.</p>
              <p><input type="text" class="form-control multidatepicker" name='multipledates' placeholder="Pick the multiple dates of the learner placement. Click here to open the date picker." value=""/></p>

            </div> <!-- End the multi dates -->



            <div class='learner_form_section'>
              <label for="exampleInputEmail1">Add the start time:</label>
              <div class="inner-addon left-addon">
                <i class="glyphicon glyphicon-home"></i>
                <input type="time" class="form-control" name='start_time' value='08:00' required/>
              </div>
            </div>

        
            <div class='learner_form_section'>
              <label for="exampleInputEmail1">Add the end time:</label>
              <div class="inner-addon left-addon">
                <i class="glyphicon glyphicon-home"></i>
                <input type="time" class="form-control" name='end_time' value='17:00' required/>
              </div>
            </div>

            <div class='learner_form_section'>
              <label for="exampleInputEmail1">Assign the coach:</label>
              <div class="inner-addon left-addon">
                <i class="glyphicon glyphicon-user"></i>
                <input type="search" class="form-control" name='coach' placeholder='Start to enter the coach details. You can search for name email or username of the coach.' value='' id='autocompletelearnercoach' pattern=".{20,}" required title="Something is wrong. Search again"/>
              </div>
               <span class='message'>You can search for the firstname, surname, or the learner ID.</span>
            </div>
              <input type='hidden' name='add_placement' value='1'/>
              <p class='form'><button id='add_placement_button' name='submit' value='submit' class="btn btn-primary">Add the placement</button><p>

              <p><a href='learner.php'>Clear the form and search again</a></p>
              
          </form>
        </div>
      </div> <!-- End row -->
    <?php endif;?>


       <!-- Open the edit box on the page and pull the record to edit the placement -->
         <?php if(isset($_POST['edit_placement'])):
          // Get all the placement details so we can use them to populate the form
          $pDetails = $placement->getPlacementDetailsFromInstance($_POST['edit_placement']);
          //echo '<pre>';var_dump($pDetails);die;
          // Get the multidates, if there are any.
          $multidates = $placement->getPlacementMultiDates($pDetails[0]['learner_id'],$pDetails[0]['paid']);
          
          // Check if the learner has been withdrawn.
          $withdrawn = $placement->checkIfWithdrawn($_POST['edit_placement']);
          //echo '<pre>';print_r($withdrawn);die;
         ?>
          <script>
            $(document).ready(
              function(){
              //$("body").hide("slow");  
              $(".edit_placement").fadeToggle( "slow", "linear" );
              }
          );
          </script>
        <?php endif;?>
        <!-- Open the edit box on the page and pull the record to edit -->


    <!-- Start edit of a placement -->

    <form method='POST' name='add a learner'>
      <fieldset>
       <div class='edit_placement'> <!-- This is the start of create a learner placement -->
         <div class="row"><!--Start of row -->
          <div class="col-xs-12" class="col-md-6" class="col-lg-6">
             <h2>Edit the <?php echo (isset($_POST['placement_name'])) ? $_POST['placement_name'] : $pDetails[0]['placement_name']; ?> placement here for <?=$pDetails[0]['forename']?> <?= $pDetails[0]['surname']?></h2>
             <p>Just update the placement details and save once they are completed. </p>

              <!-- The learner has been withdrawn -->
              <?php if($withdrawn!=false):?>
              <div class="alert alert-danger" role="alert">This learner was withdrawn on: <?=date('l jS \of F Y h:i:s A',strtotime($withdrawn[0]['date_created']))?> with the the status of: <em><?=$withdrawn[0]['details_of_withdrawal']?></em></div>
              <?php endif;?>
              <!-- The learner has been withdrawn -->

             <div class='learner_form_section'>
                <label for="exampleInputEmail1">Edit the learner:</label>
                <div class="inner-addon left-addon">
                  <i class="glyphicon glyphicon-user"></i>
                  <input disabled type="search" class="form-control" required pattern=".{10,}" title="Something is wrong. Search again" name='edit_learner' value='<?php if(isset($placementUpdatedInfo)){ echo $placementUpdatedInfo[0]['forename'] .' '.$placementUpdatedInfo[0]['surname'].' '.$pDetails[0]['learner_id'];} else{echo $pDetails[0]['forename'] .' '.$pDetails[0]['surname'].' '.$pDetails[0]['learner_id'];}?>' placeholder='Start to enter the student details. You can search for firstname, surname or student ID.' id='autocompletelearner'/>
                </div>
                <span class='message'>You can search for the firstname, surname, or the learner ID.</span> 
              </div> 

                <div class='multidate'> <!-- Start the multi dates -->

                  <label for="exampleInputEmail1">Select multiple dates for this industrial placement</label>
                   <p><input type="text" class="form-control multidatepicker" name='multipledates' placeholder="Pick the multiple dates of the learner placement" value="<?php if(isset($multidates)) echo $multidates;?>"/></p>

                </div> <!-- End the multi dates -->


               <div class='single_dates'> <!-- Start the single dates -->

                   <div class='learner_form_section'>
                    <label for="exampleInputEmail1">Add the start date of the placement:</label>
                    <div class="inner-addon left-addon">
                      <i class="glyphicon glyphicon-home"></i>
                      <input type="date" class="form-control" name='start_date' value="<?=$pDetails[0]['start_date']?>"/>
                    </div>
                  </div>

                  <div class='learner_form_section'>
                    <label for="exampleInputEmail1">Add the end date of the placement:</label>
                    <div class="inner-addon left-addon">
                      <i class="glyphicon glyphicon-home"></i>
                      <input type="date" class="form-control" name='end_date' value="<?=$pDetails[0]['end_date']?>"/>
                    </div>
                  </div>

              </div> <!-- End the single dates -->

              <div class='learner_form_section'>
                <label for="exampleInputEmail1">Edit the start time:</label>
                <div class="inner-addon left-addon">
                  <i class="glyphicon glyphicon-home"></i>
                  <input type="time" class="form-control" name='start_time' value='<?=$pDetails[0]['start_time']?>' required/>
                </div>
              </div>

            
               <div class='learner_form_section'>
              <label for="exampleInputEmail1">Edit the end time:</label>
              <div class="inner-addon left-addon">
                <i class="glyphicon glyphicon-home"></i>
                <input type="time" class="form-control" name='end_time' value='<?=$pDetails[0]['end_time']?>' required/>
              </div>
            </div>

              <div class='learner_form_section'>
                <label for="exampleInputEmail1">Edit the placement:</label>
                <div class="inner-addon left-addon">
                  <i class="glyphicon glyphicon-home"></i>
                  <input type="search" class="form-control" required pattern=".{10,}" title="Something is wrong. Search again" name='edit_placement' placeholder='Start to enter the placement details. You can search for name or address of the premises.' value="<?php if(isset($placementUpdatedInfo)){ echo $placementUpdatedInfo[0]['placement_id'].' '.$placementUpdatedInfo[0]['placement_name'];?> <?=$placementUpdatedInfo[0]['address']; } else{ echo $pDetails[0]['placement_id'].' '.$pDetails[0]['placement_name'];?> <?=$pDetails[0]['address'];}?>" id='autocompletelearnerplacement'/>
                </div>
              </div>

              <br/>

               <!-- Placement type -->

               <script>
                   if($("#ZWRKX002").is(':checked')){ // Industrial placement chosen
              alert('Test');
          }
               </script>

              <div <?php if($pDetails[0]['placement_code']==NULL) echo 'class="alert alert-danger"'?>>

                 <?php if($pDetails[0]['placement_code']==NULL):?>
                    <p><strong>Please:</strong> can you update this placement to show which <em>type of placement</em> this is. The options are: </p>
                    <br/> 
                    <ol>
                      <li>Social Action</li>
                      <li>5 Day/30hr Placement</li>
                      <li>Industrial Placement</li> 
                    </ol>  
                    <br/>
                  <?php endif;?>

                <label for="exampleInputEmail1">Is this placement Work Experience or Social Action?</label>
                
                <div class="radio">
                  <label><input type="radio" name="placement_code" <?php if(trim($pDetails[0]['placement_code']) =='social') echo 'checked'; ?> value='social' id='social' autofocus required="required"
                     <?php if(trim($pDetails[0]['placement_code']) =='ZWRKX002') echo 'disabled'; ?>


                    >Social Action</label>
                </div>

                <div class="radio">
                  <label><input type="radio" name="placement_code" <?php if(trim($pDetails[0]['placement_code']) =='ZWRKX001') echo 'checked'; ?> value='ZWRKX001' id='ZWRKX001'
                     <?php if(trim($pDetails[0]['placement_code']) =='ZWRKX002') echo 'disabled'; ?>


                    >5 Day/30hr Placement</label>
                </div>
                <div class="radio disabled">
                  <label><input type="radio" name="placement_code" <?php if(trim($pDetails[0]['placement_code']) =='ZWRKX002') echo 'checked'; ?> value='ZWRKX002' id='industrial_placement'
                    <?php if(trim($pDetails[0]['placement_code']) =='ZWRKX001' || trim($pDetails[0]['placement_code']) =='social') echo 'disabled'; ?>
                    

                    >Industrial Placement</label>
                

                </div>
                <br/>
              
              </div>

              <!-- Placement rating, bronze, silver and gold -->

             <div <?php if($pDetails[0]['placement_type']==NULL) echo 'class="alert alert-danger"'?>>

               <?php if($pDetails[0]['placement_type']==NULL):?>
                  <p><strong>Please:</strong> can you update this placement to show if it is bronze, silver or gold. </p>
                  <br/> 
                <?php endif;?>

              <label for="exampleInputEmail1">Is the placement Bronze, Silver or Gold?</label>
              <div class="radio">
                <label><input type="radio" name="placement_type" value='bronze' <?php if($pDetails[0]['placement_type']=='bronze') echo 'checked'; ?> autofocus required="required">Bronze</label>
              </div>
              <div class="radio">
                <label><input type="radio" name="placement_type" value='silver' <?php if($pDetails[0]['placement_type']=='silver') echo 'checked'; ?>>Silver</label>
              </div>
              <div class="radio disabled">
                <label><input type="radio" name="placement_type" value='gold' <?php if($pDetails[0]['placement_type']=='gold') echo 'checked'; ?>>Gold</label>
              </div>
            </div>

            <div class='learner_form_section'>
              <label for="exampleInputEmail1">Edit the coach:</label>
              <div class="inner-addon left-addon">
                <i class="glyphicon glyphicon-user"></i>
                <input type="search" class="form-control" required pattern=".{10,}" title="Something is wrong. Search again" name='edit_coach' placeholder='Start to enter the coach details. You can search for name email or username of the coach.' value='<?php if(isset($placementUpdatedInfo)){ echo $placementUpdatedInfo[0]['coach_name'].' | '.$placementUpdatedInfo[0]['coach_email'].' | '.$placementUpdatedInfo[0]['username'].' | '.$placementUpdatedInfo[0]['coach_id'];} else {echo $pDetails[0]['coach_name'].' | '.$pDetails[0]['coach_email'].' | '.$pDetails[0]['username'].' | '.$pDetails[0]['coach_id'];}?>' id='autocompletelearnercoach'/>
              </div>
            </div>

              <input type='hidden' name='paid' value='<?=$pDetails[0]['paid']?>'/>
             
              <hr/>

              <input type='hidden' name='learner_id' value='<?=$pDetails[0]['learner_id']?>'/>
              <input type='hidden' name='coach_id' value='<?=$pDetails[0]['coach_id']?>'/>
              <input type='hidden' name='placement_name' value='<?=$pDetails[0]['placement_name']?>'/>

              <p class='from_top'><button <?php if($withdrawn) echo 'disabled'?> type="submit" id="save_edit_placement" name='save_edit_placement' value='<?=$pDetails[0]['paid']?>' class="btn btn-primary">Save</button></p>
            </div> 
          </div>
        </div>
      </fieldset>
    </form>

    <?php if(isset($pDetails[0])):?>
    
    <hr/>

    <!-- The withdraw a leaner section -->

   <h3>Withdraw the learner from the placement:</h3>

   <!--See if the learner has a career coach review done and if so, hide the 'Withdraw learner section and give the reason'-->
   <?php $coachReviewed = $learner->hasUserHadCoachReview($pDetails[0]['paid']);?>
   <?php $coachReviewedDetails = $learner->getUSerCoachReview($pDetails[0]['paid']);?>

    <?php if($coachReviewed!=false): // No coach review, show the withdrawn section option ?>
    <p>This student has <strong>completed their work placement so cannot be withdrawn.</strong></p>
    <p>Status: <strong><?=$coachReviewedDetails[0]['status']?></strong>.</p> 
    <p>Date completed: <?=date('l jS \of F Y H:i \h\r\s',strtotime($coachReviewedDetails[0]['date_created']))?>.</p>
    <p>Comments from the coach: <em>'<?=trim($coachReviewedDetails[0]['comments'])?>'</em></p>
    <?php endif;?>

   <?php if($coachReviewed!=true): // If the coach review has been completed, remove the withdraw section below?>

    <p class='from_top'><button type="submit" id="withdraw_learner" name='withdraw_learner' value='1' class="btn btn-danger">Withdraw this learner from placement</button></p>

    <div class='withdraw_learner_div'>
      <div class="form-group">
        <form method='POST'>

          <input type="hidden" name='paid' value='<?=$pDetails[0]['paid']?>'/>
          <input type="hidden" name='placement_id' value='<?=$pDetails[0]['placement_id']?>'/>
          <input type="hidden" name='learner_id' value='<?=$pDetails[0]['learner_id']?>'/>

          <!--<label for="exampleFormControlTextarea1">Reason for withdrawal</label>
          <textarea class="form-control" id="details_of_withdrawal" name='details_of_withdrawal' placeholder='Please provide more details on why the student has been withdrawn from the placement.' rows="7" required><?php if($withdrawn[0]['details_of_withdrawal']) echo $withdrawn[0]['details_of_withdrawal'];?></textarea>-->


         <div class="form-group">
          <label for="sel1">Reason for the withdrawal:</label>
          <p><strong>Note</strong>: Withdrawing the learner as 'Failed with learner issues' will set the learner as <strong>'not work ready'</strong>. In order for them to be withdrawn, but <strong>still work ready</strong>, select one of the other options instead.</p>
          <select class="form-control" id="sel1" name='details_of_withdrawal'>
            <option>Please select</option>
            <option <?php if($withdrawn[0]['details_of_withdrawal']=='Failed with learner issues') echo 'selected';?>>Failed with learner issues</option>
            <option <?php if($withdrawn[0]['details_of_withdrawal']=='Failed with employer issues') echo 'selected';?>>Failed with employer issues</option>
            <option <?php if($withdrawn[0]['details_of_withdrawal']=='Failed with both employer and learner issues') echo 'selected';?>>Failed with both employer and learner issues</option>
          </select>
        </div>
        
      </div>

          <!-- The learner has not been withdrawn -->
          <?php if($withdrawn==false):?>
          <p class='from_top'><button type="submit" id="submit_withdraw_learner" name='submit_withdraw_learner' value='1' class="btn btn-primary">Submit withdrawal</button></p>
          <p>This will mark the learner as <span class='red-bold'>withdrawn</span> but will not affect their placement in the application</p>
          <?php endif;?>
          <!-- The learner has not been withdrawn -->

          <!-- The learner has been withdrawn -->
          <?php if($withdrawn!=false):?>
          <p class='from_top'><button type="submit" id="update_withdraw_learner" name='update_withdraw_learner' value='1' class="btn btn-danger">Update withdrawal</button></p>
          <div class="alert alert-danger" role="alert">This learner was withdrawn on: <?=date('l jS \of F Y h:i:s A',strtotime($withdrawn[0]['date_created']))?> with the the status of: <em><?=$withdrawn[0]['details_of_withdrawal']?></em></div>
          <?php endif;?>
          <!-- The learner has been withdrawn -->

        
        </form>
      </div>
      <?php endif;?>

     <?php endif; // End hide withdraw is coach review is completed?>

    <hr/>

    <!-- End the withdraw a leaner section -->
    
  
    <!-- End edit of a placement -->

    
    <!-- Autocomplete results here -->
    <!-- Search all other learners, regardless of coach -->
       <div class='learner_form_section'>
        <form method='POST'>
          <h3>Search the learner placements:</h3>
          <p>To find a learners details, simply search for the learner below using the learners name or learner id.</p>
          <div class="inner-addon left-addon">
            <i class="glyphicon glyphicon-user"></i>
            <input type="search" class="form-control" name='learner_placement_search' class='add_learner' value='' placeholder='Start to enter the student details. You can search for firstname, surname or student ID.' id='autocompletelearnerbottom' pattern=".{12,}" required title="Something is wrong. Search again"/>
          </div>
          <span class='message'>You can search for the firstname, surname, or the learner ID.</span> 
        </form>
        </div> 
    <!-- Autocomplete results end here -->

    <!-- Just a back button from the edit a placement screen -->
    <?php if(isset($_POST['edit_placement'])):?>
      <p class='from_top'>Back to <a href='/workexperience/CRM/learner.php'>add a placement or search learner</a></p>
    <?php endif;?>
    <!-- Just a back button from the edit a placement screen -->



    <!-- START: This is where the results will sit form the placements search immediately above -->
    <?php if(isset($learnerPlacements) && $learnerPlacements!==false):?>
       <?php //echo '<pre>';print_r($learnerPlacements);die;?>
     <div class="row"><!--Start of row -->
        <div class="col-md-12">
          <h3>Search results for: <?=explode(' ',$_POST['learner_placement_search'])[0] .''.explode(' ',$_POST['learner_placement_search'])[1]?></h3>
         <div class="table-responsive">
            <table class="table table-bordered">
              <thead>
                    <?php for($i=0;isset($learnerPlacements[$i]);$i++):?>
                    <tr>
                      <th>Student name</th>
                      <td><?=$learnerPlacements[$i]['full_name']?></td>
                    <tr>
                    <tr>
                      <th>Placement</th>
                      <td><?=$learnerPlacements[$i]['placement_name']?>, <?=$learnerPlacements[$i]['placement_address']?></td>
                    </tr>
                    <tr>
                      <th>Coach name</th>
                      <td><?=$learnerPlacements[$i]['coach_name'] ?></td>
                    </tr>
                      <tr>
                      <th>Withdrawn from placement</th>
                      <td><?php
                      echo ($placement->checkIfWithdrawn($learnerPlacements[$i]['paid'])==false) ? 'No' : 'Yes';?></td>
                    </tr>
                    <tr>
                      <form method='POST'>
                        <th><button id="edit_placement" name='edit_placement' value='<?=$learnerPlacements[$i]['paid']?>' class="btn btn-danger">Edit</button></th>
                        <td></td>
                      </form>
                    </tr>
                    <tr>
                      <th>&nbsp;</th>
                      <td>&nbsp;</td>
                    </tr>
                  <?php endfor;?>
              </thead>  
          </table>
          </div>  
        </div>
    </div>

     <p class='from_top'><a href="/workexperience/CRM/learner.php">Clear results</a> and search again </p>
    
    <?php endif;?>
    <?php if(isset($learnerPlacements) && $learnerPlacements===false):?>
      <h3>No search results for: <?=explode(' ',$_POST['learner_placement_search'])[1]?></h3>
      <p><a href="/workexperience/CRM/learner.php">Clear results</a> and search again </p>
    <?php endif;?>
  <!-- END: This is where the results will sit form the placements search immediately above -->
             
  </div> <!-- End container-->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!--<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>-->
    <script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js'></script>
     <script  src="../js/multidate.js"></script>
  

  </body>
</html>
