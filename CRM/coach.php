<?php

// Stop the cache
require_once '../stop_cache.php';

ini_set('display_errors', 'On');

/**
 * Get the PSR4 autoloader and load all the classes required.
 */
spl_autoload_register(function ($class_name) {
    include '../classes/'.$class_name . '.php';
});

// Instantiate a new instance of coach.
$coach = new CareerCoachReview;

// Check if this is a staff memeber and if not, deny access.
if($coach->isStaffMember()===false)
{
  echo 'Not a staff member, no access!';die;
}

// First find out if this is a coach - coaches, see all work experience. Check the coaches table for the users Shib and if it's there, they're a coach. 
if($isCareerCoach = $coach->isCareerCoach())
{
  // Get the coach details. 
  $isCareerCoach = $coach->isCareerCoach();
}
else
{
  // Not a coach so redirect later.
  echo 'Not a career coach so no access';die;
}

// Create an instance of the CRMCoach instance.
$coach = new CRMCoach;

// A form on the page has been posted to. Complete the function and ensure the page refreshes.
if(isset($_POST))
{ 
  // Add a coach
  if(isset($_POST['create_wp_coach']))
  {

    //echo '<pre>';print_r($_POST);die;

    // something posted to the add coach form.
    $addResult = $coach->create_wp_coach($_POST);
  }   
}

// A form on the page has been posted to. Complete the function and ensure the page refreshes.
if(isset($_POST))
{ 
  // Add a coach
  if(isset($_POST['update_wp_coach']))
  {
    // something posted to the update the coach record.
    $updateResult = $coach->update_wp_coach($_POST);
    //print_r($updateResult);
  }   
}

// Pull all the coach instances.
$allCoach = $coach->getAllCoach();
?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>CRM for My Workspace app</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="../css/bootstrap-theme.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="../css/theme.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

    <script src="../js/workexperience.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Some jQuery to handle the buttons and divs for the forms-->
     <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="crossorigin="anonymous"></script>

      <!-- Include the jQuery for buttons and navs -->
      <script src="../js/includes/button_div_jquery.js"></script>
    
  </head>
  <body>

    <!-- Fixed navbar -->
    <!--<nav class="navbar navbar-inverse navbar-fixed-top">-->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
           <a class="navbar-brand" href="/workexperience/">CRM - My Workspace app</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="../index.php">My Workspace app</a></li>
            <!--<li><a href="#about">About</a></li>-->
            <!--<li><a href="#contact">Contact</a></li>-->
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav><!-- end fixed navbar -->

    <div class="container-fluid theme-showcase" role="main">

      <div class="row"><!--Start of row -->
      <!-- Tabbed content left-->
      <div class="col-md-12">
          <ul class="nav nav-tabs">
            <li><a href="index.php">Introduction</a></li>
            <li><a href="learner.php">Manage Learners</a></li>
            <li class="active"><a href="coach.php">Manage Staff</a></li>
            <!--<li><a href="placement.php">Manage Placement</a></li>-->
             <li><a href="employer.php">Manage Employer</a></li>
             <li><a href="als.php">Manage ALS</a></li>
             <li><a href="../coach/">Review learners</a></li>
          </ul>
      <br/>
    </div><!-- end tabbed content left-->
  </div><!--End of row -->


<!-- Start of button row -->
<div class="row">
  <div class="col-md-12">
     <h3>CRM: Staff</h3>
    <P>This is the staff section, where a member of staff can be added or their details edited.</p> 

    <p><button id="add_coach" class="btn btn-info">Add a coach</button><p>

        <!-- Duplicate message start --> 
        <?php if(isset($updateResult['duplicate'])):?>
        <script>
          $(document).ready(function(){
          $("#error_message").show().delay(5000).fadeOut();
          });
        </script>

       
        <?php endif;?>
        <!-- Duplicate message ends --> 

        <!-- Success message start --> 
        <?php if(isset($addResult['success']) || isset($updateResult['success']) ):?>
        <script>
          $(document).ready(function(){
          $("#success_message").show().delay(5000).fadeOut();
          });
        </script>

        <div class="alert alert-success" id='success_message'>
          <strong><?php if(isset($addResult['success'])) echo $addResult['success'];?></strong>
          <strong><?php if(isset($updateResult['success'])) echo $updateResult['success'];?></strong>
        </div>
        <?php endif;?>
        <!-- Success message ends --> 

        <!-- Form inout errors here start --> 
        <?php if(isset($updateResult) && !isset($updateResult['success'])):?>
        <script>
          $(document).ready(function(){
          //$("#add_coach").html('Close add a coach'); // Change the button to close.
          //$(".create_coach").show(); // Keep the form window open so the user can sort the form before they resubmit.
          $("#error_message").show().delay(5000).fadeOut();
        });
        </script>

        <div class="alert alert-danger" id='error_message'>
          <strong>Sorry errors on the form as below:</strong>
          <?php foreach($updateResult AS $error):?>
            <ul>
              <li><?=$error?></li>
            </ul>
          <?php endforeach;?>
        </div>
        <?php endif;?>
        <!-- End Form input errors here start --> 


        <!-- Form inout errors here start --> 
        <?php if(isset($addResult) && !isset($addResult['success'])):?>
        <script>
          $(document).ready(function(){
          $("#add_coach").html('Close add a coach'); // Change the button to close.
          $(".create_coach").show(); // Keep the form window open so the user can sort the form before they resubmit.
          $("#error_message").show().delay(5000).fadeOut();
        });
        </script>

        <div class="alert alert-danger" id='error_message'>
          <strong>Sorry errors on the form as below:</strong>
          <?php foreach($addResult AS $error):?>
            <ul>
              <li><?=$error?></li>
            </ul>
          <?php endforeach;?>
        </div>
        <?php endif;?>
        <!-- End Form input errors here start --> 

        <!-- Open the edit box on the page and pull the record to edit -->
         <?php if(isset($_POST['edit_wp_coach'])):
         // Get all the coaches details by coach ID
         $cDetails = $coach->getCoachByID($_POST['edit_wp_coach']);
         //echo '<pre>';
         //print_r($cDetails);die;
         ?>
          <script>
             // The add a new coach section /workexperience/CRM/coach.php
            $(document).ready(
              function(){
              //$("body").hide("slow");  
              $(".edit_coach").fadeToggle( "slow", "linear" );
              }
          );
          </script>
        <?php endif;?>
        <!-- Open the edit box on the page and pull the record to edit -->

    <hr/>

    <!-- Start update a coach record -->
     <div class='edit_coach'> <!-- This is the start of create a learner placement -->
      <div class="row">
        <div class="col-md-12">
          <h3>You can edit the coach record for <?=$cDetails[0]['name']?> below:</h3>

           <h4 class="highlight">Admin users <em>are not work coaches</em> and this access is reserved just for access rights to the application. <span style='font-weight:bold;text-decoration:underline;color:red;'>Do not add a coach with admin access</span> as they will not be visible when assigning a coach to a learner. </h4>
            <hr/>
            
          <form method="POST"><!--Form starts here-->
            <div class="form-group">
              <label for="first_name">Full name</label>
              <input type="text" class="form-control" name='name' id="name" placeholder='This is your name' value='<?=$cDetails[0]['name']?>' required/>
            </div>
            <div class="form-group">
              <label for="coach">Phone</label>
              <input type="tel" class="form-control" name='phone' id="phone" placeholder='Please include the full number including the dialing code' value='<?=$cDetails[0]['phone']?>' required/>
            </div>
             <div class="form-group">
              <label for="coach">Mobile</label>
              <input type="tel" class="form-control" name='mobile' id="phone" placeholder='Please include the full number including the dialing code' value='<?=$cDetails[0]['mobile']?>' required/>
            </div>
             <div class="form-group">
              <label for="coach">E-mail</label>
              <input type="email" class="form-control" name='email' id="email" placeholder='Include the full e-mail address' required value='<?=$cDetails[0]['email']?>'/>
            </div>
             <div class="form-group">
              <label for="coach">Username for login </label>
              <input type="text" class="form-control" name='username' id="username" placeholder='This is the usual username you would use to logon to the staff homepage. You must enter this correctly' value='<?=$cDetails[0]['username']?>' required/>
              <small id="emailHelp" class="form-text text-muted"><span style='color:red;'>** If you update, you <em>must</em> ensure this is correct, as this is <strong>twinned with your learners</strong>.**</span>.</small>
            </div>


              <!-- teacher section -->
              <label for="exampleInputEmail1">Is this a tutor?</label>
              <p><small id="emailHelp" class="form-text text-muted"><span style='color:red;'>** This is an important field so please complete**</span>.</small></p>
              <div class="radio">
                <label><input type="radio" name="is_teacher" value='1' <?php if($cDetails[0]['is_teacher']=='1') echo 'checked';?>>Yes</label>
              </div>

              <div class="radio">
                <label><input type="radio" name="is_teacher" value='0' <?php if($cDetails[0]['is_teacher']=='0') echo 'checked';?>>No</label>
              </div>

             <!-- Curricukum leader section -->
              <label for="exampleInputEmail1">Is this a Curriculum Leader?</label>
              <p><small id="emailHelp" class="form-text text-muted"><span style='color:red;'>** This is an important field as only a CL can finally sign off a learner**</span>.</small></p>
              <div class="radio">
                <label><input type="radio" name="curriculum_leader" value='1' autofocus required="required" <?php if($cDetails[0]['curriculum_leader']=='1') echo 'checked';?>>Yes</label>
              </div>

              <div class="radio">
                <label><input type="radio" name="curriculum_leader" value='0' <?php if($cDetails[0]['curriculum_leader']=='0') echo 'checked';?>>No</label>
              </div>


            <!-- If the learner has assistance to complete the entry, the tick box shows this -->
              <p>Tick if you are requiring <em>admin access ONLY</em>.
              <input type="checkbox" name="admin" value="1" id="admin" class="form-check-input" <?php if($cDetails[0]['admin']==1) echo 'checked';?>></p>
              <p>This option <strong>should only be checked for admin users wanting read-only access and not</strong> for coaches or teachers. Coaches with this option, <span style='color:red;text-decoration:underline;'>will not be able to have students <u>assigned to them</u></span>.</p>
            <br/>
            <input type='hidden' name='row_id' value='<?=$cDetails[0]['id']?>'/>
            <p><button type="submit" name='update_wp_coach' value='1' class="btn btn-primary">Update coach</button></p>
          </form><!--Form end here-->
          <p><button id='close_wp_coach' name='close_wp_coach' value='1' class="btn btn-info">Close edit</button></p>
        </div>
      </div>
    </div>
    <!-- End update a coach record -->

      <div class='create_coach'> <!-- This is the start of create a learner placement -->
      <div class="row">
        <div class="col-md-12">

            <h4 class="highlight">Admin users <em>are not work coaches</em> and this access is reserved just for access rights to the application. <span style='font-weight:bold;text-decoration:underline;color:red;'>Do not add a coach with admin access</span> as they will not be visible when assigning a coach to a learner. </h4>
            <hr/>

          <p>Please complete the form below to create the coach on the system placement. All the fields are required fields</p>
          <form method="POST"><!--Form starts here-->
            <div class="form-group">
              <label for="first_name">First name</label>
              <input type="text" class="form-control" name='first_name' id="first_name" placeholder='This is your firstname' value='<?php if(isset($_POST['first_name'])) echo $_POST['first_name'];?>' required/>
            </div>
             <div class="form-group">
              <label for="last_name">Last name</label>
              <input type="text" class="form-control" name='last_name' id="last_name" placeholder='This is your surname' value='<?php if(isset($_POST['last_name'])) echo $_POST['last_name'];?>' required/>
            </div>
            <div class="form-group">
              <label for="coach">Phone</label>
              <input type="tel" class="form-control" name='phone' id="phone" placeholder='Please include the full number including the dialing code' value='<?php if(isset($_POST['phone'])) echo $_POST['phone'];?>' required/>
            </div>
             <div class="form-group">
              <label for="coach">Mobile</label>
              <input type="tel" class="form-control" name='mobile' id="phone" placeholder='Please include the full number including the dialing code' value='<?php if(isset($_POST['mobile'])) echo $_POST['mobile'];?>' required/>
            </div>
             <div class="form-group">
              <label for="coach">E-mail</label>
              <input type="email" class="form-control" name='email' id="email" placeholder='Include the full e-mail address' required value='<?php if(isset($_POST['email'])) echo $_POST['email'];?>'/>
            </div>
             <div class="form-group">
              <label for="coach">Username for login </label>
              <input type="text" class="form-control" name='username' id="username" placeholder='This is the usual username you would use to logon to the staff homepage. You must enter this correctly' value='<?php if(isset($_POST['username'])) echo $_POST['username'];?>' required/>
              <small id="emailHelp" class="form-text text-muted"><span style='color:red;'>** You must include this to be twinned with your learners correctly**</span>.</small>
            </div>


             <!-- teacher section -->
              <label for="exampleInputEmail1">Is this a tutor?</label>
              <p><small id="emailHelp" class="form-text text-muted"><span style='color:red;'>** This is an important field so please complete**</span>.</small></p>
              <div class="radio">
                <label><input type="radio" name="is_teacher" value='1' autofocus required="required">Yes</label>
              </div>

              <div class="radio">
                <label><input type="radio" name="is_teacher" value='0'>No</label>
              </div>

              <!-- Curricukum leader section -->
              <label for="exampleInputEmail1">Is this a Curriculum Leader?</label>
              <p><small id="emailHelp" class="form-text text-muted"><span style='color:red;'>** This is an important field as only a CL can finally sign off a learner**</span>.</small></p>
              <div class="radio">
                <label><input type="radio" name="curriculum_leader" value='1' autofocus required="required">Yes</label>
              </div>

              <div class="radio">
                <label><input type="radio" name="curriculum_leader" value='0'>No</label>
              </div>


              <!-- If the learner has assistance to complete the entry, the tick box shows this -->
              <p>Tick if you are requiring <em>admin access only</em>.
              <input type="checkbox" name="admin" value="1" id="admin" class="form-check-input"></p>
              <p>This option <strong>should only be checked for admin users wanting read-only access and not</strong> for coaches or teachers. Coaches with this option, <span style='color:red;text-decoration:underline;'>will not be able to have students <u>assigned to them</u></span>
              </p>

            <br/>
            <button type="submit" name='create_wp_coach' value='1' class="btn btn-primary">Add coach</button>
          </form><!--Form end here-->
        </div>
      </div>
    </div> <!-- End create a learner placement -->

    <!-- List all the coaches with the option to edit -->
    <div class='all_coach'>
      <div class="row">
        <div class="col-md-12">
          <h3>All current coaches</h3>

        <div class="table-responsive">
          <table class="table table-bordered table-striped">
            <tbody>
            <?php 
            foreach($allCoach AS $row=>$headers):?>
            <tr>
                <th>Name</th>
                <td><?=$headers['name'];?></td>
            </tr>
            <tr>
                <th>Phone</th>
                <td><?=$headers['phone'];?></td>
            </tr>
            <tr>
                <th>Mobile</th>
                <td><?=$headers['mobile'];?></td>
            </tr>
            <tr>
                <th>Email</th>
                <td><?=$headers['email'];?></td>
            </tr>
             <tr>
                <th>Username</th>
                <td><?=$headers['username'];?></td>
            </tr>
             <tr>
                <th>Admin access</th>
                <td><?php echo ($headers['admin']==1) ? 'Yes' : 'No';?></td>
            </tr>
              <tr>
                <th>Curriculum Leader (access)</th>
                <td><?php echo ($headers['curriculum_leader']==1) ? 'Yes' : 'No';?></td>
            </tr>
            </tr>
              <tr>
                <th>Is a tutor</th>
                <td><?php echo ($headers['is_teacher']==1) ? 'Yes' : 'No';?></td>
            </tr>
            <tr>
                <th>
                  <form method='POST'>
                    <!-- Form to handle the editing of a record for a days entry to the the logbook -->
                    <input type='hidden' name='row_id' value='<?=$headers['id']?>'?>
                    <button id='edit_wp_coach' name='edit_wp_coach' value="<?=$headers['id']?>" class="btn btn-danger">Edit</button>
                  </form>
                </th>
                <!-- end Form to handle the editing of a record for a days entry to the the logbook -->
                <td>&nbsp;</td>
            </tr>
            <tr>
              <th>&nbsp;</th>
              <td>&nbsp;</td>
            </tr>
            </tbody>
            <?php endforeach;?>
            </tbody>
          </table>
        </div>

        </div>
      </div>
    </div>

</div> <!--end main container -->

<!-- footer here -->
<footer class="footer">
  <hr/>
  <div class="container">
    <p style="text-align:center;">Bolton College 2017</p>
  </div>

</footer>
<!-- end footer end -->



    <!-- Bootstrap core JavaScript ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
