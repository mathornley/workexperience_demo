<?php

/**
 * Get the PSR4 autoloader and load all the classes required.
 */
spl_autoload_register(function($class_name) {
    include 'classes/'.$class_name.'.php';
});

// Set display errors to on.
//ini_set('display_errors', 'On');

// Instantiate an instance of the autocomplete class we'll use for search.
$ac = new AutoComplete;

// Got something to search to autocomplete.
if (isset($_GET['term']))
{	
	try
	{
	    $ac->autoCompleteAllPlacements($_GET['term']);
	}
	catch(PDOException $e)
	{
		echo 'ERROR: ' . $e->getMessage();
	}
}