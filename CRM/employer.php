<?php

/**
 * Get the PSR4 autoloader and load all the classes required.
 */
spl_autoload_register(function($class_name) {
    include '../classes/'.$class_name.'.php';
});

ini_set('display_errors', 'On');

// Stop the cache
require_once '../stop_cache.php';

// Instantiate a new instance of coach.
$coach = new CareerCoachReview;

// Check if this is a staff memeber and if not, deny access.
if($coach->isStaffMember()===false)
{
  echo 'Not a staff member, no access!';die;
}

// First find out if this is a coach - coaches, see all work experience. Check the coaches table for the users Shib and if it's there, they're a coach. 
if($isCareerCoach = $coach->isCareerCoach())
{
  // Get the coach details. 
  $isCareerCoach = $coach->isCareerCoach();
}
else
{
  // Not a coach so redirect later.Fc
  echo 'Not a career coach so no access';die;
}

// Pull all placements, regardless of the coach.
$allplacements = $coach->getPlacements();

// Pull all the coach specific placements using the coachID from call above.
$coachPlacements = $coach->getPlacementsByCoachID($isCareerCoach[0]['id']);

// Get all the coaches in the we_coach table.
$allCoaches = $coach->getAllCoach();

// Instantiate an instance of the autocomplete class we'll use for search.
$ac = new AutoComplete;
$learner = new learner;
$placement = new Placement;

// The placement has been searched, now show all the learners for that placement. 
if(isset($_POST['placement_search']) && $_POST['placement_search'])
{
  //echo explode(' ',$_POST['placement_search'])[1];die;
  $learnerPlacements = $placement->getLearnerByPlacementDetails($_POST);
}

// The placement has been searched, now show all the learners for that placement. 
if(isset($_POST['save_edit_placement']) && $_POST['save_edit_placement'])
{
  $uandPass = $placement->createPasswordAndUsername($_POST);
}
?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Coach section of the My Workspace application.</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="../css/bootstrap-theme.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="../css/theme.css" rel="stylesheet">

    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/themes/base/minified/jquery-ui.min.css" type="text/css" />

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- JQuery -->
    <script
    src="https://code.jquery.com/jquery-1.12.4.js"
    integrity="sha256-Qw82+bXyGq6MydymqBxNPYTaUXXq7c8v3CwiYwLLNXU="
    crossorigin="anonymous"></script>

     <!-- Autocomplete stuff -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


    <!--Autcomplete listeners here here -->
    <script>
      $(function(){
        
        // This searches and returns the learner but looks up the learner placement when chosen.
        $("#autocompletelearnerplacementsearch").autocomplete({
          source: "search_placement.php", //autocomplete here  - source is the PHP script: search_learner.php
          minLength: 1 // Match for how many letters before the magic starts.
        }); 

      });
    </script>

    <!-- Brief styles -->
    <link rel="stylesheet" type="text/css" href="../css/styles.css">
    <!-- Brief styles end --> 

  </head>

  <body>

    <!-- Fixed navbar -->
    <!--<nav class="navbar navbar-inverse navbar-fixed-top">-->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
           <a class="navbar-brand" href="/workexperience/">CRM - My Workspace app</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="/workexperience/">My Workspace app</a></li>
            <!--<li><a href="#about">About</a></li>-->
            <!--<li><a href="#contact">Contact</a></li>-->
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav><!-- end fixed navbar -->

  
      <div class="container-fluid theme-showcase" role="main">

        <div class="row"><!--Start of row -->
            <!-- Tabbed content left-->
            <div class="col-md-12">
                <ul class="nav nav-tabs">
                   <li><a href="index.php">Introduction</a></li>
                  <li><a href="learner.php">Manage Learners</a></li>
                  <li><a href="coach.php">Manage Staff</a></li>
                  <!--<li><a href="placement.php">Manage Placement</a></li>-->
                  <li class="active"><a href="employer.php">Manage Employer</a></li>
                  <li><a href="als.php">Manage ALS</a></li>
                  <li><a href="../coach/">Review learners</a></li>
                </ul>
            <br/>
          </div><!-- end tabbed content left-->
        </div><!--End of row -->
        <!-- End all other learners -->

         <style>
          input[type="search"]::-webkit-search-cancel-button{
            -webkit-appearance: searchfield-cancel-button;
          }
        </style>

        <?php if(!isset($_POST['edit_placement'])):?>
        <form method='POST'>
        <!-- Search all other learners, regardless of coach -->
        <div class="row"><!--Start of row -->
            <div class="col-xs-12" class="col-md-6" class="col-lg-6">

              <?php 
              // This checks that the password being changed isn't the same one
              if(isset($uandPass) && isset($uandPass['password'])):?>
              <script>
                $(document).ready(function(){
                $("#error_message").show().delay(5000).fadeOut();
              });
              </script>
              <div class="alert alert-danger" id='error_message'>
                <strong>Sorry!</strong> <?=$uandPass['password']?>
              </div>
              <?php endif;?>

              <?php 
              // Fire to let the user know the update is successful. Only fire after update to the details and when the email sends.
              if(isset($uandPass) && $uandPass==true && !isset($uandPass['password'])):?>
              <script>
                $(document).ready(function(){
                $("#success_message").show().delay(5000).fadeOut();
              });
              </script>
              <div class="alert alert-success" id='success_message'>
                <strong>Success!</strong> That record has been updated and the user has been emailed their credentials.
              </div>
              <?php endif;?>

              <?php 
              if(isset($uandPass) && $uandPass==false):?>
              <script>
                $(document).ready(function(){
                $("#error_message").show().delay(5000).fadeOut();
              });
              </script>
              <div class="alert alert-danger" id='error_message'>
                <strong>Sorry!</strong> Error, there has been an error with the update.
              </div>
              <?php endif;?>

             <h3>Edit the employer</h3>
             <p>Below is where you can create the employer password, which will enable the employer to log-in and review the student on the fornt end of the application. A username and password can be assigned and also edited, which is in fact a reset. If a password is reset or username changed or both, the user will be emailed these details.</p>

        </div>
      </div> <!-- End row -->
    <?php endif;?>


       <!-- Open the edit box on the page and pull the record to edit the placement -->
         <?php if(isset($_POST['edit_placement'])):
         // Get all the placement details so we can use them to populate the form
         $pDetails = $placement->getPlacementByID($_POST['edit_placement']);
         ?>
          <script>
            $(document).ready(
              function(){
              //$("body").hide("slow");  
              $(".edit_placement").fadeToggle( "slow", "linear" );
              }
          );
          </script>
        <?php endif;?>
        <!-- Open the edit box on the page and pull the record to edit -->


    <!-- Start edit of a placement -->
    <form method='POST' onsubmit="return confirm('Are you sure about these changes, as the user will now be emailed this username and password for their account?')">
       <div class='edit_placement'> <!-- This is the start of create a learner placement -->
         <div class="row"><!--Start of row -->
          <div class="col-xs-12" class="col-md-6" class="col-lg-6">
             <h2>Edit the '<?php echo $pDetails[0]['name']; ?>' username and password here</h2>

             <p>You can edit the username and password of the <?php echo $pDetails[0]['name']; ?> placement below. Once you have done this, you'll be asked to confirm and the placement will receive the login-in credentials by email.</p>

              <?php //echo'<pre>';print_r($pDetails);?>

              <?php if(!isset($pDetails[0]['username']) && !isset($pDetails[0]['password'])):?>
                <p style='font-weight:bold;'> This account has no username or password and this can be assigned below</p>
              <?php endif;?>

             <div class='learner_form_section'>
                <label for="exampleInputEmail1">Placement name:</label>
                <div class="inner-addon left-addon">
                  <i class="glyphicon glyphicon-user"></i>
                  <input disabled type="text" class="form-control" name='name' value='<?=$pDetails[0]['name']?>'/>
                </div>
              </div> 

                <div class='learner_form_section'>
                <label for="exampleInputEmail1">Placement address:</label>
                <div class="inner-addon left-addon">
                  <i class="glyphicon glyphicon-user"></i>
                  <input disabled type="text" class="form-control" name='address' value='<?=$pDetails[0]['address'].', '. $pDetails[0]['post_code']?>'/>
                </div>
              </div>

              <div class='learner_form_section'>
                <label for="exampleInputEmail1">Username:</label>
                <div class="inner-addon left-addon">
                  <i class="glyphicon glyphicon-user"></i>
                  <input type="text" class="form-control" name='username' value='<?=$pDetails[0]['username']?>'  <?php if(!isset($pDetails[0]['username'])) echo "placeholder='This account has no username and can be chosen here'";?> required/>
                </div>
              </div> 

               <div class='learner_form_section'>
                <label for="exampleInputEmail1">
                <?php echo (!isset($pDetails[0]['password'])) ? 
                'Password' : 'Change password';?>:</label>
                <div class="inner-addon left-addon">
                  <i class="glyphicon glyphicon-user"></i>
                  <input type="text" class="form-control" name='password' value='' required <?php if(!isset($pDetails[0]['password'])) echo "placeholder='This account has no password and can be chosen here'";?> />
                </div>
              </div> 

              <p class='from_top'><button type="submit" id="save_edit_placement" name='save_edit_placement' value='<?=$pDetails[0]['id']?>' class="btn btn-primary">Update</button></p>
          </div> 
        </div>
      </div>
    </form>
    <!-- End edit of a placement -->

    
    <!-- Autocomplete results here -->
      <div class="row"><!--Start of row -->
        <div class="col-xs-12" class="col-md-6" class="col-lg-6">
         <h2>Search across all the learner placements below</h2>
             <p>Below you can search and view any placement for the learner. The learners of each placement will be displayed in chronological order</p>

                <div class='learner_form_section'>
                <label for="exampleInputEmail1">Query the placements:</label>
                <div class="inner-addon left-addon">
                  <i class="glyphicon glyphicon-search"></i>

                  <form method='POST'>
                    <input type="search" class="form-control" required pattern=".{10,}" title="Something is wrong. Search again" name='placement_search' placeholder='Start to enter the placement details. You can search for name or address of the premises.' value='' id='autocompletelearnerplacementsearch' pattern=".{10,}" required title="Something is wrong. Search again"/>
                 </form>
                </div>
          </div>
        </div>
      </div>
    <!-- Autocomplete results end here -->

    <!-- Just a back button from the edit a placement screen -->
    <?php if(isset($_POST['edit_placement'])):?>
      <p class='from_top'>Back to <a href='/workexperience/CRM/employer.php'>Manage Employer</a></p>
    <?php endif;?>
    <!-- Just a back button from the edit a placement screen -->


    <!-- START: This is where the results will sit form the placements search immediately above -->
    <?php if(isset($learnerPlacements) && $learnerPlacements!==false):?>
     <div class="row"><!--Start of row -->
        <div class="col-md-12">
          <h3>Search results:</h3>
         <div class="table-responsive">
            <table class="table table-bordered">
              <thead>
                    <?php for($i=0;isset($learnerPlacements[$i]);$i++):?>
                    <tr>
                      <th>Placement name</th>
                      <td><?=$learnerPlacements[$i]['name']?></td>
                    <tr>
                    <tr>
                      <th>Address</th>
                      <td><?=$learnerPlacements[$i]['address']?>, <?=$learnerPlacements[$i]['post_code']?></td>
                    </tr>
                    <tr
                    <?=(!isset($learnerPlacements[$i]['email'])) ? "style='background-color:yellow;'" : '';?>
                    >
                      <th>Email</th>
                      <td><?=(isset($learnerPlacements[$i]['email'])) ? $learnerPlacements[$i]['email'] : '<strong>Email address not set - contact BDU to </strong> to check Salesforce for this employer as they have no email address. Andrew Hughes (2954) or Carolyn Ashworth (2028) can assist you in getting the email address added to Salesforce. Once this is complete, the record will unlock. Without an email address, login credentials cannot be sent to the employer for them to log on.';?></td>
                    </tr>
                    <tr
                     <?=(!isset($learnerPlacements[$i]['manager'])) ? "style='background-color:yellow;'" : '';?>
                    >
                      <th>Manager name</th>
                      <td><?=(isset($learnerPlacements[$i]['manager'])) ? $learnerPlacements[$i]['manager'] : 'Manager name not set - ask for this to be added to Salesforce.';?></td>
                    </tr>
                    <tr>
                      <th>Phone</th>
                      <td><?=$learnerPlacements[$i]['phone'] ?></td>
                    </tr>
                    <tr>
                      <th>Username</th>
                      <td
                       <?php 
                      if(isset($learnerPlacements[$i]['username'])) {echo 'style="background:green;color:#fff;"';}
                      else{echo 'style="background:red;color:#fff;"';}?>
                      >
                      <?php 
                      echo (isset($learnerPlacements[$i]['username'])) ? $learnerPlacements[$i]['username'] : 'Username needs creating';?>
                      </td>
                    </tr>
                     <tr>
                      <th>Password</th>
                      <td
                      <?php 
                        if(isset($learnerPlacements[$i]['password'])) {echo 'style="background:green;color:#fff;"';}
                        else{echo 'style="background:red;color:#fff;"';}?>
                      ><?php echo (isset($learnerPlacements[$i]['password'])) ? 'Password set' : 'Password needs creating';?></td>
                    </tr>
                    <tr>
                      <form method='POST'>
                        <th><button <?php if(!isset($learnerPlacements[$i]['email'])) echo 'disabled';?> id="edit_placement" name='edit_placement' value='<?=$learnerPlacements[$i]['id']?>' class="btn btn-danger">Edit</button></th>
                        <td></td>
                      </form>
                    </tr>
                    <tr>
                      <th>&nbsp;</th>
                      <td>&nbsp;</td>
                    </tr>
                  <?php endfor;?>
              </thead>  
          </table>
          </div>  
        </div>
    </div>

     <p class='from_top'><a href="/workexperience/CRM/learner.php">Clear results</a> and search again </p>
    
    <?php endif;?>
    <?php if(isset($learnerPlacements) && $learnerPlacements===false):?>
      <h3>No search results for: <?=explode(' ',$_POST['placement_search'])[1]?></h3>
      <p><a href="/workexperience/CRM/learner.php">Clear results</a> and search again </p>
  <?php endif;?>
  <!-- END: This is where the results will sit form the placements search immediately above -->
             
  </div> <!-- End container-->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
