<?php

/**
 * Get the PSR4 autoloader and load all the classes required.
 */
spl_autoload_register(function($class_name) {
    include '../classes/'.$class_name.'.php';
});

// Set display errors to on. 
ini_set('display_errors', 'On');

// Stop the cache
require_once '../stop_cache.php';

// Instantiate an instance of the autocomplete class we'll use for search and so we can use the SQL class.
$ac = new AutoComplete;

// Get the learner id via a get request from (CRM/learner.php) and sanitise the input
$learnerID = $ac->cleanseVars($_GET['learner']);

// Comp;elte
$learnerALS = $ac->conn->select('SELECT * FROM `we_als` WHERE `learner_id`=? AND `exempt`=? AND `support_details` IS NOT NULL', [$learnerID,0]);

if($learnerALS==FALSE) 
{
	echo 'no results';
}
else
{
	echo trim($learnerALS[0]['support_details']);
}

//echo $learnerID;die;