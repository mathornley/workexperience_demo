<?php

echo '<pre>'; print_r($_GET); die;

/**
 * Get the PSR4 autoloader and load all the classes required.
 */
spl_autoload_register(function($class_name) {
    include '../classes/'.$class_name.'.php';
});

// Set display errors to on. 
ini_set('display_errors', 'On');

// Stop the cache
require_once '../stop_cache.php';

// Instantiate an instance of the autocomplete class we'll use for search and so we can use the SQL class.
$ac = new AutoComplete;

// Get the learner id via a get request from (CRM/learner.php) and sanitise the input
$learnerID = $ac->cleanseVars($_GET['learner']);

// Look up the learner and see if they're in the `elp_workready2` table and if not, instance FALSE (they're not approved).
$learner = $ac->conn->select('SELECT * FROM `elp_workready2` WHERE `learner`=?', [$learnerID]);

// They're not in there, instance FALSE
if($learner==FALSE) 
{
	echo false;
}
// They're in the table, so let's see if they're work ready. 
else
{
	// If they are work ready, with all LDMS and tutors signing off. GO!
	if($_GET['ldn']=='1' && $learner[0]['Q2_TUTOR']=='1' && $learner[0]['Q3_TUTOR']=='1' && 
	$learner[0]['Q4_TUTOR']=='1' && $learner[0]['Q5_TUTOR']=='1' && $learner[0]['Q6_TUTOR']=='1' &&
	$learner[0]['Q1_LDM']=='1' && $learner[0]['Q2_LDM']=='1' && $learner[0]['Q3_LDM']=='1' && 
	$learner[0]['Q4_LDM']=='1' && $learner[0]['Q5_LDM']=='1' && $learner[0]['Q6_LDM']=='1')
	{
		echo true;
	}

	// In the table but not fully signed off or the review hasn't been fully done. 
	else
	{
		echo false;
	}
}
