<?php

/**
 * We need a course code to identify a if a placement is 30hrs/5days or an industrial placement of 50 days. 
 * This is so we can pass the learner id and get the course code back again from the SID tables:
 * We'll use this on the form to add a learner to a placement at: http://www.boltoncc.ac.uk/workexperience/CRM/learner.php
 * Course codes are:
 * ZWRKX001 = 30hrs/5 days
 * ZWRKX002 = 50 days, aka as Industry placement
 * The student will always have one code!. 
 * 
 */

/**
 * Get the PSR4 autoloader and load all the classes required.
 */
spl_autoload_register(function($class_name) {
    include '../classes/'.$class_name.'.php';
});

// Set display errors to on. 
ini_set('display_errors', 'On');

// Stop the cache
require_once '../stop_cache.php';

// Instantiate an instance of the autocomplete class we'll use for search and so we can use the SQL class.
$ac = new AutoComplete;

// Get the learner id via a get request from (CRM/learner.php) and sanitise the input.
$learnerID = $ac->cleanseVars($_GET['learner']);

// Look up the learner qual codes from the tables with a join query.
$qual = $ac->conn->select
(
	"SELECT `qual_code`, `courseinfo`.`course_code`,`courseinfo`.`occ` FROM `ext_a_courseinfo` `courseinfo` INNER JOIN `ext_ebs_data_learnergroup` `lgp` ON `courseinfo`.`course_code` = `lgp`.`course_code` AND `courseinfo`.`occ` = `lgp`.`occ` WHERE `learner` = ? AND (`qual_code` = ? OR `qual_code` = ?)"
,
	[
		$learnerID,
		'ZWRKX001',
		'ZWRKX002'
	]
);

// No course code found for the learner in the tables. 
if($qual==false) 
{
	echo false;
}
// They're in the table, so let's see if they've a course code. 
else
{
	// They have a course code. 
	if(isset($qual[0]['qual_code']))
	{
		// Yep, a course code
		if($qual[0]['qual_code']=='ZWRKX001') echo $qual[0]['qual_code'] . ' - 30hr/5 day placement';
		if($qual[0]['qual_code']=='ZWRKX002') echo $qual[0]['qual_code'] . ' - Industry placement';
	}

	// There's a row but no course code
	else
	{
		echo false;
	}
}
