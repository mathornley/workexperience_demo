<?php

// Stop the cache
require_once '../stop_cache.php';

// Error handling is on.
ini_set('display_errors', 'On');

/**
 * Get the PSR4 autoloader and load all the classes required.
 */
spl_autoload_register(function ($class_name) {
    include '../classes/'.$class_name . '.php';
});

// Instantiate a new instance of coach.
$coach = new CareerCoachReview;

// Check if this is a staff memeber and if not, deny access.
if($coach->isStaffMember()===false)
{
  echo 'Not a staff member, no access!';die;
}

// First find out if this is a coach - coaches, see all work experience. Check the coaches table for the users Shib and if it's there, they're a coach. 
if($isCareerCoach = $coach->isCareerCoach())
{
  // Get the coach details. 
  $isCareerCoach = $coach->isCareerCoach();
}
else
{
  // Not a coach so redirect later.
  echo 'Not a career coach so no access';die;
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>CRM for My Workspace app</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="../css/bootstrap-theme.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="../css/theme.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

    <script src="../js/workexperience.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Some jQuery to handle the buttons and divs for the forms-->
     <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="crossorigin="anonymous"></script>
    
  </head>
  <body>

   <!-- Fixed navbar -->
    <!--<nav class="navbar navbar-inverse navbar-fixed-top">-->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/workexperience/">My Workspace app</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            
            <li class="active"><a href="index.php">Introduction</a></li>
            <li><a href="learner.php">Manage Learners</a></li>
            <li><a href="coach.php">Manage Staff</a></li>
            <!--<li><a href="placement.php">Manage Placement</a></li>-->
            <li><a href="employer.php">Manage Employer</a></li>
            <li><a href="als.php">Manage ALS</a></li>
            <!--<li><a href="#about">About</a></li>-->
            <li><a href="../coach/">Review learners</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav><!-- end fixed navbar -->

    <div class="container-fluid theme-showcase" role="main">

   <div class="row"><!--Start of row -->
      <!-- Tabbed content left-->
      <div class="col-md-12">
          <ul class="nav nav-tabs">
            <li class="active"><a href="index.php">Introduction</a></li>
            <li><a href="learner.php">Manage Learners</a></li>
            <li><a href="coach.php">Manage Staff</a></li>
            <!--<li><a href="placement.php">Manage Placement</a></li>-->
            <li><a href="employer.php">Manage Employer</a></li>
            <li><a href="als.php">Manage ALS</a></li>
            <li><a href="../coach/">Review learners</a></li>
          </ul>
      <br/>
    </div><!-- end tabbed content left-->
  </div><!--End of row -->


<div class="row">
  <div class="col-md-12">
    <p>The My Workspace CRM lets you manage the work placement programme for learners across the College.</p>
  </div>
</div>

<div class="row">

   <div class="col-xs-12 col-sm-3">
      <div class='card_outer'>
        <div><a title='Review Learners: Here is the section for the learner review to be completed.' href='/workexperience/coach/'><img alt='Review Learners: Here is the section for the learner review to be completed.' class='card_image' src='../images/review-learners.png'/></a></div>
        <!--<div>Manage Learners: Search for learners and give them a work placement.</div>-->
      </div>
    </div>

    <div class="col-xs-12 col-sm-3">
      <div class='card_outer'>
        <div><a title='Manage Learners: Search for learners and give them a work placement.' href='learner.php'><img alt='Manage Learners: Search for learners and give them a work placement.' class='card_image' src='../images/manage-learners.jpg'/></a></div>
        <!--<div>Manage Learners: Search for learners and give them a work placement.</div>-->
      </div>
    </div>

    <div class="col-xs-12 col-sm-3">
       <div class='card_outer'>
         <div><a title='Find an employer and edit their username and password to the My Workspace app.' href='employer.php'><img alt='Find an employer and edit their username and password to the My Workspace app.' class='card_image' src='../images/manage-employers.jpg'/></a></div>
        <!--<div>Manage Employers: Find an employer and edit their username and password to the work experience app.</div>-->
      </div>
    </div>

    <div class="col-xs-12 col-sm-3">
      <div class='card_outer'>
        <div><a title='Manage Staff: Add and edit colleagues on the My Workspace app.' href='coach.php'><img alt='Manage Staff: Add and edit colleagues on the work experience app.' class='card_image' src='../images/manage-staff.jpg'/></a></div>
        <!--<div>Manage Staff: Add and edit colleagues on the work experience app.</div>-->
      </div>
    </div>

    <br/>
</div>

<div class="row">

  <div class="col-xs-12 col-sm-3">
       <div class='card_outer'>
         <div><a title='Manage learners with additional support needs.' href='als.php'><img alt='Manage learners with additional support needs.' class='card_image' src='../images/manage-als.jpg'/></a></div>
        <!--<div>Manage ALS: Manage learners with additional support needs.</div>-->
      </div>
    </div>
   
</div>

<!-- footer here -->
<footer class="footer">
  <hr/>
  <div class="container">
    <p style="text-align:center;">Bolton College 2017</p>
  </div>

    <!-- Bootstrap core JavaScript ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>