<?php

// Stop the cache
require_once '../stop_cache.php';

ini_set('display_errors', 'On');

/**
 * Get the PSR4 autoloader and load all the classes required.
 */
spl_autoload_register(function ($class_name) {
    include '../classes/'.$class_name . '.php';
});

// Instantiate a new instance of coach.
$coach = new CareerCoachReview;

// Create a new instance of the placement instance to handle the addition and edits of the placement.
$placement = new Placement;

// Check if this is a staff memeber and if not, deny access.
if($coach->isStaffMember()===false)
{
  echo 'Not a staff member, no access!';die;
}

// First find out if this is a coach - coaches, see all work experience. Check the coaches table for the users Shib and if it's there, they're a coach. 
if($isCareerCoach = $coach->isCareerCoach())
{
  // Get the coach details. 
  $isCareerCoach = $coach->isCareerCoach();
}
else
{
  // Not a coach so redirect later.
  echo 'Not a career coach so no access';die;
}

// Create an instance of the CRMCoach instance.
$coach = new CRMCoach;

// A form on the page has been posted to. Complete the function and ensure the page refreshes.
if(isset($_POST))
{ 
  // Add a coach
  if(isset($_POST['create_wp_placement']))
  {
    // something posted to the add coach form.
    $addResult = $placement->create_wp_placement($_POST);
  }   
}

// A form on the page has been posted to. Complete the function and ensure the page refreshes.
if(isset($_POST))
{ 
  // Add a coach
  if(isset($_POST['update_wp_placement']))
  {
    //print_r($_POST);die;
    // something posted to the update the placement record.
    $updateResult = $placement->update_wp_placement($_POST);
    //print_r($updateResult);
  }   
}

// Pull all the coach instances.
$allplacements = $placement->getAllPlacements();
//echo '<pre>';
//print_r($allplacements);die;
?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>CRM for Work experience app</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="../css/bootstrap-theme.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="../css/theme.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

    <script src="../js/workexperience.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Some jQuery to handle the buttons and divs for the forms-->
     <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="crossorigin="anonymous"></script>

      <!-- Include the jQuery for buttons and navs -->
      <script src="../js/includes/button_div_jquery.js"></script>
    
  </head>
  <body>

    <!-- Fixed navbar -->
    <!--<nav class="navbar navbar-inverse navbar-fixed-top">-->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
           <a class="navbar-brand" href="/workexperience/">CRM - Work experience app</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="../index.php">Work Experience app</a></li>
            <!--<li><a href="#about">About</a></li>-->
            <!--<li><a href="#contact">Contact</a></li>-->
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav><!-- end fixed navbar -->

    <div class="container-fluid theme-showcase" role="main">

      <div class="row"><!--Start of row -->
      <!-- Tabbed content left-->
      <div class="col-md-12">
          <ul class="nav nav-tabs">
            <li><a href="index.php">Introduction</a></li>
            <li><a href="learner.php">Manage Learners</a></li>
            <li><a href="coach.php">Manage Coach</a></li>
            <!--<li class="active"><a href="coach.php">Manage Placement</a></li>-->
            <li><a href="employer.php">Manage Employer</a></li>
          </ul>
      <br/>
    </div><!-- end tabbed content left-->
  </div><!--End of row -->


<!-- Start of button row -->
<div class="row">
  <div class="col-md-12">
     <h3>CRM: Placement</h3>
    <P>This is the placement section, where a placement can be added.</p>
    <hr/>
    <p><button id="add_placement" class="btn btn-info">Add a placement</button><p>

        <!-- Duplicate message start --> 
        <?php if(isset($updateResult['duplicate'])):?>
        <script>
          $(document).ready(function(){
          $("#error_message").show().delay(5000).fadeOut();
          });
        </script>

       
        <?php endif;?>
        <!-- Duplicate message ends --> 

        <!-- Success message start --> 
        <?php if(isset($addResult['success']) || isset($updateResult['success']) ):?>
        <script>
          $(document).ready(function(){
          $("#success_message").show().delay(5000).fadeOut();
          });
        </script>

        <div class="alert alert-success" id='success_message'>
          <strong><?php if(isset($addResult['success'])) echo $addResult['success'];?></strong>
          <strong><?php if(isset($updateResult['success'])) echo $updateResult['success'];?></strong>
        </div>
        <?php endif;?>
        <!-- Success message ends --> 

        <!-- Form inout errors here start --> 
        <?php if(isset($updateResult) && !isset($updateResult['success'])):?>
        <script>
          $(document).ready(function(){
          //$("#add_coach").html('Close add a coach'); // Change the button to close.
          //$(".create_coach").show(); // Keep the form window open so the user can sort the form before they resubmit.
          $("#error_message").show().delay(5000).fadeOut();
        });
        </script>

        <div class="alert alert-danger" id='error_message'>
          <strong>Sorry errors on the form as below:</strong>
          <?php foreach($updateResult AS $error):?>
            <ul>
              <li><?=$error?></li>
            </ul>
          <?php endforeach;?>
        </div>
        <?php endif;?>
        <!-- End Form input errors here start --> 


        <!-- Form inout errors here start --> 
        <?php if(isset($addResult) && !isset($addResult['success'])):?>
        <script>
          $(document).ready(function(){
          $("#add_coach").html('Close add a coach'); // Change the button to close.
          $(".create_coach").show(); // Keep the form window open so the user can sort the form before they resubmit.
          $("#error_message").show().delay(5000).fadeOut();
        });
        </script>

        <div class="alert alert-danger" id='error_message'>
          <strong>Sorry errors on the form as below:</strong>
          <?php foreach($addResult AS $error):?>
            <ul>
              <li><?=$error?></li>
            </ul>
          <?php endforeach;?>
        </div>
        <?php endif;?>
        <!-- End Form input errors here start --> 

        <!-- Open the edit box on the page and pull the record to edit -->
         <?php if(isset($_POST['edit_wp_placement'])):
         // Get all the coaches details by coach ID
         $pDetails = $placement->getPlacementByID($_POST['edit_wp_placement']);
         //echo '<pre>';
         //print_r($cDetails);die;
         ?>
          <script>
             // The add a new coach section /workexperience/CRM/coach.php
            $(document).ready(
              function(){
              //$("body").hide("slow");  
              $(".edit_placement").fadeToggle( "slow", "linear" );
              }
          );
          </script>
        <?php endif;?>
        <!-- Open the edit box on the page and pull the record to edit -->

    <hr/>

    <!-- Start update a coach record -->
     <div class='edit_placement'> <!-- This is the start of create a learner placement -->
      <div class="row">
        <div class="col-md-12">
          <p>Please complete the form below to edit the coach on the system placement. All the fields are required fields</p>
          <form method="POST"><!--Form starts here-->
            <div class="form-group">
              <label for="name">Name (full name i.e John Smith)</label>
              <input type="text" class="form-control" name='name' id="name" placeholder='This is the full name' value='<?=$pDetails[0]['name']?>' required/>
            </div>
             <div class="form-group">
              <label for="last_name">Address</label>
              <input type="text" class="form-control" name='address' id="address" placeholder='This is your full address, without the postcode' value='<?=$pDetails[0]['address']?>' required/>
            </div>
             <div class="form-group">
              <label for="coach">Post code</label>
              <input type="text" class="form-control" name='post_code' id="post_code" placeholder='Please include the full post code' value='<?=$pDetails[0]['post_code']?>' required/>
            </div>
            <div class="form-group">
              <label for="coach">Phone</label>
              <input type="tel" class="form-control" name='phone' id="phone" placeholder='Please include the full phone number including the dialing code' value='<?=$pDetails[0]['phone']?>' required/>
            </div>
             <div class="form-group">
              <label for="coach">Email</label>
              <input type="email" class="form-control" name='email' id="email" placeholder='Please include the full email address as this is what will be used to contact the person' value='<?=$pDetails[0]['email']?>' required/>
            </div>
             <div class="form-group">
              <label for="coach">Manager (full name i.e John Smith)</label>
              <input type="text" class="form-control" name='manager' id="manager" placeholder='Please include the full contact name of the manager' value='<?=$pDetails[0]['manager']?>' required/>
            </div>
             <div class="form-group">
              <label for="sel1">Amount of available places</label>
                <select class="form-control" id="sel1" name='sfplaces' required>
                  <?php for($i=1;$i<=200;$i++):?>
                    <option <?php if($pDetails[0]['sfplaces']==$i) echo 'selected'?>><?=$i?></option>
                  <?php endfor;?>
                </select>
             </div>
             <div class="form-group">
              <label for="coach">Pick a username </label>
              <input type="text" class="form-control" name='username' id="username" placeholder='This is the username which will be assigned to the business when they login to the employer section to review the learner. The password will be generated on signup' value='<?=$pDetails[0]['username']?>' required/>
              <small id="emailHelp" class="form-text text-muted"><span style='color:#000;'>** This is the username the placement will use to sign in to complete learner reviews **</span>.</small>
            </div>
            <p>Passwords can not be changed. You will need to contact the ILT team if there are password issue or, a reset is required.</p>
            <br/>
            <input type='hidden' name='row_id' value='<?=$pDetails[0]['id']?>'/>
            <button type="submit" name='update_wp_placement' value='1' class="btn btn-primary">Update placement</button>
          </form><!--Form end here-->
        </div>
      </div>
    </div>
    <!-- End update a coach record -->

      <div class='create_placement'> <!-- This is the start of create a learner placement -->
      <div class="row">
        <div class="col-md-12">
          <p>Please complete the form below to create the coach on the system placement. All the fields are required fields</p>
          <form method="POST"><!--Form starts here-->
            <div class="form-group">
              <label for="name">Name (full name i.e John Smith)</label>
              <input type="text" class="form-control" name='name' id="name" placeholder='This is the full name' value='<?php if(isset($_POST['name'])) echo $_POST['name'];?>' required/>
            </div>
             <div class="form-group">
              <label for="last_name">Address</label>
              <input type="text" class="form-control" name='address' id="address" placeholder='This is your full address, without the postcode' value='<?php if(isset($_POST['address'])) echo $_POST['address'];?>' required/>
            </div>
             <div class="form-group">
              <label for="coach">Post code</label>
              <input type="text" class="form-control" name='post_code' id="post_code" placeholder='Please include the full post code' value='<?php if(isset($_POST['post_code'])) echo $_POST['post_code'];?>' required/>
            </div>
            <div class="form-group">
              <label for="coach">Phone</label>
              <input type="tel" class="form-control" name='phone' id="phone" placeholder='Please include the full phone number including the dialing code' value='<?php if(isset($_POST['phone'])) echo $_POST['phone'];?>' required/>
            </div>
             <div class="form-group">
              <label for="coach">Email</label>
              <input type="email" class="form-control" name='email' id="email" placeholder='Please include the full email address as this is what will be used to contact the person' value='<?php if(isset($_POST['email'])) echo $_POST['email'];?>' required/>
            </div>
             <div class="form-group">
              <label for="coach">Manager (full name i.e John Smith)</label>
              <input type="text" class="form-control" name='manager' id="manager" placeholder='Please include the full contact name of the manager' value='<?php if(isset($_POST['manager'])) echo $_POST['manager'];?>' required/>
            </div>
            <div class="form-group">
              <label for="sel1">Amount of available places</label>
                <select class="form-control" id="sel1" name='sfplaces' required>
                  <?php for($i=1;$i<=200;$i++):?>
                    <option><?=$i?></option>
                  <?php endfor;?>
                </select>
             </div>
             <div class="form-group">
              <label for="coach">Pick a username </label>
              <input type="text" class="form-control" name='username' id="username" placeholder='This is the username which will be assigned to the business when they login to the employer section to review the learner. The password will be generated on signup' value='<?php if(isset($_POST['username'])) echo $_POST['username'];?>' required/>
              <small id="emailHelp" class="form-text text-muted"><span style='color:#000;'>** This is the username the placement will use to sign in to complete learner reviews **</span>.</small>
            </div>
            <br/>
            <button type="submit" name='create_wp_placement' value='1' class="btn btn-primary">Add placement</button>
          </form><!--Form end here-->
        </div>
      </div>
    </div> <!-- End create a learner placement -->

    <!-- List all the coaches with the option to edit -->
    <div class='all_placement'>
      <div class="row">
        <div class="col-md-12">
          <h3>All current placements</h3>

        <div class="table-responsive">
          <table class="table table-bordered table-striped">
            <tbody>
            <?php 
            foreach($allplacements AS $row=>$placement):?>
            <tr>
                <th>Name</th>
                <td><?=$placement['name'];?></td>
            </tr>
            <tr>
                <th>Address</th>
                <td><?=$placement['address'];?></td>
            </tr>
             <tr>
                <th>Post code</th>
                <td><?=$placement['post_code'];?></td>
            </tr>
            <tr>
                <th>Phone</th>
                <td><?=$placement['phone'];?></td>
            </tr>
             <tr>
                <th>Email</th>
                <td><?=$placement['email'];?></td>
            </tr>
            <tr>
                <th>Place available</th>
                <td></td>
            </tr>
             <tr>
                <th>Manager</th>
                <td><?=$placement['manager'];?></td>
            </tr>
             <tr>
                <th>Username</th>
                <td><?=$placement['username'];?></td>
            </tr>
            <tr>
                <th>
                  <form method='POST'>
                    <!-- Form to handle the editing of a record for a days entry to the the logbook -->
                    <input type='hidden' name='row_id' value='<?=$headers['id']?>'?>
                    <button id='edit_wp_placement' name='edit_wp_placement' value="<?=$placement['id']?>" class="btn btn-danger">Edit</button>
                  </form>
                </th>
                <!-- end Form to handle the editing of a record for a days entry to the the logbook -->
                <td>&nbsp;</td>
            </tr>
            <tr>
              <th>&nbsp;</th>
              <td>&nbsp;</td>
            </tr>
            </tbody>
            <?php endforeach;?>
            </tbody>
          </table>
        </div>

        </div>
      </div>
    </div>

</div> <!--end main container -->

<!-- footer here -->
<footer class="footer">
  <hr/>
  <div class="container">
    <p style="text-align:center;">Bolton College 2017</p>
  </div>

</footer>
<!-- end footer end -->



    <!-- Bootstrap core JavaScript ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
