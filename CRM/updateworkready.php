<?php

//i.e: http://www.boltoncc.ac.uk/workexperience/CRM/updateworkready.php?tutor=mike%20thornley&learner=190904&option=off&column=Q2_TUTOR&value=1


// Set display errors to on. 
ini_set('display_errors', 'On');

// Stop the cache.
require_once '../stop_cache.php';

// Unpack the vars from $_GET.
if(isset($_GET['tutor'])) $tutor = $_GET['tutor'];
if(isset($_GET['learner'])) $learner = $_GET['learner'];
if(isset($_GET['column'])) $column = $_GET['column'];
if(isset($_GET['value'])) $value = $_GET['value'];


// No learner id, go no further.
if($learner==FALSE) 
{
	echo false;
}

// They're a learner so let's add the tutor workready options.
else
{
		// Connection to the database and use try/catch to catch any issues. 
		try
		{
			// Handle any PDO errors here.
			$opt = array(
		    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
		    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
			);

			$this->conn = new PDO('mysql:host=<IP>;dbname=<DBNAME>;charset=utf8mb4','<USERNAME>','<PASSWORD>',$opt);
		}
		// Catch some PDO errors.
		catch (PDOException $e)
		{
			throw new Exception('PDO Database connection failed to connect to SID database failed in line: ' . __LINE__ . ' of the file: '. __FILE__ .' with the message: '. $e->getMessage());
		}

		$date_updated = 'NOW()';

		$query = "INSERT INTO `elp_workready2` set `learner` = '{$learner}', {$column} = '{$value}', `tutor` = '{$tutor}' on duplicate key update `$column` = '{$value}', `tutor` = '{tutor}', `date_updated` = NOW();";

		// Prepare the query.
		$stm = $conn->query($query);	
    		
		// Return the result set or false on failure.
		echo ($stm->rowCount()<=0) ? false : true;
		
}
