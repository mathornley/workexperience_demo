<?php

//http://www.boltoncc.ac.uk/workexperience/CRM/fullyworkready.php?learner=191751

// Set display errors to on. 
ini_set('display_errors', 'On');

// Stop the cache.
require_once '../stop_cache.php';

// Unpack the vars from $_GET.
if(isset($_GET['learner'])) $learner = $_GET['learner'];

// No learner id, go no further.
if($learner==FALSE) 
{
	echo false;
}

// There's a learner, so run the query.
else
{
		// Connection to the database and use try/catch to catch any issues. 
		try
		{
			// Handle any PDO errors here.
			$opt = array(
		    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
		    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
			);

			$this->conn = new PDO('mysql:host=<IP>;dbname=<DBNAME>;charset=utf8mb4','<USERNAME>','<PASSWORD>',$opt);
		}
		// Catch some PDO errors.
		catch (PDOException $e)
		{
			throw new Exception('PDO Database connection failed to connect to SID database failed in line: ' . __LINE__ . ' of the file: '. __FILE__ .' with the message: '. $e->getMessage());
		}

		// See if there's any rows, where the review is completed. 	
		//$query = "SELECT * FROM `elp_workready2` WHERE `learner` = '{$learner}' AND `Q1_LDM` = 1 AND `Q2_LDM` = 1 AND `Q3_LDM` = 1 AND `Q4_LDM` = 1 AND `Q5_LDM` = 1 AND `Q6_LDM` = 1";
		
		$query = "SELECT * FROM `elp_workready2` WHERE `learner` = '{$learner}' AND `Q1_LDM` = 1 AND `Q2_LDM` = 1 AND `Q3_LDM` = 1 AND `Q4_LDM` = 1";

		// Prepare the query.
		$stm = $conn->query($query);	
    		
		// Return the row count set or false on failure.
		echo ($stm->rowCount()<=0) ? false : true;
}