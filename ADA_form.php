<div id="ADA-CONTAINER">
    <div id="ADA-HEADER" class="header_shadow">
      <!-- <h4><i class="fas fa-comment-dots fa-lg"></i>Ask Ada</h4> -->
      <div id="ADA-BRAND"></div>
      <div id="ADA-OPENCLOSE">click to open</div>
      <i id="ADA-DIRECTIONARR" class="fas fa-angle-up"></i>
    </div>
 
    <div id="ADA-SWIPE-WRAPPER">
      <div id="ADA-ORACLE-WRAPPER">
        <div id="ADA-NUDGE-FLAG" class="header_shadow_minor">
          <!-- NOTIFICATION FLAG TEMPLATE HERE -->
        </div>
        <div class="ADA-SCROLL-WRAPPER-OUTER">
          <div data-simplebar class="ADA-SCROLL-WRAPPER">
            <div id="ADA-ORACLE-CONTENT">
              <div id="ADA-Q-CONTAINER" class="shadow">
                <div id="ADA-Q-TXT"><!-- ADA QUESTION --></div>
                <div class="spinner">
                  <div class="bounce1"></div>
                  <div class="bounce2"></div>
                  <div class="bounce3"></div>
                </div>
                <div id="ADA-AUDIO"><i class="far fa-play-circle"></i></div>
              </div>
              <div class="ADA-BUBBLE shadow"><!-- ADA ANSWER--></div>
            </div>
          </div>
        </div>
      </div>
      <div id="ADA-NUDGE-WRAPPER">
        <div id="ADA-NUDGE-MENU" class="header_shadow_minor">
          <!-- NUDGE MENU TEMPLATE HERE -->
        </div>
        <div class="ADA-SCROLL-WRAPPER-OUTER-NUDGE">
          <div id="ADA-NUDGE-EMPTY-BG">
            <div>
              <i class="fas fa-inbox"></i>
              <p>More nudges coming your way soon!</p>
            </div>
          </div>
          <div data-simplebar class="ADA-SCROLL-WRAPPER">
            <div id="ADA_NUDGE-CONTAINER">
              <!-- NUDGE BUBBLE TEMPLATE HERE -->
            </div>
          </div>
        </div>
      </div>
    </div>
 
    <form
      name="adaForm"
      id="ADA-FORM-WRAPPER"
      action=""
      method="post"
      autocomplete="off"
    >
      <div class="ADA-FORMGROUP">
        <div id="ADA-HISTORY-GROUP">
          <button type="button" disabled>
            <i class="fas fa-angle-left"></i>
          </button>
          <button type="button" disabled>
            <i class="fas fa-angle-right"></i>
          </button>
        </div>
        <input
          type="text"
          name="adaquery"
          id="ADAINPUT"
          maxlength="144"
          placeholder="Type a question for Ada"
        />
        <button id="ASKADA" name="adasubmit" type="submit">
          <!-- <div id="ASKADATXT">Submit</div> -->
          <i class="fas fa-paper-plane"></i>
        </button>
      </div>
    </form>
  </div>
