<?php

/**
 * Get the PSR4 autoloader and load all the classes required.
 */
spl_autoload_register(function ($class_name) {
    include 'classes/'.$class_name . '.php';
});

// Stop the cache
require_once 'stop_cache.php';

// Set a new instance of the learner.
$learner = new Learner($_GET['placement_attended_id'],$_GET['studID']);
$coach = new CareerCoachReview();

// See if there are learner details
if($learnerDetails = $learner->learnerDetails()!=false)
{
  $learnerDetails = $learner->learnerDetails();
}

// See if there are coach details
if($coachDetails = $coach->coachDetails()!=false)
{
  // This is a coach. This is their details.
  $coachDetails = $coach->coachDetails();
  // As this is a coach, get all the learner details for the page below.
  $allLearnerPlacements = $coach->getPlacements();
}

// Gets the learner details row and the placement details for that learner.
$learnerDetails = $learner->learnerDetails();

/**
 * Check if this is a learner only viewing their own part of the app - learner ids have to match.
 * The Shib learner id has to match the URL from the browser, IF they're not a coach. 
 * As usual the coach sees it all.
 * If the user is not authorised, $_SERVER['REQUEST_URI'] is used to keep them on their page.
 */
if(!$coach->isCareerCoach() && $coach->doesTheLearnerIdMatch($_GET['studID'])==false)
{
  // The user is not a coach and not the learner requesting the URL so keep them where they are.
  header('location: /workexperience/');
}

// Decide if the midpoint review should be shown - if 50 days, yes, otherwise, no.
$showMidPointReview = $learner->showMidPointReview($_GET['placement_attended_id']);
//var_dump($showMidPointReview);die;
?>

<script type="text/javascript">
var studID = "<?php echo $_GET['studID'];?>";
//alert(studID);
</script>



<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>My Workspace app</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/theme.css" rel="stylesheet">

    <!--<link rel="stylesheet" type="text/css" href="css/ada.css" />-->


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

        <!-- Bootstrap 3 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Font Awesome 5 -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
    <!-- ADA CSS -->
    

  </head>

  <body>

    <!-- Fixed navbar -->
    <!--<nav class="navbar navbar-inverse navbar-fixed-top">-->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/workexperience/">My Workspace app</a>
        </div>

        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            
              <li><a href="index.php">Choose placement</a></li>
              <li class="active"><a href="introduction.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Introduction</a></li>
              <li><a href="location.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Where is my placement</a></li>
              <li><a href="agreement.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Work placement agreement</a></li>
              <li><a href="aims.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Aims and objectives</a></li>
              <li><a href="logbook.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Logbook</a></li>
              <li><a href="placement_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Your work placement review</a></li>
              <li><a href="employer_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Employer review</a></li>
              <li><a href="coach_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Coach review review</a>
              </li>
              <li><a href="midpoint_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Midpoint Review</a>
              </li>

              <?php if($showMidPointReview):?>
                <li><a href="midpoint_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Your midpoint review</a>
                </li>
              <?php endif;?>

              <li><a href="debug.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Report a bug</a>
              </li>
            <!--<li><a href="#about">About</a></li>-->
            <!--<li><a href="#contact">Contact</a></li>-->
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav><!-- end fixed navbar -->

    <div class="container-fluid theme-showcase" role="main">
      	<div class="row"><!--Start of row -->
    			<div class="col-md-12">
    				<div class="table-responsive">
    					<table class="table table-bordered">
    		    			<thead>
    		      				<tr>
    						        <th>Student name</th>
    						        <td><?=$learnerDetails[0]['name']?></td>
    						        <th>Student ID</th>
    						        <td><?=$learnerDetails[0]['person_code']?></td>
                        <th>Coach name</th>
                        <td><?=$coach->getCoachNameForLearner($_GET['placement_attended_id'])?></td>
    		      				</tr>
    		    			</thead>	
    	  				</table>
    				</div>	
  			</div>
  		</div> <!--End of row -->

  		<div class="row" id='test'><!--Start of row -->
          <!-- Tabbed content left-->
          <div class="col-md-12">
               <ul class="nav nav-tabs">
                <li><a href="index.php">Choose placement</a></li>
                <li class="active"><a href="introduction.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Introduction</a></li>
                <li><a href="location.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Where is my placement</a></li>
                <li><a href="agreement.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Work placement agreement</a></li>
                <li><a href="aims.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Aims and objectives</a></li>
                <li><a href="logbook.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Logbook</a></li>
                <li><a href="placement_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Your work placement review</a></li>
                <li><a href="employer_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Employer review</a></li>
                <li><a href="coach_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Coach review</a></li>
                <?php if($showMidPointReview==true):?>
                  <li>
                    <a href="midpoint_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Your midpoint review</a>
                  </li>
                <?php endif;?>
              </ul>
          <br/>
        </div><!-- end tabbed content left-->
      </div><!--End of row -->

    <div class="row"><!--Start of row -->
        <div class="col-md-6"><!-- content main full page-->
         
         <img class='pic' src='images/students.jpg'/>

        </div><!-- row ends-->
   
	
	    
		     <div class="col-md-6"><!-- content main full page-->
		    	<h4>Hi <?=$learnerDetails[0]['forename']?>! Welcome to the My Workspace system, an easier way to log your work experience progress.</h4>

          <p>A work placement is an exciting opportunity for you to gain valuable experience in the world of work! Your placement will be related to your course as well as your chosen career path, don’t worry if you’re not sure where you see yourself in the future yet, work placements are a great way of helping you to find your direction.  
          </p>

          <p>Just some of the benefits:</p>

            <ul>
                <li>Better career decision making: You’ll get a taste of what a job or workplace is like, so you can see if you are interested in that kind of career.</li>
                <li>Improved self-confidence: Working with other people and performing tasks well in the workplace will build your confidence.</li>
                <li>It strengthens your CV: My Workspace shows that you are enthusiastic and ready to work hard. Future employers, colleges and universities will be interested in any experiences that help you to stand out.</li>
                <li>Gain new skills: Getting to work on time, working alongside other people and understanding employer expectations will add to your knowledge and understanding of the workplace.</li>
                <li>Connect with employers: You’ll meet people at work and you might attend meetings or events. Some of these contacts may provide you with a reference or help you in the future.</li>
                <li>It Increases your employability skills such as communication, problem solving, social skills, self-management, commitment, enthusiasm, flexibility and team work.</li>
            </ul>

            <p>Please agree to your <a title='Click here to view your work placement agreement' href="agreement.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Work Placement Agreement.</a> You will not be able to continue on your work placement journey until you have done this.</p>

            <p>Having problems with the system, please <a title='Click here to report a bug through the debug form.' href="debug.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">report a bug</a> and we’ll get that fixed.</p>

		    </div><!-- row ends-->
  
  </div>
</div>

    <!-- footer here -->
    <footer class="footer">
    <hr/>
    	<div class="container">
        	<p style="text-align:center;">Bolton College 2017</p>
      	</div>
    </footer>
    <!-- end footer end -->
  </body>

  <?php 
   // Require the ada pop up box
   require_once('ADA_form.php');
   ?>

  </div>

   <!-- Bootstrap core JavaScript ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script defer src="../learner/adanew_dist/adabundle_v1.5.1.js"></script>
    <script src="js/ie10-viewport-bug-workaround.js"></script>
    <script src="js/bootstrap.js"></script>
   



</html>
