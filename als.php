<?php

//echo exec('whoami');die;

/**
 * Get the PSR4 autoloader and load all the classes required.
 */
spl_autoload_register(function($class_name) {
    include '../classes/'.$class_name.'.php';
});

// Display errors and set filke uploads to on.
//ini_set('display_errors', 'On');

// Stop the cache
require_once '../stop_cache.php';

// Instantiate a new instance of coach.
$coach = new CareerCoachReview;

// Instantiate an instance of the autocomplete class we'll use for search.
$ac = new AutoComplete;
$learner = new learner;
$placement = new Placement;
$ALS = new ALS;

// Check if this is a staff memeber and if not, deny access.
if($coach->isStaffMember()===false)
{
  echo 'Not a staff member, no access!';die;
}

// First find out if this is a coach - coaches, see all work experience. Check the coaches table for the users Shib and if it's there, they're a coach. 
if($isCareerCoach = $coach->isCareerCoach())
{
  // Get the coach details. 
  $isCareerCoach = $coach->isCareerCoach();
}
else
{
  // Not a coach so redirect later.
  echo 'Not a career coach so no access';die;
}

// placement has been created and all the form fields have been created.
if(isset($_POST['add_als_learner']) && $_POST['add_als_learner']=='1')
{
  $addALSLearner = $ALS->addALSLearner($_POST,$_FILES);
}

// placement has been created and all the form fields have been created.
if(isset($_POST['update_als_learner']) && is_numeric($_POST['update_als_learner']))
{
  $updateALSLearner = $ALS->updateALSLearner($_POST,$_FILES);
}

if(isset($updateALSLearner) && $updateALSLearner['error'])
{
  //print_r($updateALSLearner['error']);die;
  //print_r($updateALSLearner['success']);die;
  header("location: als.php?update=failed&error={$updateALSLearner['error']}");
}

if(isset($updateALSLearner) && $updateALSLearner==true && !isset($updateALSLearner['error']))
{
  //print_r($updateALSLearner['success']);die;
  header("location: als.php?update=success");
}

// Test to see if this user is trying to inject a different learner id in to the URL
if(isset($updateALSLearner) && $updateALSLearner==false && !isset($updateALSLearner['error']))
{
  // Keep them here - this keeps the user on the current page.
  header("location: als.php?update=failed");
}

// Pull all the ALS records lastly to present at the bottom.
$getALSAll = $ALS->getAllALSLearners();
?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Coach section of the work experience application.</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="../css/bootstrap-theme.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="../css/theme.css" rel="stylesheet">

    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/themes/base/minified/jquery-ui.min.css" type="text/css" />

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- JQuery -->
    <script
    src="https://code.jquery.com/jquery-1.12.4.js"
    integrity="sha256-Qw82+bXyGq6MydymqBxNPYTaUXXq7c8v3CwiYwLLNXU="
    crossorigin="anonymous"></script>

     <!-- Autocomplete stuff -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


    <!--Autcomplete listeners here here -->
    <script>
      $(function(){
        $("#autocompletelearner").autocomplete({
          source: "search_learner.php", //autocomplete here  - source is the PHP script: search.php
          minLength: 1 // Match for how many letters before the magic starts.
        });

        $( "#autocompletelearner").focus(function() {
          $("span.message").css( "display", "inline" ).fadeOut(5000);
        });
      });
    </script>

    <!-- Include the jQuery for buttons and navs -->
    <script src="../js/includes/button_div_jquery.js"></script>

    <!-- Brief styles -->
    <link rel="stylesheet" type="text/css" href="../css/styles.css">
    <!-- Brief styles end --> 

  </head>

  <body>

    <!-- Fixed navbar -->
    <!--<nav class="navbar navbar-inverse navbar-fixed-top">-->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
           <a class="navbar-brand" href="/workexperience/">CRM - Work experience app</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="/workexperience/">Work Experience app</a></li>
            <!--<li><a href="#about">About</a></li>-->
            <!--<li><a href="#contact">Contact</a></li>-->
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav><!-- end fixed navbar -->

  
      <div class="container-fluid theme-showcase" role="main">

        <div class="row"><!--Start of row -->
            <!-- Tabbed content left-->
            <div class="col-md-12">
                <ul class="nav nav-tabs">
                  <li><a href="index.php">Introduction</a></li>
                  <li><a href="learner.php">Manage Learners</a></li>
                  <li><a href="coach.php">Manage Staff</a></li>
                  <!--<li><a href="placement.php">Manage Placement</a></li>-->
                  <li><a href="employer.php">Manage Employer</a></li>
                  <li class="active"><a href="als.php">Manage ALS</a></li>
                </ul>
            <br/>
          </div><!-- end tabbed content left-->
        </div><!--End of row -->
        <!-- End all other learners -->

         <style>
          input[type="search"]::-webkit-search-cancel-button{
            -webkit-appearance: searchfield-cancel-button;
          }
        </style>
        
        <form method='POST' enctype="multipart/form-data">
        <!-- Search all other learners, regardless of coach -->
       
        <div class="row"><!--Start of row -->
          <?php if(!isset($_POST['edit_als_learner'])):?>
            <div class="col-xs-12" class="col-md-6" class="col-lg-6">
            <h3>Add a learner needing additional learner support.</h3>
             <p>Below is where you can add a learner who requires additional learner support. The learner needs will reflect in any further placements created and will advise staff on additional needs or learners considered exempt before the placement is created in the <a href="learner.php">Manage Learners</a></li>.</p>

              <?php 
              // Fire to let the user know the update is successful. Only fire after update, not after update and during search 
              if(isset($addALSLearner['success'])):?>
                <script>
                  $(document).ready(function(){
                  $("#success_message").show().delay(5000).fadeOut();
                });
                </script>
                <div class="alert alert-success" id='success_message'>
                 <?=$addALSLearner['success']?>
                </div>
              <?php endif;?>

               <?php 
              // Fire to let the user know the update is successful. Only fire after update, not after update and during search 
              if(isset($_GET['update']) && $_GET['update']=='success'):?>
                <script>
                  $(document).ready(function(){
                  $("#success_message").show().delay(5000).fadeOut();
                });
                </script>
                <div class="alert alert-success" id='success_message'>
                 <p> The update to the record has been successful. </p>
                </div>
              <?php endif;?>


              <?php
              if(isset($addALSLearner['error'])):?>
                <script>
                  $(document).ready(function(){
                  $("#error_message").show().delay(5000).fadeOut();
                });
                </script>
                <div class="alert alert-danger" id='error_message'>
                 <?=$addALSLearner['error']?>
                </div>
              <?php endif;?>

              <?php
              if(isset($_GET['update']) && $_GET['update']=='failed' && !isset($_GET['error'])):?>
                <script>
                  $(document).ready(function(){
                  $("#error_message").show().delay(5000).fadeOut();
                });
                </script>
                <div class="alert alert-danger" id='error_message'>
                 <p> The update has failed.</p>
                </div>
              <?php endif;?>

              <?php
              if(isset($_GET['error']) && $_GET['error']):?>
                <script>
                  $(document).ready(function(){
                  $("#error_message").show().delay(5000).fadeOut();
                });
                </script>
                <div class="alert alert-danger" id='error_message'>
                 <p><?=$_GET['error']?></p>
                </div>
              <?php endif;?>
             
            <div class='form-groupx'>
              <label for="exampleInputEmail1">Add the learner:</label>
              <div class="inner-addon left-addon">
                <i class="glyphicon glyphicon-user"></i>
                <input type="search" class="form-control" name='learner' value='' placeholder='Start to enter the student details. You can search for firstname, surname or student ID.' id='autocompletelearner' pattern=".{15,}" required title="Something is wrong. Search again"/>
              </div>
              <span class='message'>You can search for the firstname, surname, or the learner ID.</span>
            </div>

            <div class='learner_form_section'>
              <p><label for="exampleInputEmail1">Is the student exempt:</label></p>
              <label class="radio-inline"><input type="radio" name="exempt" class="exempt1" value='1' required="required">Yes</label>
              <label class="radio-inline"><input type="radio" name="exempt" class="exempt2" value='0' required="required">No</label>
            </div>

             <div class="learner_form_section" id='exempt_details'>
              <label for="exampleFormControlTextarea1">Exemption further details:</label>
              <textarea class="form-control" name='exempt_details' id="exampleFormControlTextarea1" rows="5" placeholder='If the student is exempt, please explain why'></textarea>
            </div>

             <div class="learner_form_section">
              <label for="exampleFormControlTextarea1">Support details:</label>
              <textarea class="form-control" name='support_details' id="exampleFormControlTextarea1" class='support_details' placeholder='Please can you be more specific about the support details.' rows="5" required></textarea>
            </div>

            <div class='learner_form_section'>
              <p><label for="exampleInputEmail1">Choose level of support:</label></p>
              <p><label class="radio-inline"><input type="radio" name="support" value='1-to-1' required="required">1-to-1</label>
              <label class="radio-inline"><input type="radio" name="support" value='group' required="required">Group</label>
               <label class="radio-inline"><input type="radio" name="support" value='independent' required="required">Independent</label>
            </p>
            </div>

          
            <div class='learner_form_section'>
              <p><label for="exampleInputEmail1">Student on reduced hours or days:</label></p>
              <p><label class="radio-inline"><input type="radio" name="reduced" class='reduced1' value='1' required="required">Yes</label>
              <label class="radio-inline"><input type="radio" name="reduced" class='reduced2' value='0'>No</label></p>
            </div>

             <div class="form-group" id='reduced_details'>
              <label for="exampleFormControlTextarea1">Student on reduced hours or days further details:</label>
              <textarea class="form-control" name='reduced_further_details' id="exampleFormControlTextarea1" rows="5" placeholder='If the student is on redueced hours or days, please provide some further details here.'></textarea>
            </div>

             <form>
              <div class="form-group">
                <p><label for="exampleFormControlFile1">Upload the Education, Health &amp; Care Plan (EHCP):</label></p>
                <p><input type="file" class="form-control-file" id="exampleFormControlFile1" name='file_upload' required></p>
              </div>
            </form>

             <div class="form-group">
              <label for="exampleFormControlTextarea1">Further information on the learner:</label>
              <textarea class="form-control" name='further_info' id="exampleFormControlTextarea1" placeholder=' Please provide details of any further information/interventons &amp; jobs done.' rows="5" required></textarea>
            </div>

             <input type='hidden' name='add_als_learner' value='1'/>
              <p class='form'><button name='submit' class="btn btn-primary">Add ALS learner details</button><p>
      </div> <!-- End row -->
    <?php endif;?>


        <!-- Open the edit box on the page and pull the record to edit the placement -->
         <?php if(isset($_POST['edit_als_learner'])):
         // Get all the placement details so we can use them to populate the form
         $ALSLerner = $ALS->getALSLearnerByID($_POST['row_id']);
         //echo '<pre>';print_r($ALSLerner);die;
         ?>
          <script>
            // Slowly show the edit the 'ALS learner details form'.
            $(document).ready(
              function(){
              // Show the edit form so te ALS learner can be updated.
              $(".edit_als_learner").fadeToggle( "slow", "linear" );

              // If content in Exemption further details: open it up and show it
              if(document.getElementsByName('exempt_details')[0].value!='')
              {
                document.getElementById('exempt_details').style.display = 'block';
              }

              // If content in reduced_further_details further details: open it and show it
              if(document.getElementsByName('reduced_further_details')[0].value!='')
              {
                document.getElementById('reduced_details').style.display = 'block';
              }

              });
          </script>
        <?php endif;?>
        <!-- Open the edit box on the page and pull the record to edit -->
    </div>


    <div class='edit_als_learner'> <!-- This is the start of create a learner placement -->
    <!-- Start edit of a placement -->
    <form method='POST'>
         <div class="row"><!--Start of row -->
          <div class="col-xs-12" class="col-md-6" class="col-lg-6">
             <h2>Edit the ALS learner details below for '<?=$learner->learnerDetailsByID($ALSLerner[0]['learner_id'])?>'</h2>
             <p>Below you can update the ALS learner details. </p>

              <div class='form-groupx'>
              <label for="exampleInputEmail1">The learner:</label>
              <div class="inner-addon left-addon">
                <i class="glyphicon glyphicon-user"></i>
                <input readonly type="text" class="form-control" name='learner' value='<?=$learner->learnerDetailsByID($ALSLerner[0]['learner_id'])?>' placeholder='Start to enter the student details. You can search for firstname, surname or student ID.' id='autocompletelearner' pattern=".{15,}" required title="Something is wrong. Search again"/>
              </div>
            </div>

            <div class='learner_form_section'>
              <p><label for="exampleInputEmail1">Is the student exempt:</label></p>
              <label class="radio-inline">
                <input type="radio" name="exempt" class="exempt1" value='1' required="required"
                <?php if($ALSLerner[0]['exempt']=='1') echo 'checked'; ?>
                >Yes</label>
              <label class="radio-inline">
                <input type="radio" name="exempt" class="exempt2" value='0' required="required"
                <?php if($ALSLerner[0]['exempt']=='0') echo 'checked'; ?>
                >No</label>
            </div>

             <div class="learner_form_section" id='exempt_details'>
              <label for="exampleFormControlTextarea1">Exemption further details:</label>
              <textarea class="form-control" name='exempt_details' id="exampleFormControlTextarea1" class='exempt_details' rows="3" placeholder='If the student is exempt, please explain why'><?=$ALSLerner[0]['exempt_details']?></textarea>
            </div>

             <div class="learner_form_section">
              <label for="exampleFormControlTextarea1">Support details:</label>
              <textarea class="form-control" name='support_details' id="exampleFormControlTextarea1" rows="3" placeholder='Please can you be more specific about the support details.' required><?=$ALSLerner[0]['support_details']?></textarea>
            </div>

            <div class='learner_form_section'>
              <p><label for="exampleInputEmail1">Choose level of support:</label></p>
              <p><label class="radio-inline">
                <input type="radio" name="support" value='1-to-1' required="required"
                <?php if($ALSLerner[0]['support']=='1-to-1') echo 'checked'?>
                >1-to-1</label>
              <label class="radio-inline">
                <input type="radio" name="support" value='group' required="required"
                <?php if($ALSLerner[0]['support']=='group') echo 'checked'?>
                >Group </label>
              <label class="radio-inline"><input type="radio" name="support" value='independent' required="required"
              <?php if($ALSLerner[0]['support']=='independent') echo 'checked'?>
                >Independent</label>
              </p>
            </div>
          
            <div class='learner_form_section'>
              <p><label for="exampleInputEmail1">Student on reduced hours or days:</label></p>
              <p><label class="radio-inline">
                <input type="radio" name="reduced" class='reduced1' value='1' required="required"
                <?php if($ALSLerner[0]['reduced']==1) echo 'checked'?>
                >Yes</label>
              <label class="radio-inline"><input type="radio" name="reduced" class='reduced2' value='0' required="required"
                <?php if($ALSLerner[0]['reduced']==0) echo 'checked'?>
                >No</label>
              </p>
            </div>

             <div class="form-group" id='reduced_details'>
              <label for="exampleFormControlTextarea1">Student on reduced hours or days further details:</label>
              <textarea class="form-control" name='reduced_further_details' id="exampleFormControlTextarea1" rows="3" placeholder='If the student is on redueced hours or days, please provide some further details here.'><?=$ALSLerner[0]['reduced_further_details']?></textarea>
            </div>

             <form>
              <div class="form-group">
              <?php if($ALSLerner[0]['student_doc']):?>
              <div class="alert alert-primary" role="alert">
                 <p style='font-weight:bold;'>There is a document on the system for this learner already but if you'd prefer, you may upload another newer version. If this option is left, the original document remains unchanged.</p>
              </div>
              <?php endif;?>

             </p>
             <p>Current <a href='<?=$ALSLerner[0]['student_doc']?>'>Education, Health &amp; Care Plan (EHCP) document</a> </p>
              <p><label for="exampleFormControlFile1">Upload a newer Education, Health &amp; Care Plan (EHCP):</label></p>
              <p><input type="file" class="form-control-file" id="exampleFormControlFile1" name='file_upload'></p>
              </div>
            </form>

             <div class="form-group">
              <label for="exampleFormControlTextarea1">Further information on the learner:</label>
              <textarea class="form-control" name='further_info' id="exampleFormControlTextarea1" rows="3" placeholder=' Please provide details of any further information/interventons &amp; jobs done' required><?=$ALSLerner[0]['further_info']?></textarea>
            </div>

             <input type='hidden' name='update_als_learner' value="<?=$ALSLerner[0]['id']?>"/>
              <p class='form'><button name='submit' class="btn btn-primary">Update ALS learner details</button><p>
        </div>     
        </div>
      </form>
      </div>
    </div>
    <!-- End edit of a placement -->

    <!-- List all the coaches with the option to edit -->

    <!-- List all the coaches with the option to edit -->
    



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
