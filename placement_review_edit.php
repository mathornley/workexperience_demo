<?php

/**
 * Get the PSR4 autoloader and load all the classes required.
 * This page handles the validation to the updated review with PHP in the class (editReview()) and then finally Javascript
 */
spl_autoload_register(function ($class_name){
    include 'classes/'.$class_name . '.php';
});


// Stop the cache
require_once 'stop_cache.php';

// Set an instance of aims.
$aims = new Aims;
// New instance of the work place review
$workPlacementReview = new WorkPlacementReview;

// Get an instance of the career coach review.
$coach = new CareerCoachReview;

if(isset($_GET))
{
  // Get the learner details so we can populate the top section of the page with the students details. 
  $learner = new Learner($_GET['placement_attended_id'],$_GET['studID']);
  $learnerDetails = $learner->learnerDetails($_GET['studID']);
  // Instance of placement
  $placement = new PlacementAttended($_GET['studID']);
  // Get the users aims previously chosen if there are any.
  $learnerAimsChosen = $aims->learnerAims($_GET['studID'],$_GET['placement_attended_id']); 
  //Learner selected aims (just 4 all formatted)
  $formattedAndSelectedAims = $aims->learnerAimsFormatted($_GET['studID'],$_GET['placement_attended_id']); 
  // Get the learners submitted aims for the work experience section.
  $learnerReview = $workPlacementReview->learnerReview($_GET['placement_attended_id'],$_GET['studID']);
}

// Check the work placement agreement is signed. 
$agreement_signed = $placement->agreementSigned($_GET['studID'],$_GET['placement_attended_id']);

// Make sure the user has signed the agreement.
if($agreement_signed!=true)
{
  // Direct back to the agreement page to sign. 
  header("location: agreement.php?placement_attended_id={$_GET['placement_attended_id']}&studID={$_GET['studID']}");
}

// Get list of both formatted and raw aims so we can use it in the HTML.
$aimsAndObjectives = $aims->displayAimsFormatted();
$aimsAndObjectivesRaw = $aims->displayAims();

// Add the arrays: $aimsAndObjectives,$aimsAndObjectivesRaw to create just one array to use in the form.
// for the options to be selected.
$aimsAndObjectives = array_combine($aimsAndObjectives,$aimsAndObjectivesRaw);

// Update the review and add this to the table. 
if(isset($_POST) && isset($_POST['save_review']))
{
  $reviewUpdated = $workPlacementReview->editReview($_POST);

  // If there is no issue with the update, redirect to the placement_review page.
  if($reviewUpdated!==false)
  {
    header("location: placement_review.php?placement_attended_id={$_GET['placement_attended_id']}&studID={$_GET['studID']}");
  } 
}

// If there's no details, direct the user back to the placement_review page as they hve't done it. 
if($learnerReview==false)
{
  header('location: placement_review.php');
}

// Once the update is completed and is successful, redirect the user back to the placement_review.php page
if(isset($reviewUpdated) && $reviewUpdated!==false)
{
  header("location: placement_review.php?placement_attended_id={$_GET['placement_attended_id']}&studID={$_GET['studID']}");
}

/**
 * Check if this is a learner only viewing their own part of the app - learner ids have to match.
 * The Shib learner id has to match the URL from the browser, IF they're not a coach. 
 * As usual the coach sees it all.
 * If the user is not authorised, $_SERVER['REQUEST_URI'] is used to keep them on their page.
 */
if(!$coach->isCareerCoach() && $coach->doesTheLearnerIdMatch($_GET['studID'])==false)
{
  // The user is not a coach and not the learner requesting the URL so keep them where they are.
  header('location: /workexperience/');
}


// Some front end validation on the workplace review during the update.
?>
<?php if(isset($reviewUpdated) && $reviewUpdated==false):?>
  <script>
    alert("You need to fully complete the review for each aim. You need to:\n\nCheck either option of: \n\n'went well' or \n\n'could have done better' \n\nand then finally detail your 'reasons for choice'.");
  </script>
<?php endif;?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>My Workspace app</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/theme.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

    <!-- Fixed navbar -->
    <!--<nav class="navbar navbar-inverse navbar-fixed-top">-->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
           <a class="navbar-brand" href="/workexperience/">My Workspace app</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <!--<li><a href="#about">About</a></li>-->
            <!--<li><a href="#contact">Contact</a></li>-->
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav><!-- end fixed navbar -->

    <div class="container-fluid theme-showcase" role="main">
      <div class="row"><!--Start of row -->
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-bordered">
              <thead>
                  <tr>
                    <th>Student name</th>
                    <td><?=$learnerDetails[0]['name']?></td>
                    <th>Student ID</th>
                    <td><?=$learnerDetails[0]['person_code']?></td>
                    <th>Coach name</th>
                    <td><?=$coach->getCoachNameForLearner($_GET['placement_attended_id'])?></td>
                  </tr>
              </thead>  
            </table>
        </div>  
      </div>
    </div> <!--End of row -->

    <div class="row"><!--Start of row -->
        <!-- Tabbed content left-->
         <div class="col-md-12">
            <ul class="nav nav-tabs">
              <li><a href="index.php">Choose placement</a></li>
              <li class="active"><a href="introduction.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Introduction</a></li>
              <li><a href="location.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Where is my placement</a></li>
              <li><a href="agreement.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Work placement agreement</a></li>
              <li><a href="aims.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Aims and objectives</a></li>
              <li><a href="logbook.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Logbook</a></li>
              <li><a href="placement_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Your work placement review</a></li>
              <li><a href="employer_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Employer review</a></li>
              <li><a href="coach_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Career coach review</a></li>
            </ul>
        <br/>
      </div><!-- end tabbed content left-->
    </div><!--End of row -->
  
      <div class="row"><!--Start of row -->
        <div class="col-md-12"><!-- content main full page-->
          <h3>Edit your work place review</h3>
          <p>Please use the following section to edit your work placement experience. Your aims are outlined below</p>
           <p>You will need to <span class='red-bold'>fully complete</span> the form to update and when doing so, choose either 'went well' or 'could have done better' for <u>each aim</u> before finally saving.</p>
          <fieldset>
                 <div class="table-responsive">          
                  <table class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Aim &amp; objectives.</th>
                        <th>Went well</th>
                        <th>Could have done better</th>
                        <th>Reasons for choice</th>
                      </tr>
                    </thead>
                    <tbody>
                      <form method='POST' id='update'>
                        <?php $x=0;?>
                        <?php foreach($formattedAndSelectedAims AS $aim):?>
                          <tr>
                            <td><?=$aim?></td>
                              <input type="hidden" name="review[<?=$x?>]['id']" value="<?=$learnerReview[$x]['id']?>"/>
                              <td>
                                <input type="hidden" name="review[<?=$x?>]['aim']" value="<?=$aim?>"/>
                                <input type="hidden" name="placement_attended_id" value="<?=$learnerDetails[0]['id']?>"/>
                                <input type="radio" name="review[<?=$x?>]['option']" value="went_well" <?php if($learnerReview[$x]['went_well']==1) echo 'checked';?>/>
                              </td>
                              <td>
                               <input type="radio" name="review[<?=$x?>]['option']" value="could_have_done_better"  <?php if($learnerReview[$x]['could_have_done_better']==1) echo 'checked';?>/>
                              </td>
                              <td colspan=2>
                                <textarea minlength="50" class='textarea_review_edit' placeholder='Please give a little more information on why you have made this choice' name="review[<?=$x?>]['reasons_for_choice']"><?php if(isset($learnerReview[$x]['reasons_for_choice'])) echo trim($learnerReview[$x]['reasons_for_choice']);?></textarea>
                              </td>
                           </tr>
                          <?php ++$x;?>
                          <?php endforeach;?>
                        </tbody>
                      </table>
                    </div> <!-- table-responsive ends-->
                  <br/>
                  <input type="hidden" name="placement_attended_id" value="<?=$learnerDetails[0]['id']?>"/>
                  <p>Tick if you have had assistance from a tutor or coach to complete this entry.
                    <input type="checkbox" <?php if($learnerReview[0]['assisted']==1) echo 'checked';?> name="assisted" value="1" id="assisted" class="form-check-input">
                  </p>
                  <p><button type="submit" name='save_review' id='save_review' class="btn btn-primary">Save</button></p>
              </form> 
          </fieldset>
               <!--The edit button ends here-->
        </div> <!-- col ends-->  
      </div><!-- row ends-->

    </div> <!--end main container -->

    <!-- footer here -->
    <footer class="footer">
    <hr/>
      <div class="container">
        <p style="text-align:center;">Bolton College 2017</p>
      </div>
    </footer>
    <!-- end footer end -->

    

    <!-- Bootstrap core JavaScript ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
