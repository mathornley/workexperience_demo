<?php

/**
 * Get the PSR4 autoloader and load all the classes required.
 * This page handles the validation to the updated review with PHP in the class (editReview()) and then finally Javascript
 */
spl_autoload_register(function ($class_name){
    include 'classes/'.$class_name . '.php';
});

// Stop the cache
require_once 'stop_cache.php';

// Get an instance of the career coach review.
$coach = new CareerCoachReview;

// Get the learner details so we can populate the top section of the page with the students details. 
$learner = new Learner;
$learnerDetails = $learner->learnerDetails($_GET['studID']);

// Set an instance of aims.
$aims = new Aims;

// Instance of placement
$placement = new PlacementAttended($_GET['studID']);

// Check the work placement agreement is signed. 
$agreement_signed = $placement->agreementSigned($_GET['studID'],$_GET['placement_attended_id']);

// Make sure the user has signed the agreement.
if($agreement_signed!=true)
{
  // Direct back to the agreement page to sign. 
  header("location: agreement.php?placement_attended_id={$_GET['placement_attended_id']}&studID={$_GET['studID']}");
}

// Get the users aims previously chosen if there are any.
$learnerAimsChosen = $aims->learnerAims($_GET['studID'],$_GET['placement_attended_id']); 

// Get list of both formatted and raw aims so we can use it in the HTML.
$aimsAndObjectives = $aims->displayAimsFormatted();
$aimsAndObjectivesRaw = $aims->displayAims();

//Learner selected aims (just 4 all formatted)
$formattedAndSelectedAims = $aims->learnerAimsFormatted($_GET['studID'],$_GET['placement_attended_id']);


// Add the arrays: $aimsAndObjectives,$aimsAndObjectivesRaw to create just one array to use in the form.
// for the options to be selected.
$aimsAndObjectives = array_combine($aimsAndObjectives,$aimsAndObjectivesRaw);

// Set up the work placement
$workPlacementReview = new WorkPlacementReview;

if(isset($_POST) && isset($_POST['save_review']))
{
  //print_r($_POST);
  $workPlacementReview->addReview($_POST);
}

// Get the learners submitted aims for the work experience section.
$learnerReview = $workPlacementReview->learnerReview($_GET['placement_attended_id'],$_GET['studID']);

// The user wants to edit their aims so we can then direct them to the edit template: aims_edit.php.
if($_POST && isset($_POST['edit_review']))
{
  header('location: placement_review_edit.php');
}

// Set up an instance of the Employee review
$employer = new EmployeeReview();
// Get the table headings for the table to be used below in the HTML. 
$employerTableFields = $employer->employerTableFields();
//$employerReview = $employer->learnersReview();

// Save the employer review.
if(isset($_POST['save_employer_review']))
{
  $employer->saveEmployerReview($_POST);
}
$employerReview = $employer->learnersReview($_GET['studID'],$_GET['placement_attended_id']);


/**
 * Check if this is a learner only viewing their own part of the app - learner ids have to match.
 * The Shib learner id has to match the URL from the browser, IF they're not a coach. 
 * As usual the coach sees it all.
 * If the user is not authorised, $_SERVER['REQUEST_URI'] is used to keep them on their page.
 */
if(!$coach->isCareerCoach() && $coach->doesTheLearnerIdMatch($_GET['studID'])==false)
{
  // The user is not a coach and not the learner requesting the URL so keep them where they are.
  header('location: /workexperience/');
}

// Decide if the midpoint review should be shown - if 50 days, yes, otherwise, no.
$showMidPointReview = $learner->showMidPointReview($_GET['placement_attended_id']);
//var_dump($showMidPointReview);die;
?>

<script type="text/javascript">
var studID = "<?php echo $_GET['studID'];?>";
//alert(studID);
</script>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>My Workspace app</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/theme.css" rel="stylesheet">

    <!--<link rel="stylesheet" type="text/css" href="css/ada.css" />-->

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

    <!-- Bootstrap 3 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Font Awesome 5 -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="crossorigin="anonymous"></script>
    
  </head>
  <body>

    <!-- Fixed navbar -->
    <!--<nav class="navbar navbar-inverse navbar-fixed-top">-->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
           <a class="navbar-brand" href="/workexperience/">My Workspace app</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="index.php">Choose placement</a></li>
               <li><a href="introduction.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Introduction</a></li>
              <li><a href="location.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Where is my placement</a></li>
              <li><a href="agreement.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Work placement agreement</a></li>
              <li><a href="aims.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Aims and objectives</a></li>
              <li><a href="logbook.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Logbook</a></li>
              <li><a href="placement_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Your work placement review</a></li>
              <li class="active"><a href="employer_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Employer review</a></li>
              <li><a href="coach_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Coach review</a>
            </li>
               <?php if($showMidPointReview):?>
                <li><a href="midpoint_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Your midpoint review</a>
                </li>
              <?php endif;?>
            <!--<li><a href="#about">About</a></li>-->
            <!--<li><a href="#contact">Contact</a></li>-->
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav><!-- end fixed navbar -->

    <div class="container-fluid theme-showcase" role="main">
      <div class="row"><!--Start of row -->
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-bordered">
              <thead>
                  <tr>
                    <th>Student name</th>
                    <td><?=$learnerDetails[0]['name']?></td>
                    <th>Student ID</th>
                    <td><?=$learnerDetails[0]['person_code']?></td>
                    <th>Coach name</th>
                    <td><?=$coach->getCoachNameForLearner($_GET['placement_attended_id'])?></td>
                  </tr>
              </thead>  
            </table>
        </div>  
      </div>
    </div> <!--End of row -->

    <div class="row" id='test'><!--Start of row -->
        <!-- Tabbed content left-->
        <div class="col-md-12">
           <ul class="nav nav-tabs">
              <li><a href="index.php">Choose placement</a></li>
               <li><a href="introduction.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Introduction</a></li>
              <li><a href="location.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Where is my placement</a></li>
              <li><a href="agreement.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Work placement agreement</a></li>
              <li><a href="aims.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Aims and objectives</a></li>
              <li><a href="logbook.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Logbook</a></li>
              <li><a href="placement_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Your work placement review</a></li>
              <li class="active"><a href="employer_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Employer review</a></li>
              <li><a href="coach_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Coach review</a></li>
                 <?php if($showMidPointReview):?>
                  <li><a href="midpoint_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Your midpoint review</a>
                </li>
                <?php endif;?>
            </ul>
        <br/>
      </div><!-- end tabbed content left-->
    </div><!--End of row -->
  
      <div class="row"><!--Start of row -->
        <div class="col-md-12"><!-- content main full page-->
          <h3>Employer review</h3>
          <p>The section below contains the completed employer review. This will be completed by your employer once you've finished your work placement.</p>        
          <?php if($employerReview!=false):?>
          <fieldset  <?php if($employerReview!=false) echo 'disabled';?>>
            <form method='POST'>
                 <div class="table-responsive">          
                    <table class="table table-bordered table-striped">
                      <thead>
                        <tr>
                          <th></th>
                          <th>Excellent</th>
                          <th>Good</th>
                          <th>Satisfactory</th>
                          <th>Requires improvement</th>
                        </tr>
                      </thead>
                      <tbody>
                          <?php $x=0;?>
                          <?php foreach($employerTableFields AS $heading):?>
                            <tr>
                                <td>
                                  <input type="hidden" name="review[<?=$x?>]['heading']" value="<?=$heading?>"/>
                                  <input type="hidden" name="placement_attended_id" value="<?=$learnerDetails[0]['id']?>"/>
                                <?=$heading?>
                                </td>
                                 <td <td <?php if($employerReview[$x]['excellent']=='1') echo 'class="bg-success"';?> />
                                 <input type="radio" name="review[<?=$x?>]['score']" value="excellent" <?php if($employerReview[$x]['excellent']=='1') echo 'checked';?>/>
                                </td>
                                 <td <td <?php if($employerReview[$x]['good']=='1') echo 'class="bg-success"';?> />
                                 <input type="radio" name="review[<?=$x?>]['score']" value="good" <?php if($employerReview[$x]['good']=='1') echo 'checked';?>/>
                                </td>
                                <td <td <?php if($employerReview[$x]['satisfactory']=='1') echo 'class="bg-success"';?>/>
                                  <input type="radio" name="review[<?=$x?>]['score']" value='satisfactory' <?php if($employerReview[$x]['satisfactory']=='1') echo 'checked';?> />
                                </td <td <?php if($employerReview[$x]['satisfactory']=='1') echo 'class="bg-success"';?>/>
                                <td <?php if($employerReview[$x]['requires_improvement']=='1') echo 'class="bg-success"';?>/>
                                  <input type="radio" name="review[<?=$x?>]['score']" value="requires_improvement" autofocus required="required" <?php if($employerReview[$x]['requires_improvement']=='1') echo 'checked';?>/>
                                </td/>
                            </tr>
                            <?php ++$x;?>
                            <?php endforeach;?>
                            <tr>
                              <td colspan=5>
                              <p>Employer review:</p>
                                <textarea required class='textarea_review' placeholder='Please give a little more information on why you have made this choice' name="review['comments']"><?=$employerReview[0]['comments']?></textarea>
                              </td>
                            </tr>
                        </tbody>
                      </table>
                    </div> <!-- table-responsive ends-->
                  </form>
          </fieldset>
      <?php else:?>
        <h4> The employer has yet to complete the review for your work placement.</h4>
      <?php endif;?>
        </div> <!-- col ends-->  
      </div><!-- row ends-->

    </div> <!--end main container -->

    <!-- footer here -->
    <footer class="footer">
    <hr/>
      <div class="container">
        <p style="text-align:center;">Bolton College 2017</p>
      </div>
    </footer>
    <!-- end footer end -->

     <?php 
   // Require the ada pop up box
   require_once('ADA_form.php');
   ?>

    <!-- Bootstrap core JavaScript ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script defer src="../learner/adanew_dist/adabundle_v1.5.1.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
