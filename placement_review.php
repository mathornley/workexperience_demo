<?php

/**
 * Get the PSR4 autoloader and load all the classes required.
 * This page handles the validation to the updated review with PHP in the class (editReview()) and then finally Javascript
 */
spl_autoload_register(function ($class_name){
    include 'classes/'.$class_name . '.php';
});

// Stop the cache
require_once 'stop_cache.php';

// Get the learner details so we can populate the top section of the page with the students details. 
  $learner = new Learner($_GET['placement_attended_id'],$_GET['studID']);
  $learnerDetails = $learner->learnerDetails($_GET['studID']);

// Set an instance of aims.
$aims = new Aims;

// New instance of coach comments for recording the coach comments
$coachComments = new CoachComments;

// Get an instance of the career coach review.
$coach = new CareerCoachReview;

// Instance of placement
$placement = new PlacementAttended($_GET['studID']);

// See if there are **learner details** from Shibboleth
if($learnerDetails = $learner->learnerDetails()!=false)
{
  $learnerDetails = $learner->learnerDetails();
}

// See if there are **coach details** from Shibboleth
if($coachDetails = $coach->coachDetails()!=false)
{
  // This is a coach. This is their details.
  $coachDetails = $coach->coachDetails();
}

// Check the work placement agreement is signed. 
$agreement_signed = $placement->agreementSigned($_GET['studID'],$_GET['placement_attended_id']);

// Make sure the user has signed the agreement.
if($agreement_signed!=true)
{
  // Direct back to the agreement page to sign. 
  header("location: agreement.php?placement_attended_id={$_GET['placement_attended_id']}&studID={$_GET['studID']}");
}

// Get the users aims previously chosen if there are any.
$learnerAimsChosen = $aims->learnerAims($_GET['studID'],$_GET['placement_attended_id']); 

// Get list of both formatted and raw aims so we can use it in the HTML.
$aimsAndObjectives = $aims->displayAimsFormatted();
$aimsAndObjectivesRaw = $aims->displayAims();

//Learner selected aims (just 4 all formatted)
$formattedAndSelectedAims = $aims->learnerAimsFormatted($_GET['studID'],$_GET['placement_attended_id']);

// Add the arrays: $aimsAndObjectives,$aimsAndObjectivesRaw to create just one array to use in the form.
// for the options to be selected.
$aimsAndObjectives = array_combine($aimsAndObjectives,$aimsAndObjectivesRaw);

// Set up the work placement
$workPlacementReview = new WorkPlacementReview;

if(isset($_POST) && isset($_POST['save_review']))
{
  //print_r($_POST['placement_attended_id']);die;
  $workPlacementReview->addReview($_POST);
}

// The user wants to edit their review so we can then direct them to the edit template: aims_edit.php.
if($_POST && isset($_POST['edit_review']))
{
  header("location: placement_review_edit.php?placement_attended_id={$_GET['placement_attended_id']}&studID={$_GET['studID']}");
}

// Add coach comments to the page.
if(isset($_POST['add_coach_comments']) && $_POST['add_coach_comments'] == '1')
{
  $add = $coachComments->addCoachComments($_POST,'workplacement');
}

// Edit one of the coach comments.
if(isset($_POST['edit_coach_comments']) && $_POST['edit_coach_comments'] == '1')
{
  //echo 'EDIT';die;
  $edit = $coachComments->editCoachComments($_POST,'workplacement');
}

// Some action plan comments have been added.
if(isset($_POST['add_action_plan_comments']) && $_POST['add_action_plan_comments'] == '1')
{
  $actionPlanCommentsSub = $workPlacementReview->addActionPlanComments($_POST);
}

// Some action plan comments have been added.
if(isset($_POST['update_action_plan_comments']) && $_POST['update_action_plan_comments'] == '1')
{
  $actionPlanCommentsSub = $workPlacementReview->updateActionPlanComments($_POST);
}

// Get all the coach comments for this placement_attended_id
$coachComments = $coach->getCoachCommentsPlacementReview($_GET['placement_attended_id']);

// Get the learners submitted aims for the work experience section.
$learnerReview = $workPlacementReview->learnerReview($_GET['placement_attended_id'],$_GET['studID']);

/**
 * Check if this is a learner only viewing their own part of the app - learner ids have to match.
 * The Shib learner id has to match the URL from the browser, IF they're not a coach. 
 * As usual the coach sees it all.
 * If the user is not authorised, $_SERVER['REQUEST_URI'] is used to keep them on their page.
 */
if(!$coach->isCareerCoach() && $coach->doesTheLearnerIdMatch($_GET['studID'])==false)
{
  // The user is not a coach and not the learner requesting the URL so keep them where they are.
  header($_SERVER['REQUEST_URI']);
}

// Pull all the learners action plan comments back.
$actionPlanComments = $workPlacementReview->getLearnersActionPlanComments($_GET['studID']);
//var_dump($actionPlanComments);die;
//
/**
 * Check if this is a learner only viewing their own part of the app - learner ids have to match.
 * The Shib learner id has to match the URL from the browser, IF they're not a coach. 
 * As usual the coach sees it all.
 * If the user is not authorised, $_SERVER['REQUEST_URI'] is used to keep them on their page.
 */
if(!$coach->isCareerCoach() && $coach->doesTheLearnerIdMatch($_GET['studID'])==false)
{
  // The user is not a coach and not the learner requesting the URL so keep them where they are.
  header('location: /workexperience/');
}

// Decide if the midpoint review should be shown - if 50 days, yes, otherwise, no.
$showMidPointReview = $learner->showMidPointReview($_GET['placement_attended_id']);
//var_dump($showMidPointReview);die;
?>

<script type="text/javascript">
var studID = "<?php echo $_GET['studID'];?>";
//alert(studID);
</script>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>My Workspace app</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/theme.css" rel="stylesheet">

    <!--<link rel="stylesheet" type="text/css" href="css/ada.css" />-->

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

   
    <!-- Bootstrap 3 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Font Awesome 5 -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="crossorigin="anonymous"></script>
    
  </head>
  <body>

    <!-- Fixed navbar -->
    <!--<nav class="navbar navbar-inverse navbar-fixed-top">-->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
           <a class="navbar-brand" href="/workexperience/">My Workspace app</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="index.php">Choose placement</a></li>
               <li><a href="introduction.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Introduction</a></li>
              <li><a href="location.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Where is my placement</a></li>
              <li><a href="agreement.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Work placement agreement</a></li>
              <li><a href="aims.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Aims and objectives</a></li>
              <li><a href="logbook.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Logbook</a></li>
              <li class="active"><a href="placement_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Your work placement review</a></li>
              <li><a href="employer_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Employer review</a></li>
              <li><a href="coach_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Coach review</a>
            </li>
             <?php if($showMidPointReview):?>
                <li><a href="midpoint_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Your midpoint review</a>
                </li>
            <?php endif;?>
            <!--<li><a href="#about">About</a></li>-->
            <!--<li><a href="#contact">Contact</a></li>-->
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav><!-- end fixed navbar -->

    <div class="container-fluid theme-showcase" role="main">
      <div class="row"><!--Start of row -->
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-bordered">
              <thead>
                  <tr>
                    <th>Student name</th>
                    <td><?=$learnerDetails[0]['name']?></td>
                    <th>Student ID</th>
                    <td><?=$learnerDetails[0]['person_code']?></td>
                    <th>Coach name</th>
                    <td><?=$coach->getCoachNameForLearner($_GET['placement_attended_id'])?></td>
                  </tr>
              </thead>  
            </table>
        </div>  
      </div>
    </div> <!--End of row -->

   <div class="row" id='test'><!--Start of row -->
        <!-- Tabbed content left-->
        <div class="col-md-12">
            <ul class="nav nav-tabs">
              <li><a href="index.php">Choose placement</a></li>
               <li><a href="introduction.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Introduction</a></li>
              <li><a href="location.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Where is my placement</a></li>
              <li><a href="agreement.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Work placement agreement</a></li>
              <li><a href="aims.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Aims and objectives</a></li>
              <li><a href="logbook.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Logbook</a></li>
              <li class="active"><a href="placement_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Your work placement review</a></li>
              <li><a href="employer_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Employer review</a></li>
              <li><a href="coach_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Coach review</a></li>
               <?php if($showMidPointReview):?>
                  <li><a href="midpoint_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Your midpoint review</a>
                </li>
                <?php endif;?>
            </ul>
        <br/>
      </div><!-- end tabbed content left-->
    </div><!--End of row -->
  
      <div class="row"><!--Start of row -->
        <div class="col-md-12"><!-- content main full page-->
          <h3>Work place review</h3>
          <p>Please use the following section to evaluate your work placement experience. Your aims are outlined below</p>
           <p>You will need to <span class='red-bold'>fully complete</span> the form and when doing so, choose either 'went well' or 'could have done better' for <u>each aim</u> before finally saving.</p>


          <?php if(!empty($formattedAndSelectedAims)):?>
          <fieldset  <?php if($learnerReview!==false) echo 'disabled'?>>
                 <div class="table-responsive">          
                  <table class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Aim &amp; objectives.</th>
                        <th>Went well</th>
                        <th>Could have done better</th>
                        <th>Reasons for choice</th>
                      </tr>
                    </thead>
                    <tbody>
                      <form method='POST'>
                        <?php $x=0;?>
                        <?php foreach($formattedAndSelectedAims AS $aim):?>
                          <tr>
                            <td><?=$aim?></td>
                              <td <?php if($learnerReview[$x]['went_well']==1) echo " class='bg-success'";?>>
                                <input type="hidden" name="review[<?=$x?>]['aim']" value="<?=$aim?>"/>
                                <input type="radio" name="review[<?=$x?>]['option']" value="went_well" autofocus required="required"
                                <?php if($learnerReview[$x]['went_well']==1) echo 'checked';?>/>
                              </td>


                              <td <?php if($learnerReview[$x]['could_have_done_better']==1) echo " class='bg-success'";?>>
                                 <input type="radio" name="review[<?=$x?>]['option']" value="could_have_done_better"
                                  <?php if($learnerReview[$x]['could_have_done_better']==1) echo 'checked';?>/>
                              </td>

                              <td colspan=2>
                                <textarea minlength="50" class="textarea_review_edit" required placeholder='Please give a little more information on why you have made this choice' name="review[<?=$x?>]['reason']"><?php if($learnerReview[$x]['reasons_for_choice']) echo $learnerReview[$x]['reasons_for_choice'];?></textarea>
                              </td>

                           </tr>
                          <?php ++$x;?>
                          <?php endforeach;?>
                        </tbody>
                      </table>
                    </div> <!-- table-responsive ends-->
                  <br/>
                  <p>Tick if you have had assistance from a tutor or coach to complete this entry.
                    <input type="checkbox" <?php if($learnerReview[0]['assisted']==1) echo 'checked';?> name="assisted" value="1" id="assisted" class="form-check-input">
                  </p>
                  <br/>
                  <input type="hidden" name="placement_attended_id" value="<?=$_GET['placement_attended_id']?>"/>
                  <p><button type="submit" name='save_review' id='save_review' class="btn btn-primary"><?php echo ($learnerReview!=false) ? 'Saved' : 'Save'?></button></p>
              </form> 
          </fieldset>

          <?php if($learnerReview!==false):?>
          <fieldset>
           <!--The edit button goes here-->
              <form method='POST'>
                <p><button type="submit" name='edit_review' id='edit_review' class="btn btn-danger">Edit</button></p>
              </form>
          </fieldset>
        <?php endif;?>
        <!--The edit button ends here-->
      <?php else:?>
        <h3> This learner has not selected any aims and objectives.</h3>
      <?php endif;?>
        
        </div> <!-- col ends-->  
      </div><!-- row ends-->

      <hr/>

        <!-- The coach comments at the very bottom -->
        <div class="row"><!--Start of row -->
          <!-- Tabbed content left-->
          <div class="col-md-12">

            <!--<h3> Student's action plan comments</h3>


              <?php if($actionPlanComments!==false):?>
                <p> Action plan comments can be edited below. Just update the text and press 'update'</p>
              <?php endif;?>
                      
              <?php if($actionPlanComments===false):?>
               <p> Action plan comments can be added below</p>
              <?php endif;?>

              <fieldset>
                    <form method='POST'>

                        <div class="form-group">
                          <textarea required class="form-control" id="action_plan_comments" name='action_plan_comments' rows="7" placeholder="This is an area for the learner to add action plan comments at the end of the work experience placement. Here they can explain what they feel has gone well, what didn't go so well and what they think they have learnt from the experience as a whole."><?=$actionPlanComments[0]['comments']?></textarea>
                        </div>

                        <input type='hidden' name='row_id' value="<?=$actionPlanComments[0]['id']?>"/>
                        <input type='hidden' name='studID' value="<?=$_GET['studID']?>"/>
                        <input type='hidden' name='placement_attended_id' value="<?=$_GET['placement_attended_id']?>"/>

                      <?php if($actionPlanComments!==false):?>
                        <button type="submit" name='update_action_plan_comments' value='1' class="btn btn-danger">Update comment</button>
                      <?php endif;?>
                      
                      <?php if($actionPlanComments===false):?>
                        <button type="submit" name='add_action_plan_comments' value='1' class="btn btn-primary">Add comment</button>
                      <?php endif;?>
                    
                    </form>
              </fieldset>-->

            <?php if($coachDetails!=false): // Only show form if coach ?>

               <hr/>

              <h3> Add coach comments</h3>

              <fieldset>
                    <form method='POST'>

                        <div class="form-group">
                          <textarea required class="form-control" id="comments_placement" name='comments_placement' rows="7" placeholder="This is an area for the coach to add comments for the learners atten as the placement reaches various stages in an effort to guide the learner when it is felt some additional help is needed. You learner will be able to view the comments."></textarea>
                        </div>

                      <input type='hidden' name='studID' value="<?=$_GET['studID']?>"/>
                      <input type='hidden' name='placement_attended_id' value="<?=$_GET['placement_attended_id']?>"/>

                      <button type="submit" name='add_coach_comments' value='1' class="btn btn-primary">Add comment</button>  
                    </form>
              </fieldset>

            <?php endif;?>

            <?php if(isset($coachComments[0]['workplacement_comments'])):?>
              <h3>Coach comments</h3>
              <?php if($coachDetails!=false):?>
                <p><?=$learnerDetails[0]['name']?> will see the comments below on their wall as <strong>read-only</strong></p>
              <?php endif;?>

            <?php else:?>
              
            <?php endif;?>

            <?php for($i=0;isset($coachComments[$i]['workplacement_comments']);$i++):?>
             <fieldset <?php if($coachDetails==false) echo 'disabled'; // Disable form if not coach ?>>
                  <form method='POST'>

                      <div class="form-group">
                        <textarea class="form-control" id="comments_placement" name='comments_placement' rows="7" placeholder="This is an area for the coach to add comments as the placement reaches various stages in an effort to guide the learner when it is felt some additional help is needed."><?=$coachComments[$i]['workplacement_comments']?></textarea>
                      </div>
                      <input type='hidden' name='studID' value="<?=$_GET['studID']?>"/>
                      <input type='hidden' name='row_id' value="<?=$coachComments[$i]['id']?>"/>
                      <input type='hidden' name='placement_attended_id' value="<?=$_GET['placement_attended_id']?>"/>
                    <!-- If the aims have been selcted, change the button -->

                    <p>Created: <strong><?=date('l jS \of F Y H:i:s A',strtotime($coachComments[$i]['date_created']))?></strong> by 
                    <strong>
                      <?=$learner->learnerWorkCoachByCoachID($coachComments[$i]['coach_id'])[0]['name']?>
                    </strong></p>

                    <?php 
                    // If the date created and updated date differ, it's been updated.
                    if(strtotime($coachComments[$i]['date_created'])!=strtotime($coachComments[$i]['date_updated'])):?>
                      <p>Updated at: <strong><?=date('l jS \of F Y H:i:s A',strtotime($coachComments[$i]['date_updated']))?></strong></p>
                    <?php endif;?>

                    <?php if($coachDetails!=false): // If coach show update button ?>
                    <button type="submit" class="btn btn-danger" name='edit_coach_comments' value='1'>Update comments</button> 
                  <?php endif;?>
                  </form>
              </fieldset>
              <hr/>
            <?php endfor;?>
          </div>
        </div>
        <!-- The coach comments at the very bottom end -->

    </div> <!--end main container -->

    <!-- footer here -->
    <footer class="footer">
    <hr/>
      <div class="container">
        <p style="text-align:center;">Bolton College 2017</p>
      </div>
    </footer>
    <!-- end footer end -->

    <?php 
   // Require the ada pop up box
   require_once('ADA_form.php');
   ?>

    <!-- Bootstrap core JavaScript ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script defer src="../learner/adanew_dist/adabundle_v1.5.1.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
