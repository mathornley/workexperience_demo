
// Form validation for the logbook section  - first form 'Record a sick day'
function validateDetailsOfAbsence()
{
	// If no input in to the 'details of absence' section of the form, alert the user.
	if(document.getElementById('details_of_absence').value=='')
	{
		alert('Please complete the \'Details of absence information\'.');
		return false;
	}
	// All good, submit the form but let the learner know they have to let the placement know they'll be sick today.
	else
	{
		alert('Please call and let the college and your placement know you will be absent today. \nThank-you!');
		return true;
	}
}

// Validatin for the 'add a day' section of the logbook form.
function validateLogbook()
{
	// If no input in to the 'details of absence' section of the form, alert the user.
	if(document.getElementById('activities').value=='' || 
		document.getElementById('enjoyed').value=='' || 
		document.getElementById('better_next_time').value=='')
	{
		alert('Please complete all the details of the logbook form.');
		return false;
	}
	// Selected they 'left early' but they need to specify why that is in the form. 
	else if(document.getElementById('left_early').checked &&
		document.getElementById('sick_during').value=='0'
		)
	{
		alert('If you selected you left early due to sickness, you will need to provide further details of the absense.');
		return false;
	}
	// All good, finally submit the form.
	else
	{
		return true;
	}
}




