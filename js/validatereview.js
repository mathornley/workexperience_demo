

/**
 * This script loops checks the textarea HTML form fields submitted on the placement_review.php page once they're submitted.
 * It checks that for each the minimum character length is met, set in: chars var
 * @return {[bollean]} [Returns true or false as to whether the form should be submitted]
 */

function validateReview()
{

		var chars = 50; // Min characters

		// Minimum character limit on the 'Reasons for choice' section of: /workexperience/placement_review.php
		if(
			document.getElementById("review[0]['reason']").value.length<chars ||
			document.getElementById("review[1]['reason']").value.length<chars ||
			document.getElementById("review[2]['reason']").value.length<chars ||
			document.getElementById("review[3]['reason']").value.length<chars
		)
		{
			alert("Sorry, the minimum character length on the 'Reasons for choice' field is " + chars + ' characters for each aim');
			return false; // Make sure the form doesn't submit. 

		}

		// return true, all the fields are at the required length and completed. 
		else
		{
			return true;
		}
}
