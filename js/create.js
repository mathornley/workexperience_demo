
/**
 * This script loops through the HTML form fields submitted on the placement_review.php page once they're submitted.
 * It checks that for each aim row - one of the checkbox options has been completed and also the reasons for the choice text box has been completed.
 * @return {[bollean]} [Returns true or false as to whether the form should be submitted]
 */
function validateReview()
{
	// Iterate the 4 aims choices select boxes and the form field input for each aim row on the placement_review page.
	for(var i=0;i<=3;i++)
	{
		// For EACH roow, check if either checkbox is ticked and the input box is completed, all good. If any field are not, inform the user and stop form
		if(
			(document.getElementById("review["+i+"]['went_well']").checked==true ||
			document.getElementById("review["+i+"]['could_have_done_better']").checked==true) &&
			document.getElementById("review["+i+"]['reasons_for_choice']").value!=''
		)
		{

			// If they've selected 2 checkbox options, tell them and stop.
			if(
			(document.getElementById("review["+i+"]['went_well']").checked==true &&
			document.getElementById("review["+i+"]['could_have_done_better']").checked==true) 
			)
			{
				alert('Please only check one checkbox option!');
				return false;
			}


			// Incremement the log var to test for all 4 aims - this should hold 4 when all has been completed on the form (array 0 onwards). 
			var log=i;
			if(log==3) // If they're three, all good. 
			{
				// Submit the form. 
				return true;
			}

			++log; // Increment the timer on each pass. 
		}
		// Issue with at least one field on an aim so don't proceed. 
		else
		{
			// Now messsage to the user with what they need to do on the form for it to be completed. 
			alert("You need to fully complete the review for each aim. You need to:\n\nCheck either option of: \n\n'went well' or \n\n'could have done better' \n\nand then finally detail your 'reasons for choice'.");
			return false;
		}
	}
}

