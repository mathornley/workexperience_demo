
      // Toggle the form div to let the learner add a new day and log a day of sick.
      $(document).ready(function()
      {


        /******************************* This is the Work experience app section section ***************************************************/

         // Midpoint review day button and divs.
          $('#midpoint_review_button').on('click',function(){
              $(".midpoint_review_form").fadeToggle( "slow", "linear" );
              $(this).html($(this).html() == 'Open Midpoint Review' ? 'Close Midpoint Review' : 'Open Midpoint Review');

              if($(".midpoint_review_form").is(":visible"))
              {
                $('.form_hidden').hide();  
              };
          });

          // Sick day button and divs.
          $('#sick_day').on('click',function(){
              $(".sick_hidden").fadeToggle( "slow", "linear" );
              $(this).html($(this).html() == 'Record a sick day' ? 'Close sick day' : 'Record a sick day');

              if($(".form_hidden").is(":visible"))
              {
                $('.form_hidden').hide(); 
                $('#new_day').html('Add a new day'); 
              };
          });

          // New sick day button and divs.
          $('#new_day').on('click',function(){
              $(".form_hidden").fadeToggle( "slow", "linear" );
              $(this).html($(this).html() == 'Add a new day' ? 'Close new day' : 'Add a new day');

              if($(".sick_hidden").is(":visible"))
              {
                $('.sick_hidden').hide(); 
                $('#sick_day').html('Record a sick day'); 
              };  
          });

          // The interval when the sickness occured (am or pm), once the 'I left early' button is clicked, the sickenss occured div appears.
          $('#left_early').on('click',function(){
              $("#sickness_occur").slideToggle('slow');
          });

          // This is the section for the password reminder stuff and showing the user the password reminder box. 
           $('a#password_reminder').on('click',function(){
              $(".password_reminder").fadeToggle( "slow", "linear" );

              ($('a#password_reminder').html()=='click here') ? $('a#password_reminder').html('close password reminder') : $('a#password_reminder').html('click here');

          });


           /******************************* This is the CRM section ***************************************************/

          // The add a new coach section /workexperience/CRM/coach.php
          $('#add_coach').on('click',function(){
              $(".create_coach").fadeToggle( "slow", "linear" );
              $(".edit_coach").hide();
              ($('#add_coach').html() == 'Add a coach') ? $('#add_coach').html('Close add a coach') : $('#add_coach').html('Add a coach');
          });

          // The button to close the update to a coach section /workexperience/CRM/coach.php
          $('#close_wp_coach').on('click',function(){
              $(".edit_coach").fadeToggle( "slow", "linear" );
          });

          // The add a new coach section /workexperience/CRM/coach.php
          $('#add_placement').on('click',function(){
              $(".create_placement").fadeToggle( "slow", "linear" );
              $(".edit_placement").hide();
              ($('#add_placement').html() == 'Add a placement') ? $('#add_placement').html('Close add a placement') : $('#add_placement').html('Add a placement');
          });

          /***************************** The ALS section of the CRM ******************************************/

          // These options are for the: /workexperience/CRM/als.php checkboxes on the form.

          // Option 'Yes' selected so show the further information box from form. 
          $('.exempt1').on('click',function(){
            $("#exempt_details").show( "slow", "linear" );
          });

          // Option 'No' selected so remove the further information box from form. 
          $('.exempt2').on('click',function(){
            $("#exempt_details").hide( "slow", "linear" );
          });

          // Option 'Yes' selected so show the further information box from form.
          $('.reduced1').on('click',function(){
            $("#reduced_details").show( "slow", "linear" );
          });

          // Option 'No' selected so remove the further information box from form. 
          $('.reduced2').on('click',function(){
          $("#reduced_details").hide( "slow", "linear" );
          });

          // Option 'No' selected, so remove the further information box from form and reset it. 
          $('.exempt2').on('click',function(){
          document.getElementsByName('exempt_details')[0].value=''; // Reset the value to blank.
          });

           // Option 'No' selected, so remove the further information box from form and reset it. 
          $('.reduced2').on('click',function(){
          document.getElementsByName('reduced_further_details')[0].value=''; // Reset the value to blank.
          });


          

        
      }); // End of the document code.



