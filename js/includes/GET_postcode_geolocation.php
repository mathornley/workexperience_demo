<?php

/**
 * Get the PSR4 autoloader and load all the classes required.
 */
spl_autoload_register(function ($class_name) {
    include '../../classes/'.$class_name . '.php';
});

// Gets the learner details row and the placement details for that learner.
$learner = new Learner($_GET['placement_attended_id'],$_GET['studID']);
$learnerDetails = $learner->learnerDetails($_GET['studID'],$_GET['placement_attended_id']);

// Get the map coords from the placement class populateGoogleMap() function for this instance. 
$placement = new Placement;
$coords = $placement->populateGoogleMap($_GET['studID'],$_GET['placement_attended_id'],$learnerDetails[0]['placement_id']);

//echo '<pre>';var_dump($coords);die;

// Countermeasure in case the lat and lng isn't populating the array $coords above. The page is refreshed.
foreach($coords AS $keys=>$values)
{
  foreach($values AS $key=>$value)
  {
    // Iterate the source and destination lat and lng and if any are missed, recall this page and get them again.
    if(empty($value) || !$value || $value==false)
    {
        // Unset anything we have now as that's no good.
        unset($coords);
        // Recall this page and populate the array again and reload the map.
        header("location: GET_postcode_geolocation.php?placement_attended_id={$_GET['placement_attended_id']}?>&studID={$_GET['studID']}");
    }
  }
}

?>

<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Travel modes in directions</title>
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #floating-panel {
        position: absolute;
        top: 10px;
        left: 25%;
        z-index: 5;
        background-color: #fff;
        padding: 5px;
        border: 1px solid #999;
        text-align: center;
        font-family: 'Roboto','sans-serif';
        line-height: 30px;
        padding-left: 10px;
      }
    </style>
  </head>
  <body>
    <div id="floating-panel">
    <b>Mode of Travel: </b>
    <select id="mode">
      <option value="DRIVING">Driving</option>
      <option value="WALKING">Walking</option>
      <option value="BICYCLING">Bicycling</option>
      <!-- Default public transport as selected -->
      <option selected="selected" value="TRANSIT">Public Transport</option>
    </select>
    </div>
    <div id="map"></div>
    <script>
      function initMap() {
        var directionsDisplay = new google.maps.DirectionsRenderer;
        var directionsService = new google.maps.DirectionsService;
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 14,
          center: {lat:<?php echo $coords['source'][0];?>,lng:<?php echo $coords['source'][1];?>}
        });

        // Add traffic to map.
        var trafficLayer = new google.maps.TrafficLayer();
        trafficLayer.setMap(map);

        directionsDisplay.setMap(map);

        calculateAndDisplayRoute(directionsService, directionsDisplay);
        document.getElementById('mode').addEventListener('change', function() {
          calculateAndDisplayRoute(directionsService, directionsDisplay);
        });
      }

      function calculateAndDisplayRoute(directionsService, directionsDisplay) {
        var selectedMode = document.getElementById('mode').value;
        directionsService.route({
          origin: {lat:<?php echo $coords['source'][0];?>,lng:<?php echo $coords['source'][1];?>},  // Source.
          destination: {lat:<?php echo $coords['destination'][0];?>, lng:<?php echo $coords['destination'][1];?>},  // Destination
          // Note that Javascript allows us to access the constant
          // using square brackets and a string value as its
          // "property."
          travelMode: google.maps.TravelMode[selectedMode]
        }, function(response, status) {
          if (status == 'OK') {
            directionsDisplay.setDirections(response);
          } else {
            window.alert('Directions request failed due to ' + status);
          }
        });
      }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=<APIKEY>" async defer></script>
  </body>
</html>