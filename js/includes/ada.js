// adaModule v1.0
// Revealing module pattern, self-contained object interface
var adaModule = (function () {
    
    // Vars
    var query;
    
    // Response strings
    var hlpresponse = "<table class=\"ADATABLE table table-bordered table-striped\"><tr><th>Slash Command<\/th><th>Description<\/th><\/tr><tr><td>\/help<\/td><td>This page<\/td><\/tr><tr><td>\/reset<\/td><td>Remove Ada from local Storage<\/td><\/tr><tr><td>\/demo<\/td><td>Play the Ada demo<\/td><\/tr><\/table>";
    
    var demoresponse = "I'm Ada, your personal assistant. You can ask me a question about anything to do with your studies and I'll try and give you a useful answer. I can also give you some hints and tips about this app. Below are some example questions I understand.";
    
    var hipsum = "<p>Lorem ipsum dolor amet bushwick disrupt williamsburg, vexillologist mustache prism synth marfa crucifix mlkshk raclette pop-up migas neutra. Mumblecore mixtape pickled freegan pabst iceland pinterest biodiesel tousled hexagon poutine squid. Plaid cardigan franzen cloud bread. Narwhal bushwick butcher vape.</p>";
    
    // Save Ada State to local storage
    var storage = {
        save: function save() {
            var adaStore = $("#ADA-CONTENT")[0].innerHTML; // get current Ada contents, move into variable "adaStore"
            console.log("Saved to localStorage");
            localStorage.setItem("ADASTORE",adaStore);
        },
        load: function load(){
            var adaStore = localStorage.getItem("ADASTORE");
            console.log("Loaded from localStorage");
            return adaStore;
        },
        reset: function reset(){
            localStorage.removeItem("ADASTORE");
            console.log("localStorage cleared");
        }
    };
    
    var control = {
        open: function open() {
            $("#ADA-CONTAINER").addClass("OPEN");
            $("#ADA-DIRECTIONARR").toggleClass("fa-angle-up fa-angle-down").promise().done(function () {
                $("#ADA-OPENCLOSE").text("click to close");
                console.log("Ada opened");
            });
        },
        close: function close() {
            $("#ADA-CONTAINER").removeClass("OPEN");
            $("#ADA-DIRECTIONARR").toggleClass("fa-angle-down fa-angle-up").promise().done(function () {
                $("#ADA-OPENCLOSE").text("click to open");
                console.log("Ada closed");
            });
        }
    };
    
    var message = {
        send: function send(query) {            
            $(".spinner").css({ display: "block" });
            display.reset(query);
            queryAda(query)
            .done(function (result) {
                console.log("success", result);
                var response = cleanAda(result);
                console.log(response);
                setTimeout(function () {
                    display.show(response);
                }, 300);
            })
            .fail(function (xhr) {
                console.log("error", xhr);
                var response = "I'm sorry, but there was an error and I couldn't retrieve your request. Please try again.";
                setTimeout(function () {
                    display.show(response);
                }, 300);
            });
        }
    };
    
    var display = {
        reset: function reset(input) {
            $(".ADA-BUBBLE").removeClass("SHOW");
            $(".ADA-EXAMPLES li").removeClass("SHOW");
            $("#ADA-Q-TXT").text(input);
            setTimeout(function () {
                $(".ADA-EXAMPLES").css({ display: "none" });
            }, 300);
        },
        show: function show(response) {
            $(".spinner").css({ display: "none" });
            $(".ADA-BUBBLE").html(response);
            styleTable();
            $(".ADA-BUBBLE").addClass("SHOW");
            storage.save();
        },
        demo: function demo() {
            $(".ADA-BUBBLE").removeClass("SHOW");
            $(".ADA-EXAMPLES li").removeClass("SHOW");
            $("#ADA-Q-TXT").text("Who is Ada?");
            
            setTimeout(function () {
                $(".ADA-BUBBLE")
                    .html(demoresponse)
                    .addClass("SHOW");
                $(".ADA-EXAMPLES").css({ display: "block" });
                $(".ADA-EXAMPLES li").each(function (i) {
                    var $this = $(this);
                    setTimeout(function () {
                        $this.addClass("SHOW");
                    }, 500 * i + 500);
                });
            }, 500);
            console.log("Demo complete");
        }
    };
    
    // open / close event handler
    $("#ADA-HEADER").click(function () {
        $("#ADA-CONTAINER").toggleClass("OPEN");
        $("#ADA-DIRECTIONARR").toggleClass("fa-angle-up fa-angle-down").promise().done(function () {
            if ($("#ADA-DIRECTIONARR").hasClass("fa-angle-down")) {
                $("#ADA-OPENCLOSE").text("click to close");
                console.log("Ada opened");
            } else {
                $("#ADA-OPENCLOSE").text("click to open");
                console.log("Ada closed");
            }
        });
    });
    
    // event handler to execute intro sequence
    // uses jquery function one()
    // handler for element is unbound after its first invocation
    $( "#ADA-HEADER" ).one( "click", function() {
        if (localStorage.getItem("ADASTORE") === null) {
            display.demo();
        }
    });
    
    // Form handler
    $("form[name=adaForm]").on("submit", function (event) {
        event.preventDefault();
        query = $('input[name="query"]').val();
        $('input[name="query"]').val(""); // clear input field
        switch(query) {
            case "/help":
                display.reset("Ada Slash Commands");
                setTimeout(function () {
                    display.show(hlpresponse);
                }, 500);
                break;
            case "/reset":
            case "/clear":
                storage.reset();
                display.reset("Reset Ada");
                setTimeout(function () {
                    display.show("OK. I've removed myself from localStorage.");
                }, 500);
                break;
            case "/demo":
                storage.reset();
                display.demo();
                break;
            default:
                message.send(query);
        }
    });
    
    // API Query
    function queryAda(query) {
        var lid = studID;
        console.log(studID);
        return $.ajax({
            url: "http://www.boltoncc.ac.uk/api/api.php",
            cache: false,
            beforeSend: function (xhr, jqXHR) {
                xhr.setRequestHeader("X-API-KEY", "we7q73wbnSG0wGyag000Fix4mZsO6CYd");
            },
            dataType: 'json', // form of the response from API
            data: {
                learner: window.studID,
                query: query
            }
        });
    }

    // Clean up response coming back from API
    function cleanAda(input){
        // remove newline tags and extra <br> tags
        var san = input.data.replace(/\\n/g, "").replace(/<br>\s<br>/g, "<br>");
        // clean up table data; add bootstrap classes to table
        if (san.match(/<table>/i)){
            san = san.replace(/<table>/i, "<table class=\"ADATABLE table table-bordered table-condensed table-striped\">");
        }
        console.log("Cleaned up response: " + san);
        return san; // return clean html
    }
    
    function styleTable(){
        $("table tr:contains('PASS')").css("background-color", "#D5F5E3");
        $('table tr').each(function () {
           if ($(this).find('td:empty').length) $(this).remove();
        });      
    }
    
  
    // Stuff to do on document ready
    $(document).ready(function () {
        // Check if there"s anything in storage, pass into DOM
        if ("ADASTORE" in localStorage) {
            console.log("localStorage found, retrieved");
            $("#ADA-CONTENT").html(storage.load());
        }
        // click events added to example question links
        $(".ADA-EXAMPLES").delegate("li", "click", function () {
            $(".spinner").css({
                display: "block"
            });
            queryAda($(this).text())
                .done(function (result) {
                    console.log("success", result);
                    var response = cleanAda(result);
                    console.log(response);
                    display.show(response);
                })
        });
    });
    
    return {
        storage: storage,
        control: control,
        message: message
    };
    
})();