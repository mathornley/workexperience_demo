<?php

/**
 * Get the PSR4 autoloader and load all the classes required.
 */
spl_autoload_register(function($class_name) {
    include '../classes/'.$class_name.'.php';
});

ini_set('display_errors', 'On');

// Stop the cache
require_once '../stop_cache.php';

// Instantiate an instance of the autocomplete class we'll use for search.
$ac = new AutoComplete;

// Got something to search to autocomplete.
if (isset($_GET['term']))
{
	try
	{
	    $ac->autoCompleteWorkReady($_GET['term']);
	}
	catch(PDOException $e)
	{
		echo 'ERROR: ' . $e->getMessage();
	}
}