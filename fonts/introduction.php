<?php

/**
 * Get the PSR4 autoloader and load all the classes required.
 */
spl_autoload_register(function ($class_name) {
    include 'classes/'.$class_name . '.php';
});

// Stop the cache
require_once 'stop_cache.php';

// Set a new instance of the learner.
$learner = new Learner($_GET['placement_attended_id'],$_GET['studID']);
$coach = new CareerCoachReview();


// Gets the learner details row and the placement details for that learner.
$learnerDetails = $learner->learnerDetails();
//print_r($learnerDetails);die;
?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Work experience app</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/theme.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <!-- Fixed navbar -->
    <!--<nav class="navbar navbar-inverse navbar-fixed-top">-->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/workexperience/">Work experience app</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <!--<li><a href="#about">About</a></li>-->
            <!--<li><a href="#contact">Contact</a></li>-->
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav><!-- end fixed navbar -->

    <div class="container-fluid theme-showcase" role="main">
    	<div class="row"><!--Start of row -->
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-bordered">
		    			<thead>
		      				<tr>
						        <th>Student name</th>
						        <td><?=$learnerDetails[0]['name']?></td>
						        <th>Student ID</th>
						        <td><?=$learnerDetails[0]['person_code']?></td>
                    <th>Coach name</th>
                    <td><?=$coach->getCoachNameForLearner($_GET['placement_attended_id'])?></td>
		      				</tr>
		    			</thead>	
	  				</table>
				</div>	
			</div>
		</div> <!--End of row -->

		<div class="row"><!--Start of row -->
	    	<!-- Tabbed content left-->
		    <div class="col-md-12">
		        <ul class="nav nav-tabs">
              <li><a href="index.php">Choose placement</a></li>
              <li class="active"><a href="introduction.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Introduction</a></li>
              <li><a href="location.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Where is my placement</a></li>
              <li><a href="agreement.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Work placement agreement</a></li>
              <li><a href="aims.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Aims and objectives</a></li>
              <li><a href="logbook.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Logbook</a></li>
              <li><a href="placement_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Your work placement review</a></li>
              <li><a href="employer_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Employer review</a></li>
              <li><a href="coach_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Work coach review</a>
              </li>
               <li><a href="debug.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Report a bug</a>
              </li>
            </ul>
				<br/>
			</div><!-- end tabbed content left-->
		</div><!--End of row -->
	
	    <div class="row"><!--Start of row -->
		    <div class="col-md-12"><!-- content main full page-->
		    	<h3>Hi <?=$learnerDetails[0]['forename']?>! Welcome to your work experience placement application.</h3>
            <p>A work placement offers you the chance to gain valuable experience spending time working for an employer and carrying out duties or tasks as any other employee of the company would.
            Work Experience is a key part of your course and you are expected to undertake external work experience for a minimum of 30 hours.</p>

            <ul>
                <li>It will give you an insight into your chosen industry.</li>
                <li>It will give you an insight into your chosen industry.</li>
                <li>It introduces you to the requirements, disciplines and expectations of a real working life.</li>
                <li>Work Experience helps build links for future employment opportunities.</li>
                <li>It Increases your employability skills such as communication, problem solving, social skills, self-management, commitment, enthusiasm, flexibility and team work.</li>
                <li>Work Experience enables you to form relationships outside your immediate social group and to make decisions in real life situations.</li>
            </ul>

            <p>Until you've <span class='red-bold'>agreed</span> to your <a href="agreement.php" title='You\'ll need to sign your Workplace Agreement before you can progress'>Work Placement Agreement</a>, <strong>you'll not be able to access any part of the application.</strong>. The agreement is a legal requirement you must agreed to, and have read before you use the application for the first time.</p>

            <p>From this section you can also <a href="debug.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>" title='Report a bug or any issues you are experiencing whilst using this application'> report a bug</a> and let the development team know of any issues you're experiencing when using this application.</p>

		    </div><!-- row ends-->
  	</div> <!--end main container -->

    <!-- footer here -->
    <footer class="footer">
    <hr/>
    	<div class="container">
        	<p style="text-align:center;">Bolton College 2017</p>
      	</div>
    </footer>
    <!-- end footer end -->

    <!-- Bootstrap core JavaScript ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
