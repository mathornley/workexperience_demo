<?php

/**
 * Get the PSR4 autoloader and load all the classes required.
 * This page handles the validation to the updated review with PHP in the class (editReview()) and then finally Javascript
 */
spl_autoload_register(function ($class_name){
    include '../classes/'.$class_name . '.php';
});

// Stop the cache
require_once '../stop_cache.php';

// Error reporting.
ini_set('display_errors','On');

// Set up an instance of the Employee review
$employer = new EmployeeReview;

// Set a logout timer so the page will redirect to the signeurlpaiement(clent, data) in area >=30 minutes.
$login = new Login;

$learner = new Learner;

// Make sure the user hasn't been idle longer than 30 minutes.
$timeout = $login->checkTimeOut($login->getPAIDidFromPlacementID($_GET['placement_attended_id']));

if($login->checkSession()==false)
{
  header('location: /workexperience/employer/');
}

// Get the table headings for the table to be used below in the HTML. 
$employerTableFields = $employer->employerTableFields();

$employee = new EmployerReview;
$jumboDetails = $employee->getStudentDetailsForJumbo($_GET['placement_attended_id']);

// Get the review if completed
$employerReview = $employee->getLearnerReview($_GET['placement_attended_id']);

// Student agrees with their midpoint review.
if(isset($_POST['midpoint_review_agree_employer']) && isset($_POST['agree_midpoint_button']))
{
  $employerMidpointSigned = $learner->addEmployerMidpointReview($_POST);
}

// Save the employer review.
if(isset($_POST['save_employer_review']))
{
  $employer->saveEmployerReview($_POST);
  header("location: admin.php?placement_attended_id={$_GET['placement_attended_id']}");
}

// Employer review - this is to each review.
if(isset($_POST) && isset($_POST['edit_employer_review']))
{
  header("location: admin_edit.php?placement_attended_id={$_GET['placement_attended_id']}");
}

// Employer review - this is to each review.
if(isset($_POST['record_absence']) && is_numeric($_POST['record_absence']))
{
  $recordedAbsence = $employee->recordAbsence($_POST['record_absence']);
}

// Get the logbook for this instance of the learner placement. Use the placement_attended_id to get the (placement id - which is the placement_attended_id in the logbook table) and then use that to call the logbook entries.
$logbookEntries = $employee->getLogbookForEmployerReview($_GET['placement_attended_id']);

// Get the mid point review date
$mpReview = $learner->getMidpointReview($_GET['placement_attended_id']);

// Just get the placement id from the placement attended id.
$placement_id_link = $employee->getThePlacementID($_GET['placement_attended_id']);

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Work experience app</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="../css/bootstrap-theme.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="../css/theme.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

    <!--<script src="js/validatereview.js"></script>-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- JQuery -->
    <script
    src="https://code.jquery.com/jquery-1.12.4.js"
    integrity="sha256-Qw82+bXyGq6MydymqBxNPYTaUXXq7c8v3CwiYwLLNXU="
    crossorigin="anonymous"></script>
  </head>
  <body>

    <!-- Fixed navbar -->
    <!--<nav class="navbar navbar-inverse navbar-fixed-top">-->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
           <a class="navbar-brand" href="/workexperience/CRM/">Work experience app</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <!--<li class="active"><a href="#">Home</a></li>-->
            <!--<li><a href="#about">About</a></li>-->
            <!--<li><a href="#contact">Contact</a></li>-->
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav><!-- end fixed navbar -->


  <div class="container-fluid theme-showcase" role="main">
    <div class="row"><!--Start of row -->
        <div class="col-md-12"><!-- content main full page-->
          <h2>Employer review section.</h2>
          <p>The completed review will be shown to the learner once completed.</p>  
          </div>
        </div>
      </div>

      <?php if(isset($recordedAbsence) && $recordedAbsence==false ):?>
      <script>
        $(document).ready(function(){
        $("#error_message").show().delay(5000).fadeOut();
      });
      </script>
      <div class="alert alert-danger" id='error_message'>
        <strong>Sorry!</strong> an error occured adding the learner.
      </div>
      <?php endif;?>

      <?php if(isset($recordedAbsence) && $recordedAbsence!=false ):?>
      <script>
        $(document).ready(function(){
        $("#success_message").show().delay(5000).fadeOut();
      });
      </script>
      <div class="alert alert-success" id='success_message'>
      <strong>Success!</strong><?=$recordedAbsence['message']?>
      </div>
     <?php endif;?>


     <?php if(isset($employerMidpointSigned) && $employerMidpointSigned!=false):?>
      <script>
        $(document).ready(function(){
        $("#success_message").show().delay(5000).fadeOut();
      });
      </script>
    <div class="alert alert-success" id='success_message'>
      <strong>Success!</strong> You have successfully completed the midpoint review for the learner.
    </div>
    <?php endif;?>

     <?php if(isset($employerMidpointSigned) && $employerMidpointSigned==false):?>
      <script>
        $(document).ready(function(){
        $("#error_message").show().delay(5000).fadeOut();
      });
      </script>
    <div class="alert alert-danger" id='error_message'>
      <strong>Success!</strong> The midpoint review has failed to be added.
    </div>
    <?php endif;?>

  <!-- Jumbatron section showing the details of who they are reviewing-->
  <div class="container-fluid theme-showcase" role="main">
    <div class="jumbotron">
      <h3>Student name:  <?=$jumboDetails['student_name']?></h3> 
        <p>Placement: <?=$jumboDetails['placement']?></p>
         <p>Address: <?=$jumboDetails['placement_address']?>,<?=$jumboDetails['post_code']?> </p>
          <p>Agreement signed: <?=$jumboDetails['agreement_signed_date']?></p>
          <hr/>
          <p>Coach name: <?=$jumboDetails['coach_name']?></p>
          <!--<p>PAID <?=$jumboDetails['placement_attended_id']?></p>-->

          <form method='POST'>
            <!-- Form to handle adding a days absence for a learner.-->
            <th>
              <p><button <?php if($employee->hasSickbeenRecorded($_GET['placement_attended_id'])!=false) echo 'disabled';?> type="submit" class="btn btn-danger" name='record_absence' value="<?=$_GET['placement_attended_id']?>"><?php echo ($employee->hasSickbeenRecorded($_GET['placement_attended_id'])==false) ? 'Record student absence for '. date('l jS \of F Y') : 'Todays absence has been recorded for this learner today';?></button></p>
            </th>
          </form>
        
    </div>
    <p>Below is the logbook entries for the learner who has been on placement with your organisation. You can view the learners hours and then finally complete the review at the very bottom of the page.</p>
    <p>The learner will be able to see your remarks and review of their performance once you've completed the review section. If the hours in the logbook appear incorrect, don't tick the option to confirm and then perhaps mention this in your comments below.</p> 
  </div>
  <!-- End Jumbatron section showing the details of who they are reviewing-->


<!-- Start the learner logbook section-->
<div class="container-fluid theme-showcase" role="main">
  <div class="row"><!--Start of row -->
    <div class="col-md-12"><!-- content main full page--> 
   
    <h3>Student hours:</h3>

    <?php 
    // Only show if the student has logbook enytries.
    if($logbookEntries!=false):?>

    <fieldset>
      <div class="table-responsive">          
        <table class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>Date</th>
              <th>Start</th>
              <th>Finish</th>
              <th>Sick during when</th>
              <th>Entire sick day</th>
              <th>Absence details</th>
            </tr>
          </thead>
          <tbody>
              <?php 

              $x=0; // Loopy.
              $hours=0; // Keep track of the hours

              for($i=0;isset($logbookEntries[$i]);$i++):?>
                  <tr>
                      <td><?=date('l jS \of F Y',strtotime($logbookEntries[$i]['date']))?></td>
                      <td><?php if(isset($logbookEntries[$i]['start'])) echo date('H:i',strtotime($logbookEntries[$i]['start']));?></td>
                       <td><?php if(isset($logbookEntries[$i]['finish'])) echo date('H:i',strtotime($logbookEntries[$i]['finish']));?></td>
                      <td><?=($logbookEntries[$i]['sick_during']=='0') ? '--' : $logbookEntries[$i]['sick_during'];?></td>
                      <td><?=($logbookEntries[$i]['sickness']=='1') ? '<span class="glyphicon glyphicon-ok"></span>' : '--';?></td>
                      <td><?=($logbookEntries[$i]['details_of_absence']) ? $logbookEntries[$i]['details_of_absence'] : '--' ?></td>
                   </tr>
                  <?php ++$x;

                  // Work out the total hours, keeping a tally as you go.
                  $start = strtotime($logbookEntries[$i]['start']);
                  $finish = strtotime($logbookEntries[$i]['finish']);
                  $hours += round(abs($finish - $start) / 3600,2);
                 
                  endfor; // End the for loop

                  ?>

                <tr>
                  <th>Total hours</th>
                  <td><?=$hours?> hrs</td>
                </tr>

              </tbody>
            </table>
          </div> <!-- table-responsive ends-->
        </fieldset>
     
      <?php 
      // No logbook entries for the student.
      else:?>
        <p> No logbook entries for this student so far.</p>
      <?php endif;?>

    </div>
  </div>

<!-- End the learner logbook section-->


  <div class="row"><!--Start of row -->
    
         <div class="col-md-12"><!-- content main full page-->

          <h3>Midpoint Assessment</h3>

          <?php if($mpReview==false): // There is no data on this placement ?>
            <p>The Midpoint assessment hasn't been completed by the career coach at this time.</p>
          <?php endif;?>


          <?php if($mpReview!=false): // There is data ?>
          <p>This is the midpoint agreement section for <?=$jumboDetails['student_name']?></h4>. The agreement allows you to review and sign to confirm you agree with the review conducted for the learner by the career coach during the placement.</p>

          <form method='POST'>

            <hr/>

            <h4>Behaviour and Social Skills Knowledge and Technical Skills:</h4>

            <fieldset>
              <p>Does not meet expectations: <input 
              disabled type="radio" name="behaviour" value="does_not_meet_expectations" <?php if(isset($mpReview[0]['behaviour']) && $mpReview[0]['behaviour']=='does_not_meet_expectations') echo 'checked';?>/></p>
              <p>Meet expectations: <input type="radio" disabled name="behaviour" value="meet_expectations" <?php if(isset($mpReview[0]['behaviour']) && $mpReview[0]['behaviour']=='meet_expectations') echo 'checked';?>/></p>
              <p>Exceeds expectations: <input type="radio" disabled name="behaviour" value="exceeds_expectations" <?php if(isset($mpReview[0]['behaviour']) && $mpReview[0]['behaviour']=='exceeds_expectations') echo 'checked';?>/></p>

              <hr/>

               <p>I agree with the above review of <?=$jumboDetails['student_name']?>'s Midpoint Assessment: <input type='checkbox' required id='agree_midpoint' name='midpoint_review_agree_employer' value="<?=$_GET['placement_attended_id']?>" <?php if(isset($mpReview[0]['employer_signed']) && $mpReview[0]['employer_signed']=='1') echo 'disabled checked'; ?>/>
                <br/>
                <br/><input type='text' disabled name='student_name' value="<?php if(isset($mpReview[0]['employer_signed']) && $mpReview[0]['employer_signed']=='1') echo $jumboDetails['placement'];?>"/> (Employer)</p>
                <input type='hidden' name='row_id' value='<?=$mpReview[0]['id']?>'/>
                 <p><button type='submit' name='agree_midpoint_button' value="<?=$mpReview[0]['id']?>" <?php if($mpReview[0]['employer_signed']=='1') echo 'disabled';?> class="btn btn-primary">Submit</button></p>


                 <!-- Coach signed first -->
                 <?php if($mpReview[0]['employer_signed']=='1' && $mpReview[0]['signed_with_coach']=='1' && $mpReview[0]['employer_signed_first']=='0' ):?>
                 <p>This review was completed on: <strong><?=date('l jS \of F Y \a\t h:i A',strtotime($mpReview[0]['date_created']))?></strong> by <strong><?=$learner->getCoachName($mpReview[0]['coach_id'])?></strong></p>
                 <?php endif;?>

                <!-- Student signed first -->
                 <?php if($mpReview[0]['employer_signed']=='1' && $mpReview[0]['signed_with_coach']=='0' && $mpReview[0]['employer_signed_first']=='1' ):?>
                  <p>Review was completed on: <strong><?=date('l jS \of F Y \a\t h:i A',strtotime($mpReview[0]['date_created']))?></strong> by <strong><?=$jumboDetails['placement']?></strong></p>
                 <?php endif;?>

                 <!-- Student signed first -->
                 <?php if($mpReview[0]['employer_signed']=='1' && $mpReview[0]['signed_with_coach']=='1' && $mpReview[0]['employer_signed_first']=='1' ):?>
                  <p>Review was completed on: <strong><?=date('l jS \of F Y \a\t h:i A',strtotime($mpReview[0]['date_created']))?></strong> by <strong> <?=$jumboDetails['placement']?></strong></p>
                 <?php endif;?>
            </fieldset>
          </form>

          <!-- If the coach has signed and has comments, show the comments as read-only -->
          <?php if($mpReview[0]['comments']):?>
            <h4>Coach midpoint review comments:</h4>
              <textarea readonly class="form-control" id="coach_comments" name='coach_comments' rows="5"><?=$mpReview[0]['comments']?></textarea>
          <?php endif;?>

        </div><!-- row ends-->
      <?php endif;?>
  </div>

<div class="container-fluid theme-showcase" role="main">
      <div class="row"><!--Start of row -->
        <div class="col-md-12"><!-- content main full page-->      
         
         <!-- If the review has been completed, disable the form -->

          <h3>Student review by the employer:</h3>

          <fieldset <?php if($employerReview!==false) echo 'disabled' ?>>
            <form method='POST'>
                 <div class="table-responsive">          
                    <table class="table table-bordered table-striped">
                      <thead>
                        <tr>
                          <th></th>
                          <th>Excellent</th>
                          <th>Good</th>
                          <th>Satisfactory</th>
                          <th>Requires improvement</th>
                        </tr>
                      </thead>
                      <tbody>
                          <?php $x=0;?>
                          <?php foreach($employerTableFields AS $heading):?>
                            <tr>
                                <td>
                                  <input type="hidden" name="review[<?=$x?>]['heading']" value="<?=$heading?>"/>
                                  <input type="hidden" name="placement_attended_id" value="<?=$_GET['placement_attended_id']?>"/>
                                <?=$heading?>
                                </td>
                                <td <?php if($employerReview[$x]['excellent']=='1') echo 'class="bg-success"';?>>
                                 <input type="radio" name="review[<?=$x?>]['score']" value="excellent" <?php if($employerReview[$x]['excellent']=='1') echo 'checked';?>/>
                                </td>
                                <td <?php if($employerReview[$x]['good']=='1') echo 'class="bg-success"';?>>
                                 <input type="radio" name="review[<?=$x?>]['score']" value="good" <?php if($employerReview[$x]['good']=='1') echo 'checked';?>/>
                                </td>
                                <td <?php if($employerReview[$x]['satisfactory']=='1') echo 'class="bg-success"';?>>
                                  <input type="radio" name="review[<?=$x?>]['score']" value='satisfactory' <?php if($employerReview[$x]['satisfactory']=='1') echo 'checked';?> />
                                </td>
                                <td <?php if($employerReview[$x]['requires_improvement']=='1') echo 'class="bg-success"';?>>
                                  <input type="radio" name="review[<?=$x?>]['score']" value="requires_improvement" autofocus required="required" <?php if($employerReview[$x]['requires_improvement']=='1') echo 'checked';?>/>
                                </td> 
                            </tr>
                            <?php ++$x;?>
                            <?php endforeach;?>
                            <tr>
                              <td colspan=5>
                              <p>Employers review comments:</p>
                                <textarea required class='textarea_review' placeholder='Please give a little more information on why you have made this choice' name="review['comments']"><?=$employerReview[0]['comments']?></textarea>
                            </tr>
                        </tbody>
                      </table>

                      <p><input type="checkbox" name="confirm_hours" value="1"
                        <?php if($employerReview[0]['confirm_hours']=='1') echo 'checked';?>/> I can confirm these hours are correct as per the students logbook entries.
                      </p>
                    
                    </div> <!-- table-responsive ends-->

                    <!-- The review is not compelted, so show the save button -->
                    <?php if($employerReview==false):?>
                      <p><button type='submit' class="btn btn-primary" name='save_employer_review' <?php if(isset($_POST['save_employer_review'])) echo 'disabled';?>>Save</button></p>
                   <?php endif;?>
                  </form>
                 
                  </fieldset>

                 <!-- The review is completed, so show the edit button -->
                <fieldset>
                <form method='POST'>
                  <?php if($employerReview!=false):?>
                    <p>Review completed</p>
                    <p>
                      <button type='submit' class="btn btn-danger" name='edit_employer_review' value="<?=$_GET['placement_attended_id']?>">Edit</button>
                    </p>
                  <?php endif;?>
                </form>
                 <p><a href="/workexperience/employer/list.php?placement_id=<?=$placement_id_link?>"><< Back to all your learners list</a></p>
              </fieldset>
      
        </div> <!-- col ends-->  
      </div><!-- row ends-->

    </div> <!--end main container -->

    <!-- footer here -->
    <footer class="footer">
    <hr/>
      <div class="container">
        <p style="text-align:center;">Bolton College 2017</p>
      </div>
    </footer>
    <!-- end footer end -->

    <!-- Bootstrap core JavaScript ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
