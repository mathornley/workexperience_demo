<?php

/**
 * Get the PSR4 autoloader and load all the classes required.
 */
spl_autoload_register(function ($class_name) {
    include '../classes/'.$class_name . '.php';
});

/**
 * Use Strict-Transport-Security (STS) to force the use of SSL.!
 */
if($_SERVER["HTTPS"] != "on")
{
    header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
}

// Stop the cache
require_once '../stop_cache.php';

  // User has attemped to login
  if(isset($_POST) && isset($_POST['sign-in']))
  {
    // Instantiate a new instance of login and process the input. 
    $login = new Login($_POST);

    // Check the login details and if they're ok, progress.
    if($login->checkLogin()!=false)
    {
      // Get the employers details and the placement_id.
      $employer = $login->checkLogin();

      // Send the placement_id to the list file via GET, so the learners for that employer can be looked up.
      header("location: list.php?placement_id={$employer[0]['id']}");
    }
    // User details aren't found so stop here
    else
    {
      // Message to the user in the form, activated by $error being set under the log-in box.
      $error=true;
    }
  }

  // Password reminder
  if(isset($_POST) && isset($_POST['password_reminder']))
  {
    $login = new Login;
    if($password_changed = $login->sendPasswordEmail($_POST))
    {
      $password_changed = '<p class="login-success">Your password reset request was sent successfully. Please check your email for a confirmation link.</p>';
    }
    else
    {
      $password_changed = '<p class="login-error"> An account with that password can\'t be found.</p>';
    }
  }
?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Sign-in for the work experience application.</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

      <!-- Bootstrap theme -->
    <link href="../css/bootstrap-theme.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/signin.css" rel="stylesheet">

        <!-- Some jQuery to handle the buttons and divs for the forms-->
     <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="crossorigin="anonymous"></script>

      <!-- Include the jQuery for buttons and navs -->
      <script src="../js/includes/button_div_jquery.js"></script>

      <style>
        .form-signin img{
          width:90%;
        }

        h2{
          color:a0a0a0;
        }

      </style>


  </head>

  <body>

    <div class="container">
      <div class='password_login'>

        <form class="form-signin" method='POST'>


          <img src='../images/BClogo.png'/>

          <h3> Welcome to the<br/> Bolton College Work Experience System.</h3>

          <h4 class="form-signin-heading">Please sign in</h4>
            <p><label for="username" class="sr-only">Username</label>
            <input type="username" id="username" name='username' class="form-control" placeholder="Username" required autofocus/></p>
            <p><label for="password" class="sr-only">Password</label>
            <input type="password" id="password" name='password' class="form-control" placeholder="Password" required/></p>
            <div class="checkbox">
            </div>
            <button class="btn btn-lg btn-primary btn-block" name='sign-in' type="submit">Sign in</button>
            <p class='password'>If you've forgotten your password, <a id='password_reminder' href='#'>click here</a></p>
             <?php if(isset($error) && $error===true) echo '<p class="login-error">Those login details are incorrect.</p>';?>
             <?php if(isset($password_changed)) echo $password_changed;?>
        </form>
      </div>

      <div class='password_reminder'>
        <form class="form-signin" method='POST'>
          <h2 class="form-signin-heading">Password reminder</h2>
            <label for="username" class="sr-only">Username</label>
            <p>Enter the password for your account to receive a password reminder link via email.</p>
            <p><input type="email_reminder" id="email_reminder" name='reminder_email' class="form-control" placeholder="Please enter your email address" required autofocus/></p>
            <p><button class="btn btn-lg btn-primary btn-block" name='password_reminder' type="submit">Submit password request</button>
             <?php if(isset($error_password) && $error_password===true) echo '<p class="login-error">That isn\'t a password you\'ve used.</p>';?></p>
        </form>
      </div>

    </div> <!-- End main /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
