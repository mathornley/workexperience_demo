<?php

/**
 * Get the PSR4 autoloader and load all the classes required.
 */
spl_autoload_register(function ($class_name) {
    include '../classes/'.$class_name . '.php';
});

// Stop the cache
require_once '../stop_cache.php';

// Error reporting is on. 
ini_set('display_errors', 'On');


// Set a logout timer so the page will redirect to the sign in area >=30 minutes.
$login = new Login;
if(isset($_GET['placement_id'])) $timeout = $login->checkTimeOut($_GET['placement_id']);

// *************************************************** Manage the placement id first before change ***************************

// Password reset link clicked, so change the token to placement id (not the placement attended id).
if(isset($_GET['token']) && isset($_GET))
{
  // Put 'token' in  as 'placement_id' so the enployer can use the form to reset the password. Token is jst to mask it's an ID.
  $placement_id = $_GET['token'];
  // Need to set a new instance of the Login ID, so we can use HASID. (This class is set in Main.php and inherits)
  $login = new Login;
  $placement_id = $login->hashids->decode($placement_id); // This is the placement id for the employer decoded.
}
// They've come with no link or they are trying to see what is in the password reset page, so redirect to index.php for them to signin.
else
{
  header('location: /workexperience/employer/index.php');
}
//print_r($placement_id);die; // Array ( [0] => 1 ) is the ID of the placement row for the employer.

// *************************************************** Change the password here on wards ***************************


// User has entered both passwords to change the password (as password is required twice to check they don't typo).
if(isset($_POST) && isset($_POST['change_password']) && $_POST['change_password'])
{
  // Send the new password to the Login.php so it can be changed.
  //print_r($_POST);die;
  $_POST['placement_id'] = $placement_id; 
  $password_changed = $login->changePassword($_POST);

  if($password_changed!=false)
  {
    $password_changed = "Password saved successfully, now go and <a href='/workexperience/employer/index.php'>sign in</a> again";
  }

  else
  {
    $password_error = 'You must enter the password twice';
  }
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Sign-in for the work exeprience application.</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

      <!-- Bootstrap theme -->
    <link href="../css/bootstrap-theme.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/signin.css" rel="stylesheet">

        <!-- Some jQuery to handle the buttons and divs for the forms-->
     <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="crossorigin="anonymous"></script>

      <!-- Include the jQuery for buttons and navs -->
      <script src="../includes/button_div_jquery.js"></script>


      <style>
        .form-signin img{
          width:90%;
        }

        h2{
          color:a0a0a0;
        }
      </style>




  </head>

  <body>

    <div class="container">
      <div class='password_login'>
        <form class="form-signin" method='POST'>

          <img src='../images/BClogo.png'/>

          <h3 class="form-signin-heading">Please change your password below:</h3>
            <label for="password1" class="sr-only">Password</label>
            <p><input type="password1" id="password1" name='password1' class="form-control" placeholder="Password" required autofocus/></p>
            <label for="password2" class="sr-only">Password confirmation</label>
             <p><input type="password2" id="password2" name='password2' class="form-control" placeholder="Confirm password" required/></p>
            <p><button class="btn btn-lg btn-primary btn-block" name='change_password' value='1' type="submit">Sign in</button></p>
             <?php if(isset($error) && $error===true) echo '<p class="login-error">Those login details are incorrect.</p>';?>
             <?php if(isset($password_changed)) echo "<p class='login-success'>$password_changed<p>";?>
              <?php if(isset($password_error)) echo $password_error;?>
        </form>
      </div>

      </div>

    </div> <!-- End main /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
