<?php

/**
 * Get the PSR4 autoloader and load all the classes required.
 * This page handles the validation to the updated review with PHP in the class (editReview()) and then finally Javascript
 */
spl_autoload_register(function ($class_name){
    include '../classes/'.$class_name . '.php';
});

// Stop the cache
require_once '../stop_cache.php';

// Error reporting.
ini_set('display_errors','On');

// Set up an instance of the Employee review
$employer = new EmployeeReview;

// Get the table headings for the table to be used below in the HTML. 
$employerTableFields = $employer->employerTableFields();

$employee = new EmployerReview;
// Get the details for the jumbo area of the page.
$jumboDetails = $employee->getStudentDetailsForJumbo($_GET['placement_attended_id']);

// Set a logout timer so the page will redirect to the sign in area >=30 minutes.
$login = new Login;
$timeout = $login->checkTimeOut($jumboDetails['placement_id']);

// Get the review if completed by the employer already.
$employerReview = $employee->getLearnerReview($_GET['placement_attended_id']);

// Finally save the employer review. Both the row id and placement attended id have been posted. 
if(isset($_POST['update_employer_review']))
{
  if($employer->updateEmployerReview($_POST))
  {
    // Done, so redirect the user back to the edit view.
    header("location: /workexperience/employer/admin.php?placement_attended_id={$_GET['placement_attended_id']}");
  }
}


?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Work experience app</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="../css/bootstrap-theme.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="../css/theme.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

    <!--<script src="js/validatereview.js"></script>-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

    <!-- Fixed navbar -->
    <!--<nav class="navbar navbar-inverse navbar-fixed-top">-->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
           <a class="navbar-brand" href="/workexperience/">Work experience app</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <!--<li class="active"><a href="#">Home</a></li>-->
            <!--<li><a href="#about">About</a></li>-->
            <!--<li><a href="#contact">Contact</a></li>-->
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav><!-- end fixed navbar -->


  <div class="container-fluid theme-showcase" role="main">
    <div class="row"><!--Start of row -->
        <div class="col-md-12"><!-- content main full page-->
          <h2>Employer review section.</h2>
          <p>The completed review will be shown to the learner once completed.</p>  
          </div>
        </div>
      </div>

  <!-- Jumbatron section showing the details of who they are reviewing-->
  <div class="container-fluid theme-showcase" role="main">
    <div class="jumbotron">
      <h3>Student name:  <?=$jumboDetails['student_name']?></h3> 
        <p>Placement: <?=$jumboDetails['placement']?></p>
         <p>Address: <?=$jumboDetails['placement_address']?>,<?=$jumboDetails['post_code']?> </p>
          <p>Agreement signed: <?=$jumboDetails['agreement_signed_date']?></p>
          <hr/>
          <p>Coach name: <?=$jumboDetails['coach_name']?></p>
          <p>PAID <?=$jumboDetails['placement_attended_id']?></p>
        
    </div>
    <h3>Edit the review for <?=$jumboDetails['student_name']?> here:</h3>

    <p>Below you can review the learner detailed above who has been on placement with your organisation. Once the review has been completed the learner will be able to see your remarks and review of their performance.</p> 
  </div>
  <!-- End Jumbatron section showing the details of who they are reviewing-->

  <div class="container-fluid theme-showcase" role="main">
      <div class="row"><!--Start of row -->
        <div class="col-md-12"><!-- content main full page-->      
         
         <!-- If the review has been completed, disable the form -->

          <fieldset>
            <form method='POST'>
                 <div class="table-responsive">          
                    <table class="table table-bordered table-striped">
                      <thead>
                        <tr>
                          <th></th>
                          <th>Excellent</th>
                          <th>Good</th>
                          <th>Satisfactory</th>
                          <th>Requires improvement</th>   
                        </tr>
                      </thead>
                      <tbody>
                          <?php $x=0;?>
                          <?php foreach($employerTableFields AS $heading):?>
                            <tr>
                                <td>
                                  <input type="hidden" name="review[<?=$x?>]['row_id']" value="<?=$employerReview[$x]['id']?>"/>
                                  <input type="hidden" name="review[<?=$x?>]['heading']" value="<?=$heading?>"/>
                                  <input type="hidden" name="placement_attended_id" value="<?=$_GET['placement_attended_id']?>"/>
                                <?=$heading?>
                                </td>

                                <td>
                                 <input type="radio" name="review[<?=$x?>]['score']" value="excellent" <?php if($employerReview[$x]['excellent']=='1') echo 'checked';?>/>
                                </td>

                                <td>
                                 <input type="radio" name="review[<?=$x?>]['score']" value="good" <?php if($employerReview[$x]['good']=='1') echo 'checked';?>/>
                                </td>

                                 <td>
                                  <input type="radio" name="review[<?=$x?>]['score']" value='satisfactory' <?php if($employerReview[$x]['satisfactory']=='1') echo 'checked';?> />
                                </td>

                                <td>
                                  <input type="radio" name="review[<?=$x?>]['score']" value="requires_improvement" autofocus required="required" <?php if($employerReview[$x]['requires_improvement']=='1') echo 'checked';?>/>
                                </td>
                               
                                
                              
                            </tr>
                            <?php ++$x;?>
                            <?php endforeach;?>
                            <tr>
                              <td colspan=5>
                              <p>Career coach review:</p>
                                <textarea required class='textarea_review' placeholder='Please give a little more information on why you have made this choice' name="review['comments']"><?=$employerReview[0]['comments']?></textarea>
                              </td>
                            </tr>
                        </tbody>
                      </table>

                       <p><input type="checkbox" name="confirm_hours" value="1"
                        <?php if($employerReview[0]['confirm_hours']=='1') echo 'checked';?>/> I can confirm these hours are correct as per the students logbook
                      </p>
                    
                    </div> <!-- table-responsive ends-->
                    
                    <!-- Once the review edit is completed, the review can be saved here -->
                    <p><button type='submit' class="btn btn-primary" name='update_employer_review'>Update</button></p>

                  </form>
                  </fieldset>

      
        </div> <!-- col ends-->  
      </div><!-- row ends-->

    </div> <!--end main container -->

    <!-- footer here -->
    <footer class="footer">
    <hr/>
      <div class="container">
        <p style="text-align:center;">Bolton College 2017</p>
      </div>
    </footer>
    <!-- end footer end -->

    <!-- Bootstrap core JavaScript ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
