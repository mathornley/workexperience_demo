<?php

/**
 * Get the PSR4 autoloader and load all the classes required.
 * This page handles the validation to the updated review with PHP in the class (editReview()) and then finally Javascript
 */
spl_autoload_register(function ($class_name){
    include '../classes/'.$class_name . '.php';
});

// error reporting on.
ini_set('display_errors', 'Off');

// Stop the cache
require_once '../stop_cache.php';

// Instantiate a new instance of the employer review and send the placement_id (not placement_attended_id). 
$employer = new EmployerReview($_GET['placement_id']);

// Set a logout timer so the page will redirect to the sign in area >=30 minutes.
$login = new Login;

$timeout = $login->checkTimeOut($_GET['placement_id']);

if($login->checkSession()==false)
{
  header('location: /workexperience/employer/');
}

// Get all the learners details for this placement_id, from we_placement_attended - the instances of it.
// We'll list the instances in the the table below.  
$placement_attended_instances = $employer->getReviewList();
//echo '<pre>';print_r($placement_attended_instances);die;
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Work experience app</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="../css/bootstrap-theme.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="../css/theme.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

    <!--<script src="js/validatereview.js"></script>-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
      /* Override the bootstrap styles for the table element*/
      th{max-width:20%;}
      td{width:70%;}

      /* This is the Bolton College logo*/
      img {
        height: 200px;
      }
    </style>

  </head>
  <body>

    <!-- Fixed navbar -->
    <!--<nav class="navbar navbar-inverse navbar-fixed-top">-->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
           <a class="navbar-brand" href="/workexperience">Work experience app</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <!--<li class="active"><a href="#">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li>-->
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav><!-- end fixed navbar -->

    <div class="container-fluid theme-showcase" role="main">
      <div class="row"><!--Start of row -->
      <div class="col-md-12">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#">Employer review section for <?=$employer->getPlacement($placement_attended_instances[0]['placement_id'])?></a></li>
            </ul>
        <br/>
      </div><!-- end tabbed content left-->
    </div><!--End of row -->

     <img src='../images/BClogo.png'/>
  
      <div class="row"><!--Start of row -->

        <div class="col-md-12"><!-- content main full page-->
          <h3>Employer admin panel: </h3>
          <p>Below is a list of students that are on, or have been on placements within your organisation. From the list you can choose the student and complete their work placement review. You can also after selecting a student, edit their review if this has already been completed.</p>

            <?php if(isset($placement_attended_instances[0])):?>

              <?php for($i=0;isset($placement_attended_instances[$i]);$i++):?>


                 <?php 
                 // This will filter out the social action projects
                 //if(trim(strtolower($placement_attended_instances[$i]['placement_code'])=='social')) continue;?>
                  
                  <div class="table-responsive">   
                    <table class="table table-bordered table-striped">
                        <tr>
                          <th>Learner name</th>
                          <td><?=$employer->getLearner($placement_attended_instances[$i]['learner_id']);?> 
                            <a href='/workexperience/employer/admin.php?placement_attended_id=<?=$placement_attended_instances[$i]['id']?>'>[Review]</a></td>
                        </tr>
                        <tr>
                          <th>Placement</th>
                          <td><?=$employer->getPlacement($placement_attended_instances[$i]['placement_id']);?></td>
                        </tr>
                        <tr>
                          <th>Coach</th>
                          <td><?=$employer->getCoach($placement_attended_instances[$i]['coach_id']);?></td>
                        </tr>
                        <tr>
                          <th>Date</th>
                          <td><?=date('l jS \of F Y H:i a',strtotime($placement_attended_instances[$i]['agreement_signed_date']));?></td>
                        </tr>
                         <tr>
                          <th>Review Completed</th>
                          <td>
                            <?=($employer->employerReviewCompleted($placement_attended_instances[$i]['id'])==true) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>';?>
                          </td>
                        </tr>
                    </table>
                  </div> <!-- table-responsive ends-->

          <?php endfor;?>

          <?php endif;?>
         
         <?php if(!isset($placement_attended_instances[0])):?>
          <h4>Currently there are no students for review.</h4>
       <?php endif;?>

        </div> <!-- col ends-->  
      </div><!-- row ends-->

    </div> <!--end main container -->

    <!-- footer here -->
    <footer class="footer">
    <hr/>
      <div class="container">
        <p style="text-align:center;">Bolton College 2017</p>
      </div>
    </footer>
    <!-- end footer end -->

    <!-- Bootstrap core JavaScript ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
