<?php

/**
 * Autoloader for including all the classes.
 */
spl_autoload_register(function ($class_name) {
    include $class_name . '.php';
});

/**
 * A class to be associated with the learner details and associated functionality. 
 */
class Learner extends Main
{
	public function __construct($placement_attended_id=null,$stuID=null)
	{	
		try
		{
			// Call the parent first and instantiate the database SQL class instance.
			parent::__construct();

			//$coach = new CareerCoachReview;
			
			if(isset($placement_attended_id) && isset($stuID))
			{
				$this->placement_attended_id = $placement_attended_id;
				$this->stuID = $stuID;
			}
		}

		// Catch some PDO errors.
		catch (PDOException $e)
		{
			throw new Exception('PDO Database connection failed to connect to SID failed in line: ' .__LINE__ . ' of file: '. __FILE__ . $e->getMessage());
		}
	}

	/**
	 * A functon to get the learner details row and get the placement id and coach id for that learner.
	 * @return [array] The row of learner and placement details from the learner table. 
	 */
	public function learnerDetails($stuID=null,$placement_attended_id=null)
	{	
		// This is for the instance. Otherwise, it is the intial user from Shibboleth.
		if(isset($stuID)) $this->stuID = $stuID; 

		// This is for the map instance
		if(isset($stuID) && isset($placement_attended_id))
		{
			return $this->conn->select(

			'SELECT *,
				CONCAT(forename," ",surname) AS name,
					person_code AS student_id
			FROM  `ext_a_learner` `learner`
				LEFT OUTER JOIN `we_placement_attended` `placement` ON  `learner`.`person_code` =  `placement`.`learner_id` 
						WHERE `person_code` =? AND `id`=?',[$this->stuID,$placement_attended_id]
			);

		}
	
		return $this->conn->select(

			'SELECT *,
				CONCAT(forename," ",surname) AS name,
					person_code AS student_id
			FROM  `ext_a_learner` `learner`
				LEFT OUTER JOIN `we_placement_attended` `placement` ON  `learner`.`person_code` =  `placement`.`learner_id` 
						WHERE `person_code` =?',[$this->stuID]
			);
	}

	/**
	 * Get the learner id form the placement attended id table, using the id. 
	 * @param  [string] $paid [The placement attended id from the `we_placement_attended` table]
	 * @return [string] [This will be the learner id]
	 */
	public function getLearnerId($paid)
    {
    	return $this->conn->select('SELECT `learner_id` as `learner` FROM  `we_placement_attended` WHERE `id` =?',
    		[
    			$paid
    		]
		);
    }

	/**
	 * A function to get the work coach of the person based on the student id. 
	 * placement_attended is queried first and then the placement table for the coach details. 
	 * @return array Returns an array of the details for the work coach.
	 */
	public function learnerWorkCoach($stuID=null)
	{
		if(isset($stuID)) $this->stuID = $stuID; 

		// Use the student id to query the we_placement_attended table and get the coach id for the placement
		$placement_attended = $this->conn->select('SELECT * FROM  `we_placement_attended` WHERE `learner_id` =?',[$this->stuID]);

		// Query the coach details on the coach id.
		return $this->conn->select('SELECT * FROM  `we_coach` WHERE `id`=?',[$placement_attended[0]['coach_id']]);

	}

	/**
	 * Get the learner details from their learner ID.
	 * @param  [string] $learnerID [String which is the learner ID]
	 * @return [string] [The learner full name]
	 */
	public function learnerDetailsByID($learnerID)
	{	
		$result = $this->conn->select(

			'SELECT 
				CONCAT(forename," ",surname) AS `full_name`
				FROM  `ext_a_learner`
					WHERE `person_code` =?',[$learnerID]
			);

		// Get the name and return it as a string
		return $name = $result[0]['full_name'];

	}

	/**
	 * Get the coach details form the coachID being passed in.
	 * @param  [string] The ID of the coach we want the details for.
	 * @return array An array of the coach details returned
	 */
	public function learnerWorkCoachByCoachID($coachID)
	{
		// Use the coach id to query the we_coach table and get the coach details for the id
		return $this->conn->select('SELECT * FROM  `we_coach` WHERE `id` =?',[$coachID]);
	}


























	/**
	 * Quick function to test if this placement attended id placement has a withdrawn learner.
	 * @param  [string]  $placement_attended_id [This is the placement attended id]
	 * @return boolean [Returns true or false depnding on if there is a withdrawal or not]
	 */
	public function isLearnerWithdrawnOnThisPlacement($placement_attended_id)
	{
		//echo $placement_attended_id;die;
		$result = $this->conn->select('SELECT * FROM  `we_student_withdrawals` WHERE `placement_attended_id` =?',[$placement_attended_id]);

		// There is a row, so the student must have been withdrawan. 
		if($result!=false) return true;
		// No row, so the user isn't withdrawn.
		else return false;
	}

	/**
	* Get all the details of a withdrawed learner back. 
	* @param  [string]  $placement_attended_id [This is the placement attended id]
	* @return boolean [Returns true or false depnding on if there is a review completed or not]
	*/
	public function getUSerWithdrawalInfo($placement_attended_id)
	{
		$result = $this->conn->select('SELECT * FROM  `we_student_withdrawals` WHERE `placement_attended_id` =?',[$placement_attended_id]);

		// There is a row, so the student must have been withdrawn, so return the details. 
		if($result!=false) return $result;
		// No row, so the user isn't withdrawn.
		else return false;
	}









	public function hasUserHadCoachReview($placement_attended_id)
	{
		$result = $this->conn->select('SELECT * FROM  `we_career_coach_review` WHERE `placement_attended_id` =?',[$placement_attended_id]);

		// There is a row, so the student must have been withdrawn, so return the details. 
		if($result!=false) return true;
		// No row, so the user isn't withdrawn.
		else return false;
	}

	public function getUSerCoachReview($placement_attended_id)
	{
		$result = $this->conn->select('SELECT * FROM  `we_career_coach_review` WHERE `placement_attended_id` =?',[$placement_attended_id]);

		// There is a row, so the student must have been withdrawn, so return the details. 
		if($result!=false) return $result;
		// No row, so the user isn't withdrawn.
		else return false;
	}














	/**
	 * Quick function to test if this placement attended id placement has a career coach review done on the learner.
	 * @param  [string]  $placement_attended_id [This is the placement attended id]
	 * @return boolean [Returns true or false depnding on if there is a review or not]
	 */
	public function isLearnerReviewOnThisPlacement($placement_attended_id)
	{
		//echo $placement_attended_id;die;
		$result = $this->conn->select('SELECT * FROM  `we_career_coach_review` WHERE `placement_attended_id` =?',[$placement_attended_id]);

		// There is a row, so the student must have been withdrawan. 
		if($result!=false) return true;
		// No row, so the user isn't withdrawn.
		else return false;
	}

	/**
	* Get all the details of a career coach review for the learner back. 
	* @param  [string]  $placement_attended_id [This is the placement attended id]
	* @return boolean [Returns true or false depnding on if there is a review completed or not]
	*/
	public function getUSerReviewInfo($placement_attended_id)
	{
		$result = $this->conn->select('SELECT * FROM  `we_career_coach_review` WHERE `placement_attended_id` =?',[$placement_attended_id]);

		// There is a row, so the student must have been withdrawn, so return the details. 
		if($result!=false) return $result;
		// No row, so the user isn't withdrawn.
		else return false;
	}





























	/**
	 * Initially this is a stock, personal message from the `we_placement_attended table`, which the learner can see and the coach can update. All stock messages initially are the same, then the coach can change this. 
	 * Get the learner information for THIS instance of the placement - same person at the same place at a different time would have an alternative instance message. This message is not specific to the placement - it is specific to that learner placement instance.
	 * @param  [string] $placement_attended_id This is the placement_attended_id passed in when the location page loads.
	 * @return [string] This will be the custom message set for the learner. Trimmed and returned as a string.
	 */
	public function getLearnerInfo($placement_attended_id)
	{
		// Use the student id to query the we_placement_attended table and get the coach id for the placement
		$information = $this->conn->select('SELECT `information` FROM  `we_placement_attended` WHERE `id` =?',[$placement_attended_id]);

		// Return the personalised message trimmed to the location map page.
		return trim($information[0]['information']);
	}

	public function updateLearnerInfo($POST)
	{
		// Use the student id to query the we_placement_attended table and get the coach id for the placement
		$update = $this->conn->update('UPDATE `we_placement_attended` SET `information`=? WHERE `id` =?',[$POST['information'],$POST['id']]);

		if($update!=false) return true; // Return true if the update worked.
		if($update==false) return false; // Return false if the update failed. 
	}

	/**
	 * For the coach view homepage (index.php) get all the chosen learner placements.
	 * @param  string $studID This is the student id passed in.
	 * @return array or boolean false on failure. The array of placements for the learner is returned if successful. 
	 * */
	public function getLearnerPlacements($studID)
	{
		$query = "SELECT *, pa.`id` as `paid` FROM `we_placement_attended` pa LEFT OUTER JOIN `ext_a_learner` `learner` ON `pa`.`learner_id`=`learner`.`person_code` LEFT OUTER JOIN `we_placement` pl ON `pl`.`id`=`pa`.`placement_id`  WHERE `pa`.`learner_id`=?";

		return $this->conn->select($query,[$studID]);

	}

	/**
	 * Function to add the midpoint review of the learner from the coach view area of the CRM.
	 * This has a flag showing the coach has done this on the others behalf. 
	 * @param  [array] $POST [These are the posted form variables]
	 * @return [type] [description]
	 */
	public function submitMidpointReview($POST)
	{
		//	Firstly, make sure this row isn't being added again by someone refreshing the screen.
		$query = 'SELECT * FROM `we_midpoint_assessment` WHERE `placement_attended_id`=?';

		// Query the row via the placement attended id, which is unique and there should be 1 only.
		$result = $this->conn->select(
			$query,
			[
				$POST['paid']
			]
		); 
		
		// If a dupe, return NULL. 	
		if($result!=false) return NULL; // If this is a duplicate record, return NULL


		if((isset($POST['coach_signed']) && $POST['coach_signed']=='1') &&
		(isset($POST['employer_signed']) && $POST['employer_signed']) && 
		(isset($POST['learner_signed']) && $POST['learner_signed']=='1'))
		{
			$signed_with_coach = 1;
		}

		elseif((isset($POST['coach_signed']) && $POST['coach_signed']=='1') &&
		(isset($POST['employer_signed']) && $POST['employer_signed']))
		{
			$signed_with_coach = 1;
		}

		elseif((isset($POST['coach_signed']) && $POST['coach_signed']=='1') &&
		(isset($POST['learner_signed']) && $POST['learner_signed']))
		{
			$signed_with_coach = 1;
		}

		else
		{
			$signed_with_coach = 0;
		}


		$coachArray = $this->conn->select("SELECT `coach_id` FROM `we_placement_attended` WHERE `id`=?", [$POST['paid']]);

		$coachId = $coachArray[0]['coach_id'];
		$paid = $POST['paid'];
		$coachSigned = (isset($POST['coach_signed']) && $POST['coach_signed']=='1') ? '1' : '0'; 
		$employerSigned = (isset($POST['employer_signed']) && $POST['employer_signed']=='1') ? '1' : '0'; 
		$learnerSigned = (isset($POST['learner_signed']) && $POST['learner_signed']=='1') ? '1' : '0'; 
		$comments = $POST['comments'];

		// The initial review details to update
		$behaviour = $POST['behaviour'];
		$quality = $POST['quality_of_support'];
		
		// Time stamp
		$date_created = date('Y-m-d H:i:s');
		
		// Add the placement to the table as a new row.
		$query = 'INSERT INTO `we_midpoint_assessment` (`coach_id`,`placement_attended_id`,`coach_signed`,`employer_signed`,`learner_signed`,`behaviour`,`quality_of_support`,`signed_with_coach`,`date_created`,`comments`) VALUES(?,?,?,?,?,?,?,?,?,?)';

		// Add the midpoint review for the learner to the `we_midpoint_assessement` table.
		$result = $this->conn->insert(
			$query,
			[
				$coachId,
				$paid,
				$coachSigned, 
				$employerSigned, 
				$learnerSigned, 
				$behaviour,
				$quality,
				$signed_with_coach,
				$date_created,
				$comments 
			]
		);

		// Return true or false if all ok or not.
		if($result!=FALSE) return true;
		else return false;
		
	}


	/**
	 * Update the midpoiunt review of the Mid Point review. 
	 * This has a flag showing the coach has done this.
	 * @param  [array] $POST [This is the information posted in from the form]
	 * @return [type] [description]
	 */
	public function updateMidpointReview($POST)
	{

		// This is the section of logic to work out if the coach has signed the agreement on behalf of themselves and either, the learner, or the employer. Either of these adds the `signed_with_coach` flag to 1 on the record in the table.
		if((isset($POST['coach_signed']) && $POST['coach_signed']=='1') &&
		(isset($POST['employer_signed']) && $POST['employer_signed']) && 
		(isset($POST['learner_signed']) && $POST['learner_signed']=='1'))
		{
			$signed_with_coach = 1;
		}

		// Coach and employer signed together
		elseif((isset($POST['coach_signed']) && $POST['coach_signed']=='1') &&
		(isset($POST['employer_signed']) && $POST['employer_signed']))
		{
			$signed_with_coach = 1;
		}

		// The coach and the learner signed.
		elseif((isset($POST['coach_signed']) && $POST['coach_signed']=='1') &&
		(isset($POST['learner_signed']) && $POST['learner_signed']))
		{
			$signed_with_coach = 1;
		}

		// The coach hasn't signed on behalf of anyone
		else
		{
			$signed_with_coach = 0;
		}

		// The row id of the row to be updated.
		$row_id = $POST['update_midpoint_review']; 

		/****THIS SECTION is TEMP depending on if it works. You can pull it and the rest will still work ***/

		// If this is an update, check the row id to see if the row already says someone signs themselves.
		if(isset($POST['update_midpoint_review']) && $POST['update_midpoint_review']=='1')
		{
			// Update the placement to the table as a new row.
			$query = 'SELECT * FROM `we_midpoint_assessment` WHERE id=?';

			// Query the row
			$result = $this->conn->select(
				$query,
				[
					$row_id
				]
			);

			if($result[0]['signed_with_coach']=='0' && ($result[0]['learner_signed_first']=='1' || $result[0]['employer_signed_first']=='1'))
			{
				$signed_with_coach = 0;
			}

			else
			{
				$signed_with_coach = 1;
			}

		}

		/**** THIS SECTION is TEMP depending on if it works. ***/

		// Get the coach id from the placement attended id.
		$coachArray = $this->conn->select("SELECT `coach_id` FROM `we_placement_attended` WHERE `id`=?", [$POST['paid']]);

		$coachId = $coachArray[0]['coach_id'];
		$paid = $POST['paid'];
		$coachSigned = (isset($POST['coach_signed']) && $POST['coach_signed']=='1') ? '1' : '0'; 
		$employerSigned = (isset($POST['employer_signed']) && $POST['employer_signed']=='1') ? '1' : '0'; 
		$learnerSigned = (isset($POST['learner_signed']) && $POST['learner_signed']=='1') ? '1' : '0'; 
		$comments = $POST['comments'];
		
		// The review details to update
		$behaviour = $POST['behaviour'];
		$quality = $POST['quality_of_support'];
		
		// Time stamp
		$date_updated = date('Y-m-d H:i:s');
		
		// Update the placement to the table as a new row.
		$query = 'UPDATE `we_midpoint_assessment` SET `coach_id`=?,`placement_attended_id`=?,`coach_signed`=?,`employer_signed`=?,`learner_signed`=?,`behaviour`=?,`quality_of_support`=?,`signed_with_coach`=?,`date_updated`=?,`comments`=? WHERE id=?';

		// Update the midpoint review for the learner to the `we_midpoint_assessement` table.
		$result = $this->conn->update(
			$query,
			[
				$coachId,
				$paid,
				$coachSigned, 
				$employerSigned, 
				$learnerSigned, 
				$behaviour,
				$quality,
				$signed_with_coach,
				$date_updated,
				$comments,
				$row_id
			]
		);

		// Return true or false if all ok or not.
		if($result!=FALSE) return true;
		else return false;
		
	}

	/**
	 * Don't forget this is an UPDATE operation, as the coach has to be complete their bit before the learner and employer so the SQL here is updating the row already created by the coach INSERT!!
	 * This is the section where the student agrees to the midpoint review completed by their coach.
	 * @param [array] $POST [This is the row id of the review and the placement attended if]
	 */
	public function addStudentMidpointReview($POST)
	{
		// The row id of the row to be updated.
		$paid = $POST['midpoint_review_agree']; 
		$row_id = $POST['agree_midpoint_button']; 

		// Time stamp
		$date_updated = date('Y-m-d H:i:s');
		
		// Update the placement to the table as a new row.
		$query = 'UPDATE `we_midpoint_assessment` SET `learner_signed`=?, `date_updated`=?, `learner_signed_first`=? WHERE `id`=? AND `placement_attended_id`=?';

		// Update the midpoint review for the learner to the `we_midpoint_assessement` table.
		$result = $this->conn->update(
			$query,
			[
				'1',
				$date_updated,
				'1',
				$row_id,
				$paid
			]
		);


		// Return true or false if all ok or not.
		if($result!=FALSE) return true;
		else return false;
		

	}

	/**
	 * This is the employr like the learner, agreeing with the midpoint review. 
	 * @param [array] $POST [This is the checkbox that is on the employer page saying they agree with the midpoint review]
	 */
	public function addEmployerMidpointReview($POST)
	{
		// The row id of the row to be updated.
		$paid = $POST['midpoint_review_agree_employer']; 
		$id = $POST['row_id']; 
	
		// Time stamp
		$date_updated = date('Y-m-d H:i:s');
		
		// Update the placement to the table as a new row.
		$query = 'UPDATE `we_midpoint_assessment` SET `employer_signed`=?,`employer_signed_first`=? WHERE `id`=? AND `placement_attended_id`=?';

		// Update the midpoint review for the learner to the `we_midpoint_assessement` table.
		$result = $this->conn->update(
			$query,
			[
				'1',
				'1',
				$id,
				$paid			]
		);

		// Return true or false if all ok or not.
		if($result!=FALSE) return true;
		else return false;
	}

	/**
	 * This is the coach id passed in to get the coach name back. 
	 * @param  [string] $coachID [This is the coach id]
	 * @return [string] [This is the coach name that is returned]
	 */
	public function getCoachName($coachID)
	{
		// Create the query to query the coqach table to get the coach details. 
		$query = "SELECT `name` from `we_coach` WHERE id=?";

		// Get the coach info if there is any, if not, this is not a coach.
		$coach =  $this->conn->select($query,[$coachID]); // $coach is false if fails. 

		// Get the coach name from the results and make this a string from array.
		$coachName = $coach[0]['name'];

		// return the coach name as a string.
		return $coachName;
	}

	/**
	 * Get the learners midpoint review for the placement instance - the placement attended id is used to query. 
	 * @param  [string] $placement_attended_id [This is the placement attended id stored with the row of data]
	 * @return [type] [description]
	 */
	public function getMidpointReview($placement_attended_id)
	{
		$query = "SELECT * FROM `we_midpoint_assessment` WHERE `placement_attended_id`=?";

		return $this->conn->select($query,[$placement_attended_id]);

	}

	/**
	 * This is a function to set the learner as ** NOT ** work ready within the `elp_workready2` table
	 * @param [string] $learner [This is the learner id]
	 */
	public function setLearnerNotWorkReady($learner)
	{
		$result = $this->conn->update(
			'UPDATE `elp_workready2` SET `Q1_TUTOR`=?,`Q2_TUTOR`=?,`Q3_TUTOR`=?,`Q4_TUTOR`=?,`Q5_TUTOR`=?,`Q6_TUTOR`=?,
			`Q1_LDM`=?,`Q2_LDM`=?,`Q3_LDM`=?,`Q4_LDM`=?,`Q5_LDM`=?,`Q6_LDM`=? WHERE `learner`=?',
			[
				0,0,0,0,0,0,0,0,0,0,0,0,$learner
			]
		);

		if($result!=false) return true;
		else return false;

	}

	/**
	 * This is a function to set the learner as work ready within the `elp_workready2` table
	 * @param [string] $learner [This is the learner id]
	 */
	public function setLearnerToBeWorkReady($learner)
	{
		$result = $this->conn->update(
			'UPDATE `elp_workready2` SET `Q1_TUTOR`=?,`Q2_TUTOR`=?,`Q3_TUTOR`=?,`Q4_TUTOR`=?,`Q5_TUTOR`=?,`Q6_TUTOR`=?,
			`Q1_LDM`=?,`Q2_LDM`=?,`Q3_LDM`=?,`Q4_LDM`=?,`Q5_LDM`=?,`Q6_LDM`=? WHERE `learner`=?',
			[
				1,1,1,1,1,1,1,1,1,1,1,1,$learner
			]
		);

		if($result!=false) return true;
		else return false;

	}

	/**
	 * Using the placement attended id, we can get the coach review from the coach review table.
	 * @param  [string] $placement_attended_id [This the placement attended id.]
	 * @return [string] [This is the coach review for the placement as a string.]
	 */
	public function getCoachReviewFromPaid($placement_attended_id)
	{
		return $this->conn->select('SELECT * FROM `we_career_coach_review` WHERE `placement_attended_id`=?',[$placement_attended_id]);
	}

	/**
	 * Simple check to see if the midpoint review is completed.
	 * Row of the `we_midpoint_assessment` table is checked to see if there is a completed row using the palcement_attended id.
	 * The learner, coach and employer must have signed for TRUE, otherwise false.
	 * @param  [string] $placement_attended_id [This is the placement attended id which is passed in]
	 * @return [Boolean] [Returns either TRUE or FALSE]
	 */
	public function midpointReviewCompleted($placement_attended_id)
	{
		$query = "SELECT * FROM `we_midpoint_assessment` WHERE `placement_attended_id`=?";

		// Pull the entire row to check all 3 have signed.
		$result =  $this->conn->select($query,[$placement_attended_id]);

		// See if they have ALL signed the review
		if($result[0]['learner_signed'] && $result[0]['coach_signed'] && $result[0]['employer_signed'] )
		{
			return true;
		}

		else
		{
			return false;
		}
	}

	/**
	 * Use the learner ID and get the learner placements using the learner id querying the placement attended table. 
	 * @param  [string] $learner [This is the learner id]
	 * @return [array] [This the result set of placements]
	 */
	public function getTheLearnerPlacements($learner)
	{
		$query = 'SELECT * FROM `we_placement_attended` WHERE `learner_id`=?';
		return $this->conn->select($query,[$learner]);
	}

	/**
	 * Destructor function to close the DB connection and such, at the very end. 
	 */
	public function __destruct()
	{
		$this->conn = null;
	}


}
