<?php

/**
 * Get the PSR4 autoloader and load all the classes required.
 */
spl_autoload_register(function ($class_name) {
    include 'classes/'.$class_name . '.php';
});

// Stop the cache
require_once 'stop_cache.php';

// Set a new instance of the learner.
$learner = new Learner($_GET['placement_attended_id'],$_GET['studID']);

// Gets the learner details row and the placement details for that learner.
$learnerDetails = $learner->learnerDetails($_GET['studID'],$_GET['placement_attended_id']);

// We'll use Main class and get all the files in the application to populate the list of files for the select below.
$files = $learner->getFilesFromDirectoryForDebug('../workexperience', 'php');

// Something has been posted to the debug form. We're using the Main.ph class for this. 
if(isset($_POST) && $_POST['report_bug'])
{
  $bugLogged = $learner->logBug($_POST);
}

// Get an instance of the career coach review.
$coach = new CareerCoachReview;

/**
 * Check if this is a learner only viewing their own part of the app - learner ids have to match.
 * The Shib learner id has to match the URL from the browser, IF they're not a coach. 
 * As usual the coach sees it all.
 * If the user is not authorised, $_SERVER['REQUEST_URI'] is used to keep them on their page.
 */
if(!$coach->isCareerCoach() && $coach->doesTheLearnerIdMatch($_GET['studID'])==false)
{
  // The user is not a coach and not the learner requesting the URL so keep them where they are.
  header($_SERVER['REQUEST_URI']);
}
?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Work experience app</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/theme.css" rel="stylesheet">

     <!-- Some jQuery to handle the buttons and divs for the forms-->
     <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="crossorigin="anonymous"></script>

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <!-- Fixed navbar -->
    <!--<nav class="navbar navbar-inverse navbar-fixed-top">-->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/workexperience/">Work experience app</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
           <li><a href="index.php">Choose placement</a></li>
               <li><a href="introduction.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Introduction</a></li>
              <li><a href="location.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Where is my placement</a></li>
              <li><a href="agreement.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Work placement agreement</a></li>
              <li><a href="aims.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Aims and objectives</a></li>
              <li><a href="logbook.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Logbook</a></li>
              <li><a href="placement_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Your work placement review</a></li>
              <li><a href="employer_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Employer review</a></li>
              <li><a href="coach_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Career coach review</a></li>
               <li class="active"><a href="debug.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Report a bug</a>
              </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav><!-- end fixed navbar -->

    <div class="container-fluid theme-showcase" role="main">
    	<div class="row"><!--Start of row -->
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-bordered">
		    			<thead>
		      				<tr>
						        <th>Student name</th>
						        <td><?=$learnerDetails[0]['name']?></td>
						        <th>Student ID</th>
						        <td><?=$learnerDetails[0]['person_code']?></td>
		      				</tr>
		    			</thead>	
	  				</table>
				</div>	
			</div>
		</div> <!--End of row -->

     <div class="row" id='test'><!--Start of row -->
        <!-- Tabbed content left-->
        <div class="col-md-12">
            <ul class="nav nav-tabs">
              <li><a href="index.php">Choose placement</a></li>
               <li><a href="introduction.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Introduction</a></li>
              <li><a href="location.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Where is my placement</a></li>
              <li><a href="agreement.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Work placement agreement</a></li>
              <li><a href="aims.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Aims and objectives</a></li>
              <li><a href="logbook.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Logbook</a></li>
              <li><a href="placement_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Your work placement review</a></li>
              <li><a href="employer_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Employer review</a></li>
              <li><a href="coach_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Career coach review</a></li>
               <li class="active"><a href="debug.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Report a bug</a>
              </li>
            </ul>
        <br/>
      </div><!-- end tabbed content left-->
    </div><!--End of row -->

		<div class="row"><!--Start of row -->
	    	<!-- Tabbed content left-->
		    <div class="col-md-12">

          <?php if(isset($bugLogged) && $bugLogged!=false):?>
            <script>
              $(document).ready(function(){
              $("#success").show().delay(5000).fadeOut();
            });
            </script>
          <div class="alert alert-success" id='success'>
            <strong>Thank you</strong> for taking the time to report the issue. Someone will look in to this. 
          </div>
          <?php endif;?>

          <h3>Report a bug or issue with the work experience application.</h3>
          <p> Below you can report an issue or bug to the development team and let them know of any issues you are experiencing.</p>

          <!--Report a bug issue form starts here-->
          <form method='POST'>
            <fieldset>
              <div class="form-group">
                <label for="exampleInputEmail1">Username or Student ID</label>
                <input readonly="readonly" type="text" class="form-control" id="person_code" name='person_code' aria-describedby="emailHelp" 

                <?php if(isset($learnerDetails[0]['person_code'])):?>
                  value="<?=$learnerDetails[0]['person_code']?> | <?=ucfirst($learnerDetails[0]['forename']).' '.ucfirst($learnerDetails[0]['surname']);?>"/>
                  <small id="emailHelp" class="form-text text-muted">This will be prepopulated so you're not required to do anything.</small>

                <?php else:?>
                   value="<?=$_SERVER['sAMAccountName']?>"/>
                    <small id="emailHelp" class="form-text text-muted">This will be prepopulated so you're not required to do anything.</small>
                <?php endif;?>
              </div>

              <div class="form-group">
                <label for="exampleSelect1">The section of the application that has the issue. Please select all.</label>
                <select class="form-control" id="filename" name="filename" required>
                  <option value=''>Please select</option>
                  <?php foreach($files AS $file):?>
                    <option value='<?=$file?>'><?=$file?></option>
                  <?php endforeach;?>
                </select>
              </div>
              <div class="form-group">
                <label for="exampleSelect2">If multiple pages are experiencing issues</label>
                <select multiple class="form-control" id="files" name='files[]'>
                  <?php foreach($files AS $file):?>
                    <option><?=$file?></option>
                  <?php endforeach;?>
                </select>
              </div>
              <div class="form-group">
                <label for="exampleTextarea">Describe specifically what issues you are experiencing</label>
                <textarea required class="form-control" id="nature_of_issue" name='nature_of_issue' rows="7"></textarea>
              </div>
              <button type="submit" class="btn btn-primary" name='report_bug' value='1'>Report</button>
            </fieldset>
          </form>
          <!--Report a bug issue form ends here-->

		    </div><!-- col ends-->
      </div> <!--end row -->
  	</div> <!--end main container -->

    <!-- footer here -->
    <footer class="footer">
    <hr/>
    	<div class="container">
        	<p style="text-align:center;">Bolton College 2017</p>
      	</div>
    </footer>
    <!-- end footer end -->

    <!-- Bootstrap core JavaScript ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
