<?php

/**
 * Get the PSR4 autoloader and load all the classes required.
 */
spl_autoload_register(function ($class_name) {
    include 'classes/'.$class_name . '.php';
});

// Stop the cache
require_once 'stop_cache.php';

// Get a new coach instance
$coach = new CareerCoachReview();

/**
 * Check if this is a learner only viewing their own part of the app - learner ids have to match.
 * The Shib learner id has to match the URL from the browser, IF they're not a coach. 
 * As usual the coach sees it all.
 * If the user is not authorised, $_SERVER['REQUEST_URI'] is used to keep them on their page.
 */
if(!$coach->isCareerCoach() && $coach->doesTheLearnerIdMatch($_GET['studID'])==false)
{
  // The user is not a coach and not the learner requesting the URL so keep them where they are.
  header('location: /workexperience/');
}

// Get the learner instance.
$learner = new Learner($_GET['placement_attended_id'],$_GET['studID']);
// Gets the learner details row and the placement details for that learner.
$learnerDetails = $learner->learnerDetails($_GET['studID']);

// Get a new instance of placement attended.
$placement = new PlacementAttended($_GET['studID']);

// Check the work placement agreement is signed. 
$agreement_signed = $placement->agreementSigned($_GET['studID'],$_GET['placement_attended_id']);

// Is the agreement signed
if(isset($_POST['agree_wpa']))
{
    // Alter the learners row in the placement_attendance table to show the agreement has been signed. 
    $placement->signAgreement($_POST);
}

// Decide if the midpoint review should be shown - if 50 days, yes, otherwise, no.
$showMidPointReview = $learner->showMidPointReview($_GET['placement_attended_id']);
//var_dump($showMidPointReview);die;
?>


<script type="text/javascript">
var studID = "<?php echo $_GET['studID'];?>";
//alert(studID);
</script>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>My Workspace app</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/theme.css" rel="stylesheet">

    <!--<link rel="stylesheet" type="text/css" href="css/ada.css"/>-->

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

    
    <!-- Bootstrap 3 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Font Awesome 5 -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="crossorigin="anonymous"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script>
    // Some validation on the form to ensure the learner reads and ticks all options. 
    function validate()
    {
      if(document.getElementById('agree_wpa').checked==true)
        {
          document.getElementById('agree_wpa').checked=true
          return true;
        }

        if(document.getElementById('agree_wpa').checked==false)
        {
          alert('You need to agree to the conditions in order to proceed.');
          return false;
        }
    }
    </script>

  </head>
  <body>

  <!-- Fixed navbar -->
  <!--<nav class="navbar navbar-inverse navbar-fixed-top">-->
  <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/workexperience/">My Workspace app</a>
      </div>
      <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
         <li><a href="index.php">Choose placement</a></li>
               <li><a href="introduction.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Introduction</a></li>
              <li><a href="location.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Where is my placement</a></li>
              <li class="active"><a href="agreement.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Work placement agreement</a></li>
              <li><a href="aims.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Aims and objectives</a></li>
              <li><a href="logbook.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Logbook</a></li>
              <li><a href="placement_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Your work placement review</a></li>
              <li><a href="employer_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Employer review</a></li>
              <li><a href="coach_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Coach review</a>
            </li>
            <?php if($showMidPointReview):?>
                <li><a href="midpoint_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Your midpoint review</a>
                </li>
             <?php endif;?>
          <!--<li><a href="#about">About</a></li>-->
          <!--<li><a href="#contact">Contact</a></li>-->
        </ul>
      </div><!--/.nav-collapse -->
    </div>
  </nav><!-- end fixed navbar -->

    <div class="container-fluid theme-showcase" role="main">
      <div class="row"><!--Start of row -->
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-bordered">
              <thead>
                  <tr>
                    <th>Student name</th>
                    <td><?=$learnerDetails[0]['name']?></td>
                    <th>Student ID</th>
                    <td><?=$learnerDetails[0]['person_code']?></td>
                    <th>Coach name</th>
                    <td><?=$coach->getCoachNameForLearner($_GET['placement_attended_id'])?></td>
                  </tr>
              </thead>   
            </table>
        </div>  
      </div>
    </div> <!--End of row -->

   <div class="row" id='test'><!--Start of row -->
        <!-- Tabbed content left-->
        <div class="col-md-12">
             <ul class="nav nav-tabs">
              <li><a href="index.php">Choose placement</a></li>
               <li><a href="introduction.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Introduction</a></li>
              <li><a href="location.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Where is my placement</a></li>
              <li class="active"><a href="agreement.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Work placement agreement</a></li>
              <li><a href="aims.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Aims and objectives</a></li>
              <li><a href="logbook.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Logbook</a></li>
              <li><a href="placement_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Your work placement review</a></li>
              <li><a href="employer_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Employer review</a></li>
              <li><a href="coach_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Coach review</a>
            </li>
            <?php if($showMidPointReview):?>
                <li><a href="midpoint_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Your midpoint review</a>
                </li>
             <?php endif;?>
            </ul>
        <br/>
      </div><!-- end tabbed content left-->
    </div><!--End of row -->
  
      <div class="row"><!--Start of row -->
        <!-- content left-->
  
        <!-- content right-->
        <div class="col-md-12"> <!--Start of row -->
          <h3>Work placement agreement</h3>

          <p>The Learner will be responsible for:</p>
          
          <ul>
            <li>Attending the Work Placement on the agreed days, times and duration.</li>
            <li>Take only the allotted time for lunch.</li>
            <li>If unable to attend due to illness or another reason, the learner must contact their Line Manager ahead of time.</li>
            <li>Maintaining high behaviour standards, and being courteous and respectful to other staff, students and members of the public during the placement. </li>
            <li>Regularly filling in their logbook to track their Work Placement activities and progression.</li>
            <li>Maintaining a positive attitude and making the most of the opportunity.</li>
            <li>Dressing appropriately for the employer’s work environment.</li>
            <li>Making arrangements for transport and lunch, factoring enough travel time to arrive at their  placement promptly.</li>
            <li>Abiding by all rules regarding health and safety, and other policies and procedures of the Employer. Reporting any accident or injury immediately and recording the details in the departmental accident/incident book.</li>
            <li>The quality of their work, maintaining confidentiality regarding any of the Employer’s information and not doing anything which may bring the Provider and/or the Employer into disrepute.</li>
            <li>Completing all assessments and attend any briefing sessions that are required as part of the placement.</li>
            <li>Notifying the Provider and the Employer in advance of any matter which is likely to affect their undertaking of the placement including any special health or medical requirements.</li>
            <li>Keeping the Provider informed of any changes, issues or incidents which arise in connection to the placement.</li>
            <li>Ensuring adequate motor insurance is arranged in advance of the placement should it be required for travel during working hours.</li>
            <li>Assigning any intellectual property (including, but not limited to, copyright, patents and registered designs) which is created by the Learner during the Work Placement to be owned by the Employer.</li>
        </ul>

            <form method='POST'>
              <div class="form-check">
                <label class="form-check-label">
                  <input type="checkbox" id="agree_wpa" name="agree_wpa" <?php if($agreement_signed==true || isset($_POST['agree_wpa'])) echo 'checked disabled';?> class="form-check-input">
                  I have read and agree with the work placement agreement.
                </label>
                <input type='hidden' name='studID' value="<?=$_GET['studID']?>"/>
                <input type='hidden' name='placement_attended_id' value="<?=$_GET['placement_attended_id']?>"/>
                <br/>
            </div>
            
            <button type="submit" id='agree_button' onclick="return validate();" class="btn btn-primary" <?php if($agreement_signed==true || isset($_POST['agree_wpa'])) echo 'checked disabled';?>>
                <?php if($agreement_signed==true){ echo 'Agreement signed';} else{ echo 'Sign the agreement';}?>
              </button>
            </form>
         </div><!-- end content right-->  
      </div>  <!-- end row -->
    </div> <!--end container-fluid -->

    <!-- footer here -->
    <footer class="footer">
    <hr/>
      <div class="container">
           <p style="text-align:center;">Bolton College <?=date('Y')?></p>
        </div>
    </footer>
    <!-- end footer end -->

    <!-- Bootstrap core JavaScript ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

     <?php 
   // Require the ada pop up box
   require_once('ADA_form.php');
   ?>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>

    <script defer src="../learner/adanew_dist/adabundle_v1.5.1.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
