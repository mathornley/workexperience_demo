<?php

/**
 * Get the PSR4 autoloader and load all the classes required.
 */
spl_autoload_register(function ($class_name) {
    include 'classes/'.$class_name . '.php';
});

/*
 * This page establishes who the person is, whether that be a learner or coach, anyone else is redirected to Google for testing.
 * This works off sibboleth and checks for the learner id or the username, if staff.
 * Coach is checked and if there are details, we know this is a coach.
 * Learner details are checked and if there some we know this is a coach
 */ 

// New instance of learner.
$learner = new Learner;

// New instance of coach.
$coach = new CareerCoachReview;

// New instance of placement.
$placement = new Placement;

// New instance of learner.
$learner = new Learner;

// // New instance of placement attended.
$placementAttended = new PlacementAttended;

// See if there are learner details
if($learnerDetails = $learner->learnerDetails()!=false)
{
  $learnerDetails = $learner->learnerDetails();
}


// See if there are coach details
if($coachDetails = $coach->coachDetails()!=false)
{
  // This is a coach. This is their details.
  $coachDetails = $coach->coachDetails();
  // As this is a coach, get all the learner details for the page below.
  $allLearnerPlacements = $coach->getPlacements();
}


// Something posted in the form on the page.
if(isset($_POST))
{
  // A learner wants to view a learner instance of placement.
  if(isset($_POST['learner_search']))
  {

    // Separate the firstname, surname and learner id and dump in array
    $array = explode(' ',$_POST['learner_search']); 

    unset($_POST['learner_search']); // Unset the form flag.

    // Get the learner number as this is the last element of the array.
    $studID = array_pop($array);

    //echo $studID;die;

    $learner_placements = $learner->getLearnerPlacements($studID);  
    //echo '<pre>';print_r($learner_placements);die;
    // Redirect to the first page of the app as the user to their chosen instance. 
    //header("location: /Workexperience/introduction.php?placement_attended_id=$placement_attended_id&studID=$studID");
  }

   // A learner wants to view a learner instance of their placement.
  if(isset($_POST['learner_instance_chosen']))
  {

    //echo '<pre>';print_r($_POST);die;
    unset($_POST['learner_instance_chosen_by_coach']); // Unset the form flag.

    $array = explode('_',$_POST['learner_instance_chosen']); // Separate the learner id and the placement_attended_id

    $placement_attended_id = $array[0];
    $studID = $array[1];

    // Redirect to the first page of the app as the coach can see all instances to their chosen instance. 
    header("location: introduction.php?placement_attended_id=$placement_attended_id&studID=$studID");
  }

  // A coach wants to view a learner instance of placement.
  if(isset($_POST['learner_instance_chosen_by_coach']))
  {
    unset($_POST['learner_instance_chosen_by_coach']); // Unset the form flag.

    $array = explode('_',$_POST['instance']); // Separate the learner id and the placement_attended_id

    $placement_attended_id = $array[0];
    $studID = $array[1];

    // Redirect to the first page of the app as the coach can see all instances to their chosen instance. 
    header("location: /workexperience/introduction.php?placement_attended_id=$placement_attended_id&studID=$studID");
  }
}
?>

<script>
// This will only work for a learner. 
var studID = <?php echo $_SERVER['sAMAccountName'];?>;
//alert(studID);
</script>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>My Workspace</title>

    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/themes/base/minified/jquery-ui.min.css" type="text/css" />

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/theme.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="css/ada.css" />

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

    <!-- Bootstrap 3 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Font Awesome 5 -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">

    <!-- JQuery -->
    <script
    src="https://code.jquery.com/jquery-1.12.4.js"
    integrity="sha256-Qw82+bXyGq6MydymqBxNPYTaUXXq7c8v3CwiYwLLNXU="
    crossorigin="anonymous"></script>

    <!-- Autocomplete stuff -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <style>

      /* Some custom styles for the page for HTML & body. */

      body{
        background:transparent!important;
        font-weight: normal;
      }

          /* Styles for the pages Jumbotron */
       .jumbotron_splash{
        background-color:#fff;
        opacity:0.8;
        padding:12px;
        box-shadow: 2px 2px 2px #000;
        border-radius:10px;
      }

      .jumbotron_splash form{
        margin:20px 25px;
      }
        /* Styles for the pages Jumbotron */

      html{
        background-color:transparent!important;
        min-height:100%;
        color:#000;
        font-weight:bolder;
        border:0px;
        background: url('images/image.png') no-repeat top center scroll;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
      }
      
      /*This is stop the ada box being bold*/
      #ADA-CONTAINER.OPEN {
        font-weight:normal;
      }

      input#autocomplete {
        width: 100%!important;
      }

      a {
          color: #ff0700;
          text-decoration: none;
          border-bottom: 1px dotted #ff0700;
      }

      a:hover {
          color: #ff0700;
          text-decoration: none;
          border-bottom:0px;
      }

      
    /* Some custom styles for the page for HTML & body. */
    
    </style>

    <!--Autcomplete listener here here -->
     <!--Autcomplete listener here here -->
    <script>
      $(function(){
        $('#autocomplete').autocomplete({
          source: "coach/search.php", //autocomplete here  - source is the PHP script: search.php
          minLength: 1 // Match for how many letters before the magic starts.
        }); 
      });
    </script>

    <!-- Brief styles -->
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <!-- Brief styles end --> 

  </head>

  <body>

    <!-- Fixed navbar -->
    <!--<nav class="navbar navbar-inverse navbar-fixed-top">-->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="/workexperience/">My Workspace</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
        </div><!--/.nav-collapse -->
      </div>
    </nav><!-- end fixed navbar -->

    <div class="container-fluid theme-showcase" role="main">
  
      <div class="row"><!--Start of row -->
        <div class="col-sm-6 col-md-6 col-lg-6"><!-- content main full page-->
        
          <div class="jumbotron_splash">
            <fieldset>
              <form method='POST'>

               <!-- ** Not a coach, not a learner and not authorised ** -->
                <?php if($learnerDetails==false && $placementAttended->checkLearnerPlacements()==false && $coach->coachDetails()==false):?>
                   <h3>Hi. Welcome to the My Workspace app</h3>
                   <p>You will need to contact the ILT team to get set up with access permissions to this application.</p>
                <?php endif;?>
                <!-- ** Not a coach, not a learner and not authorised ** -->

                <?php if($learnerDetails!==false && $placementAttended->checkLearnerPlacements()!==false ):?>
                  <h3>Hi <?=$learnerDetails[0]['forename']?>! Welcome to your My Workspace app.</h3>
              
                   <?php //echo '<pre>';print_r($learnerDetails);die;?>

                     <p>Please select the My Workspace record you want to view and update.</p>
                    
                      <?php for($i=0;isset($learnerDetails[$i]);$i++):?>
                        

                        <!-- If they have signed the WPA -->
                        <?php if($learnerDetails[$i]['agreement_signed_date']!=NULL):?>


                          <button class="btn btn-primary" name='learner_instance_chosen' value="<?=$learnerDetails[$i]['id']?>_<?=$learnerDetails[$i]['person_code']?>"><?=date('l jS \of F Y',strtotime($learnerDetails[$i]['agreement_signed_date']))?> | <?=$placement->getPlacementDetails($learnerDetails[$i]['placement_id'])?>
                          </button>

                           <!-- They need to sign their WPA -->
                          <?php else:?>

                             <button class="btn btn-primary" name='' value="<?=$learnerDetails[$i]['id']?>_<?=$learnerDetails[0]['person_code']?>"><span>You need to sign your <a href='/workexperience/agreement.php?placement_attended_id=<?=$learnerDetails[$i]['id']?>&studID=<?=$learnerDetails[$i]['learner_id']?>'>Work Placement Agreement first.</a>
                             </button>

                      <?php endif;?>

                      </p>
                      <?php endfor;?>

                    <!-- If not coach, hide this as we're not using select with the learner, as we're using buttons -->
                    <?php if($coachDetails = $coach->coachDetails()!=false):?>
                      <br/>
                      <p><button type="submit" name='learner_instance_chosen' class="btn btn-primary">Choose placement</button></p>
                    <?php endif;?>

                    <!-- If no learner placements for the learner -->
                    <?php else:?>
                      <?php if($learnerDetails!==false):?>
                        <h3>Hi <?=$learnerDetails[0]['forename']?>! Welcome to your My Workspace app.</h3>
                        <p>You don't currently have any placements to view. </p>

                      <?php endif;?>
                  <?php endif;?>
                  <!-- ** Is learner and has placements ** -->
 
                  <!--** The coach section ** -->
                  <?php if($coachDetails!=false && $placementAttended->getPlacements()!==false):?>
                  <h3>Hi <?php echo explode(' ',$coachDetails[0]['name'])[0];?>!</h3>
                   <p>The CRM section of the My Workspace app can now be found <a href='/workexperience/CRM/'>here</a>.</p>

                    <style>
                      input[type="search"]::-webkit-search-cancel-button{
                        -webkit-appearance: searchfield-cancel-button;
                      }
                    </style>
                   
                    <!-- Search all other learners, regardless of coach -->
                    <div class="row"><!--Start of row -->
                        <div class="col-md-8">
                        <h3>Search for any learner</h3>
                          <p>Search and manage any learner's My Workspace record.</p>
        
                        <form method='POST'>
                          <div class="inner-addon left-addon">
                            <i class="glyphicon glyphicon-user"></i>
                            <input type="search" class="form-control" name='learner_search' value='' placeholder='Enter the student details, firstname, lastname or the learner id. You can search for firstname, surname or student ID' pattern=".{12,}" required title="Something is wrong. Search again" id='autocomplete'/>
                          </div>
                        </form>
                      </div>
                  </div> <!-- End row -->

              <?php endif;?>
              <!--** The coach section ** -->

             </form>
            </fieldset>
          </div>
        </div><!-- row ends-->
      </div>


      <?php if(isset($learner_placements) && $learner_placements!=false):?>

      <script>
        $(document).ready(function(){
        $(".jumbotron_splash_results").show().delay(5000);
      });
      </script>

      <?php endif;?>

        <div class="jumbotron_splash_results">
          <div class="row"><!--Start of rown of results -->
            <div class="col-md-12">
              <h3> Placements results for: <?=ucfirst($learner_placements[0]['forename']).' '.ucfirst($learner_placements[0]['surname'])?> (<?=ucfirst($learner_placements[0]['person_code'])?>) </h3>
              <div class="table-responsive">
                <table class="table table-bordered">
                <?php for($i=0;isset($learner_placements[$i]);$i++):?>
                  <thead>
                    <tr>

                      <th>Placement</th>
                      <td><a href="introduction.php?placement_attended_id=<?=$learner_placements[$i]['paid']?>&studID=<?=$learner_placements[$i]['person_code']?>"><?=ucfirst($learner_placements[$i]['name'])?></a></td>

                      <th>Date</th>
                      <!-- Which type of placement? Industrial or something else -->
                      <?php if($learner_placements[$i]['placement_code']!='ZWRKX002'): // NOT industrial placement?>
                        <td>
                          <?=date('d/m/Y',strtotime($learner_placements[$i]['start_date']))?> - <?=date('d/m/Y',strtotime($learner_placements[$i]['end_date']));?>
                        </td>
                      <?php endif;?>
                      <?php if($learner_placements[$i]['placement_code']=='ZWRKX002'): // NOT industrial placement?>
                        <td><?=$learner->getDatesInd($learner_placements[$i]['paid'])?></td>
                      <?php endif;?>
                      <!-- End which type of placement? Industrial or something else -->

                      <!-- Which type of placement? Industrial or something else -->
                      <th>Coach</th>
                      <td><?=$coach->getCoachName($learner_placements[$i]['coach_id'])?></td>
                      <th>Academic Year</th>
                      <td><?=$learner_placements[$i]['acc_year']?></td>
                      
                      <!-- Only admin Mike sees this extra field -->
                      <?php if(strtolower($_SERVER['sAMAccountName'])==strtolower('miket')):?>
                      <th>Placement Type</th>
                        <td><?=$learner_placements[$i]['placement_code'] == 'ZWRKX002' ? 'Industrial' : ucfirst($learner_placements[$i]['placement_code'])?></td>
                      <?php endif;?>
                      <!-- Only admin Mike sees this extra field -->

                    </tr>
                </thead>
                <?php endfor;?>  
              </table>
          </div>  
        </div> <!-- row col-->
      </div><!-- row ends-->
    </div> <!-- ENd Jumbatron ends-->


    </div>
  </div>
</div> <!--end main container -->

    <?php if($coach->coachDetails()==false && $learnerDetails!==false ): // Include ADA here ?>
    <?php //require_once('ADA_form.php');?>
    <?php endif;?>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>

  </body>
</html>
