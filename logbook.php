<script>

  // Redirect if there is a user using IE. 

  var ua = window.navigator.userAgent;
  var msie = ua.indexOf('MSIE ');
  var trident = ua.indexOf('Trident/');
  var edge = ua.indexOf('Edge/');
  
  if (msie > 0){
    // IE 10 or older 
    window.location.href="/workexperience/notie.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>";
  }
  else if (trident > 0){
    // IE 11 
    window.location.href="/workexperience/notie.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>";
  }
  else if (edge > 0){
    // Edge 
     window.location.href="/workexperience/notie.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>";
  }
  else{
    // other browser
    //window.location.href="/CV/notie.php";
  }

  // Redirect if there is a user using IE ends. 

</script>

<?php

//print_r($_GET);die;

// Stop the cache
require_once 'stop_cache.php';

/**
 * Get the PSR4 autoloader and load all the classes required.
 */
spl_autoload_register(function ($class_name) {
    include 'classes/'.$class_name . '.php';
});

//ini_set('display_errors', 'On');

// Set up an instance of the logbook
$logbook = new Logbook;

// Set an instance of aims.
$aims = new Aims;

// New instance of coach comments for recording the coach comments
$coachComments = new CoachComments;

// Get an instance of the career coach review.
$coach = new CareerCoachReview;

/**
 * Check if this is a learner only viewing their own part of the app - learner ids have to match.
 * The Shib learner id has to match the URL from the browser, IF they're not a coach. 
 * As usual the coach sees it all.
 * If the user is not authorised, $_SERVER['REQUEST_URI'] is used to keep them on their page.
 */
if($coach->isCareerCoach()==false && $coach->doesTheLearnerIdMatch($_GET['studID'])==false)
{
  // The user is not a coach and not the learner requesting the URL so return them to the homepage.
  header('location: /workexperience/');
}

if($_GET)
{
    // Get the learners count of days and hours on placement
  $logbookTime = $logbook->logbookTime($_GET['studID'],$_GET['placement_attended_id']);
  // Get the learner details so we can populate the top section of the page with the students details. 
  $learner = new Learner;
  $learnerDetails = $learner->learnerDetails($_GET['studID'],$_GET['placement_attended_id']);

  // Instance of placement
  $placement = new PlacementAttended($_GET['studID']);

  // Check the work placement agreement is signed. 
  $agreement_signed = $placement->agreementSigned($_GET['studID'],$_GET['placement_attended_id']);

  // Make sure the user has signed the agreement.
  if($agreement_signed!=true)
  {
    // Direct back to the agreement page to sign. 
    header("location: agreement.php?placement_attended_id={$_GET['placement_attended_id']}&studID={$_GET['studID']}");
  }

  // Get the users aims previously chosen if there are any.
  $learnerAimsChosen = $aims->learnerAims($_GET['studID'],$_GET['placement_attended_id']); 

}

if($_POST)
{
  // Get the learners count of days and hours on placement
  $logbookTime = $logbook->logbookTime($_POST['studID'],$_POST['placement_attended_id']);
  // Get the learner details so we can populate the top section of the page with the students details. 
  $learner = new Learner;
  $learnerDetails = $learner->learnerDetails($_POST['studID'],$_POST['placement_attended_id']);

  // Instance of placement
  $placement = new PlacementAttended($_POST['studID']);

  // Check the work placement agreement is signed. 
  $agreement_signed = $placement->agreementSigned($_POST['studID'],$_POST['placement_attended_id']);

// Make sure the user has signed the agreement.
if($agreement_signed!==true)
{
  // Direct back to the agreement page to sign. 
  header("location: agreement.php?placement_attended_id={$_POST['placement_attended_id']}&studID={$_POST['studID']}");
}

  // Get the users aims previously chosen if there are any.
  $learnerAimsChosen = $aims->learnerAims($_POST['studID'],$_POST['placement_attended_id']); 
}

// Get list of both formatted and raw aims so we can use it in the HTML.
$aimsAndObjectives = $aims->displayAimsFormatted();
$aimsAndObjectivesRaw = $aims->displayAims();

// Add the arrays: $aimsAndObjectives,$aimsAndObjectivesRaw to create just one array to use in the form.
// for the options to be selected.
$aimsAndObjectives = array_combine($aimsAndObjectives,$aimsAndObjectivesRaw);


// A form on the page has been posted to. Complete the function and ensure the page refreshes.
if($_POST)
{ 
  /**
   * This for the updates to the logbook and the sickness
   */
  
  // Edit a full days sick. 
  if(isset($_POST['edit_sick_record']) && is_numeric($_POST['edit_sick_record']))
  {
      //echo "Full days sick {$_POST['edit_sick_record']}";die;
      header("location: logbook_view.php?id={$_POST['edit_sick_record']}&mode=edit&type=sickness&placement_attended_id={$_GET['placement_attended_id']}&studID={$_GET['studID']}");
  }

  // Edit and view a logbook day entry. 
  if(isset($_POST['edit_record']) && is_numeric($_POST['edit_record']) || isset($_POST['view_record']) && is_numeric($_POST['view_record']))
  {
      if(isset($_POST['edit_record'])) // Edit one of the logbook records.
      {
        header("location: logbook_view.php?id={$_POST['edit_record']}&mode=edit&type=logbook&placement_attended_id={$_GET['placement_attended_id']}&studID={$_POST['studID']}"); // Had to use POST to handle losing GET through POST.
      }
      if(isset($_POST['view_record'])) // View one of the logbook records.
      {
        header("location: logbook_view.php?id={$_POST['view_record']}&mode=view&type=logbook&placement_attended_id={$_GET['placement_attended_id']}&studID={$_GET['studID']}"); 
      }
  }


  /**
    * This for the inserts to the logbook and the sickness
   */


  // The full day sick form was completed.
  if(isset($_POST['full_day_sick']) && $_POST['full_day_sick']=='1')
  {
    $duplicate = $logbook->sickness($_POST);

    if($duplicate===false) header("location: logbook.php?placement_attended_id={$_POST['placement_attended_id']}&studID={$_POST['studID']}");

    if($duplicate===true)
    {
      header("location: logbook.php?placement_attended_id={$_POST['placement_attended_id']}&studID={$_POST['studID']}&error=true");
    }
  }
 
 if(isset($_POST['add_logbook']) && $_POST['add_logbook']=='1')
  {

    $duplicate = $logbook->addLogbook($_POST);
    if($duplicate===false) header("location: logbook.php?placement_attended_id={$_POST['placement_attended_id']}&studID={$_POST['studID']}");

    if($duplicate===true)
    {
      header("location: logbook.php?placement_attended_id={$_POST['placement_attended_id']}&studID={$_POST['studID']}&error=true");
    }
  }
}


// See if there are **learner details** from Shibboleth
if($learnerDetails = $learner->learnerDetails()!=false)
{
  $learnerDetails = $learner->learnerDetails();
}

// See if there are **coach details** from Shibboleth
if($coachDetails = $coach->coachDetails()!=false)
{
  // This is a coach. This is their details.
  $coachDetails = $coach->coachDetails();
}

// Check if there has been anything posted by the form for the coach comments. 
if(isset($_POST))
{

  // Add one of the logbook coach comments.
  if(isset($_POST['add_logbook_comments']) && $_POST['add_logbook_comments'] == '1')
  {
    $add = $coachComments->addCoachComments($_POST, 'logbook');
  }

  // Edit one of the logbook coach comments.
  if(isset($_POST['edit_logbook_comments']) && $_POST['edit_logbook_comments'] == '1')
  {
    $edit = $coachComments->editCoachComments($_POST, 'logbook');
  }
}

// Get all the coach comments for this placement_attended_id
$coachComments = $coach->getCoachCommentsLogbook($_GET['placement_attended_id']);

// Decide if the midpoint review should be shown - if 50 days, yes, otherwise, no.
$showMidPointReview = $learner->showMidPointReview($_GET['placement_attended_id']);
//var_dump($showMidPointReview);die;
?>


<script type="text/javascript">
var studID = "<?php echo $_GET['studID'];?>";
//alert(studID);
</script>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>My Workspace app</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/theme.css" rel="stylesheet">

    <!--<link href="css/ada.css" rel="stylesheet">-->

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

    <script src="js/workexperience.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

      <!-- Bootstrap 3 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Font Awesome 5 -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="crossorigin="anonymous"></script>

    <!-- Include the jQuery for buttons and navs -->
    <script src="js/includes/button_div_jquery.js"></script>
    
  </head>
  <body>

    <!-- Fixed navbar -->
    <!--<nav class="navbar navbar-inverse navbar-fixed-top">-->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
           <a class="navbar-brand" href="/workexperience/">My Workspace app</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="index.php">Choose placement</a></li>
             <li><a href="introduction.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Introduction</a></li>
            <li><a href="location.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Where is my placement</a></li>
            <li><a href="agreement.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Work placement agreement</a></li>
            <li><a href="aims.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Aims and objectives</a></li>
            <li class="active"><a href="logbook.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Logbook</a></li>
            <li><a href="placement_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Your work placement review</a></li>
            <li><a href="employer_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Employer review</a></li>
            <li><a href="coach_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Coach review</a>
          </li>
           <?php if($showMidPointReview):?>
                <li><a href="midpoint_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Your midpoint review</a>
                </li>
           <?php endif;?>
            <!--<li><a href="#about">About</a></li>-->
            <!--<li><a href="#contact">Contact</a></li>-->
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav><!-- end fixed navbar -->

    <div class="container-fluid theme-showcase" role="main">
      <div class="row"><!--Start of row -->
        <div class="col-md-12">
          <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                      <th>Student name</th>
                      <td><?=$learnerDetails[0]['name']?></td>
                      <th>Student ID</th>
                      <td><?=$learnerDetails[0]['person_code']?></td>
                      <th>Days</th>
                      <td><?=$logbookTime[0]['days']?></td>
                      <th>Hours</th>
                       <td><?=$hours = ($logbookTime[0]['hours']) ? round($logbookTime[0]['hours'],1) : '0' ?></td>
                    </tr>
                </thead>  
              </table>
          </div>  
        </div>
    </div> <!--End of row -->

    <div class="row" id='test'><!--Start of row -->
      <!-- Tabbed content left-->
      <div class="col-md-12">
          <ul class="nav nav-tabs">
            <li><a href="index.php">Choose placement</a></li>
             <li><a href="introduction.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Introduction</a></li>
            <li><a href="location.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Where is my placement</a></li>
            <li><a href="agreement.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Work placement agreement</a></li>
            <li><a href="aims.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Aims and objectives</a></li>
            <li class="active"><a href="logbook.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Logbook</a></li>
            <li><a href="placement_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Your work placement review</a></li>
            <li><a href="employer_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Employer review</a></li>
            <li><a href="coach_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Coach review</a></li>
            <?php if($showMidPointReview):?>
              <li><a href="midpoint_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Your midpoint review</a>
              </li>
           <?php endif;?>
          </ul>
      <br/>
    </div><!-- end tabbed content left-->
  </div><!--End of row -->


<!-- Start of button row -->
<div class="row">
  <div class="col-md-12">
    <h3>Logbook</h3>
    <P> Please complete a logbook entry <em>per day</em> to document your experience on placement. Try to think about your answers carefully and how the placement has benefited you, or if you feel it hasn't, why that is the case.</p>
    <p>To record a <u>full day of sickness</u>, please choose the <span class='red-bold'>'Record a sick day' </span>option.</p>
    <p>If you went home early today sick but <u>did still attend placement</u>, complete the 'Add a new day day' section and ensure you select the option 'left early due to sickness today'. Then, complete your partial hours so your attendance can be recorded. </p>
    <hr/>
    <p><button id="new_day" class="btn btn-success">Add a new day</button><p>
    <p><button id="sick_day" class="btn btn-danger">Record a sick day</button><p>
    <hr/>


    <?php if(isset($_GET['error'])):?>
      <script>
        $(document).ready(function(){
        $("#error_message").show().delay(5000).fadeOut();
      });
      </script>
    <div class="alert alert-danger" id='error_message'>
      <strong>Sorry!</strong> Error, that is a duplicate date you are trying to enter.
    </div>
    <?php endif;?>

  </div>
</div><!-- End of button row -->

<div class='sick_hidden'> <!-- This is the start of sick hidden section -->
  <div class="row">
    <div class="col-md-12">
      <p>Please complete these details to explain your absence from your placement.</p>
      <form id='sick' action="<?=$_SERVER['PHP_SELF']?>" method="POST"><!--Form starts here-->
        <div class="ADA-form-group">
          <label for="absence_date">Date of absence</label>
          <input type="date" class="form-control" name='date' max="<?php echo date('Y-m-d');?>" value="<?php echo date('Y-m-d');?>" id="date"/>
        </div>
        <div class="ADA-form-group">
            <label for="exampleFormControlTextarea1">Details of absence</label>
            <textarea class="form-control" id="details_of_absence" name='details_of_absence' placeholder='Please explain the nature of the absence.' rows="3"></textarea>
          <input type='hidden' name='id' value="<?=$learnerDetails[0]['id']?>"/>
        </div>

         <!-- If tjhe learner has assistance to complete the entry, the tick box shows this -->
        <p>Tick if you have had assistance from a tutor or coach to complete this entry.
        <input type="checkbox" name="assisted" value="1" id="assisted" class="form-check-input"></p>

        <br/>
        <input type='hidden' name='studID' value='<?=$_GET['studID']?>'?>
        <input type='hidden' name='placement_attended_id' value='<?=$_GET['placement_attended_id']?>'?>
        <button type="submit" name='full_day_sick' value='1' onclick='return validateDetailsOfAbsence();' class="btn btn-primary">Add sickday</button>
      </form><!--Form end here-->
    </div>
  </div>
</div> <!-- End sick hidden -->
<!-- This is the end of the sick section -->

<div class='form_hidden'> <!-- This is the start of form hidden -->
  <div class="row">
    <div class="col-md-6">
      <form id='logbook' action="<?=$_SERVER['PHP_SELF']?>" method="POST"><!--Form starts here-->
    <br/>
      <div class="ADA-form-group">
          <div class="ADA-form-group">
            <p><label for="date">Date</label>
            <input type="date" class="form-control" name='date' value="<?php echo date('Y-m-d');?>" max="<?php echo date('Y-m-d');?>" id="date"/></p>
            <p><label for="start">Start time</label>
            <input type="time" class="form-control" name='start' id="start" value='08:00'/></p>
            <p><label for="start">End time</label>
            <input type="time" class="form-control" name='finish' id="finish" value='16:00'/></p>
        </div>
      </div>

      <?php $stuID = (isset($_GET['studID'])) ? $_GET['studID'] : $_POST['studID'];?>
      <?php $placement_attended_id = (isset($_GET['placement_attended_id'])) ? $_GET['placement_attended_id'] : $_POST['placement_attended_id'];?>
      
      <?php if($aims->learnerAimsFormatted($stuID,$placement_attended_id)):?>

        <div class="ADA-form-group">
         
          <label for="aims">Aims &amp; objectives</label>
              <?php $x=1;?>
              <?php foreach($aims->learnerAimsFormatted($stuID,$placement_attended_id) AS $aims=>$aim):?>
                <p><?=$aim?>: <input type='checkbox' name="aim_<?=$x?>" id="aim_<?=$x?>" value="<?=$aim?>"/></p>
                <?php $x++;?>
              <?php endforeach;?>
        </div>

      <?php else:?>
         <div class="card" style="width:100%;">
              <div class="card-body">
                <h5 class='red-bold'>You have not selected aims &amp; objectives and this is a requirement for the logbook. </h5>
                <p class="card-text" span style='font-weight:bold;'>Please choose some aims and objectives before you complete the logbook entries</p>
                <p><a class="btn btn-primary btn-sm" href="/workexperience/aims.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>" class="card-link">Click to choose your aims &amp; objectives.</a><p>
              </div>
            </div>
      <?php endif;?>

     <div class="ADA-form-group">
        <label class="form-check-label">
            <p><input type="checkbox" name='left_early_sick' value='1' id='left_early' class="form-check-input"/>
          &nbsp;I arrived late/left early due to sickness today</p>
        </label>
      </div>
      <div id="sickness_occur">
        <div class="ADA-form-group">
          <label for="sick_during">What part of the day did the sickness occur:</label>
          <select class="form-control" name='sick_during' id='sick_during' id="sick_during">
            <option value='0'>Please select</option>
            <option value='morning'>Morning</option>
            <option value='afternoon'>Afternoon</option>
          </select>
        </div>
    </div>

    <!-- If tjhe learner has assistance to complete the entry, the tick box shows this -->
    <hr/>
    <p>Tick if you have had assistance from a tutor or coach to complete this entry.
    <input type="checkbox" name="assisted" value="1" id="assisted" class="form-check-input"></p>
    <br/>
  
  </div>  <!-- end of col-md-6 --> 

  <div class="col-md-6">
    <div class="ADA-form-group">
      <label for="exampleFormControlTextarea1">What did you do and learn today?.</label>
      <textarea class="form-control" id="activities" name='activities' rows="3" placeholder="Be as descriptive as you can and think about the individual tasks you have undertaken."></textarea>
    </div>
    <div class="ADA-form-group">
      <label for="exampleFormControlTextarea1">What did you enjoy?</label>
      <textarea class="form-control" id="enjoyed" name='enjoyed' rows="3" placeholder="Try to think about the enjoyble aspects of your placement and why you enjoyed them."></textarea>
    </div>
    <div class="ADA-form-group">
      <label for="exampleFormControlTextarea1">How could you do it better next time?</label>
      <textarea class="form-control" id="better_next_time" name='better_next_time' rows="3" placeholder="On reflection, think of the aspects that you feel you could improve in and the steps required for you to achieve that."></textarea>
    </div>
     
  </div>  <!-- end of col -->

</div>  <!-- end of row -->
  <div class="row">
      <div class="col-md-12">
          <?php $stuID = (isset($_GET['studID'])) ? $_GET['studID'] : $_POST['studID'];?>
          <input type='hidden' name='studID' value='<?=$stuID?>'?>
          <input type='hidden' name='placement_attended_id' value='<?=$_GET['placement_attended_id']?>'?>
          <button type="submit" name='add_logbook' value='1' onclick='return validateLogbook();' class="btn btn-primary">Add logbook entry</button>
      </div>
    </form><!--Form ends here-->
  </div><!-- End of row -->
</div> <!-- This is the end of form hidden -->


<div class="row"><!--Start of row -->
  <div class="col-md-12">
    <?php 
    $placement_attended_id = (isset($_GET['placement_attended_id'])) ? $_GET['placement_attended_id'] : $_POST['placement_attended_id'];
    if($logbook->logbook($placement_attended_id)!=false):?>
      <h3>Log book entries</h3>
        <div class="table-responsive">
          <table class="table table-bordered table-striped">
            <tbody>
            <?php 
            foreach($logbook->logbook($placement_attended_id) AS $row=>$headers):?>
            <tr>
                <th>Date</th>
                <td><?=date('l, jS \of F Y',strtotime($headers['date']));?></td>
            </tr>
            <tr>
                <th>Start</th>
                <td><?=date('H:i',strtotime($headers['start']));?></td>
            </tr>
            <tr>
              <th>Finish</th>
                <td><?=date('H:i',strtotime($headers['finish']));?></td>
            </tr>
            <tr>
              <!-- If a partcial sick day, show that in the in the entry-->
              <?php if($headers['sick_during']):?>
                <th>Sick during</th>
                <td><?=ucfirst($headers['sick_during'])?></td>
              <?php endif;?>
               <!-- End if a partcial sick day, show that in the in the entry-->
            </tr>
            <tr>
              <!-- If record updated, show when here-->
              <?php if($headers['date_updated']):?>
                <th>Updated at</th>
                <td><?=date('l, jS \of F Y',strtotime($headers['date_updated']))?></td>
              <?php endif;?>
              <!-- End if record updated, show when here-->
            </tr>
            <tr>
                <form method='POST'>
                  <!-- Form to handle the editing of a record for a days entry to the the logbook -->
                  <input type='hidden' name='studID' value='<?=$_GET['studID']?>'?>
                  <th><button name='view_record' value="<?=$headers['id']?>" class="btn btn-primary">View</button>
                  <button name='edit_record' value="<?=$headers['id']?>" class="btn btn-danger">Edit</button>
                  </th>
                </form>
                <!-- end Form to handle the editing of a record for a days entry to the the logbook -->
                <td>&nbsp;</td>
            </tr>
            <tr>
              <th>&nbsp;</th>
              <td>&nbsp;</td>
            </tr>
            </tbody>
            <?php endforeach;?>
            </tbody>
          </table>
        </div>
      <?php else:?>
        <h3>No Log book entries.</h3>
      <?php endif;?>  
    </div> <!--End of column -->
</div> <!--End of row -->


<!-- Full days sickness starts here -->
<div class="row"><!--Start of row -->
  <div class="col-md-12">
    <hr/>
      <?php 
    $placement_attended_id = (isset($_GET['placement_attended_id'])) ? $_GET['placement_attended_id'] : $_POST['placement_attended_id'];?>
    <?php if($logbook->logBookSickness($placement_attended_id)):?>
      <h3>Full days of sickness</h3>
        <div class="table-responsive">
          <table class="table table-bordered table-striped">
            <tbody>            
              <?php foreach($logbook->logBookSickness($placement_attended_id) AS $row=>$item):?>
              <tr>
                <th>Date</th>
                <td><?=date('l, jS \of F Y',strtotime($item['date']))?></td>
              </tr>
              <tr>
                <th>Sickness description</th>
                <td><?=$item['details_of_absence']?></td>
              </tr>
              <tr>
               <!-- If record updated, show when here-->
                <?php if($item['date_updated']):?>
                  <th>Updated at</th>
                  <td><?=date('l, jS \of F Y',strtotime($item['date_updated']))?></td>
                <?php endif;?>
              </tr>
              <!-- End if record updated, show when here-->
              <tr>
                <!-- Form to handle the viewing & editing of a record for a full sickness -->
                <form method='POST'>
                  <th colspan="2">
                    <button name='edit_sick_record' value="<?=$item['id']?>" class="btn btn-danger">Edit</button>
                  </th>
                </form>
              </tr>
               <!-- End Form to handle the editing of a record -->
              <tr>
                <th>&nbsp;</th>
                <td>&nbsp;</td>
            <?php endforeach;?>   
            </tbody>
          </table>
        </div> 
      <?php else:?>
        <h3>No Log book sick entries.</h3>
      <?php endif;?>   
    </div>

</div> <!--End of row -->
<!-- Full days sickness starts here -->

 <hr/>

<!-- The coach comments at the very bottom -->
        <div class="row"><!--Start of row -->
          <!-- Tabbed content left-->
          <div class="col-md-12">

            <?php if($coachDetails!=false): // Only show form if coach ?>

              <h3> Add coach comments</h3>

              <fieldset>
                    <form method='POST'>

                        <div class="form-group">
                          <textarea required class="form-control" id="comments_logbook" name='comments_logbook' rows="7" placeholder="This is an area for the coach to add comments for the learners attention as the placement reaches various stages in an effort to guide the learner when it is felt some additional help is needed. You learner will be able to view the comments."></textarea>
                        </div>

                      <input type='hidden' name='studID' value="<?=$_GET['studID']?>"/>
                      <input type='hidden' name='placement_attended_id' value="<?=$_GET['placement_attended_id']?>"/>

                      <button type="submit" name='add_logbook_comments' value='1' class="btn btn-primary">Add comment</button>  
                    </form>
              </fieldset>

            <?php endif;?>

            <hr/>

            <?php if(isset($coachComments[0]['logbook_comments'])):?>
              <h3>Coach comments</h3>
              <?php if($coachDetails!=false):?>
                <p><?=$learnerDetails[0]['name']?> will see the comments below on their wall as <strong>read-only</strong></p>
              <?php endif;?>

            <?php else:?>
              
            <?php endif;?>

            <?php for($i=0;isset($coachComments[$i]['logbook_comments']);$i++):?>
             <fieldset <?php if($coachDetails==false) echo 'disabled'; // Disable form if not coach ?>>
                  <form method='POST'>

                      <div class="form-group">
                        <textarea class="form-control" id="coach_comments" name='coach_comments' rows="7" placeholder="This is an area for the coach to add comments as the placement reaches various stages in an effort to guide the learner when it is felt some additional help is needed."><?=$coachComments[$i]['logbook_comments']?></textarea>
                      </div>
                      <input type='hidden' name='row_id' value="<?=$coachComments[$i]['id']?>"/>
                      <input type='hidden' name='studID' value="<?=$_GET['studID']?>"/>
                      <input type='hidden' name='placement_attended_id' value="<?=$_GET['placement_attended_id']?>"/>
                    <!-- If the aims have been selcted, change the button -->

                    <p>Created: <strong><?=date('l jS \of F Y H:i:s A',strtotime($coachComments[$i]['date_created']))?></strong> by 
                    <strong>
                      <?=$learner->learnerWorkCoachByCoachID($coachComments[$i]['coach_id'])[0]['name']?>
                    </strong></p>

                  
                    <?php 
                    // If the date created and updated date differ, it's been updated.
                    if(strtotime($coachComments[$i]['date_created'])!=strtotime($coachComments[$i]['date_updated'])):?>
                      <p>Updated at: <strong><?=date('l jS \of F Y H:i:s A',strtotime($coachComments[$i]['date_updated']))?></strong></p>
                    <?php endif;?>

                    <?php if($coachDetails!=false): // If coach show update button ?>
                    <button type="submit" class="btn btn-danger" name='edit_logbook_comments' value='1'>Update comments</button> 
                  <?php endif;?>
                  </form>
              </fieldset>
              <hr/>
            <?php endfor;?>
          </div>
        </div>
        <!-- The coach comments at the very bottom end -->


</div> <!--end main container -->

<!-- footer here -->
<footer class="footer">
  <hr/>
  <div class="container">
    <p style="text-align:center;">Bolton College 2017</p>
  </div>

</footer>
<!-- end footer end -->
  </body>

  <?php 
   // Require the ada pop up box
   require_once('ADA_form.php');
   ?>

  <!-- Bootstrap core JavaScript ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script defer src="../learner/adanew_dist/adabundle_v1.5.1.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="/js/ie10-viewport-bug-workaround.js"></script>

</html>
