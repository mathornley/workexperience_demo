
<h3>Personal information</h3>
<hr/>

<form method='POST'>
  <div class="form-group">
    <label for="exampleInputEmail1">Full name</label>
    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="full name" placeholder="Please enter your full name" value="<?=$ldetails[0]['name']?>" disabled required/>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Street</label>
    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="street" placeholder="Please enter your street including the house number" value="<?=$ldetails[0]['address1']?>" disabled required/>
  </div>
   <div class="form-group">
    <label for="exampleInputEmail1">Town</label>
    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="town" placeholder="Please enter your town" value="<?=$ldetails[0]['town']?>" disabled required/>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Postcode</label>
    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="post_code" placeholder="Please enter your post code" value="<?=$ldetails[0]['postcode']?>" disabled required/>
  </div>
   <div class="form-group">
    <label for="exampleInputEmail1">Telephone</label>
    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="telephone" placeholder="Please enter your telephone" value="<?=$ldetails[0]['telephone']?>" disabled required/>
  </div>
   <div class="form-group">
    <label for="exampleInputEmail1">Mobile</label>
    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="mobile" placeholder="Please enter your mobile number" value="<?=$ldetails[0]['mobile']?>" disabled/>
  </div>
  <div class="form-group">
      <label for="comment">Personal statement:</label>
      <textarea class="form-control" rows="13" id="personal_statement" name="personal_statement" placeholder='Please enter your personal statement here' required><?php if(isset($personal_statement[0]['comments'])) echo $personal_statement[0]['comments'];?></textarea>
  </div>
  <br/>
  
  <h3>Add skills &amp; abilities</h3>
  <hr/>
  <p> Here you must pick a minimum of 5 skills and abilities.</p>

  <?php if(!isset($_POST['CV_edit'])): // This is the create section of skills?>

    <div class="form-group">
      <label for="exampleInputEmail1">Skill 1</label>
      <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="skill_1" name='skill_1' placeholder="Choose skill 1" required/>
    </div>
     <div class="form-group">
      <label for="exampleInputEmail1">Skill 2</label>
      <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="skill_2" name='skill_2' placeholder="Choose skill 2" required/>
    </div>
     <div class="form-group">
      <label for="exampleInputEmail1">Skill 3</label>
      <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="skill_3" name='skill_3' placeholder="Choose skill 3" required/>
    </div>
    <div class="form-group">
      <label for="exampleInputEmail1">Skill 4</label>
      <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="skill_4" name='skill_4' placeholder="Choose skill 4" required/>
    </div>
    <div class="form-group">
      <label for="exampleInputEmail1">Skill 5</label>
      <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="skill_5" name='skill_5' placeholder="Choose skill 5" required/>
    </div>

  <?php endif;?>

  <?php if(isset($_POST['CV_edit']) && $_POST['CV_edit']): // This is the update section of skills?>

  <?php for($i=0;isset($skills[$i]);$i++): // Creation of additional skills in the create process?>
    <div class="form-group">
      <label for="exampleInputEmail1">Skill <?=$skills[$i]['id']?></label>
      <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="skill_<?=$i?>" name='skill_<?=$i?>' placeholder="Choose skill <?=$i?>" value="<?=$skills[$i]['skill']?>" required/>
    </div>
  <?php endfor;?>
   
<?php endif;?>

  <button class="btn btn-primary" id="add">Add a new skill</button>

  <script>

    // Add another skill box to add more skills.
    var number=5; // Set this to carry on from 5 skills

    $(function() {
          // Add a new skill
          $('#add').on('click', function(e) {
              e.preventDefault();
              $('<div/>').addClass( 'form-group')
              .html( $('<input type="textbox" name="skill_' + ++number + '"' + 'id="class_' + number +  '" placeholder="Enter another skill"/>').addClass( 'form-control' ) )
              .append( $('<br/><button class="btn btn-danger"/>').addClass( 'remove' ).text( 'Remove' ) )
              .insertBefore( this );
          });
          // Remove the new skill here
          $(document).on('click', 'button.remove', function( e ) {
            //alert(this)
              e.preventDefault();
              $(this).closest('.form-group').remove();
          });
    });


  </script>

  <br/><br/>

  <h3>Qualifications</h3>
  <hr/>

  <?php if(!isset($_POST['CV_edit'])): // ******* create
  ?>
    <div class="form-group">
      <?php if(isset($lQuals) && $lQuals[0]['description']):?>
        <label for="exampleInputEmail1">Here are your qualifications</label>
          <div class="row"><!--Start of row -->
            <div class="col-md-12">
              <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th scope="col">Subject</th>
                        <th scope="col">Grade</th>
                        <th scope="col">Year achieved</th>
                        <th scope="col">Include on CV</th>
                        <th scope="col">Exclude from CV</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php for($i=0;isset($lQuals[$i]);$i++):?>
                          <tr>
                            <td><?=$lQuals[$i]['description']?></td>
                            <td><strong><?=$lQuals[$i]['grade']?></strong></td>
                            <td><?=$lQuals[$i]['year_achieved']?></td>
                            <td><input type="radio" checked name="Qualid_<?=$lQuals[$i]['id']?>" value="1"/></td>
                            <td><input type="radio" name="Qualid_<?=$lQuals[$i]['id']?>" value="0"/></td>
                          </tr>
                        <?php endfor;?>
                    </tbody>
                  </table>
              </div>  
            </div>
        </div> <!--End of row -->
        <?php else:?>
          <p class="lead">There are no qualifications recorded on the system</p>
        <?php endif;?>
    </div> <!-- End form group -->
  <?php endif;?>

  <?php if(isset($_POST['CV_edit'])): // ******* update
  ?>
    <div class="form-group">
      <?php if(isset($lQuals) && $lQuals[0]['description']):?>
        <label for="exampleInputEmail1">Here are your qualifications</label>
          <div class="row"><!--Start of row -->
            <div class="col-md-12">
              <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th scope="col">Subject</th>
                        <th scope="col">Grade</th>
                        <th scope="col">Year achieved</th>
                        <th scope="col">Include on CV</th>
                        <th scope="col">Exclude from CV</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php for($i=0;isset($lQuals[$i]);$i++):?>
                          <tr>
                            <td><?=$lQuals[$i]['description']?></td>
                            <td><strong><?=$lQuals[$i]['grade']?></strong></td>
                            <td><?=$lQuals[$i]['year_achieved']?></td>
                            <td><input type="radio" name="Qualid_<?=$lQuals[$i]['id']?>" value="1"
                            <?php if($learner->checkQualArray($lQuals[$i]['id'],$Quals)==true) echo 'checked';?>
                              /></td>
                            <td><input type="radio" name="Qualid_<?=$lQuals[$i]['id']?>" value="0"
                               <?php if($learner->checkQualArray($lQuals[$i]['id'],$Quals)!=true) echo 'checked';?>

                              /></td>
                          </tr>
                        <?php endfor;?>
                    </tbody>
                  </table>
              </div>  
            </div>
        </div> <!--End of row -->
        <?php else:?>
          <p class="lead">There are no qualifications recorded on the system</p>
        <?php endif;?>
    </div> <!-- End form group -->
  <?php endif;?>

  <h3>Work Experience</h3>
  <hr/>
  
<?php if(!isset($_POST['CV_edit'])): // ******* create
?>
  <div class="form-group">
    <?php if(isset($placement[0]['id'])):?>
      <label for="exampleInputEmail1">Here are your work experience placements</label>
        <div class="row"><!--Start of row -->
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th scope="col">Company</th>
                      <th scope="col">Address</th>
                      <th scope="col">Dates</th>
                      <th scope="col">Include on CV</th>
                      <th scope="col">Exclude from CV</th>
                    </tr>
                  </thead>
                  <tbody>
                      <?php for($i=0;isset($placement[$i]);$i++):?>
                        <tr>
                          <td><?=$placement[$i]['name']?></td>
                          <td><?=$placement[$i]['address']?>, <?=$placement[$i]['post_code']?> </td>
                          <td><?=date('d/m/Y',strtotime($placement[$i]['start_date']))?> to <?=(!isset($placement[$i]['end_date'])) ? '<span style="background:red;padding:5px;font-weight:bold;color:#fff;">No end date</span' : date('d/m/Y',strtotime($placement[$i]['end_date']))?></td>
                          <td><input type="radio" name="placement_id_<?=$i?>" id="we_check_<?=$placement[$i]['id']?>" value="<?=$placement[$i]['paid']?>_1" checked/>
                           
                          <td><input type="radio" name="placement_id_<?=$i?>" id="we_check_<?=$placement[$i]['id']?>" value="<?=$placement[$i]['paid']?>_0"/></td>
                        </tr>
                      <?php endfor;?>
                  </tbody>
                </table>
            </div>  
          </div>
      </div> <!--End of row -->


      <!--Work experience descriptins start here -->
      <div class="row"><!--Start of row -->
          <div class="col-md-12">
          <h3>Work Experience descriptions</h3>
          <p>Please complete the brief descriptions of your roles at each of your work experience placements</p>
          <hr/>
          <?php for($i=0;isset($placement[$i]);$i++): // Text boxes for the placement blurb - 1 placement, 1 box?>

          <div class='we_div_<?=$placement[$i]['id']?>'>
            <p>What did you do at: <?=$placement[$i]['name']?> between <?=date('d/m/Y',strtotime($placement[$i]['start_date']))?> and <?=(!isset($placement[$i]['end_date'])) ? '<span style="background:red;padding:5px;font-weight:bold;color:#fff;">No end date</span' : date('d/m/Y',strtotime($placement[$i]['end_date']))?>?</p>
            <div class="form-group">
                <textarea class="form-control" rows="10" name="we_id" value="<?=$placement[$i]['id']?>" placeholder="Please details your role and specifically you did at your placement at <?=$placement[$i]['name']?> during that time period" required><?php if(isset($personal_statement[0]['comments'])) echo $personal_statement[0]['comments'];?></textarea>
            </div>
          </div>

      <?php endfor;?>
      </div>
    </div>
     <!--Work experience descriptins finish here -->

      <?php else:?>
        <p class="lead">There is no work experience recorded on the system</p>
      <?php endif;?>
  </div> <!-- End form group -->

   <?php endif; // ******* create?>
   <?php if(isset($_POST['CV_edit'])): // ******* update
   ?>

    <div class="form-group">
    <?php if(isset($placement[0]['id'])):?>
      <label for="exampleInputEmail1">Here are your work experience placements</label>
        <div class="row"><!--Start of row -->
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th scope="col">Company</th>
                      <th scope="col">Address</th>
                      <th scope="col">Dates</th>
                      <th scope="col">Include on CV</th>
                      <th scope="col">Exclude from CV</th>
                    </tr>
                  </thead>
                  <tbody>
                      <?php for($i=0;isset($placement[$i]);$i++):?>
                        <tr>
                          <td><?=$placement[$i]['name']?></td>
                          <td><?=$placement[$i]['address']?>, <?=$placement[$i]['post_code']?> </td>
                          <td><?=date('d/m/Y',strtotime($placement[$i]['start_date']))?> to <?=(!isset($placement[$i]['end_date'])) ? '<span style="color:red;">No end date</span' : date('d/m/Y',strtotime($placement[$i]['end_date']))?></td>
                          <td><input type="radio" checked name="placement_id_<?=$i?>" value="<?=$placement[$i]['paid']?>_1"
                             <?php if($we->checkPlacementsArray($placement[$i]['paid'],$userChosenPlacements)===true) echo 'checked';?>
                            /></td>
                          <td><input type="radio" name="placement_id_<?=$i?>" value="<?=$placement[$i]['paid']?>_0"
                            <?php if($we->checkPlacementsArray($placement[$i]['paid'],$userChosenPlacements)!==true) echo 'checked';?>
                            /></td>
                        </tr>
                      <?php endfor;?>
                  </tbody>
                </table>
            </div>  
          </div>
      </div> <!--End of row -->
      <?php else:?>
        <p class="lead">There is no work experience recorded on the system</p>
      <?php endif;?>
  </div> <!-- End form group -->

  <?php endif; // ******* update?>


  <h3>Voluntary Work</h3>
  <hr/>
  <div class="form-group">
      <label for="exampleInputEmail1">Voluntary Work</label>
  </div>

  <h3>References</h3>
  <hr/>

  <p>You must choose at least <strong>one referee</strong>.</p>

    <div class="row"><!--Start of row -->
        <div class="col-md-6"><!--Start of col -->

           <h5>Reference 1</h5>
           <hr/>

            <div class="form-group">
                <label for="exampleInputEmail1">Referee name</label>
                <input type="text" class="form-control" id="referee_name_1" aria-describedby="emailHelp" placeholder="Please enter your referee name here" name='referee_name_1' value="<?php if(isset($references[0]['referee_name'])) echo $references[0]['referee_name']; ?>" required/>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Referee business name</label>
                <input type="text" class="form-control" id="referee_business_name_1" placeholder="Please enter your referee business name" name='referee_business_name_1' value="<?php if(isset($references[0]['referee_business_name'])) echo $references[0]['referee_business_name']; ?>" required/>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Referee address</label>
                <input type="text" class="form-control" id="referee_address_1" placeholder="Please enter your referee address" name='referee_address_1' value="<?php if(isset($references[0]['referee_address'])) echo $references[0]['referee_address']; ?>" required/>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Referee phone number <small>(inc dialing code i.e 0161)</small></label>
                <input type="phone" class="form-control" id="referee_phone_1" placeholder="Please enter your referee phone number" pattern='^\+?\d{0,11}' name='referee_phone_1' value="<?php if(isset($references[0]['referee_phone'])) echo $references[0]['referee_phone']; ?>"  required/>
            </div>
             <div class="form-group">
                <label for="exampleInputPassword1">Referee email Address</label>
                <input type="email" class="form-control" id="referee_email_1" placeholder="Please enter your referee phone number" name='referee_email_1' value="<?php if(isset($references[0]['referee_email'])) echo $references[0]['referee_email']; ?>"  required/>
            </div>

            <hr/>

        </div>

        <div class="col-md-6"><!--Start of col -->

          <h5>Reference 2</h5>
           <hr/>

            <div class="form-group">
                <label for="exampleInputEmail1">Referee name</label>
                <input type="text" class="form-control" id="referee_name_2" aria-describedby="emailHelp" placeholder="Please enter your referee name here" name='referee_name_2' value="<?php if(isset($references[1]['referee_name'])) echo $references[1]['referee_name']; ?>"/>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Referee business name</label>
                <input type="text" class="form-control" id="referee_business_name_2" placeholder="Please enter your referee business name" name='referee_business_name_2' value="<?php if(isset($references[1]['referee_business_name'])) echo $references[1]['referee_business_name']; ?>"/>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Referee address</label>
                <input type="text" class="form-control" id="referee_address_2" placeholder="Please enter your referee address" name='referee_address_2' value="<?php if(isset($references[1]['referee_address'])) echo $references[1]['referee_address']; ?>"/>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Referee phone number <small>(inc dialing code i.e 0161)</small></label>
                <input type="phone" class="form-control" id="referee_phone_1" placeholder="Please enter your referee phone number" pattern='^\+?\d{0,11}' name='referee_phone_2' value="<?php if(isset($references[1]['referee_phone'])) echo $references[1]['referee_phone']; ?>"/>
            </div>
             <div class="form-group">
                <label for="exampleInputPassword1">Referee email Address</label>
                <input type="email" class="form-control" id="referee_email_2" placeholder="Please enter your referee phone number" name='referee_email_2' value="<?php if(isset($references[1]['referee_email'])) echo $references[1]['referee_email']; ?>"/>
            </div>

            <hr/>
        
      </div>

    </div> <!-- End the row -->


    <div class="row"><!--Start of row -->
      <div class="col-md-12"> <!--Start of col -->
        <h5>Name your CV</h5>
        <hr/>
          <div class="form-group">
            <p> Below is where you have the <em>option</em> to name your CV if you wish (ie. admin, tech, engineering)</p>
            <p><input type="text" class="form-control" id="CV_name" aria-describedby="CV_name" placeholder="Enter the name of your CV here. This is not a mandatory field." name='CV_name' value="<?php if(isset($CVInfo[0]['CV_name'])) echo $CVInfo[0]['CV_name'];?>" /></p>
             <p><small>This would allow you to organise your CVs by catgory should you wish to create several CVs for several different roles.</small></p>
          </div>
      </div> <!--End of col -->
    </div> <!--End of row -->
 
    <?php if(!isset($_POST['CV_edit'])): // SUbmit button on form as nothing posted?>
      <button type="submit" class="btn btn-primary">Submit</button>
    <?php else: // Form has been edited and time to be updated?>
      <button type="submit" class="btn btn-danger" name='update_CV' value="<?=$CVInfo[0]['id']?>">Update CV</button>
    <?php endif;?>

</form>

<br/>

<?php if(isset($_POST['CV_edit']) && $_POST['CV_edit']): // Show all the CVs if in edit mode. ?>
   <div class="row"><!--Start of row -->
    <div class="col-md-12"> <!--Start of col -->
      <h5>All your CVs</h5>
      <hr/>
    </div>
  </div>
<?php endif;?>
