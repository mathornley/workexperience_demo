<?php

/**
 * Get the PSR4 autoloader and load all the classes required.
 */
spl_autoload_register(function ($class_name) {
    include 'classes/'.$class_name . '.php';
});

// Stop the cache
require_once 'stop_cache.php';

// Display errors
//ini_set('display_errors', 'On');

// Gets the learner details row and the placement details for that learner.
$learner = new Learner($_GET['placement_attended_id'],$_GET['studID']);
$learnerDetails = $learner->learnerDetails($_GET['studID'],$_GET['placement_attended_id']);

// Update the learner custom information section next to the map.
if(isset($_POST['update_student_message']) && $_POST['update_student_message'])
{
  $informationUpdate = $learner->updateLearnerInfo($_POST);
}

// Get the placement coach details.
$placement = new Placement;
$placementDetails = $placement->getPlacementDetailsFromID($_GET['studID'],$_GET['placement_attended_id'],$learnerDetails[0]['placement_id']);

//echo '<pre>';print_r($placementDetails);die;

// Get a new coach instance
$coach = new CareerCoachReview();

// Get the learners custom message for the area next to the map.
$information = $learner->getLearnerInfo($_GET['placement_attended_id']);

/**
 * Check if this is a learner only viewing their own part of the app - learner ids have to match.
 * The Shib learner id has to match the URL from the browser, IF they're not a coach. 
 * As usual the coach sees it all.
 * If the user is not authorised, $_SERVER['REQUEST_URI'] is used to keep them on their page.
 */
if(!$coach->isCareerCoach() && $coach->doesTheLearnerIdMatch($_GET['studID'])==false)
{
  // The user is not a coach and not the learner requesting the URL so keep them where they are.
  header('location: /workexperience/');
}

// Decide if the midpoint review should be shown - if 50 days, yes, otherwise, no.
$showMidPointReview = $learner->showMidPointReview($_GET['placement_attended_id']);
//var_dump($showMidPointReview);die;
?>

<script type="text/javascript">
var studID = "<?php echo $_GET['studID'];?>";
//alert(studID);
</script>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>My Workspace app</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/theme.css" rel="stylesheet">

    <!--<link rel="stylesheet" type="text/css" href="css/ada.css" />-->

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

        <!-- Bootstrap 3 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Font Awesome 5 -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="crossorigin="anonymous"></script>
  </head>
  <body>

  <!-- Fixed navbar -->
  <!--<nav class="navbar navbar-inverse navbar-fixed-top">-->
  <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
         <a class="navbar-brand" href="/workexperience/">My Workspace app</a>
      </div>
      <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
          <li><a href="index.php">Choose placement</a></li>
              <li><a href="introduction.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Introduction</a></li>
              <li class="active"><a href="location.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Where is my placement</a></li>
              <li><a href="agreement.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Work placement agreement</a></li>
              <li><a href="aims.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Aims and objectives</a></li>
              <li><a href="logbook.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Logbook</a></li>
              <li><a href="placement_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Your work placement review</a></li>
              <li><a href="employer_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Employer review</a></li>
              <li><a href="coach_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Coach review</a>
            </li>
             <?php if($showMidPointReview):?>
                <li><a href="midpoint_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Your midpoint review</a>
                </li>
             <?php endif;?>
          <!--<li><a href="#about">About</a></li>-->
          <!--<li><a href="#contact">Contact</a></li>-->
        </ul>
      </div><!--/.nav-collapse -->
    </div>
  </nav><!-- end fixed navbar -->

    <div class="container-fluid theme-showcase" role="main">
      <div class="row"><!--Start of row -->
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-bordered">
              <thead>
                  <tr>
                    <th>Student name</th>
                    <td><?=$learnerDetails[0]['name']?></td>
                    <th>Student ID</th>
                    <td><?=$learnerDetails[0]['person_code']?></td>
                    <th>Coach name</th>
                    <td><?=$coach->getCoachNameForLearner($_GET['placement_attended_id'])?></td>
                  </tr>
              </thead>   
            </table>
        </div>  
      </div>
    </div> <!--End of row -->

      <div class="row" id='test'><!--Start of row -->
          <!-- Tabbed content left-->
          <div class="col-md-12">
               <ul class="nav nav-tabs">
                <li><a href="index.php">Choose placement</a></li>
                <li><a href="introduction.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Introduction</a></li>
                <li class="active"><a href="location.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Where is my placement</a></li>
                <li><a href="agreement.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Work placement agreement</a></li>
                <li><a href="aims.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Aims and objectives</a></li>
                <li><a href="logbook.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Logbook</a></li>
                <li><a href="placement_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Your work placement review</a></li>
                <li><a href="employer_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Employer review</a></li>
                <li><a href="coach_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Coach review</a></li>
                <?php if($showMidPointReview):?>
                  <li><a href="midpoint_review.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>">Your midpoint review</a>
                </li>
                <?php endif;?>
              </ul>
          <br/>
        </div><!-- end tabbed content left-->
      </div><!--End of row -->
  
      <div class="row"><!--Start of row -->

        <!-- content left & map-->
        <div class="col-md-6">
          <h3>Directions to your placement:</h3>
          <script>
           $(document).ready(function(){
              $('.map').html('loading the map.....').load("js/includes/map.php?placement_attended_id=<?=$_GET['placement_attended_id']?>&studID=<?=$_GET['studID']?>");
          });
          </script>

          <div class='map'>Loading the map....</div>

        <p class='from_top'>Transport for Greater Manchester has a route planner<a href='https://my.tfgm.com/#/planner/'> here</a> also. </p>

        <div class="panel panel-primary">
          <div class="panel-heading">
            <h3 class="panel-title">Your coach is: <?=$coach->getCoachNameForLearner($_GET['placement_attended_id'])?></h3>
          </div>
          <div class="panel-body">

            <?php // Get the coach email and create a link 
            $cemail = $coach->getCoachDetails($_GET['placement_attended_id'])[0]['email'];?>
            <p>You can contact <?=explode(' ', $coach->getCoachNameForLearner($_GET['placement_attended_id']))[0]?> by <a title='You can email or call your coach with any conerns or issues you may have.' href="mailto:<?=$cemail?>?subject=From the Work Experience app">email</a>, or call on <strong><?=$coach->getCoachDetails($_GET['placement_attended_id'])[0]['phone'];?> </strong>
            <?php if(isset($coach->getCoachDetails($_GET['placement_attended_id'])[0]['mobile'])) echo 'and mobile <strong> '.$coach->getCoachDetails($_GET['placement_attended_id'])[0]['mobile'];?>. </strong></p>
          </div>
        </div>


        </div><!-- End content left & map-->
    
        <!-- content right-->
        <div class="col-md-6"> <!--Start of row -->
          <h3>Placement details:</h3>

          <?php foreach($placementDetails AS $row=>$item): ?>
            <h4>Name: <?=$item['name']?></h4>
            <h4>Address: <?=$item['address']?>, <?=$item['post_code']?></h4>
            <h4>Phone: <a title="Call: <?=$item['name']?>" href='tel:<?=$item['phone']?> '><?=$item['phone']?></a></h4>
            <h4>Placement type: <?=ucfirst($placement->getPlacementAttenedInfo($_GET['placement_attended_id'])[0]['placement_code'])?>
            <h4>Manager: <?php echo (!isset($item['manager'])) ? 'No details currently.' : $item['manager'] ?></h4>
            <h4>Dates: <?php if($placement->getPlacementAttenedInfo($_GET['placement_attended_id'])[0]['placement_code']!='ZWRKX002'):// This is not an industrial placement?>
                     <?=date('d/m/Y',strtotime($placement->getPlacementAttenedInfo($_GET['placement_attended_id'])[0]['start_date']))?> - <?=date('d/m/Y',strtotime($placement->getPlacementAttenedInfo($_GET['placement_attended_id'])[0]['end_date']))?>
                    <br/>
                  </h4>
              <?php else: 
                  // This is an undustrial placement - get the dates for it?>
                <?=$learner->getDatesInd($_GET['placement_attended_id']);?></h4>
              <?php endif;?>

               <h4>Hours will be: <?=date('H:i',strtotime($placement->getPlacementAttenedInfo($_GET['placement_attended_id'])[0]['start_time']))?> - <?=date('H:i',strtotime($placement->getPlacementAttenedInfo($_GET['placement_attended_id'])[0]['end_time']))?>hrs
               </h4>

            </h4>

          <?php endforeach; ?>
            
            <h4>Further information:</h4>
            
            <form method='POST'>

            <!-- This is the area where the coach can update the information for each student - this is read-only to the student -->
            <div class="form-group">
              <textarea class="form-control" name='information' placeholder='You are reminded to act professionally at all times.' rows="10"<?php if(!$coach->isCareerCoach()) echo 'disabled';?>><?=trim($information)?></textarea>
              <input type='hidden' name='id' value="<?=$learnerDetails[0]['id']?>"/>
            </div> 
            <!-- This is the area where the coach can update the information for each student - this is read-only to the student -->

            <?php if($coach->isCareerCoach()): // Is a career coach?>
            <p><small id="emailHelp" class="form-text text-muted"><span style='color:red;'>** Just click update in the box to update the message to the learner.They will see this area in read-only mode. **</span>.</small></p>
            <?php endif;?>

            <?php 
            // This is the area where the coach can update the information for each student - the button is only visible to the coach --> 
            if($coach->isCareerCoach()):?>
            <p><button type="submit" name='update_student_message' value='1' class="btn btn-primary">Update</button></p><?php endif;?>

            </form>

              <?php 
              // Fire to let the user know the update is successful. Only fire after update
              if(isset($informationUpdate) && $informationUpdate=='true'):?>
              <script>
                $(document).ready(function(){
                $("#success_message").show().delay(5000).fadeOut();
              });
              </script>
              <div class="alert alert-success" id='success_message'>
                <strong>Success!</strong> The message has been updated successfully for the learner. 
              </div>
            <?php endif;?>
    
        </div><!-- end content right-->  

      </div>  <!-- end row -->
    </div> <!--end container-fluid -->

    <!-- footer here -->
    <footer class="footer">
    <hr/>
      <div class="container">
           <p style="text-align:center;">Bolton College <?=date('Y')?></p>
        </div>
    </footer>
    <!-- end footer end -->

    <!-- Bootstrap core JavaScript ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="/js/ie10-viewport-bug-workaround.js"></script>
  </body>

    <?php 
   // Require the ada pop up box
   require_once('ADA_form.php');
   ?>

 </div>
      
     <!-- Bootstrap core JavaScript ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script defer src="../learner/adanew_dist/adabundle_v1.5.1.js"></script>
    <script src="js/ie10-viewport-bug-workaround.js"></script>
    <script src="js/bootstrap.js"></script>

</html>
