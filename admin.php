<?php

/**
 * Get the PSR4 autoloader and load all the classes required.
 * This page handles the validation to the updated review with PHP in the class (editReview()) and then finally Javascript
 */
spl_autoload_register(function ($class_name){
    include '../classes/'.$class_name . '.php';
});

// Stop the cache
require_once '../stop_cache.php';

// Error reporting.
ini_set('display_errors','On');

// Instantiate a new instance of coach.
$coach = new CareerCoachReview;

// Instantiate learner - mainly for the Midpoint Review review submission.
$learner = new Learner;

// ***** First find out if this is a coach - coaches, see all work experience. Check the coaches table for the users Shib and if it's there, they're a coach. 
if($isCareerCoach = $coach->isCareerCoach())
{
  // Get the coach details. 
  $isCareerCoach = $coach->isCareerCoach();
}
else
{
  // Not a coach so redirect later.
  echo 'Not a career coach stop';die;
} // ***** End find out if career coach 


/*
* $_GET['token'] is the placement_attended_id and we use token as a red herring. It is passed in from index.php via $_GET.
* This is coming from the index.php page and the student has been selected, so we now present them with a form to add their coach review.
* We'll use this in the review form when it's posted so we know what review placement_attended_id is being updated.
 */
$placement_attended_id =  $coach->hashids->decode($_GET['token'])[0]; // This is the placement_attended_id

//echo '<pre>';
//print_r($reviewCompleted);die;

// Add the review
if(isset($_POST) && isset($_POST['save_employer_review']))
{
  $reviewAdded = $coach->AddCoachReview($_POST);
}

// Edit the review
if(isset($_POST) && isset($_POST['edit_employer_review']))
{
  $placement_attended_id = $coach->hashids->encode($placement_attended_id);
  //$editedReview = $coach->EditCoachReview($placement_attended_id); // Doesn't need hashing!
  header("location: /workexperience/coach/admin_edit.php?token={$placement_attended_id}");

}

// Add the midpoint review section
if(isset($_POST) && isset($_POST['create_midpoint_review']) && $_POST['create_midpoint_review']=='1')
{
  //echo '<pre>'; print_r($_POST);die;
  $midPointReview = $learner->submitMidpointReview($_POST);
}

// Update the midpoint review section 
if(isset($_POST) && isset($_POST['update_midpoint_review']))
{
  //echo '<pre>'; print_r($_POST);die;
  $updatePointReview = $learner->updateMidpointReview($_POST);
}


// Pull all placements, regardless of the coach.
$allplacements = $coach->getPlacements();
//print_r($allplacements);die;

// Pull all the coach specific placements using the coachID from call above.
$coachPlacements = $coach->getPlacementsByCoachID($isCareerCoach[0]['id']);
//print_r($coachPlacements);die;

// Get the learner name from the studentID passed in.
$learner_test = $coach->learner->learnerDetailsByID('12346'); // Test **

// Get the placement details from the placement id passed in.
$placement = $coach->placementDetails->getPlacementDetails('1'); // Test **
//print_r($placement);die;

$coachName = $coach->getCoachName(2); // Test **

// This gets the student, placement and the coach details for the Jumbo area.
$employee = new EmployerReview;
$jumboDetails = $employee->getStudentDetailsForJumbo($placement_attended_id);

// This gets the logbook entries for the student so the coach can see them before completing the review.
$logbookEntries = $employee->getLogbookForEmployerReview($placement_attended_id);

// Check if review has already been submitted. This will be true or false. This also holds the input if any that has been submitted by this form so can use it to prepopulate. 
$reviewCompleted = $coach->CheckCoachReview($placement_attended_id);

// Pull any possible Midpoint Review data for this placement instance.
$mrData = $learner->getMidpointReview($placement_attended_id);

//echo '<pre>';print_r($mrData);die;
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Work experience app</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="../css/bootstrap-theme.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="../css/theme.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

    <!--<script src="js/validatereview.js"></script>-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script> -->


    <style>

    .jumbotron{
      /*background-color:transparent!important;*/
      color:#000;
      font-weight:bolder;
      border:0px;
    }
    .jumbotron{
      /*background: url('../images/image.png') no-repeat top scroll;*/
      -webkit-background-size: cover;
      -moz-background-size: cover;
      -o-background-size: cover;
      background-size: cover;
    }

    </style>

    <![endif]-->

  
    <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="crossorigin="anonymous"></script>

    <!-- This covers the buttons and hidden divs -->
    <script src="../js/includes/button_div_jquery.js"></script>

  </head>
  <body>

    <!-- Fixed navbar -->
    <!--<nav class="navbar navbar-inverse navbar-fixed-top">-->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
           <a class="navbar-brand" href="/workexperience/">Work experience app</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <!--<li class="active"><a href="#">Home</a></li>-->
            <!--<li><a href="#about">About</a></li>-->
            <!--<li><a href="#contact">Contact</a></li>-->
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav><!-- end fixed navbar -->


  <div class="container-fluid theme-showcase" role="main">
    <div class="row"><!--Start of row -->
        <div class="col-md-12"><!-- content main full page-->
          <h2>Career coach review section.</h2>
          <p>The completed review will be shown to the learner once completed.</p>  
          </div>
        </div>
      </div>

  <!-- Jumbatron section showing the details of who they are reviewing-->
  <div class="container-fluid theme-showcase" role="main">
    <div class="jumbotron">
      <h3>Student name:  <?=$jumboDetails['student_name']?></h3> 
        <p>Placement: <?=$jumboDetails['placement']?></p>
         <p>Address: <?=$jumboDetails['placement_address']?>, <?=$jumboDetails['post_code']?> </p>
          <p>Agreement signed: <?=date('l jS \of F Y',strtotime($jumboDetails['agreement_signed_date']))?></p>
          <hr/>
          <p>Coach name: <?=$jumboDetails['coach_name']?></p>
        
    </div>
    <p>Below is the logbook entries for the learner who has been on placement with your organisation. You can view the learners hours and then finally complete the review at the very bottom of the page.</p>
    <p>The learner will be able to see your remarks and review of their performance once you've completed the review section.</p> 
  </div>
  <!-- End Jumbatron section showing the details of who they are reviewing-->


<!-- Start the learner logbook section-->
<div class="container-fluid theme-showcase" role="main">
  <div class="row"><!--Start of row -->
    <div class="col-md-12"><!-- content main full page--> 
   
    <h3>Student hours:</h3>

    <?php 
    // Only show if the student has logbook enytries.
    if($logbookEntries!=false):?>

    <fieldset>
      <div class="table-responsive">          
        <table class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>Date</th>
              <th>Start</th>
              <th>Finish</th>
              <th>Sick during when</th>
              <th>Entire sick day</th>
              <th>Absence details</th>
              <th>Activities</th>
              <th>Enjoyed</th>
              <th>Better next time</th>
            </tr>
          </thead>
          <tbody>
              <?php 

              $x=0; // Counter.
              $hours=0; // Keep track of the hours

              for($i=0;isset($logbookEntries[$i]);$i++):?>
                <tr>
                  <td><?=date('l jS \of F Y',strtotime($logbookEntries[$i]['date']))?></td>
                  <td><?=(isset($logbookEntries[$i]['start'])) ? date('H:i',strtotime($logbookEntries[$i]['start'])) : '--';?></td>
                   <td><?=(isset($logbookEntries[$i]['finish'])) ? date('H:i',strtotime($logbookEntries[$i]['finish'])) : '--';?></td>
                  <td><?=($logbookEntries[$i]['sick_during']=='0') ? '--' : $logbookEntries[$i]['sick_during'];?></td>
                  <td><?=($logbookEntries[$i]['sickness']=='1') ? '<span class="glyphicon glyphicon-ok"></span>' : '--';?></td>
                  <td><?=($logbookEntries[$i]['details_of_absence']) ? $logbookEntries[$i]['details_of_absence'] : '--' ?></td>
                  <td><?=($logbookEntries[$i]['activities']) ? $logbookEntries[$i]['activities'] : '--' ?></td>
                  <td><?=($logbookEntries[$i]['enjoyed']) ? $logbookEntries[$i]['enjoyed'] : '--' ?></td>
                  <td><?=($logbookEntries[$i]['better_next_time']) ? $logbookEntries[$i]['better_next_time'] : '--' ?></td>
                </tr>
                
                <?php ++$x;

                  // Work out the total hours, keeping a tally as you go.
                  $start = strtotime($logbookEntries[$i]['start']);
                  $finish = strtotime($logbookEntries[$i]['finish']);
                  $hours += round(abs($finish - $start) / 3600,2);
                 
                  endfor; // End the for loop

                  ?>

                <tr>
                  <th>Total hours</th>
                  <td colspan=2><?=$hours?> hrs</td>
                </tr>

              </tbody>
            </table>
          </div> <!-- table-responsive ends-->
        </fieldset>
     
      <?php 
      // No logbook entries for the student.
      else:?>
        <p> No logbook entries for this student so far.</p>
      <?php endif;?>

    </div>
  </div>
</div>
<!-- End the learner logbook section-->

<!-- This the midpoint review section-->
<div class="container-fluid theme-showcase" role="main">
      <div class="row"><!--Start of row -->
        <div class="col-md-12"><!-- content main full page-->   
          <h3>Midpoint Assessment of Learner:</h3> 
          <p>Here is the section for completing the MidPoint review of the learner on placement.</p>
          <p><button id="midpoint_review_button" class="btn btn-success">Open Midpoint Review</button><p>
          
           <div class='midpoint_review_form'>
            <br/>
            <p>For each of the aspects for assessment, please mark choose whether the learner does not meet expectations, meets expectations, or exceeds expectations at the Work Placement at the midpoint. Please refer to the <a href='../appendix.docx'>Appendix</a> as a reference guide outlining the Behaviour and Social Skills standards we expect learners to be meeting. Please use the Knowledge and Technical Skills relevant to their course of study to determine their readiness.</p>
          
          <hr/>

          <form method='POST'>

            <h4>Behaviour and Social Skills Knowledge and Technical Skills:</h4>


            <p>Does not meet expectations: <input autofocus required="required" type="radio" name="behaviour" value="does_not_meet_expectations" <?php if(isset($mrData[0]['behaviour']) && $mrData[0]['behaviour']=='does_not_meet_expectations') echo 'checked';?>/></p>
            <p>Meet expectations: <input type="radio" name="behaviour" value="meet_expectations" <?php if(isset($mrData[0]['behaviour']) && $mrData[0]['behaviour']=='meet_expectations') echo 'checked';?>/></p>
            <p>Exceeds expectations: <input type="radio" name="behaviour" value="exceeds_expectations" <?php if(isset($mrData[0]['behaviour']) && $mrData[0]['behaviour']=='exceeds_expectations') echo 'checked';?>/></p>

            <hr/>

            <h4>Please assess the quality of support given by the employer:</h4>

            
            <p>Below expected standard: <input autofocus required="required" type="radio" name="quality_of_support" value="below_expected_standard" <?php if(isset($mrData[0]['quality_of_support']) && $mrData[0]['quality_of_support']=='below_expected_standard') echo 'checked';?>/></p>
            <p>Meeting expected standard: <input type="radio" name="quality_of_support" value="meeting_expected_standard" <?php if(isset($mrData[0]['quality_of_support']) && $mrData[0]['quality_of_support']=='meeting_expected_standard') echo 'checked';?>/></p>
            <p>Exceeded expected standard: <input type="radio" name="quality_of_support" value="exceeding_expected_standard" <?php if(isset($mrData[0]['quality_of_support']) && $mrData[0]['quality_of_support']=='exceeding_expected_standard') echo 'checked';?>/></p>
            <hr/>

            <p>The learner, coach and employer names will show once each has agreed and checked the checkbox.</p>

            <p>Signed by: <input type='checkbox' id='coach_signed' name='coach_signed' value='1' <?php if(isset($mrData[0]['coach_signed']) && $mrData[0]['coach_signed']=='1') echo 'checked';?>/>
              <br/><input type='text' disabled name='provider_name' value="<?php if($mrData[0]['coach_signed']=='1') echo $learner->getCoachName($mrData[0]['coach_id']) ?>"/> (Bolton College career coach/staff)</p>
            <p>Signed by: <input type='checkbox' id='learner_signed' name='learner_signed' value='1' <?php if(isset($mrData[0]['learner_signed']) && $mrData[0]['learner_signed']=='1') echo 'checked';?>/>
              <br/><input type='text' disabled name='learner_name' value="<?php if($mrData[0]['learner_signed']=='1') echo $jumboDetails['student_name'];?>"/> (Learner)</p>
            <p>Signed by: <input type='checkbox' id='employer_signed' name='employer_signed' value='1' <?php if(isset($mrData[0]['employer_signed']) && $mrData[0]['employer_signed']=='1') echo 'checked';?>/>
              <br/><input type='text' disabled name='employer_name' value="<?php if($mrData[0]['employer_signed']=='1') echo $jumboDetails['placement'];?>"> (Employer)</p>

            <p><input type='hidden' name='paid' value='<?=$placement_attended_id?>'></p>
            <p><input type='hidden' name='signed_with_coach' value='1'></p>
            <p><input type='hidden' name='submit_midpoint_review' value='1'></p>

            <!--No review so far, so show a submit button-->
            <?php if(!isset($mrData[0]['id'])):?>
              <p><button type='submit' name='create_midpoint_review' value='1' class="btn btn-primary">Submit Midpoint review</button></p>
            <?php endif;?>

             <!--A review with data, so show an edit button and a message with when the review was created and if there is one, an update date also-->
            <?php if(isset($mrData[0]['id'])):?>
              <p><button type='submit' name='update_midpoint_review' value='<?=$mrData[0]['id']?>' class="btn btn-danger">Update Midpoint review</button></p>
              <p>This Midpoint Review was created on: <strong><?=date('l jS \of F Y h:i:s A',strtotime($mrData[0]['date_created']))?></strong> by: <strong><?=$learner->getCoachName($mrData[0]['coach_id'])?></strong></p>
            <?php endif;?>

             <?php if(isset($mrData[0]['date_updated'])):?>
              <p>The record was updated on: <strong><?=date("F j, Y, H:i",strtotime($mrData[0]['date_updated']))?></strong> by: <strong><?=$learner->getCoachName($mrData[0]['coach_id'])?></strong></p>
            <?php endif;?>


          </form>

          </div>

        </div>
      </div>
</div>
<!-- End the midpoint review section-->


<div class="container-fluid theme-showcase" role="main">
      <div class="row"><!--Start of row -->
        <div class="col-md-12"><!-- content main full page-->      
         
         <!-- If the review has been completed, disable the form -->

          <h3>Career coach review:</h3>

          <fieldset <?php if($reviewCompleted!=false) echo 'disabled';?>>
            <form method='POST'>
                 <div class="table-responsive">          
                    <table class="table table-bordered table-striped">
                      <tbody>
                        <tr>
                          <td>
                            <p>Career coach review comments:</p>
                            <input type='hidden' name='coach_id' value='<?=$isCareerCoach[0]['id']?>'>
                            <textarea required class='textarea_review' placeholder='Please give a little more information on how well you think the student did in their placement.' name="review['comments']"><?php if(isset($reviewCompleted[0]['comments'])) echo $reviewCompleted[0]['comments'];?></textarea>
                          </td>
                        </tr>
                        </tbody>
                      </table>
                    </div> <!-- table-responsive ends-->

                      <p><input <?php if($reviewCompleted[0]['hours_confirmed']==1) echo 'checked';?> type="checkbox" name="hours_confirmed" value="1"/>
                        I can confirm these hours are correct as per the students logbook entries and the student can finally be signed off the work experience placement.
                      </p>

                    <!-- The review is not compelted, so show the save button -->
                    <?php if($reviewCompleted==false):?>
                      <p>
                        <button type='submit' class="btn btn-primary" name='save_employer_review' value="<?=$placement_attended_id?>" <?php if(isset($_POST['save_employer_review']) || (isset($reviewAdded) && $reviewAdded!=false)) echo 'disabled';?>>Save</button>
                      </p>
                    <?php endif;?>
                  </form>
              </fieldset>

                 <!-- The review is completed, so show the edit button -->
                <fieldset>

                <form method='POST'>
                   <?php if($reviewCompleted!=false):?>
                    <p>Review completed at: <strong><?=date('l jS \of F Y H:i:s A',strtotime($reviewCompleted[0]['date_created']))?></strong>. Click the edit button to edit this review</p>
                    <p>
                      <button type='submit' class="btn btn-danger" name='edit_employer_review' value="<?=$placement_attended_id?>">Edit</button>
                    </p>
                     <?php endif;?>
                     <p><a href='/workexperience/coach/'>Click to return to your reviews list</a></p>
                </form>
              </fieldset>
      
        </div> <!-- col ends-->  
      </div><!-- row ends-->

    </div> <!--end main container -->

    <!-- footer here -->
    <footer class="footer">
    <hr/>
      <div class="container">
        <p style="text-align:center;">Bolton College 2017</p>
      </div>
    </footer>
    <!-- end footer end -->

    <!-- Bootstrap core JavaScript ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
